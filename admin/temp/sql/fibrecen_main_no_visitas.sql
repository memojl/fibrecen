-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-02-2020 a las 21:06:52
-- Versión del servidor: 5.6.45
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fibrecen_main`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_blog`
--

CREATE TABLE `fib_blog` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_blog`
--

INSERT INTO `fib_blog` (`ID`, `cover`, `titulo`, `descripcion`, `contenido`, `tag`, `autor`, `fmod`, `fecha`, `visible`) VALUES
(1, 'blog_FO_petrolera.jpg', 'Mi primer blog', 'Si vives con EPOC, tener una fuente de oxígeno confiable es importante para mantener...', '<p>Si vives con EPOC, tener una fuente de ox&iacute;geno confiable es importante para mantener tu calidad de vida. Sin embargo, existen tantos tipos diferentes de concentradores de ox&iacute;geno en el mercado hoy en d&iacute;a, que puede ser dif&iacute;cil elegir el que mejor se adapte a sus necesidades. A medida que esta tecnolog&iacute;a contin&uacute;a avanzando, aparecen caracter&iacute;sticas m&aacute;s nuevas y opciones m&aacute;s c&oacute;modas, &iexcl;y desea aprovecharlas al m&aacute;ximo!</p>\r\n<p>La buena noticia es que hay m&aacute;s opciones para la terapia de ox&iacute;geno disponibles para ti; a continuaci&oacute;n, hemos recopilado informaci&oacute;n excelente sobre los dos principales concentradores de oxigeno dom&eacute;sticos de Philips Respironics:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"299\"><strong>EVERFLO</strong>\r\n<p>&nbsp;</p>\r\n<p>El concentrador de ox&iacute;geno EverFlo de 5 litros es una m&aacute;quina silenciosa, liviana y compacta que es menos llamativa que muchas otras.</p>\r\n<p>Los usuarios pueden comprar el modelo est&aacute;ndar, o el que tiene un indicador de porcentaje de ox&iacute;geno (OPI) y usa ultrasonido para medir el flujo de ox&iacute;geno.</p>\r\n<p>Los controles se encuentran en el lado delantero izquierdo de la m&aacute;quina y una perilla de rodillo controla el medidor de flujo de ox&iacute;geno empotrado en el centro. Una botella de humidificador se puede conectar a la parte posterior izquierda de la m&aacute;quina con velcro. &iexcl;El tubo se conecta f&aacute;cilmente a la c&aacute;nula de metal encima del interruptor de encendido, y tambi&eacute;n se pueden almacenar tubos adicionales en el interior.</p>\r\n<p>&iexcl;EverFlo 5L pesa 14 kgs y entrega ox&iacute;geno a .5-5 LPM con una concentraci&oacute;n de ox&iacute;geno de hasta 95% en todas las velocidades de flujo. La m&aacute;quina mide 58 cm de profundidad.</p>\r\n<p>El concentrador EverFlo de 5L viene con una garant&iacute;a est&aacute;ndar de 1 a&ntilde;o.</p>\r\n</td>\r\n<td width=\"299\"><strong>MILLENNIUM</strong>\r\n<p>&nbsp;</p>\r\n<p>El concentrador de ox&iacute;geno Millenium proporciona hasta 10 LPM de ox&iacute;geno, d&aacute;ndole las especificaciones de una unidad de &ldquo;alto flujo&rdquo;.</p>\r\n<p>El concentrador de ox&iacute;geno est&aacute; disponible en dos modelos: el modelo est&aacute;ndar y uno dise&ntilde;ado con un indicador de porcentaje de ox&iacute;geno (OPI), una funci&oacute;n que utiliza tecnolog&iacute;a de ultrasonido para medir el flujo de ox&iacute;geno.</p>\r\n<p>El dise&ntilde;o rectangular blanco es fuerte y resistente, y cuatro ruedas grandes (junto con un asa insertada en la parte superior) lo hacen bastante f&aacute;cil de mover.</p>\r\n<p>Este concentrador tiene una v&aacute;lvula SMC de &ldquo;ciclo seguro&rdquo;, dise&ntilde;ada espec&iacute;ficamente para manejar los mayores flujos de presi&oacute;n necesarios para una m&aacute;quina de 10 LPM. Millenium tambi&eacute;n est&aacute; dise&ntilde;ado con un compresor de doble cabezal equipado para impulsar m&aacute;s aire a trav&eacute;s de los lechos de tamices de la m&aacute;quina para eliminar el nitr&oacute;geno.</p>\r\n<p>Philips Respironics &ldquo;Millennium&rdquo; viene con una garant&iacute;a est&aacute;ndar de un a&ntilde;o.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Caracter&iacute;sticas y Beneficios</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Silencioso y f&aacute;cil de usar\r\n<p>&nbsp;</p>\r\n<p>Controles claros y visibles</p>\r\n<p>Dise&ntilde;o ergon&oacute;mico: rueda f&aacute;cilmente</p>\r\n<p>Peso ligero de 14 kg</p>\r\n<p>Medidor de flujo empotrado para proteger contra la rotura</p>\r\n<p>Velcro asegura la botella del humidificador en la m&aacute;quina</p>\r\n<p>Proporciona ox&iacute;geno a .5-5 LPM con 95% de ox&iacute;geno</p>\r\n<p>Alarmas de seguridad por fallas</p>\r\n<p>Garant&iacute;a de producto de tres a&ntilde;os</p>\r\n</td>\r\n<td width=\"299\">F&aacute;cil de usar: los controles son claros y visibles\r\n<p>&nbsp;</p>\r\n<p>Pesa 24 kg</p>\r\n<p>Indicador de Porcentaje de Ox&iacute;geno (OPI) se puede agregar en</p>\r\n<p>Proporciona ox&iacute;geno a 1-10 LPM al 96% de ox&iacute;geno</p>\r\n<p>Alarmas de seguridad por fallas y bajo porcentaje de ox&iacute;geno</p>\r\n<p>Menos partes m&oacute;viles que otros concentradores</p>\r\n<p>Garant&iacute;a est&aacute;ndar de un a&ntilde;o</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Pros</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Funcionamiento silencioso y sonido silencioso cuando se inicia (45 db)\r\n<p>&nbsp;</p>\r\n<p>F&aacute;cil de usar</p>\r\n<p>Confiable y ligero</p>\r\n<p>Port&aacute;til y f&aacute;cil de mover</p>\r\n<p>Consumo de energ&iacute;a de 350 w</p>\r\n<p>&nbsp;</p>\r\n</td>\r\n<td width=\"299\">Bien hecho y f&aacute;cil de configurar\r\n<p>&nbsp;</p>\r\n<p>Robusto, confiable y de bajo mantenimiento</p>\r\n<p>Produce hasta 10 LPM de ox&iacute;geno</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Contras</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Bip fuerte cuando se inicia\r\n<p>&nbsp;</p>\r\n<p>Baja altitud de trabajo</p>\r\n<p>Produce hasta 5 LPM de ox&iacute;geno</p>\r\n</td>\r\n<td width=\"299\">Demasiado ruidoso para algunos usuarios (50 db)\r\n<p>&nbsp;</p>\r\n<p>Pesa 24 kg</p>\r\n<p>Tiene m&aacute;s potencia de la que muchos usuarios necesitan</p>\r\n<p>Consumo de energ&iacute;a de 600 w</p>\r\n<p><strong>&nbsp;</strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>La elecci&oacute;n de los dispositivos de administraci&oacute;n de ox&iacute;geno depende del requerimiento del paciente, la eficacia del dispositivo, la fiabilidad, la facilidad de aplicaci&oacute;n terap&eacute;utica y la aceptaci&oacute;n del paciente. <a href=\"http://samsung-healthcare.mx/contacto\"><span style=\"text-decoration: underline;\">Para m&aacute;s informaci&oacute;n sobre la elecci&oacute;n de su concentrador de ox&iacute;geno no dude en contactarnos.</span></a></p>', 'EPOC, Oxígeno', 'admin', '2018-09-24 22:23:34', '2017-01-18 14:05:23', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_blog_coment`
--

CREATE TABLE `fib_blog_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_blog_coment`
--

INSERT INTO `fib_blog_coment` (`ID`, `ip`, `nombre`, `email`, `comentario`, `id_b`, `fecha`, `visible`) VALUES
(1, '127.0.0.1', 'Arturo L&oacute;pez', 'alopez@gmail.com', 'Comentario de prueba.', 1, '09/02/2018 23:43', 1),
(2, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'memojl08@gmail.com', 'Mensaje de Prueba.', 1, '11/02/2018 21:31', 1),
(3, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'memotablet08@gmail.com', 'mensaje de prueba', 1, '2018-09-11 10:03:31', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_comp`
--

CREATE TABLE `fib_comp` (
  `ID` int(1) UNSIGNED NOT NULL,
  `page` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_comp`
--

INSERT INTO `fib_comp` (`ID`, `page`) VALUES
(1, 'usuarios/login.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_config`
--

CREATE TABLE `fib_config` (
  `ID` int(1) UNSIGNED NOT NULL,
  `logo` varchar(100) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `dominio` varchar(100) NOT NULL,
  `path_root` varchar(150) NOT NULL,
  `page_url` varchar(100) NOT NULL,
  `keyword` varchar(200) NOT NULL,
  `description` varchar(300) NOT NULL,
  `metas` text NOT NULL,
  `g_analytics` varchar(1000) NOT NULL,
  `tel` varchar(100) NOT NULL,
  `tel2` varchar(20) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `wapp` varchar(20) NOT NULL,
  `webMail` varchar(100) NOT NULL,
  `contactMail` varchar(100) NOT NULL,
  `contactMail2` varchar(100) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `chartset` varchar(30) NOT NULL,
  `dboard` varchar(50) NOT NULL,
  `dboard2` varchar(50) NOT NULL,
  `direc` varchar(250) NOT NULL,
  `direc2` varchar(250) NOT NULL,
  `CoR` varchar(100) NOT NULL,
  `CoE` varchar(100) NOT NULL,
  `BCC` varchar(100) NOT NULL,
  `CoP` varchar(100) NOT NULL,
  `fb` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `gp` varchar(100) NOT NULL,
  `lk` varchar(100) NOT NULL,
  `yt` varchar(100) NOT NULL,
  `ins` varchar(100) NOT NULL,
  `licencia` varchar(300) NOT NULL,
  `version` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_config`
--

INSERT INTO `fib_config` (`ID`, `logo`, `page_name`, `title`, `dominio`, `path_root`, `page_url`, `keyword`, `description`, `metas`, `g_analytics`, `tel`, `tel2`, `phone`, `wapp`, `webMail`, `contactMail`, `contactMail2`, `mode`, `chartset`, `dboard`, `dboard2`, `direc`, `direc2`, `CoR`, `CoE`, `BCC`, `CoP`, `fb`, `tw`, `gp`, `lk`, `yt`, `ins`, `licencia`, `version`) VALUES
(1, 'logo_circulo.png', 'FIBRECEN', 'FIBRECEN - Fibra de vidrio y carbono - Resina Poli&eacute;ster - Ep&oacute;xica y mucho m&aacute;s...', 'http://www.fibrecen.com.mx/', '', 'http://fibrecen.com.mx/', 'Resinas, Fibras, Ep&oacute;xico, Curso, Encapsulados, Fibra de vidrio, L&aacute;tex, Pigmentos, Fotoluminiscentes, Fibra de Carbon, Resina Eco, Uso General,  ', 'Somos fibras y resinas del centro, tenemos m&aacute;s de 30 a&ntilde;os de experiencia en la creaci&oacute;n estructuras de fibra de vidrio as&iacute; como de la venta de materiales como resinas, fibras de vidrio, gel coats, cat', '<!--Responsive Meta-->\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\r\n<!-- META-TAGS generadas por http://metatags.miarroba.es -->\r\n<META NAME=\"DC.Language\" SCHEME=\"RFC1766\" CONTENT=\"Spanish\">\r\n<META NAME=\"AUTHOR\" CONTENT=\"Guillermo Jimenez\">\r\n<META NAME=\"REPLY-TO\" CONTENT=\"multiportal@outlook.com\">\r\n<LINK REV=\"made\" href=\"mailto:multiportal@outlook.com\">\r\n', '<!-- Global site tag (gtag.js) - Google Ads: 786424895 --> <script async src=\"https://www.googletagmanager.com/gtag/js?id=AW-786424895\"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag(\'js\', new Date()); gtag(\'config\', \'AW-786424895\'); </script> \r\n<!-- Facebook Pixel Code -->\r\n<script>\r\n  !function(f,b,e,v,n,t,s)\r\n  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?\r\n  n.callMethod.apply(n,arguments):n.queue.push(arguments)};\r\n  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';\r\n  n.queue=[];t=b.createElement(e);t.async=!0;\r\n  t.src=v;s=b.getElementsByTagName(e)[0];\r\n  s.parentNode.insertBefore(t,s)}(window, document,\'script\',\r\n  \'https://connect.facebook.net/en_US/fbevents.js\');\r\n  fbq(\'init\', \'1164747303709671\');\r\n  fbq(\'track\', \'PageView\');\r\n</script>\r\n<noscript><img height=\"1\" width=\"1\" style=\"display:none\"\r\n  src=\"https://www.facebook.com/tr?id=1164747303709671&ev=PageView&noscript=1\"\r\n/></noscript>\r\n<!-- End -->', '(442) 2128353', '', '', '4422199937', 'contacto@fibrecen.com.mx', 'qro@fibrecen.com.mx', '', 'page', 'iso-8859-1', 'dashboard', 'AdminLTE', 'Guanajuato No. 5-B, Col. San Francisquito, Quer&eacute;taro, Qro.', '', 'qro@fibrecen.com.mx', 'contacto@fibrecen.com.mx', 'rdelaportilla@fibrecen.com.mx', 'memojl08@gmail.com', 'https://www.facebook.com/fibrecen.mx/', '', '', 'https://www.linkedin.com/in/fibrecen-fibras-y-resinas-bba407176/', 'https://www.youtube.com/channel/UCi6YWo0xjGHXWQcbgH_qMCA', 'https://www.instagram.com/fibrecen/', 'cms-px31q2hponix31q2x.admx31q2in458x31q2x.201x31q29.05.x31q212.01x31q2.2.4.x31q27x31q2', '01.2.5.1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_contacto`
--

CREATE TABLE `fib_contacto` (
  `ID` int(9) UNSIGNED NOT NULL,
  `ip` varchar(25) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `asunto` varchar(150) NOT NULL,
  `msj` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_list` varchar(50) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `adjuntos` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_login` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_contacto`
--

INSERT INTO `fib_contacto` (`ID`, `ip`, `nombre`, `email`, `tel`, `titulo`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `status`, `ID_user`, `ID_login`, `visible`) VALUES
(1, '177.232.84.138', 'Guillermo Jim&eacute;nez L&oacute;pez ', 'd@d.com', '', '', 'CONTACTO - Prueba', 'Mensaje de prueba', '2019-06-16 17:59:14', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(2, '187.145.78.107', 'RAMSES ', 'fibrecen@gmail.com', '', '', 'CONTACTO - informes', 'mensaje de prueba', '2019-07-22 15:29:52', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(16, '185.206.224.213', 'Dannywhish ', 'abira_a@hotmail.com', '', '', 'CONTACTO - Triff jetzt eine sexy Frau', '10 besten britischen Dating-Sites (2019): http://gravacafer.tk/i77un?&amp;oyiqs=ANokCwc6c', '2019-08-05 21:49:53', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(14, '185.93.3.114', 'Josephdef ', 'demotronix@yahoo.com.mx', '', '', 'CONTACTO - Estamos en contacto, c&aacute;mbiate con nosotros', 'Es muy grato poder contactarle y poner a sus disposici&oacute;n \r\nnuestros servicios de web hosting empresarial. \r\n \r\nDigitalServer compa&ntilde;&iacute;a que representamos, cuenta con 19 \r\na&ntilde;os en el mercado de web hosting en M&eacute;xico, contamos con \r\ndiferentes planes que se puede adecuar a sus necesidades. \r\n \r\nNuestros Servicios: \r\n \r\nWeb Hosting Empresarial - Sin L&iacute;mite de Env&iacute;o por Hora con Filtrado Anti Spam \r\nServidores VPS \r\nServidores Dedicados \r\nRegistro de Dominios \r\n \r\nVisite nuestro sitio web: https://www.digitalserver.com.mx \r\n \r\nCualquier duda que tenga o inter&eacute;s con gusto le podemos atender \r\n \r\nEsperando poder servirle pronto, reciba un saludo.', '2019-08-04 16:04:30', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(15, '177.227.103.164', 'Agus Cardenas ', 'asusbravo@yahoo.com.mx', '4421475653', '', 'CURSOS - INTRODUCCI&Oacute;N A LOS MATERIALES COMPUESTOS', 'Buen dia, aun puedo inscribirme a este curso?\r\nGracias,', '2019-08-08 17:48:54', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(5, '31.7.57.246', 'Dannywhish ', 'chatdaddy1@hotmail.com', '', '', 'CONTACTO - UPDATE: Cryptocurrency Investing Strategy - Q2 2019. Receive passive income of $ 70,000 per month', 'Bitcoin rate is growing. Manage to invest. Get passive income of $ 3,500 per week: http://v.ht/tq0Yc?xdeUnrR', '2019-07-25 23:22:34', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(6, '84.39.112.90', 'Dannywhish ', 'jeremiahharper@yahoo.com', '', '', 'CONTACTO - Cryptocurrency Trading &amp; Investing Strategy for 2019. Receive passive income of $ 70,000 per month', 'UPDATE: Cryptocurrency Investing Strategy - Q2 2019. Receive passive income of $ 70,000 per month: https://chogoon.com/srt/ph0e2?lQRXCnE1nvqFl', '2019-07-27 15:54:34', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(7, '84.39.112.90', 'Dannywhish ', 'tonyhenriquez@hotmail.com', '', '', 'CONTACTO - Single girls want sex in your city', '?Meet Local Women Looking For Sex Tonight On XPress: http://vemulcete.tk/dp2bd?&amp;kuaif=hfyNp54bvpU7', '2019-07-30 13:08:14', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(8, '104.129.200.54', 'VICTOR MOJICA ', 'mv-comercial@hotmail.com', '', '', 'CONTACTO - MACETAS', 'Hola, tienen macetas de fibra de vidrio ?', '2019-07-31 18:03:04', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(13, '185.206.224.213', 'Dannywhish ', 'scaryant@msn.com', '', '', 'CONTACTO - 2019 Cryptocurrency Investment Guide', 'How to invest in bitcoins in 2019 and receive passive income of $ 70,000 per month: http://pertombsenep.ga/thbj?dauhOevGQq1P', '2019-08-04 08:56:58', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(12, '177.232.84.90', 'Guillermo Jim&eacute;nez L&oacute;pez ', 'multiportal@outlook.com', '', '', 'CONTACTO - Prueba', 'Mensaje de prueba', '2019-08-03 04:44:37', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(17, '187.189.215.2', 'Joel Murillo C ', 'ventas.servitecd@hotmail.com', '', '', 'CONTACTO - Calentadores de aceite t&eacute;rmico, bombas...', 'Buen d&iacute;a.\r\n\r\nA quien corresponda.\r\n\r\nMi nombre es Joel Murillo del &aacute;rea de ventas, somos una empresa que ha estado trabajando en proyectos con muchas empresas de su rama, es por eso que por este medio queremos que conozca nuestros servicios que ofrecemos a nivel nacional y en el extranjero, somos una empresa enfocada a trav&eacute;s de nuestros recursos aumentar el rendimiento, eficiencia y control de sus procesos. En la actualidad contamos con Empresa y marca propia de forma independiente ofreciendo mas y mejores servicios debido a las exigencias en los procesos industriales de mano con la innovaci&oacute;n y tecnolog&iacute;a, ademas de la Fabricaci&oacute;n de equipos de la propia marca SERVITEC D siendo una Empresa 100 % mexicana con Precios competitivos.\r\nPara cualquier duda,cotizaci&oacute;n o comentario puede hacerlo a los tel&eacute;fonos o correos al final del enlistado de servicios.\r\n\r\nActualmente tenemos como principales clientes: BIMBO, CARIOCAS (TOLUCA), RALOY (HIDALGO), FENORESINAS (HIDALGO), SEM MATERIALS ( MONTERREY), PACSA (TEPEJI) , OLEOQUIMICAS, entre otros.\r\n\r\n*Fabricaci&oacute;n de calentadores t&eacute;rmicos de diferentes capacidades.\r\n*Reparaci&oacute;n de unidades t&eacute;rmicas.\r\n*Fabricaci&oacute;n de tanques de expansi&oacute;n.\r\n*Mantenimiento preventivo, predictivo y correctivo.\r\n*Instalaci&oacute;n de instrumentaci&oacute;n.\r\n*Equipo el&eacute;ctrico y tableros.\r\n*Instrumentaci&oacute;n para control.\r\n*Montaje de sellos mec&aacute;nicos.\r\n*Montaje de quemadores.\r\n*Montaje de aislamiento t&eacute;rmico.\r\n*Montaje de empaquetadura en bomba.\r\n*Fabricaci&oacute;n de serpentines.\r\n*Reparaci&oacute;n de serpentines.\r\n*Bombas centrifugas KSB, SIHI, etc.\r\n*Reparaci&oacute;n de bombas centrifugas.\r\n*Sellos mec&aacute;nicos.\r\n*Refacciones para quemadores.\r\n*Refacciones para bombas.\r\n*Quemadores industriales.\r\n*Empaquetaduras de grafito.\r\n*Venta de serpentines.\r\n*V&aacute;lvulas de esfera y compuerta. En 150lbs y 300 lbs.\r\n*Controles de temperatura y flama en varias marcas.\r\n*Cursos de capacitaci&oacute;n de personal.\r\nVentas\r\nJoel Murillo C.\r\nTel Oficina: 58 56 28 15 \r\nCel whatsapp 55 64 79 66 94\r\nventas.servitecd@hotmail.com\r\n&amp;#8203;Gerente General\r\nIng. Daniel Ignacio Segura A.\r\nNextel 62 99 63 84\r\nservitec20111@hotmail.com\r\nLunes a Viernes 8:00 a.m  a 5:30 p.m', '2019-08-05 17:57:22', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(22, '189.180.38.182', 'Memo Jimenez ', 'multiportal@outlook.com', '4421234567', '', 'CURSOS - MESAS DE PAROTA CON RESINA EP&Oacute;XICA PACHUCA', 'Mensaje de prueba.', '2019-08-08 18:01:43', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(18, '185.234.219.246', 'ProFunding247 ', 'noreply@profunding247.co', '', '', 'CONTACTO - Capital for Your Business', 'Hi, letting you know that http://ProFunding247.co can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://ProFunding247.co \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://ProFunding247.co \r\n \r\nHave a great day, \r\nThe Pro Funding 247 Team \r\n \r\nunsubscribe/remove - http://profunding247.co/r.php?url=fibrecen.com.mx&amp;id=e164', '2019-08-06 23:48:49', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(19, '', 'Aldo Sergio Montes De Oca Del Valle', 'alsermontes@hotmail.com', '5549860340', '', '(RE) CURSOS - APLICACION DE RESINA EPOXICA EN PISOS, ACABADOS 3D ', 'Me interesa este curso y también el de mesas de madera y vidrio gracias', '2019-08-08 17:49:09', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(20, '', 'Max Adrian', 'arq.max@live.com.mx', '4423645891', '', '(RE) MOLDES FLEXIBLES 3D', 'Comentario...', '2019-08-08 17:49:16', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(21, '', 'Agus Cardenas', 'asusbravo@yahoo.com.mx', '4421475653', '', '(RE) INTRODUCCION A LOS MATERIALES COMPUESTOS', 'Buen dia, aun puedo inscribirme a este curso? Gracias', '2019-08-08 17:49:27', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(23, '84.17.49.11', 'Anthony Russell ', 'anthonyrussell428@gmail.com', '', '', 'CONTACTO - investment opportunity', 'My name is Anthony Russell, a UK registered private Investment Manager . \r\n \r\nWe seek individuals with Financial Management knowledge that are capable of handling investment portfolio and management from private investors. If you have fund management abilities, credible projects in need of loan, JV or existing businesses that requires expansion we will be delighted to work with you on a minimal ROI . \r\n \r\nDetails will be provided on hearing back from you with your business executive summary via my direct email : anthonyrusselll329@gmail.com \r\n \r\nKind Regards, \r\n \r\nAnthony Russell \r\nManaging Partner \r\nWhatsApp Line: +447440934350', '2019-08-09 14:00:42', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(24, '185.253.97.237', 'http://bronasinex.tk/abuox ', 'marte@sapo.pt', '', '', 'CONTACTO - There is a goodgift for victory.  http://repolice.ml/mcw9i', 'Here is  an intriguing  broadside as a antivenin seeking your team.  http://ovinabin.tk/gtfs2', '2019-08-09 17:47:22', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(25, '193.148.19.188', 'Marioknoge ', 'raphaefavegemoff@gmail.com', '', '', 'CONTACTO - Mailing via the feedback form.', 'Hello!  fibrecen.com.mx \r\n \r\nWe suggesting \r\n \r\nSending your message through the feedback form which can be found on the sites in the contact partition. Feedback forms are filled in by our application and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This method raise the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com', '2019-08-09 19:36:00', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(26, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba.</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 16:21:15', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(27, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '014421944950', 'Cursos Web Fibrecen', 'APLICACIÓN DE RESINA EPÓXICA EN PISOS, ACABADOS 3D Y MÁRMOL  EN QUERÉTAR', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\n        <td colspan=\"4\">014421944950</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">APLICACIÓN DE RESINA EPÓXICA EN PISOS, ACABADOS 3D Y MÁRMOL  EN QUERÉTAR</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 16:23:13', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(28, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba.</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 16:30:21', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(29, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 16:59:40', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(30, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba.</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 17:00:23', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(31, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje deprueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 17:15:03', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(32, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">MENSAJE DE PRUEBA</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 17:22:39', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(33, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 17:29:45', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(34, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensa de prueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 17:45:34', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(35, '177.232.85.250', 'Guillermo Jiménez López', 'multiportal@outlook.com', '4421234567', 'Cursos Web Fibrecen', 'APLICACIÓN DE RESINA EPÓXICA EN PISOS, ACABADOS 3D Y MÁRMOL  EN QUERÉTAR', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Guillermo Jiménez López</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">multiportal@outlook.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\n        <td colspan=\"4\">4421234567</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">APLICACIÓN DE RESINA EPÓXICA EN PISOS, ACABADOS 3D Y MÁRMOL  EN QUERÉTAR</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-10 18:27:46', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(36, '177.232.85.250', 'Arturo', 'd@d.com', '', 'Contacto Web Fibrecen', 'Prueba web', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Arturo</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">d@d.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Prueba web</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje...</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-11 03:15:31', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(37, '185.239.200.226', 'Aitor Lopez', 'aitorlopez@imperatool.com', '', 'Contacto Web Fibrecen', 'Reseas en Google Maps', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Aitor Lopez</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">aitorlopez@imperatool.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Reseas en Google Maps</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Buenos d\ras,\r\n\r\nSoy Aitor de Imperatool, te contacto porque quer\ramos ofrecerte la posibilidad de potenciar tu negocio.\r\n\r\nActualmente cuenta con tan solo 6 reseas y una puntuacin de 5(https://www.google.com/maps/place/Fibrecen/@20.619191,-100.4580493,12z/data=!4m8!1m2!2m1!1sProveedor+de+materiales+de+construcciB3n+Santiago+de+QuerA9taro+MA9xico!3m4!1s0x85d344d2955c9989:0x68bcbcd16881326b!8m2!3d20.5880654!4d-100.3847684?hl=1091) pero su competencia con mayor nmero de reseas tienen mejor posicionamiento, adems obtener ms resea le proteger contra ataques de reputacin y mejorar su posicionamiento obteniendo algunas reseas adicionales. \n\r\n\r\nA d\ra de hoy, tener un 4,6 en Google Maps, le transmite no slo autoridad y reputacin a tu empresa, sino tambi	n nuevos clientes al encontrarte ms facilmente o ser recomendado por Maps, que es a fin y al cabo lo que necesita cualquier negocio.\r\n\r\nTe invito a que le eches un vistazo a nuestro panel (sin compromiso y con 1 publicacin gratis) y que veas como puedes conseguir reseas de usuarios reales, adems de trakeo de keywords, poder contactar a todos los reviewers directamente y otras herramientas para tu local seo.\r\n\r\nSi prefieres puedes enviar un email a mi correo o contactar en nuestro chat al soporte de atencin al cliente en directo.\r\n\r\nEstoy a tu disposicin.\r\n\r\nUn saludo</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-13 15:17:48', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1);
INSERT INTO `fib_contacto` (`ID`, `ip`, `nombre`, `email`, `tel`, `titulo`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `status`, `ID_user`, `ID_login`, `visible`) VALUES
(38, '185.93.3.114', 'Anthony Russell', 'anthonyrussell428@gmail.com', '', 'Contacto Web Fibrecen', 'investment opportunity', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Anthony Russell</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">anthonyrussell428@gmail.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">investment opportunity</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Good day, \r\n \r\nMy name is Anthony Russell, a UK registered private Investment Manager . \r\n \r\nWe seek individuals with Financial Management knowledge that are capable of handling investment portfolio and management from private investors. If you have fund management abilities, credible projects in need of loan, JV or existing businesses that requires expansion we will be delighted to work with you on a minimal ROI . \r\n \r\nDetails will be provided on hearing back from you with your business executive summary via my direct email : anthonyrusselll329@gmail.com \r\n \r\nKind Regards, \r\n \r\nAnthony Russell \r\nManaging Partner \r\nTel Line: +447440934350</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-15 07:47:05', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(39, '185.93.182.136', 'AustinEmups', 'support@monkeydigital.co', '', 'Contacto Web Fibrecen', 'Daily Social Posting Service', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">AustinEmups</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">support@monkeydigital.co</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Daily Social Posting Service</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Dropped in website’s rankings? Try our new Slow Link building Service. \r\nWe will run a slow & steady link building campaign for 7 days and send you a report with 140 live Social posts \r\n \r\nMore info and more variations in posts: \r\nhttps://monkeydigital.co/product/daily-social-posting-service/ \r\n \r\nThanks and regards \r\nMike \r\nmonkeydigital.co@gmail.com</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-15 19:03:33', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(40, '187.144.92.1', 'OMAR GONZÁLEZ', 'ventas@gruposervex.com', '', 'Contacto Web Fibrecen', 'OFRECIMIENTO DE EQUIPOS PARA LAVANDERÍA INDUSTRIAL ESPECIALIZADA', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">OMAR GONZÁLEZ</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">ventas@gruposervex.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">OFRECIMIENTO DE EQUIPOS PARA LAVANDERÍA INDUSTRIAL ESPECIALIZADA</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Buenas tardes, les envió un cordial saludo y aprovecho para informarles que la empresa a la que represento, se dedica a la comercialización de maquinaria especializada en el sector de lavandería industrial, como lo son barreras sanitarias, mangles, lavadoras y secadoras, contamos con más de 25 años en el mercado y múltiples clientes satisfechos que avalan nuestro trabajo; así mismo, contamos con los mejores precios del mercado y cobertura a nivel nacional, me encantaría poder agendar una cita con ustedes y formalizar nuestra presencia, ya que podríamos hacer una gran mancuerna comercial.\r\n\r\nSin más que agregar por el momento.\r\nQuedo a sus órdenes.</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-19 18:41:55', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(41, '185.93.3.110', 'Williamlip', 'jason_seipp@hotmail.com', '', 'Contacto Web Fibrecen', 'Regard is  an remarkablesacrifice in return you. http://tiariachlorga.tk/44m2', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Williamlip</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">jason_seipp@hotmail.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">Regard is  an remarkablesacrifice in return you. http://tiariachlorga.tk/44m2</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Look at an captivatingoffer in place of you. http://centiabreachbah.tk/d7tb4</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-21 09:05:42', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(42, '189.180.126.57', 'Miguel', 'mherco@hotmail.com', '', 'Contacto Web Fibrecen', 'prueba', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Miguel</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">mherco@hotmail.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">prueba</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Mensaje de prueba</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-21 21:04:41', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(43, '189.180.126.57', 'Jordan', 'j@jordan.com', '', 'Contacto Web Fibrecen', 'jdufikf', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Jordan</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">j@jordan.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">jdufikf</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">jejlfkoeokfldfo.flldfdf</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-21 21:20:53', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(44, '187.134.29.44', 'Daniel Garcia', 'Danydgj15@gmail.com', '', 'Contacto Web Fibrecen', 'COTIZACION', '\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title>Documento sin título</title>\n<style type=\"text/css\">\n.fuente1,.fuente2,.fuente3{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:11px;\n	color:#1f205a;\n	text-align:left;\n}\n.fuente2{font-size:12px; font-weight:700;}\n.fuente3{font-size:13px; font-weight:bold;}\n.fuente1 a{\n	font-family: Calibri, \"Trebuchet MS\";\n	font-size:12px;\n	color:#B20800;\n	text-decoration:none;\n}\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\n.bg_gris{background-color:#F5F5F5;}\n.center{text-align:center;}\n.right{text-align:right;}\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\n</style>\n</head>\n<body>\n<div>\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	  <tr>\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\n      </tr>    \n\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #b20800;\"><br></td>\n	  </tr>\n      <tr>\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\n	  </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\n        <td colspan=\"4\">Daniel Garcia</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\n        <td colspan=\"4\">Danydgj15@gmail.com</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\n        <td colspan=\"4\">COTIZACION</td>\n      </tr>\n      <tr>\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\n        <td colspan=\"4\">Buen día, me comunico con ustedes para saber el precio de la fibra de carbono, de igual modo conocer el horario de atención en su sucursal en Queretaro</td>\n      </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\" align=\"right\"></td>\n        </tr>\n      <tr>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td>&nbsp;</td>\n        <td colspan=\"2\">&nbsp;</td>\n      </tr>\n      <tr>\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\n      </tr>  \n    </table>\n</div>\n</body></html>', '2019-08-22 03:33:55', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(45, '177.232.87.8', 'Miguel', 'mherco@hotmail.com', '', 'Contacto Web Percco', 'Prueba', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Miguel</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">mherco@hotmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Prueba</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Mensaje de prueba.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-10-07 18:12:15', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(46, '177.232.87.8', 'Miguel', 'mheco@hotmail.com', '', 'Contacto Web FIBRECEN', 'Prueba', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Miguel</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">mheco@hotmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Prueba</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Mensaje de prueba.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-10-07 18:32:03', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(47, '177.232.87.8', 'Miguel', 'mherco@hotmail.com', '', 'Contacto Web FIBRECEN', 'Prueba', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Miguel</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">mherco@hotmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Prueba</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Mensaje de prueba.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-10-07 18:37:32', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(48, '187.140.253.160', 'Fernando soria', 'solof11@yahoo.com.mx', '', 'Contacto Web FIBRECEN', 'compra de materiales', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Fernando soria</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">solof11@yahoo.com.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">compra de materiales</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Soy de Celaya. Aun tienen abierto en celaya, si es as&Atilde;&shy;  en que horario\r\nY si no, en que horario abren en queretaro</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-10-12 19:04:58', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(49, '177.227.57.175', 'Heliodoro Martinez Granados', 'heliodoromtzg@hotmail.com', '', 'Contacto Web FIBRECEN', 'Cotizacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Heliodoro Martinez Granados</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">heliodoromtzg@hotmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Cotizacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buenas d&Atilde;&shy;as, me podr&Atilde;&shy;a enviar la cotizaci&Atilde;&sup3;n de un tambo de 200 litros de resina para fibra de vidrio.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-06 15:54:47', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(50, '187.134.35.30', 'LUIS SANCHEZ', 'lantonioss73@gmail.com', '', 'Contacto Web FIBRECEN', 'informacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">LUIS SANCHEZ</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">lantonioss73@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">informacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Vivo en Celaya,  ya no abrieron la tienda ubicada en el bulevar, me gustar&Atilde;&shy;a saber si tienen otra direccion   aqu&Atilde;&shy; en Celaya o si de plano ya no van a estar aqu&Atilde;&shy;.\r\nGracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-09 18:27:35', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(51, '187.134.35.30', 'LUIS SANCHEZ', 'lantonioss73@gmail.com', '', 'Contacto Web FIBRECEN', 'informacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">LUIS SANCHEZ</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">lantonioss73@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">informacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Vivo en Celaya,  ya no abrieron la tienda ubicada en el bulevar, me gustar&Atilde;&shy;a saber si tienen otra direccion   aqu&Atilde;&shy; en Celaya o si de plano ya no van a estar aqu&Atilde;&shy;.\r\nGracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-09 18:27:35', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1);
INSERT INTO `fib_contacto` (`ID`, `ip`, `nombre`, `email`, `tel`, `titulo`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `status`, `ID_user`, `ID_login`, `visible`) VALUES
(52, '187.134.35.30', 'LUIS SANCHEZ', 'lantonioss73@gmail.com', '', 'Contacto Web FIBRECEN', 'informacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">LUIS SANCHEZ</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">lantonioss73@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">informacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Vivo en Celaya,  ya no abrieron la tienda ubicada en el bulevar, me gustar&Atilde;&shy;a saber si tienen otra direccion   aqu&Atilde;&shy; en Celaya o si de plano ya no van a estar aqu&Atilde;&shy;.\r\nGracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-09 18:27:36', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(53, '187.134.35.30', 'LUIS SANCHEZ', 'lantonioss73@gmail.com', '', 'Contacto Web FIBRECEN', 'informacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">LUIS SANCHEZ</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">lantonioss73@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">informacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Vivo en Celaya,  ya no abrieron la tienda ubicada en el bulevar, me gustar&Atilde;&shy;a saber si tienen otra direccion   aqu&Atilde;&shy; en Celaya o si de plano ya no van a estar aqu&Atilde;&shy;.\r\nGracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-09 18:27:36', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(54, '187.134.35.30', 'LUIS SANCHEZ', 'lantonioss73@gmail.com', '', 'Contacto Web FIBRECEN', 'informacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">LUIS SANCHEZ</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">lantonioss73@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">informacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Vivo en Celaya,  ya no abrieron la tienda ubicada en el bulevar, me gustar&Atilde;&shy;a saber si tienen otra direccion   aqu&Atilde;&shy; en Celaya o si de plano ya no van a estar aqu&Atilde;&shy;.\r\nGracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-09 18:27:36', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(55, '187.134.35.30', 'LUIS SANCHEZ', 'lantonioss73@gmail.com', '', 'Contacto Web FIBRECEN', 'informacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">LUIS SANCHEZ</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">lantonioss73@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">informacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Vivo en Celaya,  ya no abrieron la tienda ubicada en el bulevar, me gustar&Atilde;&shy;a saber si tienen otra direccion   aqu&Atilde;&shy; en Celaya o si de plano ya no van a estar aqu&Atilde;&shy;.\r\nGracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-09 18:27:37', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(56, '187.193.50.198', 'Diana Muñoz', 'munozr.diana@gmail.com', '', 'Contacto Web FIBRECEN', 'Sucursal ', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Diana Muñoz</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">munozr.diana@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Sucursal </td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buen d&Atilde;&shy;a!\r\nEstoy interesada en abrir una sucursal de Fibrecen en Le&Atilde;&sup3;n, me podr&Atilde;&shy;an dar m&Atilde;&iexcl;s informaci&Atilde;&sup3;n por favor. \r\nMuchas gracias </td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-12 16:25:07', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(57, '189.129.81.22', 'claudia gomez', 'gomezvas.clau@outlook.com', '', 'Contacto Web FIBRECEN', 'cotizacion', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">claudia gomez</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">gomezvas.clau@outlook.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">cotizacion</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buenas tardes, disculpe me podr&Atilde;&shy;a proporcionar una cotizaci&Atilde;&sup3;n para resina epoxica para pisos VF y de resina epoxica para uso vertical, en las diferentes presentaciones que lo maneje. \r\nDe ante mano muchas gracias por su pronta respuesta\r\n</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-12 22:32:07', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(58, '177.244.30.226', 'Jaime Espinoza', 'jaime.espinoza@cidesi.edu.mx', '', 'Contacto Web FIBRECEN', 'Cotizaci&Atilde;&sup3;n', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Jaime Espinoza</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">jaime.espinoza@cidesi.edu.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Cotizaci&Atilde;&sup3;n</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">A quien corresponda, podr&Atilde;&shy;as hacerme el favor de cotizarme los siguientes materiales:\r\nEspuma de poliuretano (rigida&Acirc;&nbsp;o para boya). 1 litro\r\nGel Coat blanco con su activador.&Acirc;&nbsp; &Acirc;&nbsp; 1 litro\r\nLimpiador para moldes, Acetona. 1 litro\r\nCera desmoldante.&Acirc;&nbsp; 1 bote\r\nPelicula&Acirc;&nbsp;desmoldante. 1 litro\r\nTijeras para corte de fibra de vidrio. 2 piezas\r\nVasos para mezcla de resina (1 litro). 10 pzas\r\nBatelenguas. 20 piezas\r\nPasta para resanar (plaster). 1/2 kg\r\nlija medida 400-800.&Acirc;&nbsp; 5 pzas\r\nBolsa de trapos. 2 kg&Acirc;&nbsp;\r\n\r\nMadera Balsa\r\nSi pudieran enviarme una cotizaci&Atilde;&sup3;n se los agradecer&Atilde;&shy;a.\r\nSaludos\r\nJaime E.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-14 16:56:53', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(59, '189.156.34.44', 'Karina Ojeda', 'karina.ojeda@ttermic.com', '', 'Contacto Web FIBRECEN', 'Solicitud de informaci&Atilde;&sup3;n', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Karina Ojeda</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">karina.ojeda@ttermic.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Solicitud de informaci&Atilde;&sup3;n</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buen dia.\r\n\r\nActualmente estamos buscando proveedores para un proyecto de moldeo y nos gustar&Atilde;&shy;a que usted apoyara nuestros esfuerzos. Estamos interesados &acirc;??&acirc;??en obtener precios para los siguientes art&Atilde;&shy;culos a medida que comenzamos nuestro reto:\r\n\r\nDesmoldantes.\r\nFibras de vidrio de hebra picada y tejidas.\r\nGel coat.\r\nResina de poli&Atilde;&copy;ster de mas 100 grados centigrados de trabajo.\r\nNucleos de madera de balsa y espuma.\r\nMallas de infusi&Atilde;&sup3;n y tejidos pelables.\r\nEquipo de protecci&Atilde;&sup3;n personal.\r\n\r\nEspero que podamos crear una buena sinergia y desarrollar una buena relaci&Atilde;&sup3;n comercial gracias de antemano por todo su apoyo.\r\n\r\nAtentamente \r\n\r\nKarina Ojeda</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-19 20:35:14', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(60, '189.156.34.44', 'Karina Ojeda', 'karina.ojeda@ttermic.com', '', 'Contacto Web FIBRECEN', 'Solicitud de informaci&Atilde;&sup3;n', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Karina Ojeda</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">karina.ojeda@ttermic.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Solicitud de informaci&Atilde;&sup3;n</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buen dia.\r\n\r\nActualmente estamos buscando proveedores para un proyecto de moldeo y nos gustar&Atilde;&shy;a que usted apoyara nuestros esfuerzos. Estamos interesados &acirc;??&acirc;??en obtener precios para los siguientes art&Atilde;&shy;culos a medida que comenzamos nuestro reto:\r\n\r\nDesmoldantes.\r\nFibras de vidrio de hebra picada y tejidas.\r\nGel coat.\r\nResina de poli&Atilde;&copy;ster de mas 100 grados centigrados de trabajo.\r\nNucleos de madera de balsa y espuma.\r\nMallas de infusi&Atilde;&sup3;n y tejidos pelables.\r\nEquipo de protecci&Atilde;&sup3;n personal.\r\n\r\nEspero que podamos crear una buena sinergia y desarrollar una buena relaci&Atilde;&sup3;n comercial gracias de antemano por todo su apoyo.\r\n\r\nAtentamente \r\n\r\nKarina Ojeda</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-11-19 20:35:15', 'papelera', 'contacto', '', '', 1, 0, 0, 0, 1),
(61, '201.130.14.203', 'Karen Araujo', 'karaujo@sumyt.com', '', 'Contacto Web FIBRECEN', 'Consulta', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Karen Araujo</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">karaujo@sumyt.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Consulta</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buen d&Atilde;&shy;a, me interesa saber si tambi&Atilde;&copy;n aplica la fabricaci&Atilde;&sup3;n de objetos a base de fibra de vidrio </td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-12-05 23:17:46', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(62, '189.203.248.138', 'Ricardo Medina', 'rmedina03@yahoo.com', '', 'Contacto Web FIBRECEN', 'cotizaci&Atilde;&sup3;n', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Ricardo Medina</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">rmedina03@yahoo.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">cotizaci&Atilde;&sup3;n</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Hola buen d&Atilde;&shy;a.\r\n\r\nRequiero la cotizaci&Atilde;&sup3;n de 8 rollos de FIBRA DE VIDRIO RF3075 CON FOIL DE ALUMINIO REFORZADO DE 1 1/2&acirc;?? X 1.22 X 15.24 y pegamento para el mismo. Saludos.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-12-06 21:47:20', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(63, '189.203.248.138', 'Ricardo Medina', 'rmedina03@yahoo.com', '', 'Contacto Web FIBRECEN', 'cotizaci&Atilde;&sup3;n', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://www.fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">www.www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://www.fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Ricardo Medina</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">rmedina03@yahoo.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">cotizaci&Atilde;&sup3;n</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Hola buen d&Atilde;&shy;a.\r\n\r\nRequiero la cotizaci&Atilde;&sup3;n de 8 rollos de FIBRA DE VIDRIO RF3075 CON FOIL DE ALUMINIO REFORZADO DE 1 1/2&acirc;?? X 1.22 X 15.24 y pegamento para el mismo. Saludos.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-12-06 21:47:20', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(64, '138.186.200.202', 'Marcos Daniel', 'm@daniels.com', '', 'Contacto Web FIBRECEN', 'Google Adwors', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Marcos Daniel</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">m@daniels.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Google Adwors</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Hi this mensaage spam.</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-12-12 22:19:14', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1);
INSERT INTO `fib_contacto` (`ID`, `ip`, `nombre`, `email`, `tel`, `titulo`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `status`, `ID_user`, `ID_login`, `visible`) VALUES
(65, '64.90.48.204', 'fibrecen.com.mx', 'registration@fibrecen.com.mx', '', 'Contacto Web FIBRECEN', 'Hv drg b Gon', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">fibrecen.com.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">registration@fibrecen.com.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Hv drg b Gon</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Attention: fibrecen.com.mx\r\n\r\nContact: 1(315)517-8088\r\n\r\nAttention: Accounts Payable Or Domain Owner\r\nwww.fibrecen.com.mx\r\n\r\nRequested Reply before: December 23, 2019\r\n\r\n\r\nFailure to complete your fibrecen.com.mx search engine registration by the expiration date may result in cancellation of this proposal making it difficult for your customers to locate you on the web.\r\n\r\n\r\nThis Notice for: www.fibrecen.com.mx will be terminated on December 23, 2019.\r\n\r\n\r\nPayment by Credit Card or Check\r\n-Call our main office: 1(315)517-8088\r\nOr\r\n-Website: https://bit.ly/38bCoAQ\r\n</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2019-12-21 13:12:22', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(66, '187.188.239.202', 'Brenda Mejía', 'compraspromare@gmail.com', '', 'Contacto Web FIBRECEN', 'Cotizaci&Atilde;&sup3;n de productos. ', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Brenda Mejía</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">compraspromare@gmail.com</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Cotizaci&Atilde;&sup3;n de productos. </td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Buena tarde\r\nDeseo una cotizaci&Atilde;&sup3;n de los siguientes materiales espero puedan ayudarme.\r\n2.5 Toneladas de Roving 2400 tex\r\n5 toneladas de Colchoneta mat fv 450\r\n8 toneladas de resina para aplicaci&Atilde;&sup3;n con fibra de vidrio, pre acelerada\r\n1/2 tonelada de catalizador para la resina\r\n\r\nQuedo de ustedes, gracias</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2020-01-16 20:30:37', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(67, '172.83.43.137', 'BradleyGex', 'admin@fibrecen.com.mx', '', 'Contacto Web FIBRECEN', 'Re: Regarding fibrecen.com.mx', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">BradleyGex</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">admin@fibrecen.com.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Re: Regarding fibrecen.com.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">Hello there \r\n \r\nBuy all styles of Oakley &amp; Ray Ban Sunglasses only 19.95 dollars.  If interested, please visit our site: supersunglasses.online \r\n \r\n \r\nThank You, \r\n \r\nContacto | FIBRECEN - Fibra de vidrio y carbono - Resina Poli&amp;eacute;ster - Ep&amp;oacute;xica y mucho m&amp;aacute;s... - fibrecen.com.mx</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2020-01-18 21:24:59', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1),
(68, '199.114.218.118', 'Avery Cohen', 'team@israeltelemarketing.online', '', 'Contacto Web FIBRECEN', 'Nl furhh', '\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Documento sin título</title>\r\n<style type=\"text/css\">\r\n.fuente1,.fuente2,.fuente3{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:11px;\r\n	color:#1f205a;\r\n	text-align:left;\r\n}\r\n.fuente2{font-size:12px; font-weight:700;}\r\n.fuente3{font-size:13px; font-weight:bold;}\r\n.fuente1 a{\r\n	font-family: Calibri, \"Trebuchet MS\";\r\n	font-size:12px;\r\n	color:#B20800;\r\n	text-decoration:none;\r\n}\r\n.dominio, .dominio a{font-size:22px;font-weight:bold;text-align:left;vertical-align:bottom;}\r\n.bg_gris{background-color:#F5F5F5;}\r\n.center{text-align:center;}\r\n.right{text-align:right;}\r\n.footer{background-color:#1f205a;color:#fff;font-size:12px;font-weight:bold;text-align:center;padding:6px}\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n    <table class=\"fuente1\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n      	<td colspan=\"6\" class=\"dominio\"><img src=\"http://fibrecen.com.mx/temas/fibrecen/images/logo_circulo.png\" alt=\"Logo\" style=\"width:90px\" />&nbsp;&nbsp;<a target=\"_blank\" href=\"http://fibrecen.com.mx/\">www.fibrecen.com.mx</a></td>\r\n      </tr>    \r\n\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">Mensaje recibido desde la p&aacute;gina web <b><a target=\"_blank\" href=\"http://fibrecen.com.mx/\">FIBRECEN</a></b> a tr&aacute;ves de la secci&oacute;n <b>Contacto</b>.<br><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1 center\" style=\"border-top:2px solid #333;\"><br></td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"6\" class=\"fuente1\">&nbsp;</td>\r\n	  </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Nombre:</td>\r\n        <td colspan=\"4\">Avery Cohen</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Correo:</td>\r\n        <td colspan=\"4\">team@israeltelemarketing.online</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Telefono:</td>\r\n        <td colspan=\"4\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Asunto:</td>\r\n        <td colspan=\"4\">Nl furhh</td>\r\n      </tr>\r\n      <tr>\r\n        <td colspan=\"2\" class=\"fuente2\">Mensaje:</td>\r\n        <td colspan=\"4\">My company based out of Jerusalem and California, provides the highest quality American telemarketing and cold calling services.\r\n\r\nTo earn your business, we are offering significantly slashed prices for a trial/pilot period. Now is the time to try new lead generation tactics. \r\n\r\nEmail: team@israeltelemarketing.online\r\nWebsite: www.israeltelemarketing.online\r\n*Limited Time Offer/Some Restrictions Apply</td>\r\n      </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\" align=\"right\"></td>\r\n        </tr>\r\n      <tr>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td>&nbsp;</td>\r\n        <td colspan=\"2\">&nbsp;</td>\r\n      </tr>\r\n      <tr>\r\n      	<td colspan=\"6\" class=\"footer\">Formulario de Contacto v.2.1</td>\r\n      </tr>  \r\n    </table>\r\n</div>\r\n</body></html>', '2020-01-23 03:14:38', 'inbox', 'contacto', '', '', 1, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_contacto_forms`
--

CREATE TABLE `fib_contacto_forms` (
  `ID` int(6) UNSIGNED NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `modulo` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `bcc` varchar(200) NOT NULL,
  `CoE` varchar(100) NOT NULL,
  `CoP` varchar(100) NOT NULL,
  `usuario` varchar(300) NOT NULL,
  `url_m` varchar(500) NOT NULL,
  `fecha` varchar(22) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_contacto_forms`
--

INSERT INTO `fib_contacto_forms` (`ID`, `seccion`, `modulo`, `email`, `bcc`, `CoE`, `CoP`, `usuario`, `url_m`, `fecha`, `activo`) VALUES
(1, 'Contacto', 'contacto', 'contacto@fibrecen.com.mx', '', 'contacto@fibrecen.com.mx', 'memojl08@gmail.com', 'admin', 'index.php?mod=contacto', '2018-09-28 18:31:45', 0),
(2, 'Cursos', 'cursos', 'contacto@fibrecen.com.mx', '', 'cursos@fibrecen.com.mx', 'memojl08@gmail.com', 'admin', 'index.php?mod=cursos', '2019-01-28 21:33:03', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_cursos`
--

CREATE TABLE `fib_cursos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `fechas` varchar(100) NOT NULL,
  `lugar` varchar(200) NOT NULL,
  `horario` varchar(100) NOT NULL,
  `video` varchar(300) NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_cursos`
--

INSERT INTO `fib_cursos` (`ID`, `cover`, `titulo`, `descripcion`, `contenido`, `fechas`, `lugar`, `horario`, `video`, `tag`, `autor`, `fmod`, `fecha`, `visible`) VALUES
(4, 'moldes3d-01.jpg', 'REPRODUCCI&Oacute;N CON RESINA', ' &iquest;Te gustar&iacute;a CREAR tus propios moldes y Reproducir en Serie? Entonces este curso es para ti. ', '<p><strong><span>&nbsp;&iquest;Te gustar&iacute;a CREAR tus propios moldes y Reproducir en Serie? Entonces este curso es para ti.&nbsp;<br /><br /></span></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull;&nbsp;Porcentajes de cat&aacute;lisis del silic&oacute;n de caucho</p>\r\n<p>&bull;&nbsp;Aplicaci&oacute;n de SILICON DE CAUCHO con la t&eacute;cnica de molde tipo Bloque</p>\r\n<p>&bull; Porcentajes de cat&aacute;lisis de la Resina Poli&eacute;ster usos generales</p>\r\n<p>&bull; T&eacute;cnica de Vaciado utilizando la resina</p>\r\n<p><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente necesitar&aacute;s traer una pieza que no rebase 10cm de largo y 5cm de alto y que no tenga nada de humedad.</p>\r\n<p><br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $1 100 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>FORMAS DE PAGO&nbsp;</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Transferencia bancaria &oacute; dep&oacute;sito a cuenta.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Dep&oacute;sito en Oxxo &oacute; Farmacias Guadalajara.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en l&iacute;nea con Tarjeta de cr&eacute;dito&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en sucursal&nbsp;</p>', 'Del 22 al 24 de Enero', 'Parque Industrial Bernardo Quintana, Av de Marques #45 Bodega 4', '9:30am a 2:30pm ', 'https://www.youtube.com/embed/tbke3MAec3o', 'Curso poliester', 'admin', '2020-01-30 11:35:00', '', 0),
(5, 'alumnos.jpg', 'INTRODUCCI&Oacute;N A LOS MATERIALES COMPUESTOS EN GUANAJUATO ', 'Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.', '<p><strong>&nbsp;</strong></p>\r\n<p><strong>Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.<br /><br /></strong></p>\r\n<p><span><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull; Teor&iacute;a de cargas en resinas y pr&aacute;ctica.<br />&bull; Teor&iacute;a de Resinas.<br />&bull; Practica de encapsulados con resina cristal.<br />&bull; Teor&iacute;a de Silic&oacute;n de caucho.<br />&bull; Pr&aacute;ctica donde se har&aacute;n moldes flexibles sencillos de una sola cara (2D).<br />&bull; Pr&aacute;ctica de vaciados con resina y cargas.<br /><br /><br /></p>\r\n<p><span><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente requerimos que traiga una foto de 10 x 15cm en papel fotografico, tambi&eacute;n una pieza para reproducir y otra para encapsular que sea de no m&aacute;s de 3cm.<br /><br /></p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $1 150 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>&nbsp;</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $200.00 pues tenemos cupo limitado.</p>', '15 y 16 de Agosto ', 'Preparatoria  CETAB , ALHONDIGA 25 A CENTRO GUANAJUATO; GTO.', '11:00am a 3:00pm', '', 'Curso poliester', '', '2019-08-17 10:14:35', '', 0),
(6, 'moldes2d.jpg', 'MOLDES FLEXIBLES 2D', 'Aprender&aacute;s a crear un Molde 2D con silicon de Caucho para reproducir en serie. ', '<p><strong>Aprender&aacute;s a crear un Molde 2D con silic&oacute;n de Caucho para reproducir en serie.&nbsp;</strong></p>\r\n<p><strong><br /></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO</strong></span></p>\r\n<p>&bull; Porcentajes de cat&aacute;lisis del silic&oacute;n de caucho</p>\r\n<p>&bull; Aplicaci&oacute;n de SILICON DE CAUCHO con la t&eacute;cnica de molde tipo Bloque</p>\r\n<p>&bull; Porcentajes de cat&aacute;lisis de la Resina Poli&eacute;ster usos generales</p>\r\n<p>&bull; T&eacute;cnica de Vaciado utilizando la resina<span style=\"font-size: 0.9em;\">&nbsp;</span></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente necesitar&aacute;s traer una pieza que no rebase 10cm de largo y 5cm de alto y que no tenga nada de humedad.</p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>Tiene un costo de $840.00 por persona e incluye todos los materiales para las pr&aacute;cticas as&iacute; como tu equipo de seguridad.&nbsp;Es necesario apartar el lugar con un anticipo de m&iacute;nimo $250.00 pues tenemos cupo limitado.</p>', 'PROXIMAMENTE', '', '', 'https://www.youtube.com/embed/S__UxGdqkW8', 'Curso poliester', '', '2019-11-12 11:03:14', '', 0),
(13, 'cubieta.jpg', 'CUBIERTAS CON RESINA EP&Oacute;XICA', 'Desarrollar proyectos a gran escala en cubiertas.', '<p><strong>Obtendr&aacute;s los conocimientos necesarios para emprender tu propio negocio o hacerlo crecer con nuevas ideas.&nbsp;</strong></p>\r\n<p><strong><br /></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>Aprendizaje pr&aacute;ctico intensivo, aprender&aacute;s las t&eacute;cnicas de aplicaci&oacute;n de materiales ep&oacute;xicos para diferentes acabados.</p>\r\n<p>&#9642;Tablas de porcentajes</p>\r\n<p>&#9642; Tablas de proporciones</p>\r\n<p>&#9642; Datos t&eacute;cnicos</p>\r\n<p><strong>Acabados</strong></p>\r\n<p><span>&bull; Met&aacute;lico</span></p>\r\n<p><span>&bull; S&oacute;lido</span></p>\r\n<p><span>&bull; M&aacute;rmol&nbsp;</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 18px;\"><strong>&iquest;QU&Eacute; INCLUYE EL CURSO?<br /></strong></span></p>\r\n<p><span style=\"font-size: 18px;\"><strong><br /></strong></span></p>\r\n<p>&bull; Material para realizar una cubierta de 120x60cm.</p>\r\n<p>&bull; Informaci&oacute;n te&oacute;rica.</p>\r\n<p>&bull; Cubierta con efecto piedra m&aacute;rmol<br />&bull; Todos los materiales de preparaci&oacute;n. <br />&bull; Resina Ep&oacute;xica<br />&bull; Pigmentos<br />&bull; Equipo de protecci&oacute;n personal.<br />&bull; Pr&aacute;ctica de aplicaci&oacute;n.</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>El costo de inversi&oacute;n es de $3,499 por persona, en caso de requerir factura ser&iacute;a m&aacute;s IVA.<span style=\"font-size: 0.9em;\">&nbsp;</span></p>\r\n<p>Para apartar se requiere de un anticipo de $300 y se liquida el d&iacute;a de curso.</p>', 'PROXIMAMENTE', '', '', 'https://www.youtube.com/embed/ir5qpRU8s2I', 'Curso epoxicos', '', '2019-12-26 11:27:30', '', 1),
(7, 'PARAWEB-01.jpg', 'MOLDE CON FIBRA DE VIDRIO', 'Crea y reproduce en serie con fibra de vidrio', '<p><strong>Crea y reproduce en serie con fibra de vidrio.</strong></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO</strong>&nbsp;</span></p>\r\n<p><span style=\"font-size: 0.9em;\"><br /><br /></span></p>\r\n<p>&bull; Teoria resina poliester<br />&bull;&nbsp; Teoria refuerzos<br />&bull; Aplicaci&oacute;n de desmoldantes<br />&bull; Creaci&oacute;n de molde en fibra de vidrio<br />&bull; Reproducci&oacute;n de pieza en fibra de vidrio</p>\r\n<p>&nbsp;</p>\r\n<p><strong><span style=\"font-size: 18px;\">COSTOS</span></strong></p>\r\n<p>Tiene un costo de $1 100.00 por persona e incluye todos los materiales para las pr&aacute;cticas.&nbsp;</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado</p>', 'S&aacute;bados 18 y 2 de Enero', 'Parque Industrial Bernardo Quintana, Av de Marques #45 Bodega 4', '9:30am a 1:30pm ', '', 'Curso poliester', 'Yuli', '2020-01-30 11:34:22', '', 0),
(8, 'cursos1.jpg', 'MATERIALES COMPUESTOS ', 'Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.', '<p><strong><span>Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.<br /><br /></span></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull; Teor&iacute;a de cargas en resinas y pr&aacute;ctica.<br />&bull; Fibra de vidrio, se har&aacute; un laminado utilizando desmoldantes.<br />&bull; Teor&iacute;a de Resinas.<br />&bull; Practica de encapsulados con resina cristal.<br />&bull; Teor&iacute;a de Silic&oacute;n de caucho.<br />&bull; Pr&aacute;ctica donde se har&aacute;n moldes flexibles sencillos de una sola cara (2D).<br />&bull; Pr&aacute;ctica de vaciados con resina y cargas.<br /><br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente requerimos que traiga una foto de 10 x 15cm en papel fotografico, tambi&eacute;n una pieza para reproducir y otra para encapsular que sea de no m&aacute;s de 3cm.<br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $772 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 18px;\"><strong>FORMAS DE PAGO&nbsp;</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Transferencia bancaria &oacute; dep&oacute;sito a cuenta.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Dep&oacute;sito en Oxxo &oacute; Farmacias Guadalajara.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en l&iacute;nea con Tarjeta de cr&eacute;dito&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en sucursal&nbsp;</p>\r\n<p><span style=\"font-size: 18px;\"><strong><br /></strong></span></p>', 'S&aacute;bados 15 &amp; 22 de Febrero', ' PARQUE INDUSTRIAL BERNARDO QUINTANA, AV DEL MARQUES #45 ', '9:30am - 1:30pm', 'https://www.youtube.com/embed/tbke3MAec3o', 'Curso poliester', '', '2020-01-30 11:35:30', '', 1),
(9, '1.jpg', 'CREACI&Oacute;N DE M&Aacute;SCARAS', 'Modela y Reproduce tu propia M&aacute;scara. ', '<p><strong>&nbsp;&iquest;Te gustar&iacute;a CREAR tus propias M&aacute;scaras y Reproducir en Serie? Entonces este curso es para ti.&nbsp;<br /><br /></strong></p>\r\n<p><span><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull; Breve teor&iacute;a de silic&oacute;n y porcentajes de cat&aacute;lisis.</p>\r\n<p>&bull; Aplicaci&oacute;n de silic&oacute;n de caucho al modelo, para obtener el molde.</p>\r\n<p>&bull; Modelado sobre espuma de poliuretano con plastilina para escultor.<br />&bull; Fabricaci&oacute;n de molde.<br />&bull; Extracci&oacute;n de m&aacute;scara de l&aacute;tex o fibra de vidrio.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $1, 300 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>FORMAS DE PAGO&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Transferencia bancaria &oacute; dep&oacute;sito a cuenta.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Dep&oacute;sito en Oxxo &oacute; Farmacias Guadalajara.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en l&iacute;nea con Tarjeta de cr&eacute;dito&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en sucursal&nbsp;</p>', 'S&aacute;bado 30 de Nov &amp; S&aacute;bados 7, 14 y 31 de Dic. ', 'Parque Industrial Bernardo Quintana, Av de Marques #45 Bodega 4', '9:30am a 1:30pm ', '', 'Curso poliester', '', '2019-12-20 09:58:52', '', 0),
(10, 'carbono_web.jpg', 'FIBRA DE CARBONO EN LAMINADO', 'Crea y reproduce en serie con fibra de vidrio', '<p><strong>Reproduce en serie con Fibra de Carbon<br /><br /></strong></p>\r\n<p><span><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>- Teor&iacute;a de materiales compuestos <br />- Preparaci&oacute;n y an&aacute;lisis de la pieza<br />- Aplicaci&oacute;n de desmoldantes<br />- Creaci&oacute;n de molde<br />- Reproducci&oacute;n de pieza en fibra de carb&oacute;no, por medio de t&eacute;cnica de laminado</p>\r\n<p><br /><br /><br /></p>\r\n<p><span><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente requerimos que traiga una pieza 2D de 20cmx20cm M&aacute;ximo.&nbsp;<br /><br /></p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $1 250 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>FORMAS DE PAGO&nbsp;</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Transferencia bancaria &oacute; dep&oacute;sito a cuenta.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Dep&oacute;sito en Oxxo &oacute; Farmacias Guadalajara.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en l&iacute;nea con Tarjeta de cr&eacute;dito&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en sucursal&nbsp;</p>', '27 y 28 de Febrero', 'PARQUE INDUSTRIAL BERNARDO QUINTANA, AV DEL MARQUES #45 BODEGA 4. ', '9:30am a 1:30pm ', '', 'Curso poliester', '', '2020-01-30 12:53:40', '', 1),
(11, '45w4.jpg', 'VACIADO Y ENCAPSULADO DE RESINA POLI&Eacute;STER Y EP&Oacute;XICA ', 'Curso te&oacute;rico - Practico ', '<p><strong>Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos deResinas Pol&eacute;ster y Ep&oacute;xica<br /><br /></strong></p>\r\n<p><span><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>- Teor&iacute;a de resinas<br />- Vaciado en resina epoxica con pigmentos.<br />- Vaciado en resina poli&eacute;ster, para encapsulado</p>\r\n<p><br /><br /><br /></p>\r\n<p><span><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente requerimos que traiga varias piezas menores a 5cm.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $850 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>FORMAS DE PAGO&nbsp;</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Transferencia bancaria &oacute; dep&oacute;sito a cuenta.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Dep&oacute;sito en Oxxo &oacute; Farmacias Guadalajara.</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en l&iacute;nea con Tarjeta de cr&eacute;dito&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; Pago en sucursal&nbsp;</p>', '20 y 21 de Febrero', 'PARQUE INDUSTRIAL BERNARDO QUINTANA, AV DEL MARQUES #45', '10:00am a 12:00pm', '', 'Curso poliester', '', '2020-01-30 13:52:29', '', 1),
(15, 'mesas.jpg', 'MESAS DE PAROTA CON RESINA EP&Oacute;XICA PACHUCA', 'Desarrollar proyectos a gran escala en mesas de parota con r&iacute;o de resina ep&oacute;xica.', '<p>Desarrollar proyectos a gran escala en mesas de parota con r&iacute;o de resina ep&oacute;xica.</p>\r\n<p><strong><br /></strong></p>\r\n<p><span><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>Aprendizaje pr&aacute;ctico intensivo, aprender&aacute;s las t&eacute;cnicas de aplicaci&oacute;n de materiales ep&oacute;xicos para diferentes acabados y superficies.</p>\r\n<p>&#9642;Tablas de porcentajes</p>\r\n<p>&#9642; Tablas de proporciones</p>\r\n<p>&#9642; Datos t&eacute;cnicos</p>\r\n<p><strong>Acabados</strong></p>\r\n<p>&#9642; S&oacute;lido</p>\r\n<p>&bull; Met&aacute;lico</p>\r\n<p>&bull; Trasl&uacute;cido&nbsp;</p>\r\n<p>&bull;&nbsp;&nbsp;Fotoluminiscente</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>&iquest;QU&Eacute; INCLUYE EL CURSO?</strong></span></p>\r\n<p>&bull; Material para realizar una cubierta de Parota de 40x35cm.</p>\r\n<p>&bull; Informaci&oacute;n te&oacute;rica.</p>\r\n<p>&bull; Material did&aacute;ctico:</p>\r\n<p>&bull; Parota</p>\r\n<p>&bull; Molde</p>\r\n<p>&bull; Todos los materiales de preparaci&oacute;n.</p>\r\n<p>&bull; Resina para Madera Altos S&oacute;lidos</p>\r\n<p>&bull; Pigmentos</p>\r\n<p>&bull; Equipo de protecci&oacute;n personal.</p>\r\n<p>&bull; Pr&aacute;ctica de aplicaci&oacute;n.</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>El costo de inversi&oacute;n es de $4,990 por persona, en caso de requerir factura ser&iacute;a m&aacute;s IVA.<span>&nbsp;</span></p>\r\n<p>Para apartar se requiere de un anticipo de $300 y se liquida el d&iacute;a de curso.</p>', 'PROXIMAMENTE', '', '', 'https://www.youtube.com/embed/O3m4OyS-iLA', 'Curso epoxicos', '', '2019-10-14 16:41:56', '', 1),
(16, '51UYIZuodDL._SX425_.jpg', 'MOLDE DE GRADO ALIMENTICIO', 'Crea y reproduce en serie ', '<p><strong>Aprender&aacute;s a crear un Molde 2D con caucho de silicon de grado alimenticio</strong></p>\r\n<p><strong><br /></strong></p>\r\n<p><span><strong>TEMARIO</strong></span></p>\r\n<p><br />*Breve teor&iacute;a de silic&oacute;n y TIPOS DE MOLDES<br />*Porcentajes de catalizaci&oacute;n <br />*Delimitaci&oacute;n de paredes <br />*Preparaci&oacute;n de caucho<br />*Vaciado</p>\r\n<p><span><br /></span></p>\r\n<p><span><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente necesitar&aacute;s traer varias piezas no mayores a 5cm</p>\r\n<p>&nbsp;</p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>Tiene un costo de $990.00 por persona e incluye todos los materiales para las pr&aacute;cticas as&iacute; como tu equipo de seguridad.&nbsp;Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>', '10 &amp; 11 de Febrero ', 'PARQUE INDUSTRIAL BERNARDO QUINTANA, AV DEL MARQUES #45', 'De 10:00am a 12:00pm & de 10:00am a 11:00pm', '', 'Curso poliester', '', '2020-01-30 14:01:56', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_cursos_coment`
--

CREATE TABLE `fib_cursos_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_cursos_coment`
--

INSERT INTO `fib_cursos_coment` (`ID`, `ip`, `nombre`, `email`, `comentario`, `id_b`, `fecha`, `visible`) VALUES
(1, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'mherco@hotmail.com', 'Mensaje de prueba para noticias', 1, '2018-12-21 22:14:55', 1),
(2, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'mherco@hotmail.com', 'Mensaje de prueba para cursos.', 1, '2019-01-23 23:09:25', 1),
(3, '127.0.0.1', 'Miguel', 'mherco@hotmail.com', 'Mensaje de prueba', 1, '2019-01-26 20:00:32', 1),
(4, '127.0.0.1', 'Arturo', 'mherco@hotmail.com', 'Mensaje de prueba.', 2, '2019-01-26 20:06:03', 1),
(5, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'multiportal@outlook.com', 'Mensaje de prueba de comentario.', 2, '2019-01-28 22:09:21', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_depa`
--

CREATE TABLE `fib_depa` (
  `ID` int(2) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `list_depa` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `nivel` int(1) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_depa`
--

INSERT INTO `fib_depa` (`ID`, `nombre`, `list_depa`, `puesto`, `nivel`, `icono`, `visible`) VALUES
(1, 'Administrador', 'Administradores', 'Administrador', 1, '', 0),
(2, 'Edecan', 'Edecanes', 'Edecan', 2, '', 1),
(3, 'Modelo', 'Modelos', 'Modelo', 2, '', 0),
(4, 'Fotografo', 'Fotografos', 'Fotografo', 2, '', 1),
(5, 'Agencia', 'Agencias', 'Agencia', 2, '', 1),
(6, 'Escuela', 'Escuelas', 'Escuela', 2, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_directorio`
--

CREATE TABLE `fib_directorio` (
  `ID` int(6) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `url_link` varchar(300) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `des` varchar(250) NOT NULL,
  `filtro` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `fecha` varchar(22) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_galeria`
--

CREATE TABLE `fib_galeria` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` varchar(500) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_galeria`
--

INSERT INTO `fib_galeria` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `precio`, `cate`, `resena`, `url_page`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `visible`, `alta`, `fmod`, `user`) VALUES
(1, '', '', '10.jpg', '', 'Cursos en tu escuela', 0.00, 'Poliester', '<p>Descripcion</p>', '', '', '', '', '', '', 1, '2019-03-27 12:33:38', '', 'admin'),
(15, '', '', 'hom3.jpg', '', 'T&eacute;cnicos Especializados', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:09:24', '', 'admin'),
(14, '', '', 'hom_.jpg', '', 'Manufactura', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:08:13', '', 'admin'),
(13, '', '', '17.jpg', '', 'Fibra de Vidrio', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:07:09', '', 'admin'),
(33, '', 'Espuma de poliuretano proyectado por aspersi&oacute;n', 'ESPUEMA.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-09-02 16:18:29', '', 'FIBRECEN'),
(34, '', 'Pisos Ep&oacute;xicos con pigmentos  met&aacute;licos. ', 'piso epoxico.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-09-02 16:21:39', '', 'FIBRECEN'),
(35, '', 'Aspersi&oacute;n de Gel Coat', 'GelCoat.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-09-02 16:31:26', '', 'FIBRECEN'),
(36, '', 'Pisos Ep&oacute;xicos Industriales', 'pisos.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-09-03 12:56:24', '', 'FIBRECEN'),
(37, '', 'Molde con Fibra de Vidrio', 'fibralijado.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-09-03 12:57:25', '', 'FIBRECEN'),
(38, '', 'Cubiertas con resina epoxica ', 'cubierta.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-10-16 12:14:13', '', 'FIBRECEN'),
(39, '', 'Mesas con resina epoxica', 'mesa.jpg', '', '', 0.00, 'Poliester', '', '', '', '', '', '', '', 1, '2019-10-16 12:16:11', '', 'FIBRECEN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_glosario`
--

CREATE TABLE `fib_glosario` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_glosario`
--

INSERT INTO `fib_glosario` (`ID`, `cover`, `nom`, `descripcion`, `visible`) VALUES
(1, '', 'Curado', '<p>Proceso qu&iacute;mico posterior al gelado, donde la resina se solidifica por completo gracias a la reacci&oacute;n exot&eacute;rmica provocada por adici&oacute;n de un catalizador, dando lugar a un producto que no cambia sus propiedades.</p>', 1),
(2, '', 'Gelado', '<p><span>Proceso que forma parte del curado en el cual la resina cambia su aspecto l&iacute;quido a uno gelatinoso. Ocasionado por la reacci&oacute;n qu&iacute;mica de adici&oacute;n de catalizador.</span></p>', 1),
(3, '', 'Exotermia', '<p><span>Reacci&oacute;n qu&iacute;mica en la cual se produce energ&iacute;a desprendiendo luz o calor.</span></p>', 1),
(4, '', 'Resistencia Qu&iacute;mica', '<p><span>Describe la capacidad de resistencia de los materiales a los diferentes agentes qu&iacute;micos.</span></p>', 1),
(5, '', 'Resistencia Mec&aacute;nica', '<p><span>La resistencia mec&aacute;nica es la capacidad de los cuerpos para resistir las fuerzas aplicadas sin romperse.</span></p>', 1),
(6, '', 'Tac', '<p><span>Termino coloquial que se da cuando el curado de la resina no es correcto y el producto final tiene el aspecto pegajoso al tacto.</span></p>', 1),
(7, '', 'Resina  Virgen', '<p><span>Se refiere a toda ausencia de acelerantes y diluyentes. En &eacute;ste estado no se puede polimerizar.</span></p>', 1),
(8, '', 'Pre-acelerado', '<p><span>Sustancia que est&aacute; previamente acelerada, y solo necesita agregarle catalizador para la polimerizaci&oacute;n.</span></p>', 1),
(9, '', 'Polimerizaci&oacute;n', '<p><span>Proceso qu&iacute;mico en el cual un pol&iacute;mero pasa de estado l&iacute;quido a s&oacute;lido.</span></p>', 1),
(10, '', 'Tixotropia', '<p><span>Capacidad de una resina para que no escurra en posici&oacute;n vertical.</span></p>', 1),
(11, '', 'Viscosidad', '<p><span>Apariencia de una sustancia l&iacute;quida refiri&eacute;ndose a su espesor. A mayor viscosidad menor velocidad de impregnaci&oacute;n y menor facilidad de desburbujeo. A temperaturas bajas aumenta la viscosidad.</span></p>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_histo_backupdb`
--

CREATE TABLE `fib_histo_backupdb` (
  `ID` int(9) UNSIGNED NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `archivo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_histo_backupdb`
--

INSERT INTO `fib_histo_backupdb` (`ID`, `fecha`, `archivo`) VALUES
(1, '2019-04-04 09:40:24', '/db-backup-fibrecen_main-20190404-094024.sql'),
(2, '2019-08-04 22:46:40', '/db-backup-fibrecen_main-20190804-224640.sql'),
(3, '2019-08-20 23:20:30', '/db-backup-fibrecen_main-20190820-232030.sql'),
(4, '2019-08-29 09:51:13', '/db-backup-fibrecen_main-20190829-095114.sql'),
(5, '2019-09-09 14:24:05', '/db-backup-fibrecen_main-20190909-142406.sql'),
(6, '2019-11-24 19:26:16', '/db-backup-fibrecen_main-20191124-192617.sql');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_home_config`
--

CREATE TABLE `fib_home_config` (
  `ID` int(1) UNSIGNED NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `selc` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_home_config`
--

INSERT INTO `fib_home_config` (`ID`, `titulo`, `contenido`, `selc`, `activo`) VALUES
(1, 'Fibrecen - Home1', '<div class=\"container\">\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-5 bt-bottom-info\" data-aos=\"fade-right\">\r\n<h5 style=\"font-size: 42px; font-weight: bold;\">&iexcl;Bienvenido!</h5>\r\n</div>\r\n<div class=\"col-lg-7 bt-bottom-info\" data-aos=\"fade-left\">\r\n<p style=\"font-size: 29px;\">Nosotros MOLDEAMOS SOLUCIONES junto contigo transformamos una idea en PROYECTOS A GRAN ESCALA.</p>\r\n</div>\r\n</div>\r\n<!--\r\n            <div class=\"row inner-sec-wthree\">\r\n                <div class=\"col-lg-4 bottom-sub-grid text-center aos-init aos-animate\" data-aos=\"zoom-in\">\r\n                    <div class=\"bt-icon\">\r\n                        <span class=\"fas fa-trophy\"></span>\r\n                    </div>\r\n\r\n                    <h4 class=\"sub-tittle\">Humility</h4>\r\n                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n                    <p>\r\n                        <a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    </p>\r\n                </div>\r\n                <div class=\"col-lg-4 bottom-sub-grid text-center aos-init aos-animate\" data-aos=\"zoom-in\">\r\n                    <div class=\"bt-icon\">\r\n                        <span class=\"far fa-thumbs-up\"></span>\r\n                    </div>\r\n\r\n                    <h4 class=\"sub-tittle\">Passion</h4>\r\n                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n                    <p>\r\n                        <a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    </p>\r\n                </div>\r\n                <div class=\"col-lg-4 bottom-sub-grid text-center aos-init aos-animate\" data-aos=\"zoom-in\">\r\n                    <div class=\"bt-icon\">\r\n                        <span class=\"fas fa-tv\"></span>\r\n                    </div>\r\n\r\n                    <h4 class=\"sub-tittle\">Honesty</h4>\r\n                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n                    <p>\r\n                        <a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    </p>\r\n                </div>\r\n            </div>\r\n--></div>\r\n<!-- /stats ->\r\n    <section class=\"stats_test container-fluid\">\r\n\r\n        <div class=\"row inner_stat\">\r\n            <div class=\"col-lg-6 stats-left-info\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6 stats_left counter_grid\">\r\n                        <i class=\"far fa-building\"></i>\r\n                        <p class=\"counter\">32</p>\r\n                        <h4>A&ntilde;os De Experiencia</h4>\r\n                    </div>\r\n                    <div class=\"col-md-6 stats_left counter_grid1\">\r\n                        <i class=\"far fa-edit\"></i>\r\n                        <p class=\"counter\">3840</p>\r\n                        <h4>Proyectos Realizados</h4>\r\n                    </div>\r\n                    <div class=\"col-md-6 stats_left counter_grid2\">\r\n                        <i class=\"far fa-smile\"></i>\r\n                        <p class=\"counter\">10879</p>\r\n                        <h4>Clientes Atendidos</h4>\r\n                    </div>\r\n                    <div class=\"col-md-6 stats_left counter_grid3\">\r\n                        <i class=\"fas fa-users\"></i>\r\n                        <p class=\"counter\">0</p>\r\n                        <h4>Visitas</h4>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-lg-6 stats-right-img\">\r\n\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!-- //stats -->\r\n<p>&nbsp;</p>\r\n<!--/services-->\r\n<div class=\"container\">\r\n<h3 class=\"tittle\">Crea Con Nosotros</h3>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-left\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes rigidos y piezas de fibra de vidrio</h5>\r\n<p>Crea piezas de gran tama&ntilde;o y poco detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Vaciado, encapsulados y roto moldeo</h5>\r\n<p>Crea figuras a partir de tus moldes.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-4 grid_info_main mid\" data-aos=\"flip-down\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/ab02.jpg\" alt=\" \" /></div>\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-right\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes flexibles</h5>\r\n<p>Crea piezas peque&ntilde;as con gran detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Resina para fotograf&iacute;a</h5>\r\n<p>Recubre tu fotograf&iacute;a con un acabado unico.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!--//services->\r\n    <section class=\"grids_sec_2\">\r\n        <div class=\"row style-grids_main\">\r\n            <div class=\"col-md-6 grid_sec_info\">\r\n                <div class=\"style-grid-2-text_info\" data-aos=\"fade-right\">\r\n                    <h3>\r\n                        Turning big ideas into great products.</h3>\r\n                    <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur.sed quia non numquam eius modi tempora incidunt ut labore .Lorem ipsum dolor sit amet,vehicula vel sapien et.</p>\r\n                    <div class=\"bnr-button\">\r\n                        <button type=\"button\" class=\"btn btn-primary play\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n							<i class=\"fas fa-play\"></i>\r\n						</button>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-6 style-image-2\">\r\n            </div>\r\n            <div class=\"col-md-6 style-image-2 second\">\r\n            </div>\r\n            <div class=\"col-md-6 grid_sec_info\">\r\n                <div class=\"style-grid-2-text_info\" data-aos=\"fade-right\">\r\n                    <h3>\r\n                        Turning big ideas into great products.</h3>\r\n                    <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur.sed quia non numquam eius modi tempora incidunt ut labore .Lorem ipsum dolor sit amet,vehicula vel sapien et</p>\r\n                    <div class=\"bnr-button\">\r\n                        <button type=\"button\" class=\"btn btn-primary play\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n							<i class=\"fas fa-play\"></i>\r\n						</button>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </section>\r\n    <!---->\r\n<p>&nbsp;</p>\r\n<!--/Projects->\r\n    <section class=\"services\">\r\n        <div class=\"container\">\r\n            <h3 class=\"tittle\">Our Latest Projects\r\n            </h3>\r\n            <div class=\"row inner-sec-wthree\">\r\n\r\n                <div class=\"col-md-4 proj_gallery_grid\" data-aos=\"zoom-in\">\r\n                    <div class=\"section_1_gallery_grid\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g1.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g1.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g2.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g2.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g3.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g3.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4 proj_gallery_grid\" data-aos=\"zoom-in\">\r\n                    <div class=\"section_1_gallery_grid\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g4.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g4.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g5.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g5.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g6.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g6.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4 proj_gallery_grid\" data-aos=\"zoom-in\">\r\n                    <div class=\"section_1_gallery_grid\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g7.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g7.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g8.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g8.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g9.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g9.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--//Projects-->', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_home_elementos`
--

CREATE TABLE `fib_home_elementos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `tit1` text NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tit2` text NOT NULL,
  `exp` varchar(12) NOT NULL,
  `pro` varchar(12) NOT NULL,
  `cli` varchar(12) NOT NULL,
  `txt1` text NOT NULL,
  `txt2` text NOT NULL,
  `txt3` text NOT NULL,
  `txt4` text NOT NULL,
  `ima0` varchar(100) NOT NULL,
  `ima1` varchar(100) NOT NULL,
  `ima2` varchar(100) NOT NULL,
  `ima3` varchar(100) NOT NULL,
  `ima4` varchar(100) NOT NULL,
  `ima5` varchar(100) NOT NULL,
  `ima6` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_home_elementos`
--

INSERT INTO `fib_home_elementos` (`ID`, `tit1`, `foto`, `tit2`, `exp`, `pro`, `cli`, `txt1`, `txt2`, `txt3`, `txt4`, `ima0`, `ima1`, `ima2`, `ima3`, `ima4`, `ima5`, `ima6`) VALUES
(1, '<h5 style=\"font-size: 42px; font-weight: bold;\">&iexcl;Bienvenido!</h5>', '', '<p style=\"font-size: 29px;\">Nosotros MOLDEAMOS SOLUCIONES junto contigo transformamos una idea en PROYECTOS A GRAN ESCALA.</p>', '32', '3840', '10879', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes Rigidos y piezas de fibra de vidrio</h5>\r\n<p>Crea piezas de gran tama?o y poco detalle.</p>', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Vaciado, encapsulados y roto moldeo</h5>\r\n<p>Crea figuras a partir de tus moldes.</p>', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes Flexibles</h5>\r\n<p>Crea piezas con gran detalle.</p>', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Aislantes</h5>\r\n<p>Contamos con&nbsp;<span>aislantes t&eacute;rmicos y ac&uacute;sticos.</span></p>', '', 'home46.jpg', 'MOSTRADOR.png', '10.jpg', '13.jpg', '8.jpg', '11.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_iconos`
--

CREATE TABLE `fib_iconos` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `fa_icon` varchar(100) NOT NULL,
  `icon` text NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_iconos`
--

INSERT INTO `fib_iconos` (`ID`, `nom`, `fa_icon`, `icon`, `tipo`) VALUES
(1, 'Descarga', 'fa-download', '<i class=\"fa fa-download\"></i>', 'awesome'),
(2, 'Menu', 'fa-list', '<i class=\"fa fa-list\"></i>', 'awesome'),
(3, 'Configuracion', 'fa-gear', '<i class=\"fa fa-gear\"></i>', 'awesome'),
(4, 'Configuraciones', 'fa-gears', '<i class=\"fa fa-gears\"></i>', 'awesome'),
(5, 'Modulos', 'fa-cubes', '<i class=\"fa fa-cubes\"></i>', 'awesome'),
(6, 'Home', 'fa-home', '<i class=\"fa fa-home\"></i>', 'awesome'),
(7, 'Portafolio', 'fa-briefcase', '<i class=\"fa fa-briefcase\"></i>', 'awesome'),
(8, 'Blog', 'fa-comments', '<i class=\"fa fa-comments\"></i>', 'awesome'),
(9, 'BlockIP', 'fa-crosshairs', '<i class=\"fa fa-crosshairs\"></i>', 'awesome'),
(10, 'Estadisticas', 'fa-bar-chart', '<i class=\"fa fa-bar-chart\"></i>', 'awesome'),
(11, 'Moneda', 'fa-usd', '<i class=\"fa fa-usd\"></i>', 'awesome'),
(12, 'Dashboard', 'fa-dashboard', '<i class=\"fa fa-dashboard\"></i>', 'awesome'),
(13, 'Usuario', 'fa-user', '<i class=\"fa fa-user\"></i>', 'awesome'),
(14, 'Usuarios', 'fa-users', '<i class=\"fa fa-users\"></i>', 'awesome'),
(15, 'Global', 'fa-globe', '<i class=\"fa fa-globe\"></i>', 'awesome'),
(16, 'Ver', 'fa-eye', '<i class=\"fa fa-eye\"></i>', 'awesome'),
(17, 'Enviar', 'fa-send-o', '<i class=\"fa fa-send-o\"></i>', 'awesome'),
(18, 'Mail', 'fa-envelope', '<i class=\"fa  fa-envelope\"></i>', 'awesome'),
(19, 'Marca de Mapa', 'fa-map-marker', '<i class=\"fa  fa-map-marker\"></i>', 'awesome'),
(20, 'Formularios', 'fa-pencil-square-o', '<i class=\"fa  fa-pencil-square-o\"></i>', 'awesome'),
(21, 'Carrito', 'fa-shopping-cart', '<i class=\"fa fa-shopping-cart\"></i>', 'awesome'),
(22, 'Folder Open Blanco', 'fa-folder-open-o', '<i class=\"fa fa-folder-open-o\"></i>', 'awesome'),
(23, 'Folder Open', 'fa-folder-open', '<i class=\"fa fa-folder-open\"></i>', 'awesome'),
(24, 'Tesmoniales', 'fa-commenting', '<i class=\"fa fa-commenting\"></i>', 'awesome'),
(25, 'Clientes', 'fa-child', '<i class=\"fa fa-child\"></i>', 'awesome'),
(26, 'Mapa', 'fa-map', '<i class=\"fa fa-map\" aria-hidden=\"true\"></i>', 'awesome'),
(27, 'Sitemap', 'fa-sitemap', '<i class=\"fa fa-sitemap\"></i>', 'awesome'),
(28, 'Check Square', 'fa-check-square', '<i class=\"fa fa-check-square\"></i>', 'awesome'),
(29, 'Play', 'fa-caret-square-o-right ', '<i class=\"fa fa-caret-square-o-right \"></i>', 'awesome'),
(30, 'Lupa', 'fa-search', '<i class=\"fa fa-search\"></i>', 'awesome'),
(31, 'Book', 'fa-book', '<i class=\"fa fa-book\"></i>', 'awesome'),
(32, 'Saludo', 'fa-handshake-o', '<i class=\"fa fa-handshake-o\"></i>', 'awesome'),
(33, 'Imagen', 'fa-file-image-o', '<i class=\"fa fa-file-image-o\"></i>', 'awesome'),
(34, 'Universidad', 'fa-university', '<i class=\"fa fa-university\"></i>', 'awesome'),
(35, 'Birrete', 'fa-graduation-cap', '<i class=\"fa fa-graduation-cap\"></i>', 'awesome'),
(36, 'Herramienta', 'fa-wrench', '<i class=\"fa fa-wrench\"></i>', 'awesome'),
(37, 'Señas', 'fa-american-sign-language-interpreting', '<i class=\"fa fa-american-sign-language-interpreting\"></i>', 'awesome'),
(38, 'Imagen', 'fa-picture-o', '<i class=\"fa fa-picture-o\"></i>', 'awesome'),
(39, 'Lista top', 'fa-list-alt', '<i class=\"fa fa-list-alt\"></i>', 'awesome');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_ipbann`
--

CREATE TABLE `fib_ipbann` (
  `ID` int(11) NOT NULL,
  `ip` varchar(256) NOT NULL DEFAULT '',
  `bloqueo` tinyint(1) NOT NULL,
  `alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_landingpage_seccion`
--

CREATE TABLE `fib_landingpage_seccion` (
  `ID` int(6) UNSIGNED NOT NULL,
  `seccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tit` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `conte` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `url_page` varchar(300) NOT NULL,
  `visible` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_landingpage_seccion`
--

INSERT INTO `fib_landingpage_seccion` (`ID`, `seccion`, `tit`, `conte`, `url_page`, `visible`) VALUES
(1, '', 'Nuevo Landing', '<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\"><iframe src=\"http://www.youtube.com/embed/fc3JqR68e-c\" frameborder=\"0\" width=\"1000\" height=\"824\"></iframe><br /></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 0.9em;\">Contenido del landingpage</span></p>', 'http://www.fibrecen.com.mx/landingpage/fibrecen/1-Nuevo-Landing', '1'),
(2, '', 'Industrias', '<p style=\"text-align: justify;\"><span style=\"font-size: 24px;\"><strong>&nbsp;</strong></span></p>\r\n<p align=\"center\"><span style=\"color: #8e0300; font-size: 24px;\"><strong>LOS MATERIALES COMPUESTOS EN LAS DIFERENTES INDUSTRIAS</strong></span></p>\r\n<p align=\"center\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 16px;\"><strong>El uso de los materiales compuestos se ha ido ampliando en los &uacute;ltimos a&ntilde;os y se han colocado a la vanguardia en las diferentes industrias ya que la sinergia que se genera rebasa las virtudes de las propiedades de diversos materiales que se utilizan individualmente. Tambi&eacute;n conocidos como PRFV, los materiales compuestos cuentan con buenas propiedades mec&aacute;nicas, resistentes a la corrosi&oacute;n y a los agentes qu&iacute;micos, y a causa de sus propiedades f&iacute;sicas pueden ser moldeados con absoluta libertad de formas. Se puede decir que los materiales compuestos ofrecen cada vez un abanico m&aacute;s amplio de materiales y soluciones a ingenieros, arquitectos y artesanos.&nbsp;</strong></span><span style=\"font-size: 0.9em;\">&nbsp;</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<hr style=\"background-color: #070f37; height: 3px;\" align=\"center\" noshade=\"noshade\" size=\"20\" width=\"30%\" />\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<table style=\"border-color: #ffffff; border-width: 0px; border-style: solid;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/rejillapfr.jpg\" alt=\"\" width=\"545\" height=\"545\" border=\"0\" /></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><br /></strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><br /></strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 24px; color: #2e2e2e;\"><strong>CONSTRUCCI&Oacute;N</strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 14px; color: #4d4d4d;\"><strong><br /></strong></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">El uso de FRP en este sector ha ido incrementando en los &uacute;ltimos a&ntilde;os debido a que sus propiedades hacen que resulte potencialmente ventajoso frente a materiales tradicionales como el hormig&oacute;n o el acero.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\"><br /></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">Por su excelente caracter&iacute;stica a la resistencia mec&aacute;nica, la rigidez, a la corrosi&oacute;n y ataque de agentes ambientales, se ha convertido en muchas ocasiones en sustito del acero, ya que aparte es m&aacute;s ligero y se reducen los costos de traslado y maniobra.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\"><br /></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">Actualmente son utilizados en puentes, vigas, cimentaciones, como refuerzo para productos de hormig&oacute;n, restauraci&oacute;n de estructuras. Los vemos tambi&eacute;n aplicado en teatros, cines y acuarios utilizando aislantes termo-ac&uacute;sticos. Debido a que &eacute;stos materiales resultan ser muy maleables se ven aplicados en p&aacute;neles de decoraci&oacute;n gracias a la libertad con el dise&ntilde;o sin necesidad de utilizar gran maquinaria y a bajo costo. En la industria es com&uacute;n observar el uso de los materiales aplicados en rejillas para pisos antiderrapantes, mezzanines, trincheras.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\">SANITARIA&nbsp;</span></strong></p>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\"><br /></span></strong></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">Debido a la alta resistencia a la corrosi&oacute;n, a la agresi&oacute;n de agentes ambientales, y por su gran ligereza los materiales compuestos resultan ser un gran aliado para &eacute;ste sector. Com&uacute;nmente los vemos aplicados en cubiertas de cocina, ovalines, duchas, o muebles de ba&ntilde;o, ya que se le puede dar un est&eacute;tico efecto marmoleado de gran brillo o con apariencia de piedra natural. En &eacute;ste sector los PRFV ayudan a realzar los terminados volvi&eacute;ndolos atractivos a la vista y de bajo costo en mantenimiento.&nbsp;</span></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: justify;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/19876560.jpg\" alt=\"\" width=\"400\" height=\"400\" border=\"0\" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: justify;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/2.jpg\" alt=\"\" width=\"500\" height=\"500\" border=\"0\" /></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\">AUTOMOTRIZ&nbsp;</span></strong></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #4d4d4d; font-family: arial, helvetica, sans-serif;\"><span style=\"font-size: 14px;\"><br /></span></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">Definitivamente los materiales reforzados en &eacute;ste sector son una tendencia en nuestros d&iacute;as, gracias a su favorable relaci&oacute;n entre resistencia y peso, se estima que estos materiales pl&aacute;sticos comprenden aproximadamente 50% del volumen de un veh&iacute;culo liviano, pero representan s&oacute;lo 8% del peso total. Aparte de las virtudes est&eacute;ticas que se le pueden a&ntilde;adir o modificar en el dise&ntilde;o evitando grandes maquinarias y por ende elevaci&oacute;n de costos de operaci&oacute;n.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">Las ventajas m&aacute;s importantes de los compuestos surgen en la fabricaci&oacute;n de piezas individuales, las unidades grandes y complejas pueden moldearse en una sola pieza, adem&aacute;s de m&aacute;s ligeras y f&aacute;ciles de ensamblar, pueden ajustarse mucho m&aacute;s precisamente.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; font-family: arial, helvetica, sans-serif; color: #4d4d4d;\">Los compuestos avanzados ofrecen un gran potencial debido a su alta reducci&oacute;n de masa (2 veces menos que el aluminio y 5 veces menos que el acero). Reducir la masa de un veh&iacute;culo disminuye el consumo de combustible, lo hace m&aacute;s f&aacute;cil de conducir, de acelerar y de frenar.</span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\"><strong>N&Aacute;UTICO</strong></span></strong></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 14px; color: #888888;\"><br /></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">Este uno de los sectores de mayor antig&uuml;edad y experiencia en el uso de los materiales compuestos, ha ido evolucionando desde la aplicaci&oacute;n manual y meramente artesanal hasta la producci&oacute;n en masa desde lanchas pesqueras hasta yates lujosos. Tambi&eacute;n los podemos ver aplicados en toboganes, albercas, chapoteaderos, jacuzzi, boyas, entre otros.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">As&iacute; como en las dem&aacute;s industrias, es conocido que sus principales ventajas de los materiales compuestos para aplicaciones n&aacute;uticas son: resistencia a las agresiones del medio ambiente y corrosi&oacute;n, la capacidad de poder moldear estructuras de geometr&iacute;a &uacute;nica y compleja,&nbsp;excelente resistencia mec&aacute;nica en relaci&oacute;n a su peso, capacidad adaptar su resistencia mec&aacute;nica de acuerdo a las condiciones de carga, bajo mantenimiento, facilidad de reparaci&oacute;n&nbsp;y excelente durabilidad</span>.</p>\r\n</td>\r\n<td style=\"text-align: justify;\">\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td style=\"text-align: justify;\">\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td style=\"text-align: justify;\">\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td style=\"text-align: justify;\">\r\n<p style=\"text-align: center;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/gelcoat.jpg\" alt=\"\" width=\"545\" height=\"408\" border=\"0\" />&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>&nbsp;</td>\r\n<td>\r\n<p style=\"text-align: center;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/avones2.jpg\" alt=\"\" width=\"500\" height=\"422\" border=\"0\" /></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\">AERONAUTICA Y E&Oacute;LICA</span></strong></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">El porcentaje de partes fabricadas con materiales compuestos en las aeronaves ha ido en aumento, ya que dan a los aviones ventajas en cuesti&oacute;n de reducci&oacute;n de peso y costo de mantenimiento (hasta en un 30%), permitiendo contar con m&aacute;s autonom&iacute;a de vuelo. Actualmente los materiales de refuerzo en aeronaves de tipo comercial han llegado hasta un 50%, es el caso del 787 de Boeing y el A350 de Airbus que son los principales contendientes en lo que se refiere a aviones grandes. Los podemos ver aplicados en alas, superficies sustentadoras e incluyendo secciones de fuselaje. En el tema de energ&iacute;a e&oacute;lica, lo vemos en la fabricaci&oacute;n de turbinas y aspas e&oacute;licas que ya se encuentran presentes actualmente en diversos parques e&oacute;licos en nuestro pa&iacute;s.</span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\">INDUSTRIAL</span></strong></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\"><br /></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">En las diversas industrias sus procesos est&aacute;n expuestos a condiciones de agresi&oacute;n qu&iacute;mica o ambiental, por lo que deben considerar que la aplicaci&oacute;n de los PRFV es forzosamente necesaria para la protecci&oacute;n y el buen funcionamiento de su maquinaria y herramental.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">Dentro de lo que com&uacute;nmente son aplicados los materiales compuestos es en&nbsp; las &nbsp;campanas de extracci&oacute;n, contenedores de &aacute;cidos, trincheras, tinas de lavado o de galvanizado, elaboraci&oacute;n de contenedores de alta capacidad por medio de embobinado, aplicaci&oacute;n de resinas de alta resistencia qu&iacute;mica para pisos, torres de enfriamiento entre otros.</span></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/12.jpg\" alt=\"\" width=\"500\" height=\"392\" border=\"0\" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\"><span><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/6.jpg\" alt=\"\" width=\"500\" height=\"331\" border=\"0\" /></span></span></strong></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\"><strong><span style=\"font-size: 24px; color: #2e2e2e;\"><span>ARTESANAL&nbsp;</span></span></strong></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #888888;\"><br /></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">Este sector en especial es donde se puede jugar con las configuraciones de los materiales compuestos, pues &uacute;nicamente depende del desarrollo de la creatividad de los artesanos plasmados en art&iacute;culos como joyer&iacute;a, muebles, art&iacute;culos decorativos para el hogar, estatuas, bustos, en diversas ferias y artesan&iacute;as coloniales.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px; color: #4d4d4d; font-family: arial, helvetica, sans-serif;\">Gracias a la versatilidad de los materiales, &eacute;stos pueden ser combinados en diversas formas, para producir una pieza &uacute;nica o en serie. Espec&iacute;ficamente en este sector los moldes r&iacute;gidos y flexibles juegan un papel muy importante.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'http://www.fibrecen.com.mx/landingpage/fibrecen/2-Industrias', '1'),
(3, '', 'INDUSTRIAS', '<p style=\"text-align: center;\" dir=\"rtl\"><span style=\"font-size: 24px;\"><strong>NUESTROS MATERIALES COMPUESTOS EN LAS DIFERENTES INDUSTRIAS</strong></span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px;\">El uso de los materiales compuestos ha ido en incremento y se han colocado a la vanguardia ya que la sinergia que se genera rebasa las virtudes de las propiedades de diversos materiales que se utilizan individualmente. Tambi&eacute;n conocidos como FRP, los materiales compuestos tienen&nbsp; con buenas propiedades mec&aacute;nicas, resistentes a la corrosi&oacute;n y a los agentes qu&iacute;micos, y a causa de sus propiedades f&iacute;sicas, pueden ser moldeados con absoluta libertad de formas.&nbsp;</span></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<table style=\"background-color: #070f37; border-color: #070f37; border-width: 10px; border-style: solid;\" border=\"0px\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\"><br /></span></strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\">CONSTRUCCI&Oacute;N</span></strong></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 14px;\"><strong><span style=\"color: #ffffff;\"><br /></span></strong></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">El uso de FRP en este sector ha ido incrementando en los &uacute;ltimos a&ntilde;os debido a que sus propiedades hacen que resulte potencialmente ventajoso frente a materiales tradicionales como el hormig&oacute;n o el acero.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">Por su excelente caracter&iacute;stica a la resistencia mec&aacute;nica, la rigidez, a la corrosi&oacute;n y ataque de agentes ambientales, se ha convertido en muchas ocasiones en sustito del acero, ya que aparte es m&aacute;s ligero y se reducen los costos de traslado y maniobra.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">Actualmente son utilizados en puentes, vigas, cimentaciones, como refuerzo para productos de hormig&oacute;n, restauraci&oacute;n de estructuras. Los vemos tambi&eacute;n aplicado en teatros, cines y acuarios utilizando aislantes termo-ac&uacute;sticos. Debido a que &eacute;stos materiales resultan ser muy maleables se ven aplicados en p&aacute;neles de decoraci&oacute;n gracias a la libertad con el dise&ntilde;o sin necesidad de utilizar gran maquinaria y a bajo costo. En la industria es com&uacute;n observar el uso de los materiales aplicados en rejillas para pisos antiderrapantes, mezzanines, trincheras.&nbsp;</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\"><br /></span></p>\r\n</td>\r\n<td style=\"border-color: #070f37; border-style: solid; border-width: 10px; text-align: justify;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/Sinttulo-4.jpg\" alt=\"\" width=\"400\" height=\"400\" border=\"0\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style=\"border-color: #8e0b00; border-width: 10px; background-color: #8e0b00; border-style: solid;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/Sinttulo-3.jpg\" alt=\"\" width=\"400\" height=\"400\" border=\"0\" /></span></strong></span></p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\">SANITARIA</span></strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\"><br /></span></strong></span></p>\r\n<p style=\"text-align: justify; padding-left: 30px;\"><span style=\"color: #ffffff;\">Debido a la alta resistencia a la corrosi&oacute;n, a la agresi&oacute;n de agentes ambientales, y por su gran ligereza los materiales compuestos resultan ser un gran aliado para &eacute;ste sector.</span><br /><span style=\"color: #ffffff;\"> Actualmente lo vemos aplicado com&uacute;nmente en cubiertas de cocina, ovalines, duchas, o muebles de ba&ntilde;o, ya que se le puede dar un bello efecto marmoleado, de gran brillo o con apariencia de piedra natural, lo que realza los terminados y se vuelven atractivos a la vista y de bajo costo en mantenimiento.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style=\"border-color: #070f37; border-width: 10px; background-color: #070f37; border-style: solid;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\"><br /></span></strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\">AUTOMOTRIZ</span></strong></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"font-size: 20px;\"><strong><span style=\"color: #ffffff;\"><br /></span></strong></span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">Definitivamente los materiales reforzados en &eacute;ste sector son una tendencia en nuestros d&iacute;as, gracias a su favorable relaci&oacute;n entre resistencia y peso, se estima que estos materiales pl&aacute;sticos comprenden aproximadamente 50% del volumen de un veh&iacute;culo liviano, pero representan s&oacute;lo 8% del peso total. Aparte de las virtudes est&eacute;ticas que se le pueden a&ntilde;adir o modificar en el dise&ntilde;o evitando grandes maquinarias y por ende elevaci&oacute;n de costos de operaci&oacute;n.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">Las ventajas m&aacute;s importantes de los compuestos surgen en la fabricaci&oacute;n de piezas individuales, las unidades grandes y complejas pueden moldearse en una sola pieza, adem&aacute;s de m&aacute;s ligeras y f&aacute;ciles de ensamblar, pueden ajustarse mucho m&aacute;s precisamente.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">Los compuestos avanzados ofrecen un gran potencial debido a su alta reducci&oacute;n de masa (2 veces menos que el aluminio y 5 veces menos que el acero). Reducir la masa de un veh&iacute;culo disminuye el consumo de combustible, lo hace m&aacute;s f&aacute;cil de conducir, de acelerar y de frenar.</span></p>\r\n<p style=\"text-align: justify;\"><span style=\"color: #ffffff;\">Por estas cuestiones es que actualmente los FRP son utilizados en partes de motor, alerones, fascias, cantoneras, cubiertas de tablero, partes de carrocer&iacute;a as&iacute; como para el tuneado de cualquier auto.</span></p>\r\n</td>\r\n<td style=\"border-color: #070f37; border-style: solid; border-width: 10px; text-align: justify;\">\r\n<p style=\"text-align: center;\"><img src=\"http://www.fibrecen.com.mx/assets/plugins/tiny_mce/plugins/ibrowser/Gallery/2.jpg\" alt=\"\" width=\"500\" height=\"500\" border=\"0\" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'http://www.fibrecen.com.mx/landingpage/fibrecen/3-INDUSTRIAS', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_map_config`
--

CREATE TABLE `fib_map_config` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `lng` varchar(20) NOT NULL,
  `zoom` varchar(2) NOT NULL,
  `cover` varchar(50) NOT NULL,
  `on_costo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_map_config`
--

INSERT INTO `fib_map_config` (`ID`, `nom`, `lat`, `lng`, `zoom`, `cover`, `on_costo`) VALUES
(1, 'Querétaro', '20.5931297', '-100.3920483', '12', 'g_intelmex.png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_map_ubicacion`
--

CREATE TABLE `fib_map_ubicacion` (
  `ID` int(9) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `adres` varchar(150) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `info` varchar(250) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `tel` varchar(30) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nivel` varchar(2) NOT NULL,
  `rol` varchar(2) NOT NULL,
  `lat` varchar(15) NOT NULL,
  `lng` varchar(15) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_map_ubicacion`
--

INSERT INTO `fib_map_ubicacion` (`ID`, `nom`, `adres`, `descripcion`, `info`, `precio`, `tel`, `cover`, `nivel`, `rol`, `lat`, `lng`, `alta`, `fmod`, `visible`, `activo`) VALUES
(1, 'Intelmex', 'Calle 1 303, Jurica, 76130 Santiago de Querétaro, Qro.', '', 'Reparación de telefonos', 0.00, '4421234567', 'nodisponible.jpg', '1', '3', '20.6500317', '-100.4290312', '2018-04-03 13:44:50', '2018-04-03 13:59:06', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_menu_admin`
--

CREATE TABLE `fib_menu_admin` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom_menu` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `nivel` int(1) NOT NULL,
  `ID_menu_adm` int(2) NOT NULL,
  `ID_mod` int(2) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_menu_admin`
--

INSERT INTO `fib_menu_admin` (`ID`, `nom_menu`, `icono`, `link`, `nivel`, `ID_menu_adm`, `ID_mod`, `visible`) VALUES
(1, 'Config. Sistema', 'fa-gear', 'index.php?mod=sys&ext=admin/index', -1, 0, 9, 1),
(2, 'Modulos', 'fa-cubes', 'index.php?mod=sys&ext=modulos', -1, 0, 0, 1),
(3, 'Logs', 'fa-globe', 'index.php?mod=sys&ext=admin/index&opc=logs', -1, 0, 9, 1),
(4, 'Bloquear IP', 'fa-crosshairs', 'index.php?mod=sys&ext=admin/index&opc=bloquear', -1, 0, 9, 1),
(5, 'Temas', 'fa-sticky-note-o', 'index.php?mod=sys&ext=admin/index&opc=temas', -1, 0, 9, 1),
(6, 'Admin. Usuarios', 'fa-users', 'index.php?mod=usuarios&ext=admin/index', -1, 0, 4, 0),
(7, 'Menu Admin', 'fa-list', 'index.php?mod=sys&ext=menu_admin', -1, 0, 9, 1),
(8, 'Iconos', 'fa-smile-o', 'index.php?mod=sys&ext=admin/index&opc=iconos', -1, 0, 9, 1),
(9, 'Informe de Visitas', 'fa-download', 'index.php?mod=estadisticas&ext=admin/index', 1, 0, 10, 1),
(10, 'Backup DB', 'fa-download', 'index.php?mod=sys&ext=backup', -1, 0, 9, 1),
(11, 'Config. Mailbox', 'fa-gear', 'index.php?mod=mailbox&ext=admin/index', 1, 0, 12, 0),
(12, 'Mensajes', 'fa-envelope', 'index.php?mod=mailbox', 1, 0, 12, 1),
(13, 'Editar', 'fa-home', 'index.php?mod=Home&ext=admin/index', 1, 0, 3, 1),
(14, 'Menu Web', 'fa-list', 'index.php?mod=Home&ext=admin/index&opc=menu_web', 1, 0, 3, 1),
(15, 'Admin productos', 'fa-shopping-cart', 'index.php?mod=productos&ext=admin/index&opc=producto', 1, 0, 15, 1),
(16, 'Categoria de productos', 'fa-folder-open', 'index.php?mod=productos&ext=admin/index&opc=categoria', 1, 0, 15, 1),
(17, 'Subcategoria de productos', 'fa-folder-open-o', 'index.php?mod=productos&ext=admin/index&opc=subcategoria', 1, 0, 15, 1),
(18, 'Config. Gmaps', 'fa-gear', 'index.php?mod=gmaps&ext=admin/index', 1, 0, 16, 1),
(19, 'Ubicaciones', 'fa-map-marker', 'index.php?mod=gmaps&ext=admin/index&opc=ubicaciones', 1, 0, 16, 1),
(20, 'Config. Contacto', 'fa-gear', 'index.php?mod=contacto&ext=admin/index', 1, 0, 8, 1),
(21, 'Correos de Formulario', 'fa-pencil-square-o', 'index.php?mod=contacto&ext=admin/index&opc=forms', 1, 0, 8, 1),
(22, 'Generador Sitemap', 'fa-sitemap', 'index.php?mod=sys&ext=admin/index&opc=sitemap', 1, 0, 9, 1),
(23, 'Opciones', 'fa-gears', 'index.php?mod=sys&ext=opciones', 1, 0, 9, 1),
(24, 'Slider', 'fa-caret-square-o-right ', 'index.php?mod=Home&ext=admin/index&opc=slider', 1, 0, 3, 1),
(25, 'Licencia', 'fa-eye', 'index.php?mod=sys&ext=licencia', -1, 0, 9, 1),
(26, 'Portafolio', 'fa-briefcase', 'index.php?mod=portafolio&ext=admin/index', 1, 0, 20, 1),
(27, 'Editar', 'fa-pencil-square-o', 'index.php?mod=glosario&ext=admin/index', 1, 0, 23, 0),
(28, 'Definiciones', 'fa-list', 'index.php?mod=glosario&ext=admin/index&opc=def', 1, 0, 23, 1),
(29, 'Testimonios', 'fa-child', 'index.php?mod=Home&ext=admin/index&opc=testimonios', 1, 0, 3, 1),
(30, 'Elementos', 'fa-folder-open', 'index.php?mod=Home&ext=admin/index&opc=elementos&action=form&ctrl=edit&id=1', 1, 0, 3, 1),
(31, 'Editar', 'fa-pencil-square-o', 'index.php?mod=nosotros&ext=admin/index&action=editar', 1, 0, 5, 1),
(32, 'Imagenes Top', 'fa-file-image-o', 'index.php?mod=Home&ext=admin/index&opc=ima_top', 1, 0, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_menu_web`
--

CREATE TABLE `fib_menu_web` (
  `ID` int(6) UNSIGNED NOT NULL,
  `menu` varchar(50) NOT NULL,
  `url` varchar(254) NOT NULL,
  `modulo` varchar(100) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `ord` varchar(2) NOT NULL,
  `subm` varchar(3) NOT NULL,
  `tit_sec` varchar(100) NOT NULL,
  `ima_top` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_menu_web`
--

INSERT INTO `fib_menu_web` (`ID`, `menu`, `url`, `modulo`, `ext`, `ord`, `subm`, `tit_sec`, `ima_top`, `visible`) VALUES
(1, 'Nosotros', 'nosotros/', 'nosotros', '', '1', '', '', 'nosotros1.jpg', 1),
(2, 'Productos', 'productos/', 'productos', '', '2', '', '', 'producots.jpg', 1),
(3, 'Cursos', '#', 'cursos', '', '7', '', '', '99.jpg', 1),
(4, 'Como Se Hace', 'comosehace/', 'comosehace', '', '3', '3', 'Videos Tutoriales', 'comosehace.jpg', 1),
(5, 'Sector Industrial', '#', 'manufactura', '', '4', '', '', 'manu78.jpg', 1),
(6, 'Blog', 'blog/', 'blog', '', '5', '', '', 'hom4.jpg', 0),
(7, 'Contacto', 'contacto/', 'contacto', '', '8', '', 'Contactanos', 'contactos89-01.jpg', 1),
(8, 'Galer&iacute;a', 'galeria/', 'galeria', '', '10', '', '', 'medidor.jpg', 1),
(9, 'Cursos Poli&eacute;ster', 'cursos/poliester/', 'cursos', 'poliester', '9', '3', '', '', 1),
(10, 'Cursos Ep&oacute;xicos', 'cursos/epoxicos/', 'cursos', 'epoxicos', '10', '3', '', '', 1),
(11, 'Poliester', 'manufactura/poliester/', 'manufactura', 'poliester', '11', '5', 'Manufactura Poli&eacute;ster', '', 1),
(12, 'Ep&oacute;xicos', 'manufactura/epoxicos/', 'manufactura', 'epoxicos', '12', '5', 'Manufactura Ep&oacute;xicos', '', 1),
(13, 'Buscar', 'index.php?mod=search', 'search', '', '13', '', 'Busqueda', 'home1.jpg', 0),
(14, 'Industrias', 'landingpage/fibrecen/2-Industrias', 'landingpage', '', '6', '', 'lndustrias', '6.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_mode_page`
--

CREATE TABLE `fib_mode_page` (
  `ID` int(2) UNSIGNED NOT NULL,
  `page_mode` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_mode_page`
--

INSERT INTO `fib_mode_page` (`ID`, `page_mode`) VALUES
(1, 'page'),
(2, 'landingpage'),
(3, 'extranet'),
(4, 'ecommerce'),
(5, 'CRM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_modulos`
--

CREATE TABLE `fib_modulos` (
  `ID` int(6) NOT NULL,
  `nombre` varchar(25) NOT NULL DEFAULT '',
  `modulo` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL,
  `dashboard` tinyint(1) NOT NULL,
  `nivel` tinyint(4) NOT NULL DEFAULT '0',
  `home` tinyint(4) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `sname` varchar(10) NOT NULL DEFAULT '',
  `icono` varchar(50) NOT NULL,
  `link` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_modulos`
--

INSERT INTO `fib_modulos` (`ID`, `nombre`, `modulo`, `description`, `dashboard`, `nivel`, `home`, `visible`, `activo`, `sname`, `icono`, `link`) VALUES
(1, 'admin', 'admin', '', 0, 0, 0, 0, 0, 'false', '', ''),
(2, 'Dashboard', 'dashboard', '', 1, -1, 0, 0, 1, 'false', '', 'index.php?mod=dashboard'),
(3, 'Home', 'Home', 'Administración y gestión del Home.', 0, 0, 1, 1, 1, 'false', 'fa-home', 'index.php?mod=Home'),
(4, 'Usuarios', 'usuarios', 'Administación y gestión de usuarios.', 0, -1, 0, 1, 1, 'false', 'fa-users', 'index.php?mod=usuarios'),
(5, 'Nosotros', 'nosotros', 'Administración del contenido del modulo de nosotros.', 0, 0, 0, 1, 1, 'false', 'fa-users', 'index.php?mod=nosotros'),
(6, 'Portafolio', 'portafolio', 'Administración y gestión del portafolio.', 0, 1, 0, 0, 1, 'false', 'fa-briefcase', 'index.php?mod=portafolio'),
(7, 'Blog', 'blog', 'Administración del contenido del modulo de blog.', 0, 0, 0, 0, 0, 'false', 'fa-comments', 'index.php?mod=blog'),
(8, 'Contacto', 'contacto', 'Consultas del modulo de contacto.', 0, 0, 0, 1, 1, 'false', 'fa-map-marker', 'index.php?mod=contacto'),
(9, 'Sistema', 'sys', 'Configuración y administración del sistema.', 1, -1, 0, 1, 1, 'false', 'fa-gear', 'index.php?mod=sys'),
(10, 'Estadistica', 'estadisticas', 'Estadisticas de trafico. ', 0, -1, 0, 1, 1, 'false', 'fa-bar-chart', 'index.php?mod=estadisticas'),
(11, 'Formularios', 'forms', 'Administracion de Formularios para la web.', 1, 1, 0, 0, 0, 'false', 'fa-pencil-square-o', 'index.php?mod=forms'),
(12, 'Mailbox', 'mailbox', 'Mailbox de formularios', 1, 1, 0, 1, 1, 'false', ' fa-envelope', 'index.php?mod=mailbox'),
(13, 'Ecommerce', 'ecommerce', 'Administración y gestión del modulo ecommerce.', 0, 1, 0, 0, 0, 'false', 'fa-shopping-cart', 'index.php?mod=ecommerce'),
(14, 'Marketing', 'marketing', '', 0, 1, 0, 0, 0, 'false', 'fa-globe', 'index.php?mod=marketing'),
(15, 'Productos', 'productos', 'Administración de productos', 0, 1, 0, 1, 1, 'false', 'fa-shopping-cart', 'index.php?mod=productos'),
(16, 'Gmaps', 'gmaps', 'Mapas de Google', 0, 0, 0, 0, 0, 'false', 'fa-map', 'index.php?mod=gmaps'),
(17, 'Chat', 'chat', 'Administración del modulo chat.', 0, 1, 0, 0, 1, 'false', 'fa-commenting', 'index.php?mod=chat'),
(18, 'Directorio', 'directorio', 'Administrador del modulo de Directorio.', 0, 1, 0, 0, 0, 'false', 'fa-globe', 'index.php?mod=directorio'),
(19, 'Cursos', 'cursos', 'Administrador del modulo cursos', 0, 0, 0, 1, 1, 'false', 'fa-university', 'index.php?mod=cursos'),
(20, 'Manufactura', 'manufactura', 'Administrador del modulo Manufactura', 0, 0, 0, 1, 1, 'false', 'fa-handshake-o', 'index.php?mod=manufactura'),
(21, 'Como se hace', 'comosehace', 'Administrador del modulo Como se hace', 0, 0, 0, 0, 1, 'false', 'fa-american-sign-language-interpreting', 'index.php?mod=comosehace'),
(22, 'Buscar', 'search', 'Buscador', 0, 0, 0, 0, 1, 'false', 'fa-eye', 'index.php?mod=search'),
(23, 'Glosario', 'glosario', 'Administrador del modulo glosario', 0, 1, 0, 1, 1, 'false', 'fa-book', 'index.php?mod=glosario'),
(24, 'Galeria', 'galeria', 'Administrador del modulo galeria', 0, 1, 0, 1, 1, 'true', 'fa-picture-o', 'index.php?mod=galeria'),
(25, 'LandingPage', 'landingpage', 'Administrador del modulo landingpage', 0, 1, 0, 1, 1, 'false', 'fa-folder-open', 'index.php?mod=landingpage');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_nosotros`
--

CREATE TABLE `fib_nosotros` (
  `ID` int(9) UNSIGNED NOT NULL,
  `historia` text NOT NULL,
  `mision` text NOT NULL,
  `vision` text NOT NULL,
  `valores` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_nosotros`
--

INSERT INTO `fib_nosotros` (`ID`, `historia`, `mision`, `vision`, `valores`) VALUES
(1, '<p style=\"text-align: justify;\"><strong><span style=\"font-size: 16px;\">Somos una empres</span><span style=\"font-size: 16px;\">a con m&aacute;s de 30 a&ntilde;os en el mercado en constante crecimiento, nuestra ubicaci&oacute;n nos permite ofrecer un servicio eficiente principalmente en la zona baj&iacute;o y actualmente nuestra marca es reconocida a nivel nacional.</span></strong></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">En FIBRECEN trabajamos de manera sostenible con el fin de generar valor a nuestros clientes, proveedores y colaboradores. Por lo que nuestro equipo ha enfocado su esfuerzo en desarrollar mejoras continuas para proveer un servicio y productos de alta calidad, as&iacute; mismo actuamos con responsabilidad en los procesos para mantener una relaci&oacute;n sana y amigable con el medio ambiente.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Entendemos las necesidades de nuestros clientes, as&iacute; que vamos m&aacute;s all&aacute; del producto, gracias a nuestra experiencia tenemos la capacidad de ofrecer y crear soluciones apropiadas enfocadas al proyecto espec&iacute;fico de cada cliente en las diversas industrias: Automotriz, Artesanal, Construcci&oacute;n, Aeron&aacute;utica, Acu&aacute;tica, Industrial, Pinturas, y m&aacute;s.</p>', '<p style=\"font-size: 16px; text-align: justify;\">Ser el proveedor m&aacute;s reconocido y especializado en la industria de los materiales compuestos (resinas y fibras de vidrio), ofreciendo productos de alta calidad y asesor&iacute;a a trav&eacute;s de un grupo humano comprometido a dar el mejor servicio, para contribuir al desarrollo exitoso de nuestros clientes, colaboradores y del pa&iacute;s.</p>', '<p style=\"font-size: 16px; text-align: justify;\">Ser una empresa l&iacute;der y reconocida en el mercado nacional como proveedores de materiales compuestos, generando valor y ventaja competitiva sostenible a sus clientes y colaboradores. FIBRECEN se consolida como una empresa l&iacute;der en el mercado nacional de resinas sint&eacute;ticas y fibras de vidrio, ofreciendo productos de alta calidad, a precios competitivos y con un servicio excepcional.</p>', '<p style=\"font-size: 16px; text-align: justify;\">Honestidad, Calidad, Confianza y Servicio son los pilares para fortalecer la relaci&oacute;n de nuestros clientes y sus proyectos.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_notificacion`
--

CREATE TABLE `fib_notificacion` (
  `ID` int(11) UNSIGNED NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_user2` int(11) NOT NULL,
  `nombre_envio` varchar(255) NOT NULL,
  `mensaje` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `fecha` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_opciones`
--

CREATE TABLE `fib_opciones` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `valor` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_opciones`
--

INSERT INTO `fib_opciones` (`ID`, `nom`, `descripcion`, `valor`) VALUES
(1, 'google_analytics', '', '1'),
(2, 'form_registro', '', '0'),
(3, 'geo_loc_visitas', '', '0'),
(4, 'slide_active', '', '1'),
(5, 'API_facebook', '', '0'),
(6, 'API_google_maps', '', '1'),
(7, 'api_noti_chrome', '', '0'),
(8, 'link_var', '', '0'),
(9, 'link_productos', '', '0'),
(10, 'tiny_text_des', '', '0'),
(11, 'email_test', '', '0'),
(12, 'skin_AdminLTE', '', 'blue'),
(13, 'mini_bar_AdminLTE', '', '0'),
(14, 'wordpress', '', '0'),
(15, 'bar_login', '', '0'),
(16, 'bar_productos', '', '1'),
(17, 'toogle_nombre', '', '0'),
(18, 'mostrar_precio', '', '0'),
(19, 'mostrar_nombre', '', '0'),
(20, 'mostrar_des_corta', '', '0'),
(21, 'mostrar_des', '', '1'),
(22, 'mostrar_galeria', '', '1'),
(23, 'b_vista_rapida', '', '0'),
(24, 'b_ver_pro', '', '0'),
(25, 'b_cotizar', '', '0'),
(26, 'b_cart', '', '0'),
(27, 'b_paypal', '', '0'),
(28, 'blog_coment', '', '1'),
(29, 'noticias_coment', '', '0'),
(30, 'cursos_coment', '', '0'),
(31, 'productos_coment', '', '0'),
(32, 'all_productos', '', '0'),
(33, 'e_rates', '', '0'),
(34, 'footer_dir', '', '0'),
(35, 'validacion_json', '', '0'),
(36, 'url_var_json', '', '0'),
(37, 'VUE2', '', '0'),
(38, 'act_dir2', '', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_pages`
--

CREATE TABLE `fib_pages` (
  `ID` int(6) UNSIGNED NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `tema` varchar(100) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `url` varchar(250) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_pages`
--

INSERT INTO `fib_pages` (`ID`, `titulo`, `contenido`, `tema`, `modulo`, `ext`, `url`, `fmod`, `alta`, `visible`, `activo`) VALUES
(1, 'Nosotros 1', '<p><br />&nbsp;</p>\r\n<!--/Nosotros-->\r\n<div class=\"container\">\r\n<h5>&nbsp;</h5>\r\n<h3 class=\"tittle\">Nosotros</h3>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-5 bt-bottom-info aos-init aos-animate\" data-aos=\"fade-right\">\r\n<h5>Historia</h5>\r\n<h5><span style=\"font-size: 10px;\"><br /></span></h5>\r\n<h5><span style=\"font-size: 10px;\"><br /></span></h5>\r\n</div>\r\n<div class=\"col-lg-7 bt-bottom-info aos-init aos-animate\" data-aos=\"fade-left\"><!--img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/banner1.jpg\" class=\"img-fluid\" alt=\" \"-->\r\n<p><span style=\"font-size: 16px;\"><strong>Somos una empres</strong></span><span style=\"font-size: 16px;\"><strong>a con m&aacute;s de 30 a&ntilde;os en el mercado en constante crecimiento, nuestra ubicaci&oacute;n nos permite ofrecer un servicio eficiente principalmente en la zona baj&iacute;o y actualmente nuestra marca es reconocida a nivel nacional.</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>En FIBRECEN trabajamos de manera sostenible con el fin de generar valor a nuestros clientes, proveedores y colaboradores. Por lo que nuestro equipo ha enfocado su esfuerzo en desarrollar mejoras continuas para proveer un servicio y productos de alta calidad, as&iacute; mismo actuamos con responsabilidad en los procesos para mantener una relaci&oacute;n sana y amigable con el medio ambiente.</p>\r\n<p>&nbsp;</p>\r\n<p>Entendemos las necesidades de nuestros clientes, as&iacute; que vamos m&aacute;s all&aacute; del producto, gracias a nuestra experiencia tenemos la capacidad de ofrecer y crear soluciones apropiadas enfocadas al proyecto espec&iacute;fico de cada cliente en las diversas industrias: Automotriz, Artesanal, Construcci&oacute;n, Aeron&aacute;utica, Ac&uacute;atica, Industrial, Pinturas, y m&aacute;s.</p>\r\n</div>\r\n</div>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-4 bottom-sub-grid text-center aos-init\" data-aos=\"zoom-in\">\r\n<div class=\"bt-icon\">&nbsp;</div>\r\n<h4 class=\"sub-tittle\" style=\"font-size: 24px;\">Misi&oacute;n</h4>\r\n<p style=\"font-size: 16px;\">Ser el proveedor m&aacute;s reconocido y especializado en la industria de los materiales compuestos (resinas y fibras de vidrio), ofreciendo productos de alta calidad y asesor&iacute;a a trav&eacute;s de un grupo humano comprometido a dar el mejor servicio, para contribuir al desarrollo exitoso de nuestros clientes, colaboradores y del pa&iacute;s.</p>\r\n<!--p>\r\n                        	<a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    	</p--></div>\r\n<!-- /.col-lg-4 -->\r\n<div class=\"col-lg-4 bottom-sub-grid text-center aos-init\" data-aos=\"zoom-in\">\r\n<div class=\"bt-icon\">&nbsp;</div>\r\n<h4 class=\"sub-tittle\" style=\"font-size: 24px;\">Visi&oacute;n</h4>\r\n<p style=\"font-size: 16px;\">Ser una empresa l&iacute;der y reconocida en el mercado nacional como proveedores de materiales compuestos, generando valor y ventaja competitiva sostenible a sus clientes y colaboradores. FIBRECEN se consolida como una empresa l&iacute;der en el mercado nacional de resinas sint&eacute;ticas y fibras de vidrio, ofreciendo productos de alta calidad, a precios competitivos y con un servicio excepcional.</p>\r\n<!--p>\r\n                        	<a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    	</p--></div>\r\n<!-- /.col-lg-4 -->\r\n<div class=\"col-lg-4 bottom-sub-grid text-center aos-init\" data-aos=\"zoom-in\">\r\n<div class=\"bt-icon\">&nbsp;</div>\r\n<h4 class=\"sub-tittle\" style=\"font-size: 24px;\">Valores</h4>\r\n<p style=\"font-size: 16px;\">Honestidad, Calidad, Confianza y Servicio son los pilares para fortalecer la relaci&oacute;n de nuestros clientes y sus proyectos.</p>\r\n<!--p>\r\n                        	<a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    	</p--></div>\r\n<!-- /.col-lg-4 --></div>\r\n</div>\r\n<!--//Nostotros-->', 'fibrecen', 'nosotros', '', '', '', '', 0, 0),
(2, 'Fibrecen ', '<div class=\"row inner_stat\">\r\n<div class=\"col-lg-6 stats-left-info\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 stats_left counter_grid\"><em class=\"far fa-building\"></em>\r\n<p class=\"counter\">32</p>\r\n<h4>A&ntilde;os De Experiencia</h4>\r\n</div>\r\n<div class=\"col-md-6 stats_left counter_grid1\"><em class=\"far fa-edit\" style=\"color: #ed1c24;\"></em>\r\n<p class=\"counter\" style=\"color: #ed1c24;\">3840</p>\r\n<h4 style=\"color: #ed1c24;\">Proyectos Realizados</h4>\r\n</div>\r\n<div class=\"col-md-6 stats_left counter_grid2\"><em class=\"far fa-smile\" style=\"color: #ed1c24;\"></em>\r\n<p class=\"counter\" style=\"color: #ed1c24;\">10879</p>\r\n<h4 style=\"color: #ed1c24;\">Clientes Atendidos</h4>\r\n</div>\r\n<div class=\"col-md-6 stats_left counter_grid3\"><em class=\"fas fa-users\"></em>\r\n<p class=\"counter\">43</p>\r\n<h4>Visitas</h4>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-6 stats-right-img\">&nbsp;</div>\r\n</div>\r\n<!-- //stats -->\r\n<p>&nbsp;</p>\r\n<!--//ban-->\r\n<div class=\"container\">\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-5 bt-bottom-info\" data-aos=\"fade-right\">\r\n<h5 style=\"font-size: 42px; font-weight: bold;\">&iexcl;Bienvenido!</h5>\r\n</div>\r\n<div class=\"col-lg-7 bt-bottom-info\" data-aos=\"fade-left\">\r\n<p style=\"font-size: 29px;\">Nosotros MOLDEAMOS SOLUCIONES junto contigo transformamos una idea en PROYECTOS A GRAN ESCALA.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<!--/services-->\r\n<div class=\"container\">\r\n<h3 class=\"tittle\">Crea Con Nosotros</h3>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-left\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes rigidos y piezas de fibra de vidrio</h5>\r\n<p>Crea piezas de gran tama&ntilde;o y poco detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Vaciado, encapsulados y roto moldeo</h5>\r\n<p>Crea figuras a partir de tus moldes.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-4 grid_info_main mid\" data-aos=\"flip-down\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/ab.jpg\" alt=\" \" /></div>\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-right\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes flexibles</h5>\r\n<p>Crea piezas peque&ntilde;as con gran detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Resina para fotograf&iacute;a</h5>\r\n<p>Recubre tu fotograf&iacute;a con un acabado unico.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!--//services->\r\n   <!--reviews_sec-->\r\n<h3 class=\"tittle cen\">Testimonios</h3>\r\n<div class=\"inner-sec-wthree\">\r\n<div class=\"flexslider\">\r\n<ul class=\"slides\">\r\n<li>\r\n<div class=\"testimonials_grid\">\r\n<div class=\"testimonials_grid-inn\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/testimonial_person2.jpg\" alt=\" \" width=\"120\" />\r\n<div class=\"test_social_pos\">&nbsp;</div>\r\n</div>\r\n<h3><span>- Ingeniera Civil -</span></h3>\r\n<!--i>United States</i-->\r\n<p style=\"font-size: 25px;\">\"Super recomendado, la atencion es buenisima y te ayudan con cualquier duda\"</p>\r\n</div>\r\n</li>\r\n<li>\r\n<div class=\"testimonials_grid\">\r\n<div class=\"testimonials_grid-inn\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/testimonial_person1.jpg\" alt=\" \" width=\"120\" />\r\n<div class=\"test_social_pos\">&nbsp;</div>\r\n</div>\r\n<h3><span>- Emprendedor -</span></h3>\r\n<!--i>United States</i-->\r\n<p style=\"font-size: 25px;\">\"Su curso se me hizo f&aacute;cil y muy creativo, impartidos por excelentes maestros.\"</p>\r\n</div>\r\n</li>\r\n<li>\r\n<div class=\"testimonials_grid\">\r\n<div class=\"testimonials_grid-inn\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/testimonial_person3.jpg\" alt=\" \" width=\"120\" />\r\n<div class=\"test_social_pos\">&nbsp;</div>\r\n</div>\r\n<h3><span>- Ingeniera Industrial -</span></h3>\r\n<!--i>United States</i-->\r\n<p style=\"font-size: 25px;\">\"Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda.\"</p>\r\n</div>\r\n</li>\r\n</ul>\r\n</div>\r\n</div>', 'fibrecen', 'Home', '', '', '', '', 0, 0),
(3, 'Glosario', '<p>&nbsp;</p>\r\n<!--/Glosario-->\r\n<div class=\"container\">\r\n<h3 class=\"tittle\">Glosario</h3>\r\n<div class=\"row inner-sec-wthree\"><!-- glosario URL:(modulos/glosario/glosario.json)--> <!-- glosario.json --><!--[1] -1-->\r\n<div id=\"Curado\" class=\"col-md-12\" style=\"padding-bottom: 50px;\">\r\n<h3 class=\"red-line\">Curado</h3>\r\n<p>Proceso qu&iacute;mico posterior al gelado, donde la resina se solidifica por completo gracias a la reacci&oacute;n exot&eacute;rmica provocada por adici&oacute;n de un catalizador, dando lugar a un producto que no cambia sus propiedades.</p>\r\n</div>\r\n<!-- /glosario.json --></div>\r\n</div>\r\n<!--//Glosario-->', 'fibrecen', 'glosario', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_portafolio`
--

CREATE TABLE `fib_portafolio` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` varchar(500) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_portafolio`
--

INSERT INTO `fib_portafolio` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `precio`, `cate`, `resena`, `url_page`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `visible`, `alta`, `fmod`, `user`) VALUES
(1, '', 'PISO INDUSTRIAL', '08.png', '', 'PISOS EP&Oacute;XICOS', 0.00, 'Manufactura epoxicos', '<p>REVESTIMIENTO CON EP&Oacute;XICO</p>\r\n<p>PARA TR&Aacute;FICO PESADO</p>', '', '', '', '', '', '', 1, '07/01/2018 21:10', '', 'admin'),
(2, '', 'PISO ACABADO MET&Aacute;LICO', '02.png', '', 'RESINA PARA PISOS AUTONIVELANTE', 0.00, 'Manufactura epoxicos', '<p>Descripci&oacute;n</p>\r\n<p>Acabado met&aacute;lico marmoleado</p>', '', '', '', '', '', '', 1, '03/03/2019 22:05', '', 'admin'),
(3, '', 'MOLDE DE DOMO', '3.jpg', '', 'Fabricacion de Molde y Reproducci&oacute;n de pieza bajo dise&ntilde;o. ', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '03/03/2019 22:06', '', 'admin'),
(5, '', 'TINA PARA PROCESO DE PETAR', '5.jpg', '', 'Fabricaci&oacute;n de tina para proceso de petar, para sustancias liquidas corrosivas y no corrosivas.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.&nbsp;</p>', '', '', '', '', '', '', 1, '04/03/2019 22:11', '', 'admin'),
(6, '', 'Pull trution', '6.jpg', '', 'Ensamblado de estructuras por medio de prefabricados por pull trutions, anti &aacute;cidas, para interiores, sumergibles y exteriores.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 0, '05/03/2019 22:11', '', 'admin'),
(7, '', 'FILTROS PARA PROCESOS DE CROMO', '7.jpg', '', 'Recubrimiento interior de alta resistencia qu&iacute;mica. ', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '06/03/2019 22:11', '', 'admin'),
(9, '', 'Nombre', '9.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '08/03/2019 22:11', '', 'admin'),
(10, '', 'Nombre', '10.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n. -</p>', '', '', '', '', '', '', 0, '09/03/2019 22:11', '', 'admin'),
(11, '', 'TRINCHERA PARA SISTEMA DE PETAR', '11.jpg', '', 'Recubrimiento para alta resistencia qu&iacute;mica', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n..</p>', '', '', '', '', '', '', 1, '10/03/2019 22:11', '', 'admin'),
(12, '', 'REVESTIMIENTO', '12.jpg', '', 'Resinas isoft&aacute;licas con resistencia qu&iacute;mica media y protecci&oacute;n a los rayos UV.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '11/03/2019 22:11', '', 'admin'),
(13, '', 'Nombre', '13.jpg', '', 'Descripcion.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '12/03/2019 22:11', '', 'admin'),
(14, '', 'Nombre', '14.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '13/03/2019 22:11', '', 'admin'),
(15, '', 'Nombre', '15.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '14/03/2019 22:11', '', 'admin'),
(16, '', 'Nombre', '16.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '15/03/2019 22:11', '', 'admin'),
(17, '', 'Nombre', '17.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '16/03/2019 22:11', '', 'admin'),
(18, '', 'Nombre', '18.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '17/03/2019 22:11', '', 'admin'),
(19, '', 'MOLDE ESTRUCTURAL', '19.jpg', '', 'Fabricaci&oacute;n de tanque de almacenamiento qu&iacute;mico, con 50 m&sup3; de capacidad. ', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '18/03/2019 22:11', '', 'admin'),
(20, '', ' EXTRACTORES', '20.jpg', '', 'Extractores para gases &aacute;cidos, fabricados en prfv con resinas anti&aacute;cidas de alta resistencia qu&iacute;mica. ', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '19/03/2019 22:11', '', 'admin'),
(21, '', 'Nombre', '21.jpg', '', 'Descripcion', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '20/03/2019 22:11', '', 'admin'),
(22, '', ' RECUBRIMIENTO DE TINA DE DECAPADO', '22.jpg', '', 'Revestimiento de tinas para lavado de metales y o galvanizado de aceros.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '21/03/2019 22:11', '', 'admin'),
(23, '', 'RECUBRIMIENTO  DE  PRFV ', '23.jpg', '', 'Recubrimiento anticorrosivo a piso y fabricaci&oacute;n e instalaci&oacute;n de muro de contenci&oacute;n.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '22/03/2019 22:11', '', 'admin'),
(24, '', 'LAVADORES DE AIRE', '24.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '23/03/2019 22:11', '', 'admin'),
(25, '', 'REVESTIMIENTO  ANTICORROSIVO. ', '25.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '24/03/2019 22:11', '', 'admin'),
(26, '', 'REVESTIMIENTO  ', '26.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '25/03/2019 22:11', '', 'admin'),
(27, '', 'REVESTIMIENTO  ', '27.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '26/03/2019 22:11', '', 'admin'),
(28, '', 'REVESTIMIENTO  ', '28.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', 0.00, 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '27/03/2019 22:11', '', 'admin'),
(29, '', 'ACABADO ESCAMAS', '03.png', '', 'Aplicaci&oacute;n de Piso Antiderrapante', 0.00, 'Manufactura epoxicos', '<p>Deja un acabado</p>', '', '', '', '', '', '', 1, '2019-07-12 16:28:23', '', 'admin'),
(30, '', 'PISO ACABADO M&Aacute;RMOL', '04.png', '', '', 0.00, 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 16:59:42', '', 'admin'),
(31, '', 'PISO ACABADO 3D', '05.png', '', '', 0.00, 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 17:31:16', '', 'admin'),
(32, '', 'BARRA EPOXICA ', '06.png', '', '', 0.00, 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 17:31:40', '', 'admin'),
(33, '', 'MESA CON RIO DE RESINA', '07.png', '', '', 0.00, 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 17:52:51', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos`
--

CREATE TABLE `fib_productos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `tag` varchar(500) NOT NULL,
  `marca` varchar(150) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `moneda` varchar(10) NOT NULL,
  `unidad` varchar(10) NOT NULL,
  `stock` int(6) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `ID_sub_cate` int(6) NOT NULL,
  `ID_marca` int(6) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `url_name` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` text NOT NULL,
  `land` tinyint(1) NOT NULL,
  `file` varchar(100) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos`
--

INSERT INTO `fib_productos` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `tag`, `marca`, `tipo`, `precio`, `moneda`, `unidad`, `stock`, `ID_cate`, `ID_sub_cate`, `ID_marca`, `visible`, `url_name`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `cate`, `resena`, `land`, `file`, `alta`, `fmod`, `user`) VALUES
(1, '', 'RESINA VIRGEN', 'resinavirgen.png', '', '', 'resina, resinas, virgen', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Es una resina poli&eacute;ster ortoft&aacute;lica virgen. Cuenta con las caracter&iacute;sticas de excelente humectaci&oacute;n con la fibra de vidrio, buen&nbsp;curado&nbsp;aun en secciones delgadas, buen acabado superficial, buena aceptaci&oacute;n de cargas, buen ajuste de&nbsp;viscosidad, puede ser modificado a las necesidades del usuario.</p>\r\n<p style=\"text-align: justify;\">Usos: Fabricaci&oacute;n de m&aacute;rmol sint&eacute;tico, reproducci&oacute;n de piezas y figuras s&oacute;lidas o huecas, piezas decorativas, laminados o vaciados, y piezas de fibra de vidrio automotriz.</p>', 0, '', '2019-03-03 22:27:03', '', 'admin'),
(89, 'MONOMERO', 'MON&Oacute;MERO DE ESTIRENO Y METACRILATO', 'MONOMEROS.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 17, 0, 0, 1, '', '', '', '', '', '', '17', '<h4><strong>MON&Oacute;MERO DE ESTIRENO</strong></h4>\r\n<div align=\"justify\">Hidrocarburos arom&aacute;ticos.</div>\r\n<h4>Usos</h4>\r\n<div align=\"justify\">Manufactura de pl&aacute;sticos de poliestireno, hule sint&eacute;tico a partir de butadieno, resinas, aisladores, impregnaci&oacute;n de piezas fundidas de magnesio, material fiberglass, etc.</div>\r\n<div align=\"justify\">&nbsp;</div>\r\n<div align=\"justify\"><strong>MON&Oacute;MERO METIL METACRILATO</strong></div>\r\n<div align=\"justify\">\r\n<div class=\"descripcion\">\r\n<p>Compuesto l&iacute;quido transparente que confiere brillo y transparencia a las resinas&nbsp;</p>\r\n</div>\r\n<ul class=\"content_caracteristicas\">\r\n<li>Proporciona gran estabilidad a la resina</li>\r\n<li>Proporciona dureza</li>\r\n<li>Proporciona traslucidez</li>\r\n<li>Proporciona brillo</li>\r\n</ul>\r\n</div>', 0, '', '2019-10-04 10:02:49', '', 'FIBRECEN'),
(2, '', 'RESINA VIRGEN PARA VACIADOS', 'resinavirgen.png', '', 'DESDE $99.00 / 1KG', 'resina virgen, recinas', '', '', 99.00, 'MNX', 'KG', 100, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"box-sizing: border-box; margin-bottom: 1.5em; color: rgba(0, 0, 0, 0.5); font-family: Roboto, Arial, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"color: #333399;\">Es una resina poli&eacute;ster virgen con reactividad media que permite mayor control sobre el curado, excelente aceptaci&oacute;n en la mezcla con cargas, baja contracci&oacute;n lo que ayuda a evitar el craquelado a altos espesores, buen ajuste de viscosidad. Buena aceptaci&oacute;n de fibra de vidrio.</span></p>\r\n<p style=\"box-sizing: border-box; margin-bottom: 1.5em; color: rgba(0, 0, 0, 0.5); font-family: Roboto, Arial, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"color: #333399;\">Usos: Reproducci&oacute;n de piezas y figuras s&oacute;lidas o huecas, piezas decorativas, piezas interiores como plafones, marcos y cubiertas. Para sector el&eacute;ctrico. Aplicaciones diversas en fabricaci&oacute;n de m&aacute;rmol sint&eacute;tico y laminaci&oacute;n de pl&aacute;stico reforzado con fibra de vidrio.</span></p>', 0, '', '2019-03-03 22:33:26', '', 'admin'),
(3, '', 'RESINAS DE USO GENERAL', 'RESINA-ROJA.jpg', '', '', 'resina, resinas, general', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Tambi&eacute;n conocida como de usos generales, por su versatilidad en diferentes aplicaciones. Es una resina poli&eacute;ster ortoft&aacute;lica&nbsp;pre-aclerada&nbsp;no&nbsp;tixotr&oacute;pica, de&nbsp;curado&nbsp;r&aacute;pido aun en secciones delgadas, excelente humectaci&oacute;n con la fibra de vidrio, excelente compatibilidad con cargas minerales, buen desarrollo de dureza.</p>\r\n<p>Usos: Reproducci&oacute;n de piezas y figuras s&oacute;lidas o huecas. Fabricaci&oacute;n de laminados, vaciados y piezas de fibra de vidrio, as&iacute; como piezas automotrices interiores.</p>', 0, '', '2019-03-04 20:51:35', '', 'admin'),
(4, '', 'CRISTAL PRE-ACELERADA', 'resina-trans.jpg', '', '', 'cristal, preacelerada, pre-acelerada', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Es una resina poli&eacute;ster ortoft&aacute;lica&nbsp;pre-aclerada&nbsp;con promotor, de gran transparencia y brillo siendo &eacute;sta su caracter&iacute;stica principal, de reactividad media, aceptaci&oacute;n de cargas minerales, baja contracci&oacute;n,&nbsp;viscosidad&nbsp;adecuada para eliminar el aire del producto final.</p>\r\n<p style=\"text-align: justify;\">Usos: Reproducci&oacute;n de piezas y figuras s&oacute;lidas con un encapsulado traslucido. &Uacute;til para art&iacute;culos promocionales, joyer&iacute;a y piezas artesanales o decorativas.</p>', 0, '', '2019-03-04 20:53:18', '', 'admin'),
(5, '', 'RESINA ISOFT&Aacute;LICA DE MEDIA RESISTENCIA QU&Iacute;MICA', 'isoftalica.png', '', '', 'resinas, resina isoftalica', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina isoft&aacute;lica virgen&nbsp;tixotr&oacute;pica&nbsp;de&nbsp;curado&nbsp;r&aacute;pido, alta dureza,&nbsp;resistencia qu&iacute;mica&nbsp;media, caracter&iacute;sticas anticorrosivas, buena aceptaci&oacute;n de estireno, excelente afinidad a la fibra de vidrio, buen acabado superficial. Permite ser modificada a las necesidades del usuario.</p>\r\n<p style=\"text-align: justify;\">Usos: Reproducci&oacute;n de piezas automotrices exteriores. En piezas interiores para la fabricaci&oacute;n de consolas y otros accesorios. Piezas del sector acu&aacute;tico como tanques, tinacos, albercas y canaletas.</p>', 0, '', '2019-03-04 20:55:59', '', 'admin'),
(6, '', 'RESINA DION DE ALTA RESISTENCIA QU&Iacute;MICA (ATLAC)', 'resina-amarilla012.jpg', '', '', 'resina, resina dion', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina virgen de alta&nbsp;resistencia qu&iacute;mica, excelente resistencia a la corrosi&oacute;n, afinidad con fibra de vidrio, alta dureza, control sobre&nbsp;curado, excelente&nbsp;viscosidad. A diferencia de las otras resinas, se presenta en hojuelas color &aacute;mbar y para transformarla a&nbsp;pre-aclerada&nbsp;lleva un proceso con mon&oacute;mero de estireno hasta que las hojuelas quedan totalmente diluidas. Puede ser almacenada indefinidamente sin perder sus propiedades, una vez que se transforma en&nbsp;pre-aclerada&nbsp;pierde esa cualidad. Permite ser modificada a las necesidades del usuario.</p>\r\n<p style=\"text-align: justify;\">Usos: Para la construcci&oacute;n de equipamiento qu&iacute;mico de poli&eacute;ster reforzado con fibra de vidrio incluyendo tanques de almacenamientos, reactores, tuber&iacute;as, canalizaciones, chimeneas y recubrimientos sobre hormig&oacute;n y acero. Es excelente para t&eacute;cnicas de fabricaci&oacute;n con enrollamiento filamentario, moldeo por contacto, prensado en fr&iacute;o e inyecci&oacute;n.</p>', 0, '', '2019-03-05 13:53:02', '', 'admin'),
(90, '', 'CAUCHO DE SILIC&Oacute;N P-48', '9-48.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 5, 0, 0, 1, '', '', '', '', '', '', '5', '<p><span>Hule l&iacute;quido de silic&oacute;n vulcanizable a temperatura ambiente, alta viscosidad , excelente resistencia Ideal para,&nbsp;</span><span>&nbsp;vaciados de resinas poli&eacute;ster, ceras, yesos, etc.</span></p>', 0, '', '2019-10-14 15:36:23', '', 'FIBRECEN'),
(7, '', 'RESINA FLEXIBLE PURA', 'resina-trans.jpg', '', '', 'resinas, resina flexible', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina poli&eacute;ster ortoft&aacute;lica flexible de baja reactividad, proporciona un mayor control sobre el&nbsp;curado, gran afinidad con mezcla de cargas. Se puede combinar con resinas r&iacute;gidas con el fin de evitar el craquelado y aumentar la resistencia al impacto en las piezas. Se puede modificar de acuerdo a sus necesidades.</p>\r\n<p style=\"text-align: justify;\">Usos: Fabricaci&oacute;n de molduras con capacidad de soportar ser deformadas en su uso</p>', 0, '', '2019-03-05 13:54:22', '', 'admin'),
(8, '', 'RESINA PARA TERMOFORMADO', 'resina-verde.jpg', '', '', 'resinas, resina termoformado', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina poli&eacute;ster&nbsp;pre-aclerada,&nbsp;tixotr&oacute;pica&nbsp;y de baja reactividad, alta adherencia al acr&iacute;lico, buena aceptaci&oacute;n en la mezcla con cargas minerales, reacci&oacute;n&nbsp;exot&eacute;rmica&nbsp;moderada, buena impregnaci&oacute;n de la fibra de vidrio,&nbsp;pre-aclerado&nbsp;y&nbsp;tixotr&oacute;pico, Debido a su&nbsp;exotermia&nbsp;controlada y a sus componentes de formulaci&oacute;n, no da&ntilde;a al acr&iacute;lico.</p>\r\n<p style=\"text-align: justify;\">Uso: En la industria de construcci&oacute;n de muebles de ba&ntilde;o, y fabricaci&oacute;n de art&iacute;culos de acr&iacute;lico termoformado.</p>', 0, '', '2019-03-05 13:55:34', '', 'admin'),
(9, '', 'RESINA PARA TIROL', 'resina-blanca.jpg', '', '', 'resinas, resina tirol', '', '', 0.00, 'MNX', 'PZ', 0, 1, 0, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Mejora la consistencia e incrementa la adherencia de las pastas para Tirol, facilita el planchado y aplanados y evita posibles desprendimientos</p>\r\n<p style=\"text-align: justify;\">Usos: Fabricaci&oacute;n de revoques para pared. Pegamento de papel, cart&oacute;n, madera, etc. sellador de paredes porosas y aprestos textiles.</p>', 0, '', '2019-03-05 13:56:40', '', 'admin'),
(10, '', 'COLCHONETA', 'colchoneta012.jpg', '', '', 'colchoneta, fibra de vidrio', '', '', 0.00, 'MNX', 'PZ', 0, 2, 0, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es un Mat de hilos cortados, distribuidos multidireccionalmente en un solo plano. Para soporte de Gel coat, Resinas Poli&eacute;ster, Ep&oacute;xicas y Vinil&eacute;ster. Cuenta con una r&aacute;pida impregnaci&oacute;n, uniformidad de espesor y alta resistencia a la tensi&oacute;n. Es multicompatible e id&oacute;neo como refuerzo para laminados y aplicaciones de moldeo abierto. F&aacute;cil de manipular y cortar.</p>\r\n<p style=\"text-align: justify;\">Existen diferentes pesos est&aacute;ndar siendo el m&aacute;s com&uacute;n de 450g/m2</p>', 0, '', '2019-03-05 15:31:27', '', 'admin'),
(11, '', 'PETATILLO', 'petatillo.jpg', '', '', 'petatillo, fibra de vidrio', '', '', 0.00, 'MNX', 'PZ', 0, 2, 0, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es un material de refuerzo que se fabrica tejiendo un patr&oacute;n de Roving en forma de cuadr&iacute;cula, formando una tela de construcci&oacute;n pesada dise&ntilde;ada para reforzar laminados est&aacute;ndar en aplicaciones de moldeo manual. Es compatible con resinas poli&eacute;ster, ep&oacute;xicas y vinil&eacute;ster. Se utiliza principalmente para moldeo en las que se requiere alto contenido de vidrio y principalmente para dar alta&nbsp;resistencia mec&aacute;nica. Es de r&aacute;pida humectaci&oacute;n, compatible con sistemas de resina con o sin carga. Da como resultado una superficie atractiva en apariencia en las piezas finales.</p>\r\n<p style=\"text-align: justify;\">Pesos est&aacute;ndar: 600 g/m2 y 800 g/m2, siendo el m&aacute;s com&uacute;n el de 800g/m2</p>', 0, '', '2019-03-05 15:32:49', '', 'admin'),
(12, '', 'FILAMENTO CORTADO', 'filamentocortado01.png', '', '', 'Filamento Recortado - Fibra de Vidrio', '', '', 0.00, 'MNX', 'PZ', 0, 2, 0, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">El filamento cortado est&aacute; formado por hilos continuos a una longitud espec&iacute;fica, est&aacute;n dise&ntilde;ados para usarse en compuestos moldeables de resinas poli&eacute;ster, ep&oacute;xicas y para mezclado en seco en aplicaciones con resinas fen&oacute;licas.</p>\r\n<p style=\"text-align: justify;\">Contiene excelente pigmentaci&oacute;n, excelentes propiedades de laminado.</p>', 0, '', '2019-03-05 15:47:24', '', 'admin'),
(13, '', 'TELA MARINA', 'TELAMARINA.png', '', '', 'tela marina, fibra de vidrio', '', '', 0.00, 'MNX', 'PZ', 0, 2, 0, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es una tela fabricada 100% con hilo de fibra de vidrio, usada especialmente para aplicaciones donde se requiere buena resistencia qu&iacute;mica al contacto del laminado con alg&uacute;n agente corrosivo.</p>\r\n<p style=\"text-align: justify;\">Es de bajo gramaje lo que beneficia en la reducci&oacute;n de peso del producto final, es ahorrador en costos de producci&oacute;n ya que hay menor consumo de resina-fibra, y de f&aacute;cil acomodo en curvaturas peque&ntilde;as o en geometr&iacute;as complejas.</p>\r\n<p style=\"text-align: justify;\">Se puede utilizar en laminados interiores como barrera anticorrosiva o que est&eacute;n expuestos al intemperie. Se aplica manualmente o con equipos de aspersi&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Proporciona una resistencia qu&iacute;mica de 575&deg;C con acabado blanco.</p>', 0, '', '2019-03-05 15:48:17', '', 'admin'),
(14, '', 'FIBRA DE CARBONO', 'fibradecarbon.png', '', '', 'fibra de carbono, fibra de vidrio', '', '', 0.00, 'MNX', 'PZ', 0, 2, 0, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\"><span>Materia de refuerzo tejido en forma de cuadr&iacute;cula color negro. Se utiliza principalmente para altos requerimientos para incrementar la resistencia estructural sin a&ntilde;adirle un peso significativo al producto final. Aporta alta&nbsp;</span>resistencia mec&aacute;nica<span>, resistividad el&eacute;ctrica, tambi&eacute;n resiste al ataque de &aacute;cidos y solventes org&aacute;nicos. Otra caracter&iacute;stica principal es que se utiliza para alta conductividad t&eacute;rmica, ya que es capaz de soportar hasta 1500&deg;C sin perder sus caracter&iacute;sticas. Se utiliza principalmente en la industria aeron&aacute;utica como exteriores de aviones, partes de sat&eacute;lites; en la industria de construcci&oacute;n, qu&iacute;mica y petrolera; en la industria deportiva como pesca, para palos de golf, raquetas, lanchas, estructuras de bicicletas. Entre otros.</span></p>', 0, '', '2019-03-05 15:49:42', '', 'admin'),
(15, '', 'ROVING DE ASPERSI&Oacute;N Y DE POLTRUSI&Oacute;N', 'ROVING.png', '', '', 'roving, fibra de vidrio', '', '', 0.00, 'MNX', 'PZ', 0, 2, 0, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Filamentos de longitud indefinida puestos en forma pr&aacute;cticamente paralela, formando una mecha sin torsi&oacute;n mec&aacute;nica. Compatible con sistemas de resina con o sin carga.</p>\r\n<p style=\"text-align: justify;\">Contiene una excelente impregnaci&oacute;n, facilidad de corte, facilidad de desembobinado. En laminados con resinas poli&eacute;ster da excelente translucidez en los paneles. Ahorro en el mantenimiento de cortadores y en el consumo de resina.</p>', 0, '', '2019-03-05 15:50:42', '', 'admin'),
(16, '', 'TRANSPARENTE / BLANCO / NEGRO', 'gelcoat003.png', '', '', 'transparente, blanco, negro', '', '', 0.00, 'MNX', 'PZ', 0, 3, 0, 0, 1, '', '', '', '', '', '', '3', '<p style=\"text-align: justify;\">Compuesto de resina poli&eacute;ster ortoft&aacute;lico y&nbsp;tixotr&oacute;pico&nbsp;con dos finalidades, ayuda a darle un acabado liso a la pieza final y proteger la misma de agresiones exteriores e impactos. Al igual que la resina, tiene la posibilidad de agregarle pigmentos y requiere de un catalizador para el&nbsp;gelado. Puede aplicarse con brocha o con pistola.</p>\r\n<p style=\"text-align: justify;\">Para pigmentar gel coat transparente se recomienda el siguiente porcentaje: 10% para colores azul, rojo, negro y verde primavera. 20% para colores amarillo, verde pasto, blanco, naranja, caf&eacute; y gris.</p>\r\n<p style=\"text-align: justify;\">Uso: Para la fabricaci&oacute;n de piezas desde artesanales hasta automotrices y proceso de laminaci&oacute;n</p>', 0, '', '2019-03-05 16:09:25', '', 'admin'),
(17, '', 'TOOLING', 'TOOLING01.png', '', '', 'gel coat', '', '', 0.00, 'MNX', 'PZ', 0, 3, 0, 0, 1, '', '', '', '', '', '', '3', '<p>Compuesto de resina poli&eacute;ster ortoft&aacute;lica&nbsp;pre-acelerado, gran retenci&oacute;n de brillo, su principal caracter&iacute;stica es su excelente nivel&nbsp;tixotr&oacute;pico, de alta&nbsp;resistencia qu&iacute;mica.</p>\r\n<p style=\"text-align: justify;\">Usos: Para la fabricaci&oacute;n de diversos moldes, desde artesanal hasta automotriz. Para proceso de laminaci&oacute;n.</p>', 0, '', '2019-03-05 16:12:04', '', 'admin'),
(18, '', 'ISOFT&Aacute;LICO', 'gelcoat_clarito_png.png', '', '', 'isoftalico', '', '', 0.00, 'MNX', 'PZ', 0, 3, 0, 0, 1, '', '', '', '', '', '', '3', '<p style=\"text-align: justify;\">Compuesto de resina Isoft&aacute;lica&nbsp;pre-acelerado, alta&nbsp;resistencia qu&iacute;mica, adecuado nivel de&nbsp;viscosidad&nbsp;y&nbsp;tixotrop&iacute;a,&nbsp;curado&nbsp;r&aacute;pido. Resistencia a intemperie y corrosi&oacute;n. Apto para la aplicaci&oacute;n manual o aspersi&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Uso: Para piezas automotrices y acu&aacute;ticas, moldeo en general y fabricaci&oacute;n de tanques de almacenamiento. Para piezas donde se necesite protecci&oacute;n contra la corrosi&oacute;n.</p>', 0, '', '2019-03-05 16:13:39', '', 'admin'),
(19, '', 'TRANSPARENTE Y BLANCO', 'PEGAMENTOTRANSPARENTEBLANCO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 4, 0, 0, 1, '', '', '', '', '', '', '4', '<p style=\"text-align: justify;\">Es un adherente para superficies, principalmente de m&aacute;rmol.</p>\r\n<p style=\"text-align: justify;\">Resina poli&eacute;ster de&nbsp;gelado&nbsp;r&aacute;pido, da excelente brillo y adhesi&oacute;n sobre diferentes superficies, se puede limpiar con solvente y volver a pulir para obtener su brillo nuevamente. Color blanco y transparente. Para el&nbsp;curado&nbsp;debe aplicarse catalizador del 1 al 2%.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;gelado&nbsp;aprox. 5&plusmn;2min. Tiempo de&nbsp;curado&nbsp;10&plusmn;4min.</p>', 0, '', '2019-03-05 16:34:37', '', 'admin'),
(20, '', 'MASILLA MARFIL', 'resina-del-centro.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 18, 0, 0, 1, '', '', '', '', '', '', '18', '<p style=\"text-align: justify;\">Pasta s&oacute;lida que funciona como pegamento para m&aacute;rmol, granito y piedra. Requiere de per&oacute;xido de benzoilo como catalizador para su&nbsp;curado, aplicar del 2 al 3% lo que dar&aacute; un tiempo de&nbsp;curado&nbsp;de 7 a 10 min.</p>\r\n<p style=\"text-align: justify;\">En presentaci&oacute;n marfil, transparente y neutra.</p>', 0, '', '2019-03-05 16:38:25', '', 'admin'),
(21, '', 'BUTANOX M-50', 'BUTANOX.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\">Es utilizado para el&nbsp;curado&nbsp;a temperatura ambiente de resinas poli&eacute;ster y gel-coats, es l&iacute;quido e incoloro, r&aacute;pido tiempo de&nbsp;gelado. Alta reactividad, concentraci&oacute;n y reacci&oacute;n&nbsp;exot&eacute;rmica. Gran productividad por molde. Ayuda a catalizar en menor tiempo en condiciones de humedad.</p>\r\n<p style=\"text-align: justify;\">Es un catalizador de buena pureza y estabilidad.</p>', 0, '', '2019-03-05 16:40:21', '', 'admin'),
(22, '', 'TRIGONOX', 'TRIGONOX.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\">Excelente&nbsp;curado&nbsp;en resinas poli&eacute;ster y gel-coats, excelente tiempo de&nbsp;gelado, de alta reactividad y alta concentraci&oacute;n, sin embargo no eleva la reacci&oacute;n&nbsp;exot&eacute;rmica, evitando la contracci&oacute;n del producto final durante el proceso de&nbsp;polimerizaci&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Es un catalizador de excelente pureza ayudando a una mejor estabilidad.</p>', 0, '', '2019-03-05 16:42:10', '', 'admin'),
(23, '', 'TP', 'CATALIZADOR-TP.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\"><span>Es un catalizador mezclado con alcoxisilianos y se tu utiliza &uacute;nicamente para el inicio del proceso de&nbsp;</span>polimerizaci&oacute;n<span>&nbsp;de silicones. Proporciona excelente tiempo de&nbsp;</span>curado<span>&nbsp;as&iacute; como uniformidad en el mismo.</span></p>', 0, '', '2019-03-05 16:44:22', '', 'admin'),
(24, '', 'PER&Oacute;XIDO DE BENZOILO', 'PEROXIDODEBENZOILO.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\"><span>Catalizador en pasta suave que se utiliza principalmente para pastas resanadoras automotriz y de uso marino, as&iacute; como para masilla para m&aacute;rmol. En presentaci&oacute;n de color blanco, rojo y azul.</span></p>', 0, '', '2019-03-05 16:47:13', '', 'admin'),
(25, '', 'CAUCHO DE SILIC&Oacute;N P-53', 'P-53.jpg', '', '', 'silicones, silicon', '', '', 0.00, 'MNX', 'PZ', 0, 5, 0, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">Liquido blanco y viscoso que permite la reproducci&oacute;n de piezas con detalle fino a regular. No requiere de diluyente para trabajarlo, sin embargo se le puede a&ntilde;adir si as&iacute; se desea, aunque en exceso puede afectar la resistencia del mismo. Admite el vaciado de diversos materiales como: resinas, yeso, cera, espuma de poliuretano, cuenta con alta&nbsp;resistencia qu&iacute;mica, al igual que resiste a altas temperaturas de hasta 220&deg;C (no exceder los 220&deg;C). Se cataliza del 2 al 3% con Catalizador TP. Se puede aplicar con brocha, esp&aacute;tula o manualmente. Debe dejarse madurar 24hrs. antes del primer vaciado. Para alargar la vida del molde, ayudarlo a bajar la temperatura y a darle estabilidad hay aplicar lubricante para silic&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;gelado&nbsp;aprox. 10&plusmn;5min. Tiempo de vulcanizado 18&plusmn;5min.</p>', 0, '', '2019-03-05 16:48:17', '', 'admin'),
(26, '', 'CAUCHO DE SILIC&Oacute;N P-85', 'p-85.jpg', '', '', 'silicones, silicon', '', '', 0.00, 'MNX', 'PZ', 0, 5, 0, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">L&iacute;quido blanco y viscoso cuenta con excelente flexibilidad y permite fabricar moldes que requieran una reproducci&oacute;n con detalles muy finos. No necesita a&ntilde;adirle diluyente. Resistente a desgarres y alargamientos. Admite el vaciado de diversos materiales como: resinas, yeso, cera, espuma de poliuretano, cuenta con alta&nbsp;resistencia qu&iacute;mica, al igual que resiste a altas temperaturas de hasta 220&deg;C (no exceder los 220&deg;C). Se cataliza del 3 al 4% con Catalizador TP. Se puede aplicar con brocha, esp&aacute;tula o manualmente. Debe dejarse madurar 24hrs. antes del primer vaciado. Para alargar la vida del molde, ayudarlo a bajar la temperatura y a darle estabilidad aplicar lubricante para silic&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;gelado&nbsp;aprox. 80&plusmn;20min. Tiempo de vulcanizado 110&plusmn;30min.</p>', 0, '', '2019-03-05 17:16:51', '', 'admin'),
(27, '', 'LATEX', 'LATEX.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 5, 0, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">Es utilizado para moldear artesan&iacute;as en yeso, y como pegamento para unir materiales naturales o sint&eacute;ticos. De fuerte olor a amoniaco. Especial para vaciado en fr&iacute;o. Se puede aplicar con brocha, rodillo o por sumersi&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;curado&nbsp;depende de la cantidad aplicada. De 3 a 10min.</p>', 0, '', '2019-03-05 17:19:03', '', 'admin'),
(28, '', 'MOLDUFLEX', 'MOLDUFLEX.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 5, 0, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">Silic&oacute;n viscoso de color gris&aacute;ceo, que permite reproducir piezas de poco detalle. Requiere de diluyente para ajustar su&nbsp;viscosidad. Resiste a altas temperaturas de hasta 180&deg;C (no exceder los 180&deg;C). Buen rendimiento y para el vulcanizado se cataliza al 3% con catalizador TP.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;gelado&nbsp;aprox. 4min. Tiempo de vulcanizado 8&plusmn;3min.</p>', 0, '', '2019-03-05 17:20:18', '', 'admin'),
(29, '', 'DIMETIL ANILINA', 'dimetil03.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 7, 0, 0, 1, '', '', '', '', '', '', '7', '<p style=\"text-align: justify;\">Es un acelerador para resina poli&eacute;ster, da una leve coloraci&oacute;n amarilla clara a resina, alta reacci&oacute;n&nbsp;exot&eacute;rmica.</p>\r\n<p style=\"text-align: justify;\">Se recomienda una o dos gotas cada 100gr. de resina. El uso inadecuado puede craquelar el producto final.</p>', 0, '', '2019-03-10 20:34:13', '', 'admin'),
(30, '', 'COBALTO', 'COBALTO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 7, 0, 0, 1, '', '', '', '', '', '', '7', '<p style=\"text-align: justify;\">Es un acelerador de Carboxilato de cobalto diluido en solventes, de fuerte olor a solvente, color violeta. Aplicado a las resinas poli&eacute;ster tiende a darles un leve tono violeta.</p>\r\n<p style=\"text-align: justify;\">Se recomienda aplicar de 5 a 10 gotas por cada 100gr. de resina o al 5%, dependiendo sus necesidades.</p>', 0, '', '2019-03-10 20:53:57', '', 'admin'),
(31, '', 'PROMOTOR NCV-3', 'PROMOTOR.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 7, 0, 0, 1, '', '', '', '', '', '', '7', '<p style=\"text-align: justify;\"><span>Es un acelerador a base de cobalto que descompone r&aacute;pidamente los per&oacute;xidos a temperatura ambiente, da un resultado de&nbsp;</span>gelado<span>&nbsp;y&nbsp;</span>curado<span>&nbsp;r&aacute;pido y uniforme. No da coloraci&oacute;n lo que ayuda a mantener la transparencia de la resina. Ideal para resina cristal.</span></p>', 0, '', '2019-03-10 20:55:48', '', 'admin'),
(32, '', 'CAUCHO DE SILIC&Oacute;N P-21', 'P - 21.jpg', '', '', 'silicones, caucho de silicon', '', '', 0.00, 'MNX', 'PZ', 0, 5, 0, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">L&iacute;quido blanco y viscoso que permite la fabricaci&oacute;n de moldes con mayor flexibilidad y que requiera una reproducci&oacute;n con detalles finos. No necesita a&ntilde;adirle diluyente. Resistente a desgarres y alargamientos. Admite el vaciado de diversos materiales como: resinas, yeso, cera, espuma de poliuretano, cuenta con alta&nbsp;resistencia qu&iacute;mica, al igual que resiste a altas temperaturas de hasta 220&deg;C (no exceder los 220&deg;C). Se cataliza del 4% con Catalizador TP. Se puede aplicar con brocha, esp&aacute;tula o manualmente. Debe dejarse madurar 24hrs. antes del primer vaciado. Para alargar la vida del molde, ayudarlo a bajar la temperatura y a darle estabilidad, hay aplicar lubricante para silic&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;gelado&nbsp;aprox. 90&plusmn;20min. Tiempo de vulcanizado 110&plusmn;30min.</p>', 0, '', '2019-03-10 21:02:14', '', 'admin'),
(33, '', 'DESMOLDANTE CP-500', 'CP500.png', '', '', 'desmoldante', '', '', 0.00, 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\">Presentaci&oacute;n en pasta de f&aacute;cil aplicaci&oacute;n manual, secado r&aacute;pido, buena fluidez, da excelente acabado y brillo a moldes, econ&oacute;mico.</p>\r\n<p style=\"text-align: justify;\">Dise&ntilde;ada para lograr un excelente desprendimiento en art&iacute;culos de resina, fibra de vidrio, y en moldes de diversos materiales como vidrio, madera, yeso, etc.</p>', 0, '', '2019-03-10 21:03:09', '', 'admin'),
(34, '', 'CERA D OPORTO', 'deoporto.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\"><span>Pasta formulada con ceras vegetales y minerales, de f&aacute;cil aplicaci&oacute;n manual, de mayor rendimiento, m&aacute;s de 10 moldeos por aplicaci&oacute;n, de r&aacute;pido secado, f&aacute;cil de pulir, f&aacute;cil desmolde en caucho de silic&oacute;n, art&iacute;culos de resina y fibra de vidrio.</span></p>', 0, '', '2019-03-10 21:04:16', '', 'admin'),
(35, '', 'PELICULA SEPARADORA', 'desmoldante.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\"><span>L&iacute;quido azuloso que forma una pel&iacute;cula para evitar contacto entre el molde y la resina. Da acabado brilloso a la pieza final, es de f&aacute;cil aplicaci&oacute;n y limpieza, r&aacute;pida evaporaci&oacute;n, soluble en agua, usar despu&eacute;s de haber aplicado la cera desmoldante CP-500. Puede aplicarse por aspersi&oacute;n o manualmente.</span></p>', 0, '', '2019-03-10 21:05:02', '', 'admin'),
(36, '', 'LUBRICANTE PARA SILIC&Oacute;N', 'LUBRICIL.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\">Lubricante en aerosol de diversas aplicaciones. Funciona como antiadherente para moldes, ayuda a lubricar piezas en constante fricci&oacute;n. Funciona como abrillantado de muebles y superficies con laca. Limpia y lubrica componentes el&eacute;ctricos. Da mayor vida a los moldes de silic&oacute;n de caucho, pl&aacute;stico, metal, etc. resistiendo altas temperaturas. Con una sola capa es sufriente.</p>\r\n<p style=\"text-align: justify;\">Debe dejarse actuar por 1 minuto.</p>', 0, '', '2019-03-10 21:05:46', '', 'admin'),
(37, '', 'TR CERA', 'TRCERA01.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\">Cera desmoldante ideal para aplicaciones aceleradas por calor. Debe dejarse secar antes de quitarlo. &Eacute;sta cera contiene carnauba (cera que se obtiene de las hojas de la palma Copernicia prunifera de grandes propiedades), lo que ayuda a que se acumule menos desmoldante.</p>\r\n<p style=\"text-align: justify;\">Especialmente para altas temperaturas.</p>', 0, '', '2019-03-10 21:06:30', '', 'admin'),
(38, '', 'FREKOTE WOLO', 'frekotewolo.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\"><span>L&iacute;quido desmoldante polim&eacute;rico de r&aacute;pido&nbsp;</span>curado<span>, para m&uacute;ltiples desmoldes en cualquier tipo de resina poli&eacute;ster. De f&aacute;cil y r&aacute;pida aplicaci&oacute;n, se debe frotar y dejar evaporar, quedando una capa delgada y transparente que no requiere pulirse y es de bajo acomulamiento. Ayuda a reducir gastos de mantenimiento.</span></p>', 0, '', '2019-03-10 21:07:09', '', 'admin'),
(39, '', 'TALCO INDUSTRIAL', 'TALCOINSDUSTRIAL.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\">El talco es un mineral blanco que incrementa las propiedades mec&aacute;nicas de los laminados y tiende a incrementar la&nbsp;viscosidad&nbsp;de las resinas poli&eacute;ster. Aporta tonalidad gris aperlado.</p>\r\n<p style=\"text-align: justify;\">Uso: Se puede utilizar en el yeso. Por su resistencia a elevadas temperaturas se utiliza en la fabricaci&oacute;n de materiales termoresistentes.</p>', 0, '', '2019-03-10 21:08:14', '', 'admin'),
(40, '', 'CALCITA (CARBONATO DE CALCIO)', 'CALCITA.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Polvo blanco altamente puro que se homog&eacute;nea perfectamente con la resina, en el&nbsp;</span>curado<span>&nbsp;da un color hueso a la resina y le aumenta peso al producto final. Utilizada en la industria qu&iacute;mica y para la fabricaci&oacute;n de pintura, pigmentos, papel, pl&aacute;stico, en el &aacute;rea alimenticia e industrial.</span></p>', 0, '', '2019-03-10 21:08:51', '', 'admin'),
(41, '', 'BIOXIDO DE TITANIO', 'BIOXIDODETITANIO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\">Polvo blanco de f&aacute;cil integraci&oacute;n con otras cargas para obtener un mejor acabado. Generalmente lo utilizan para darle tono blanco la resina.</p>\r\n<p style=\"text-align: justify;\">Se utiliza para la fabricaci&oacute;n de piezas artesanales, medicamentos y cosm&eacute;ticos.</p>', 0, '', '2019-03-10 21:10:37', '', 'admin'),
(42, '', 'AL&Uacute;MINA TRIHIDRATADA', 'ALUMINAHIDRATADA.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Polvo blanco extra fino, en el&nbsp;</span>curado<span>&nbsp;da una tonalidad ligeramente rosada y opaca. Mezclado con la resina se utiliza principalmente como retardante a la flama. Se recomienda aplicar del 30 al 60%.</span></p>', 0, '', '2019-03-10 21:11:21', '', 'admin'),
(43, '', 'BLANCO DE ESPA&Ntilde;A', 'BLANCODEESP.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Es un sulfato de calcio Dihidratado en presentaci&oacute;n de polvo blanco fino y homog&eacute;neo que se utiliza para diversas industrias y aplicaciones, como resanado de techos y paredes tipo pasta, trabajos de carpinter&iacute;a, carga en pinturas, preparaci&oacute;n de tirol, entre otros.</span></p>', 0, '', '2019-03-10 21:12:25', '', 'admin'),
(44, '', 'DICALITE', 'DICALITE-1.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Es un polvo blanco espeso que se utiliza principalmente para darle cuerpo a la resina, ayuda a que la pieza final no quede traslucida. Proporciona propiedades mec&aacute;nicas a la resina. Tiene la cualidad de no precipitarse en corto tiempo.</span></p>', 0, '', '2019-03-10 21:13:13', '', 'admin'),
(45, '', 'MICROESFERA', 'MICROESFERA.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\">Carga extra ligera formada por peque&ntilde;as esferas de vidrio, que ayuda a dar propiedades de aislamiento y ayudan a dar volumen a la resina. Es de&nbsp;viscosidad&nbsp;alta lo que no la hace apta para vaciados que requieran de fluidez.</p>\r\n<p style=\"text-align: justify;\">Se recomienda agregar un 10% en relaci&oacute;n a la resina. Var&iacute;a el porcentaje dependiendo del acabado o la necesidad que se tenga para el producto final.</p>', 0, '', '2019-03-10 21:14:01', '', 'admin'),
(46, '', 'PIGMENTOS L&Iacute;QUIDOS', 'PIGMENTOSliquidos.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p style=\"text-align: justify;\">Desarrollados para pigmentar la resina cristal, no le quita su propiedad traslucida.</p>\r\n<p style=\"text-align: justify;\">Existencia de colores: Amarillo, verde, violeta, azul y rojo.</p>', 0, '', '2019-03-11 08:48:28', '', 'admin'),
(47, '', 'PIGMENTOS EN PASTA', 'pigmentossolidos_.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p style=\"text-align: justify;\">Desarrollado para pigmentar gel coat, todo tipo de resinas y pegamentos. En resina cristal el producto final queda de color s&oacute;lido, no traslucido.</p>\r\n<p style=\"text-align: justify;\">Existencia de colores: blanco, negro, verde pasto, verde primavera, rojo, amarillo cromo, naranja, azul y caf&eacute;.</p>', 0, '', '2019-03-11 08:49:45', '', 'admin'),
(48, '', ' PIGMENTO FOTOLUMINICENTE', 'FOTOLUMINISCENTES.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p style=\"text-align: justify;\">Pigmento en polvo con la caracter&iacute;stica principal de absorber, almacenar y emitir luz con largo tiempo de activaci&oacute;n, hasta 12 hrs en la obscuridad. Libre de sustancias peligrosas o radioactivas, por lo que lo hace seguro en aplicaciones como juguetes, ropa, calzado, relojes, decoraci&oacute;n, etc.</p>', 0, '', '2019-03-11 08:51:38', '', 'admin'),
(49, '', 'MBI (METAL BULDING INSULATION)', 'ailantembi.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 16, 0, 0, 1, '', '', '', '', '', '', '16', '<p style=\"text-align: justify;\">Rollo de fibra de vidrio aislante fabricado con fibra de vidrio aglutinada con resinas termo- fijas puede ser utilizado como aislamiento t&eacute;rmico o ac&uacute;stico, con barrera de vapor en una de sus caras.</p>\r\n<p style=\"text-align: justify;\">Ideal para recubrimiento de muros o techos de naves industriales y comerciales, hangares, centros comerciales, bodegas, colegios, etc.</p>\r\n<p style=\"text-align: justify;\">Las medidas de este rollo son de 15.24m o 30.48m de largo x 1.22m de ancho y los hay en espesores de 2\", 3\", 3.5\" y 4\".</p>', 0, '', '2019-03-11 08:56:37', '', 'admin'),
(50, '', 'TRS 10', 'aislanteTRS.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 16, 0, 0, 1, '', '', '', '', '', '', '16', '<p style=\"text-align: justify;\">Se presenta en rollos y placas flexibles en color blanco y se utiliza como aislamiento para hornos de estufas est&aacute;ndar, calentadores de agua, calentadores solares, chimeneas, hornos comerciales, etc. Se puede utilizar hasta en aplicaciones de 538&deg;C</p>\r\n<p style=\"text-align: justify;\">Las dimensiones de los rollos son de 15.24m de largo x .61m de ancho y de 1\" o 2\" de espesor.</p>', 0, '', '2019-03-11 09:00:28', '', 'admin'),
(51, '', 'RF3000 (RF3075, RF3100, RF3150 Y RF3200)', 'rf3000.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 16, 0, 0, 1, '', '', '', '', '', '', '16', '<p style=\"text-align: justify;\">Se utiliza principalmente en el exterior de ductos de aire acondicionado y calefacci&oacute;n soportando hasta 232&deg;C.</p>\r\n<p style=\"text-align: justify;\">Los hay sin barrera de vapor o con barrera de vapor (foil de aluminio o aluminio reforzado)</p>\r\n<p style=\"text-align: justify;\">Se encuentra en espesores de 1\", 1.5\" &oacute; 2.5\" x 15.24m de largo y .61m de ancho para el foil de aluminio. Y de 1.22m ancho para el aluminio reforzado.</p>', 0, '', '2019-03-11 09:23:40', '', 'admin'),
(52, '', 'AISLHOGAR', 'ailanteAISLHOGAR.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 16, 0, 0, 1, '', '', '', '', '', '', '16', '<p style=\"text-align: justify;\">Aislamiento termo-ac&uacute;stico fabricado con fibra de vidrio de baja densidad, aglutinada con resina fen&oacute;lica de fraguado t&eacute;rmico.</p>\r\n<p style=\"text-align: justify;\">Se recomienda como aislante t&eacute;rmico y ac&uacute;stico en el ramo de la construcci&oacute;n, en usos como interior de muros y canceles divisorios, sobre falsos plafones y como absorbentes de sonido, ayudando a crear un ambiente m&aacute;s silencioso y c&oacute;modo.</p>\r\n<p style=\"text-align: justify;\">Los hay en rollos desde 2\" hasta 5\" de espesor x 15.24m de largo x .61m de ancho.</p>', 0, '', '2019-03-11 09:25:10', '', 'admin'),
(53, '', 'FIBERGLAS', 'FIBERGLASS.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 16, 0, 0, 1, '', '', '', '', '', '', '16', '<p style=\"text-align: justify;\">Es un aislante para tuber&iacute;a, preformado con corte longitudinal abisagrado y puede ser con barrera de vapor. Se produce con fibras minerales de vidrio aglutinado con resinas especiales para emplearse en tuber&iacute;as que operen en un rango de temperatura de -18&deg;C hasta 454&deg;C.</p>\r\n<p style=\"text-align: justify;\">Son ideales para tuber&iacute;as de proceso y servicio que conducen vapor, agua caliente, agua helada, refrigerantes, gases y toda clase de fluidos en que se requiere ahorrar energ&iacute;a.</p>\r\n<p style=\"text-align: justify;\">Se presenta en abisagrados de 36\" de largo (91.4m), con espesores m&iacute;nimos de 1\" y di&aacute;metro m&iacute;nimo de &frac12;\".</p>', 0, '', '2019-03-11 09:26:17', '', 'admin'),
(54, '', 'PLACAS SERIE 700', 'placas.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 16, 0, 0, 1, '', '', '', '', '', '', '16', '<p style=\"text-align: justify;\">Aislamiento termo-ac&uacute;stico fabricado con fibra de vidrio aglutinada con resinas fen&oacute;licas de fraguado t&eacute;rmico para soportar temperaturas de hasta 232&deg;C. Se presentan en forma de placas flexibles, semir&iacute;gidas y r&iacute;gidas.</p>\r\n<p style=\"text-align: justify;\">Serie 700 Fiberglas&reg; se recomienda para el aislamiento t&eacute;rmico de equipos industriales, tales como calderas, hornos, tanques, reactores y equipos de proceso. Tiene un excelente desempe&ntilde;o de absorci&oacute;n de ac&uacute;stica en cines, teatros, auditorios, estudios de grabaci&oacute;n, etc.</p>', 0, '', '2019-03-11 09:27:24', '', 'admin'),
(55, '', 'ESPUMA DE POLIURETANO PARA VACIADOS', 'ESPUMADEPOLIURETANO.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Son dos componentes l&iacute;quidos que al mezclarlos en partes iguales dan como resultado un poliuretano s&oacute;lido poroso. Debe mezclarse en partes iguales (A Y B) para que inicie la reacci&oacute;n y as&iacute; comience a expandir. Resiste a envejecimiento, condiciones clim&aacute;ticas y solventes. Tiempo de&nbsp;curado&nbsp;aproximadamente de 2 a 3 minutos a temperatura ambiente. Un kilo abarca un aproximado de 1 m<span>2</span>&nbsp;a 1 pulgada de espesor.</p>\r\n<p style=\"text-align: justify;\">Se utiliza para el aislamiento t&eacute;rmico en refrigeradores, termos peque&ntilde;os o carritos de paletas. Se pueden aplicar manualmente o por aspersi&oacute;n en m&aacute;quinas especiales.</p>', 0, '', '2019-03-11 09:28:54', '', 'admin'),
(56, '', 'PRESENTACI&Oacute;N EN SPRAY', 'espuma-de-poliuretano-400ml-selladora-y-aislante-D_NQ_NP_725903-MLM29934684941_042019-F.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Espuma de poliuretano de un solo componente que endurece con la humedad. La porci&oacute;n aplicada se expande de 2 a 3 veces. Se adhiere a toda superficie, excepto poliestireno, silic&oacute;n y tefl&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Se utiliza para aislamiento t&eacute;rmico de ventanas, puertas, marcos, etc. Rellena huecos, paredes, conductos, etc. Sella herm&eacute;ticamente fisuras y grietas.</p>', 0, '', '2019-03-11 09:40:38', '', 'admin'),
(57, '', 'ESPUMA DE POLIURETANO PARA BOYAS', 'espumaparaboy.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Son dos componentes l&iacute;quidos que al mezclarlos dan como resultado un poliuretano s&oacute;lido poroso, especial para la fabricaci&oacute;n manual de boyas para W.C. y flotadores de varios usos. Su tiempo de reacci&oacute;n es lento para un mejor manejo</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: en partes iguales. Tiempo de mezclado: 5-8 seg. Tiempo de elevaci&oacute;n: 25-40 seg.</p>', 0, '', '2019-03-11 09:46:38', '', 'admin'),
(58, '', 'ESPUMA DE POLIURETANO PARA ASIENTOS', 'ESPUMAPARAASIENTO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Sistema de dos componentes que necesariamente se mezclan para la fabricaci&oacute;n de asientos y respaldos, dando buena apariencia y comodidad.</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: 100(A)X 44(B)partes por peso. Tiempo de mezclado: 5-7 seg. Tiempo de elevaci&oacute;n: 55-1&acute;10 min.</p>', 0, '', '2019-03-11 09:51:03', '', 'admin'),
(59, '', 'ESPUMA DE POLIURETANO IMITACI&Oacute;N MADERA', 'espumaparamadera.jpg.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Sistema de dos componentes que necesariamente se mezclan para la fabricaci&oacute;n de piezas imitaci&oacute;n madera como marcos, frentes de caj&oacute;n, puertas para cocinas, etc.</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: partes iguales. Tiempo de mezclado: 30seg. Tiempo de elevaci&oacute;n: 100+-10 seg</p>', 0, '', '2019-03-11 09:52:09', '', 'admin'),
(60, '', 'ESPUMA DE POLIURETANO PARA PIEL', 'espumaparapiel01.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Espuma de Poliuretano de dos componentes, desarrollado para la fabricaci&oacute;n de asientos de bicicleta, coderas, y dem&aacute;s piezas en la industria automotriz y mueblera.</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: 100(A) X 41(B) partes por peso. Tiempo de mezclado: 15 seg. Tiempo de elevaci&oacute;n: 1&rsquo;10-1&acute;15min.</p>', 0, '', '2019-03-11 09:54:18', '', 'admin'),
(62, '', 'MALLA DE REFUERZO', 'MAYADEREFUERZO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '', 0, '', '2019-06-13 09:10:58', '', 'admin'),
(63, '', 'PASTA USO AUTOMOTR&Iacute;Z', 'PASTAAUTO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<h3 style=\"text-align: justify;\"><span style=\"color: #1f205a; font-size: 14px; letter-spacing: 1px; text-align: justify;\">Pasta suave a base de resina poli&eacute;ster, de f&aacute;cil aplicaci&oacute;n, adecuada viscosidad, gran durabilidad y f&aacute;cil lijado, lo que ayuda a acortar tiempos de trabajo. Se le debe aplicar catalizador al 2% para un tiempo de curado de media hora aproximadamente. Aparte del mercado automotriz se utiliza tambi&eacute;n para restaurar o modificar figuras o muebles.</span></h3>', 0, '', '2019-06-25 10:25:19', '', 'admin'),
(64, '123456', 'PASTA USO MARINO', 'USOMARINO.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<h3 style=\"text-align: center;\">&nbsp;</h3>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Pasta suave a base de resina poli&eacute;ster con filamentos de fibra de vidrio, permite una aplicaci&oacute;n sin escurrimientos, de f&aacute;cil aplicaci&oacute;n, adecuada viscosidad, gran durabilidad y f&aacute;cil lijado, lo que ayuda a acortar tiempos de trabajo. Se le debe aplicar catalizador al 2% para un tiempo de curado de media hora aproximadamente. Sella, rellena y restaura lanchas, yates y botes. La diferencia que hay con la pasta automotriz, es que &eacute;sta es resistente a ambientes marinos y h&uacute;medos.</p>', 0, '', '2019-06-26 09:00:20', '', 'admin'),
(71, '', 'PROTECTOR ANTIRAYONES', 'ANTIRAYONES.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 1, '', '', '', '', '', '', '15', '<p>&nbsp;</p>\r\n<p>Sistema de recubrimiento de uretano base agua de dos componentes. Proporciona una apariencia excepcional, as&iacute; como excelentes propiedades f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>\r\n<p>&nbsp;</p>\r\n<p>Nivel de COV: 31 g / L</p>\r\n<p>Velocidad de secado r&aacute;pido</p>\r\n<p>R&aacute;pida resistencia al agua</p>\r\n<p>Excelentes propiedades adhesivas, que permiten su aplicaci&oacute;n en otros recubrimientos firmes y duros, as&iacute; como una buena adherencia al material sustrato.</p>\r\n<p>Aspecto excepcional</p>', 0, '', '2019-07-11 11:41:59', '', 'admin'),
(72, '', 'PROTECTOR RAYOS UV', 'PROTECCIONUV.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 1, '', '', '', '', '', '', '15', '<p>&nbsp;</p>\r\n<p>Sistema de 2 componentes, 83% s&oacute;lidos, cumple con COV, poliurea poliasp&aacute;rtica alif&aacute;tica que fue desarrollada para recubrimientos de suelo estables a los rayos UV. Presenta excelentes propiedades f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>', 0, '', '2019-07-11 11:48:43', '', 'admin'),
(66, '', 'RESINA PARA VACIADOS VF 2.5CM', 'PARAVACIADOS25VF.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 1, '', '', '', '', '', '', '15', '<p>RESINA PARA VACIADOS VF 2.5CM DE ESPESOR</p>\r\n<p>Sistema de 2 componentes, 100% s&oacute;lidos de curado a temperatura ambiente, f&aacute;cil manejo y excelente adhesi&oacute;n. Ha sido formulado con resina ep&oacute;xica dise&ntilde;ada para el encapsulado y decoraci&oacute;n en madera dando acabados con transparencia y brillo inigualables.</p>\r\n<p>&nbsp;</p>\r\n<p>F&aacute;cil de mezclar, relaci&oacute;n de volumen A:B = 2:1</p>\r\n<p>Tiempo de manejo y de curado largo</p>\r\n<p>Encapsulados de m&aacute;ximo 2.5cm</p>\r\n<p>L&iacute;quido libre de grumos y part&iacute;culas en suspensi&oacute;n</p>', 0, '', '2019-07-10 13:56:51', '', 'admin'),
(67, '', 'RESINA PARA VACIADOS VF 5CM', 'PARAVACIADOS5CM.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 1, '', '', '', '', '', '', '15', '<p>&nbsp;</p>\r\n<p>Sistema de 2 componentes, 100% s&oacute;lidos de curado a temperatura ambiente, f&aacute;cil manejo y excelente adhesi&oacute;n. Ha sido formulado con resina ep&oacute;xica dise&ntilde;ada para el encapsulado y decoraci&oacute;n en madera dando acabados con transparencia y brillo inigualables.</p>\r\n<p>F&aacute;cil de mezclar, relaci&oacute;n de volumen A:B = 4:1</p>\r\n<p>Tiempo de manejo y de curado largo</p>\r\n<p>Encapsulados de 2.5 a 5 cm</p>\r\n<p>L&iacute;quido libre de grumos y part&iacute;culas en suspensi&oacute;n.&nbsp;<span>Siguiendo el proceso de capas se puede alcanzar espesores hasta de 10 cm sin afectar la transparencia del encapsulado.</span></p>', 0, '', '2019-07-10 13:59:06', '', 'admin');
INSERT INTO `fib_productos` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `tag`, `marca`, `tipo`, `precio`, `moneda`, `unidad`, `stock`, `ID_cate`, `ID_sub_cate`, `ID_marca`, `visible`, `url_name`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `cate`, `resena`, `land`, `file`, `alta`, `fmod`, `user`) VALUES
(68, '', 'RESINA PARA PISOS VF', 'PARAPISOSVF.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 1, '', '', '', '', '', '', '15', '<p>&nbsp;</p>\r\n<p>Sistema de 2 componentes, 100% s&oacute;lidos, formulado para adquirir</p>\r\n<p>excelentes propiedades mec&aacute;nicas y qu&iacute;micas en 24 horas a temperatura ambiente.</p>\r\n<p>&nbsp;</p>\r\n<p>F&aacute;cil de mezclar, relaci&oacute;n de volumen 2:1.</p>\r\n<p>No emite olores y puede aplicarse en interiores.</p>\r\n<p>Autonivelante.</p>\r\n<p>L&iacute;quido libre de grumos y part&iacute;culas en suspensi&oacute;n.</p>', 0, '', '2019-07-10 14:00:37', '', 'admin'),
(70, '', 'RESINA EPOXICA PARA USO VERTICAL', 'DEUSOVERTICAL.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 1, '', '', '', '', '', '', '15', '<p>Sistema de recubrimiento ep&oacute;xico de dos componentes. Tiene muy buena apariencia, as&iacute; como propiedades qu&iacute;micas y f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>\r\n<p>&nbsp;</p>\r\n<p>No contiene solventes.</p>\r\n<p>Superficie densa resistente a las bacterias y la humedad y f&aacute;cil de limpiar.</p>\r\n<p>Se pueden aplicar varias capas sobre s&iacute; misma con excelente adherencia.</p>\r\n<p>Excelentes propiedades de adhesi&oacute;n, permite aplicaciones de otras marcas y con recubrimiento duro, as&iacute; como un buen enlace al sustrato.</p>\r\n<p>No emite olores y puede aplicarse en interiores.</p>', 0, '', '2019-07-11 11:41:13', '', 'admin'),
(69, '', 'RESINA SUPERFICIES HORIZONTALES', 'SOLIDOSEPOXICA.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 0, '', '', '', '', '', '', '15', '<p>Sistema de recubrimiento ep&oacute;xico de dos componentes. Tiene muy buena apariencia, as&iacute; como propiedades qu&iacute;micas y f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>\r\n<p>&nbsp;</p>\r\n<p>No contiene solventes.</p>\r\n<p>Superficie densa resistente a las bacterias y la humedad y f&aacute;cil de limpiar.</p>\r\n<p>Se pueden aplicar varias capas sobre s&iacute; misma con excelente adherencia.</p>\r\n<p>Excelentes propiedades de adhesi&oacute;n, permite aplicaciones de otras marcas y con recubrimiento duro, as&iacute; como un buen enlace al sustrato.</p>\r\n<p>No emite olores y puede aplicarse en interiores.</p>', 0, '', '2019-07-10 14:11:38', '', 'admin'),
(73, '', 'ESCAMAS PARA EP&Oacute;XICO', 'ESCAMAS.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 9, 0, 0, '', '', '', '', '', '', '14', '<p>Escamas decorativas compuestas por resinas a base de agua, minerales inorg&aacute;nicos, aditivos y varios colorantes. Este producto original tiene colores firmes y brillantes en su totalidad, varia en sus formas, puede ser recortado a rangos estandarizados y combinado en opciones infinitas para obtener la apariencia &oacute;ptima y la calidad de textura requerida para pisos a base de resina, o sistema de recubrimientos en paredes.</p>\r\n<p>&nbsp;</p>\r\n<p>Personalizados para cualquier ambiente</p>\r\n<p>M&aacute;s de 50 estilos de color</p>\r\n<p>No inflamable</p>\r\n<p>Qu&iacute;mica a base agua</p>\r\n<p>Dureza y flexibilidad &oacute;ptima</p>\r\n<p>Pigmentos de color estables a los rayos UV</p>\r\n<p>Aplicaci&oacute;n sencilla</p>\r\n<p>Igualaci&oacute;n de color personalizada</p>\r\n<p>Medidas de &frac14;&rdquo;, 1/8&rdquo; y 1/16&rdquo;</p>\r\n<p>Compatible con la mayor&iacute;a de las resinas</p>\r\n<p>Esconde imperfecciones en el piso</p>', 0, '', '2019-07-11 12:00:50', '', 'admin'),
(74, '', 'ABRASIVOS Y HERRAMIENTAS', 'abarsivos.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 12, 0, 0, 1, '', '', '', '', '', '', '12', '<p><span style=\"font-size: 14px;\">Encuentra todo lo necesario para desarrollar tus proyectos.</span></p>\r\n<p><span style=\"font-size: 14px; color: #000080;\"><br /></span></p>\r\n<ul>\r\n<li><span style=\"font-size: 14px; color: #000080;\">Rodillos</span></li>\r\n<li><span style=\"font-size: 14px; color: #000080;\">Brochas</span></li>\r\n<li><span style=\"font-size: 14px; color: #000080;\">B&aacute;sculas</span></li>\r\n<li><span style=\"font-size: 14px; color: #000080;\">Tapes</span></li>\r\n<li><span style=\"font-size: 14px; color: #000080;\">Sopletes</span></li>\r\n<li><span style=\"font-size: 14px; color: #000080;\">Muchas Herramientas m&aacute;s...</span></li>\r\n</ul>', 0, '', '2019-07-12 15:52:45', '', 'admin'),
(75, '', 'EQUIPO DE SEGURIDAD', 'equipodeseguridad.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 13, 0, 0, 1, '', '', '', '', '', '', '13', '<p>EQUIPO DE SEGUIRIDAD Y PROTECCI&Oacute;N PERSONAL</p>\r\n<p>&nbsp;</p>\r\n<p>El equipo de seguridad y protecci&oacute;n personal para tus trabajos y proyectos los encontrar&aacute;s con nosotros</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Lentes de seguridad</li>\r\n</ul>\r\n<ul>\r\n<li>Mascarilla con o sin v&aacute;lvula</li>\r\n<li>Mascarilla cara completa par vapores org&aacute;nicos</li>\r\n<li>Mascarilla media cara</li>\r\n<li>Cartuchos de repuesto</li>\r\n<li>Overol blanco</li>\r\n<li>Entre otras cosas m&aacute;s...</li>\r\n</ul>', 0, '', '2019-07-12 16:03:00', '', 'admin'),
(77, '', 'MON&Oacute;MERO DE ESTIRENO', 'monomero de estireno.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 10, 0, 1, '', '', '', '', '', '', '14', '<p>Dise&ntilde;ado para disolver y/o diluir la resina. A diferencia de lo que generalmente uno conoce por \"diluyente\" el mismo polimeriza junto a la resina o el gelcoat, es decir, no se evapora como un disolvente.</p>', 0, '', '2019-07-19 14:37:29', '', 'admin'),
(78, '', 'PASTA PARA PULIR BLANCA', 'blanca.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p>A diferencia de la pasta rosa, &eacute;sta nos ayuda a dar brillo a la pieza despu&eacute;s del pulido. Permite resultados r&aacute;pidos sin da&ntilde;ar las superficies a las que se aplica y sin dejar residuos, es de f&aacute;cil aplicaci&oacute;n en superficies como m&aacute;rmol, pisos, metales en general, pulido automotriz y acabados poli&eacute;ster.</p>', 0, '', '2019-07-19 15:02:54', '', 'admin'),
(79, '', 'PASTA PARA PULIR ROSA', 'rosa.jpg', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p>Pasta dise&ntilde;ada para pulir materiales r&uacute;sticos y de uso rudo, permite resultados r&aacute;pidos sin da&ntilde;ar las superficies a las que se aplica y sin dejar residuos. De f&aacute;cil aplicaci&oacute;n en superficies como m&aacute;rmol, pisos, metales y loseta.</p>', 0, '', '2019-07-19 15:04:31', '', 'admin'),
(80, '', 'DILUYENTE PARA SILIC&Oacute;N', 'DILUYENTEPSILICON.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p>Su funci&oacute;n es disminuir la viscosidad del caucho as&iacute; como eliminar suciedad y desengrase de los moldes. Se recomienda agregarlo del 30 al 50%.</p>', 0, '', '2019-07-19 15:40:09', '', 'admin'),
(81, '', 'CAB-O-SIL', 'CABOSIL.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 9, 0, 0, 1, '', '', '', '', '', '', '9', '<p>Polvo fino de baja densidad e inoloro, aporta la cualidad tixotr&oacute;pica a las resinas proporcionando una viscosidad aparente. Est&aacute; dise&ntilde;ada para mejorar las propiedades de los compuestos pl&aacute;sticos reforzados con fibra de vidrio. No aporta ninguna tonalidad a la resina, sin embargo opaca el color.</p>', 0, '', '2019-07-19 15:56:16', '', 'admin'),
(82, '', 'EPOXIPARCHE 1C', 'EPOXIPARCHE1C.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un adhesivo de dos componentes (resina y endurecedor) que al mezclarlos crean un sellador de alta resistencia, gelado r&aacute;pido y vers&aacute;til. Es resistente a aceites, solventes, calor, humedad y detergentes. Se utiliza principalmente para uni&oacute;n de concreto, fisuras, reparaci&oacute;n de grietas en superficies que van en contacto con agua como cisternas y tuber&iacute;as. O reparaci&oacute;n de moldes, modelos de fundici&oacute;n, reparaci&oacute;n de porcelana, laminados y fugas. A prueba de fallas el&eacute;ctricas.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Color: BLANCO</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Relaci&oacute;n: Resina (100) x Endurecedor (44). Tiempo de gelado de 10 a 15 min. Endurecimiento de 3 a 4 hrs. Curado: 24hrs.</p>', 0, '', '2019-07-19 16:04:46', '', 'admin'),
(83, '', 'EPOXIPARCHE 6C', 'EPOXIPARCHE6C.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un adhesivo de dos componentes (resina y endurecedor) que al mezclarlos crean un sellador de alta resistencia, gelado r&aacute;pido y vers&aacute;til. Es resistente a aceites, solventes, calor, humedad y detergentes. Se utiliza principalmente para uni&oacute;n de concreto, fisuras, reparaci&oacute;n de grietas en superficies que van en contacto con agua como cisternas y tuber&iacute;as. O reparaci&oacute;n de moldes, modelos de fundici&oacute;n, reparaci&oacute;n de porcelana, laminados y fugas. A prueba de fallas el&eacute;ctricas.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Color: GRIS</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Relaci&oacute;n: Resina (100) x Endurecedor (44). Tiempo de gelado de 10 a 15 min. Endurecimiento de 3 a 4 hrs. Curado: 24hrs.</p>', 0, '', '2019-07-19 16:16:58', '', 'admin'),
(84, '', 'EPOXIPARCHE 151', 'EPOXIPARCHE151.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un adhesivo de dos componentes (resina y endurecedor) que al mezclarlos crean un sellador de alta resistencia, gelado r&aacute;pido y vers&aacute;til. Es resistente a aceites, solventes, calor, humedad y detergentes. Se utiliza principalmente para uni&oacute;n de concreto, fisuras, reparaci&oacute;n de grietas en superficies que van en contacto con agua como cisternas y tuber&iacute;as. O reparaci&oacute;n de moldes, modelos de fundici&oacute;n, reparaci&oacute;n de porcelana, laminados y fugas. A prueba de fallas el&eacute;ctricas.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Color: TRANSPARENTE</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Relaci&oacute;n: Resina (100) x Endurecedor (44). Tiempo de gelado de 10 a 15 min. Endurecimiento de 3 a 4 hrs. Curado: 24hrs.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">De uso: Flexible</p>', 0, '', '2019-07-19 16:34:25', '', 'admin'),
(85, '', 'THINNER', 'THINNER.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p>Es un una mezcla de solventes que ha sido desarrollado para disolver, diluir o adelgazar sustancias insolubles en agua, como pinturas, aceites y grasas. Es incoloro y de olor fuerte caracter&iacute;stico.</p>', 0, '', '2019-07-19 16:51:54', '', 'admin'),
(86, '', 'ACETONA INDUSTRIAL', 'ACETONA.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 14, 0, 0, 1, '', '', '', '', '', '', '14', '<p>Solvente l&iacute;quido incoloro y de olor caracter&iacute;stico, se evapora f&aacute;cilmente, es inflamable y soluble en agua. Para limpieza de superficies y manos despu&eacute;s de estar en contacto con la resina.</p>', 0, '', '2019-07-19 16:52:09', '', 'admin'),
(87, '', 'BARINIZ Y QUITA BURBUJA PARA FOTOGRAF&Iacute;A', 'BARNICESPARAFOTO.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 15, 0, 0, 0, '', '', '', '', '', '', '15', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un sistema de dos componentes 06A/06B , 12A/12B Y 24A/24B (tiempo de secado de 06, 12 y 24 hrs), curan a temperatura ambiente creando una capa brillante y transparente, para recubrimiento de fotograf&iacute;as y p&oacute;sters. Para eliminar las burbujas atrapadas, despu&eacute;s de vaciar el barniz sobre la foto rociar una capa con quitaburbuja en aerosol. Su principal aplicaci&oacute;n es en encapsulados de fotograf&iacute;as, p&oacute;sters, trabajos de decoraci&oacute;n y protecci&oacute;n de superficies como madera y metal.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Tiempo de gelado 30&plusmn;10min. Tiempo de secado al tacto 3 a 4 hrs. Dejar reposar para el curado completo el tiempo de la presentaci&oacute;n que se ocupe (06, 12 o 24 hrs).</p>', 0, '', '2019-07-19 17:09:07', '', 'admin'),
(88, '', 'PIGMENTOS MET&Aacute;LICOS', 'metalicos.png', '', '', '', '', '', 0.00, 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p>Pigmentos con efectos especiales compuestos por nanopart&iacute;culas de mica</p>\r\n<p>recubiertas con distintos pigmentos org&aacute;nicos y no org&aacute;nicos que crean efectos aperlados o tornasolados que simulan el aspecto natural de las formaciones de piedra y roca. Estos pigmentos &uacute;nicos est&aacute;n dise&ntilde;ados para mezclarse con resina ep&oacute;xica transparente 100% s&oacute;lidos. Cuando las part&iacute;culas met&aacute;licas</p>\r\n<p>se mezclan con la resina ep&oacute;xica, brindan al recubrimiento un brillo que refleja la luz y que crea un color dram&aacute;tico que llama la atenci&oacute;n.</p>', 0, '', '2019-07-25 13:47:25', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_cate`
--

CREATE TABLE `fib_productos_cate` (
  `ID_cate` int(6) UNSIGNED NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL,
  `ord` int(2) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos_cate`
--

INSERT INTO `fib_productos_cate` (`ID_cate`, `categoria`, `des`, `ord`, `cover`, `visible`) VALUES
(1, 'RESINAS POLI&Eacute;STER', 'Liquido viscoso que se transforma en solido', 1, 'poliesteer_2.jpg', 1),
(2, 'FIBRA DE VIDRIO DE REFUERZO', 'Filamentos que a&ntilde;aden resistencia mecanica', 3, 'FIBRASS.jpg', 1),
(3, 'GEL COAT', 'Da acabado liso y brilloso a la pieza final', 6, 'gel coat.jpg', 1),
(4, 'PEGAMENTO PARA MARMOL', 'Adhesivo para el pegado de m&aacute;rmol natural y sint&eacute;tico', 9, 'pegamento.jpg', 1),
(5, 'SILICONES', 'Material para hacer moldes flexibles', 7, 'CAUCHO_2.jpg', 1),
(6, 'CATALIZADORES', 'Hace gelar y curar la resina', 8, 'CATALIZADORESS.jpg', 1),
(7, 'ACELERADORES', 'Acelera el curado de la resina', 11, 'ACELERADORES.jpg', 1),
(8, 'DESMOLDANTES', 'Ayuda a desprender la forma del molde', 12, 'DESMOL.jpg', 1),
(9, 'CARGAS', 'Se a&ntilde;ade a la resina para dar volumen o forma al producto final', 14, 'CARGASSS.jpg', 1),
(10, 'PIGMENTOS', 'A&ntilde;ade color deseado', 13, 'pigmentosportada.jpg', 1),
(11, 'ESPUMAS DE POLIURETANO', 'Material empleado para rellenar, sellar y aislar', 15, 'ESPUMASPOLIURETANO.jpg', 1),
(12, 'ABRASIVOS Y HERRAMIENTAS', '', 16, 'abrasivosjpgg.jpg', 1),
(13, 'EQUIPO DE SEGURIDAD', '', 17, 'equipodeseguridad.jpg', 1),
(14, 'OTROS PRODUCTOS', '', 18, 'thinneryacetona.png', 1),
(15, 'RESINAS  EP&Oacute;XICAS', '', 2, 'epoxicos.jpg', 1),
(16, 'FIBRA DE VIDRIO AISLANTE', '', 4, 'WhatsApp Image 2019-10-21 at 11.01.09 AM.jpeg', 1),
(17, 'MON&Oacute;MERO', '', 5, 'monomero de estireno.jpg', 1),
(18, 'MASILLA PARA MARMOL', '', 10, 'MASILLA.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_coti`
--

CREATE TABLE `fib_productos_coti` (
  `ID_bills` int(9) UNSIGNED NOT NULL,
  `ID_pro` int(9) NOT NULL,
  `cant` int(6) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `ID_cate` int(9) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `login` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_marcas`
--

CREATE TABLE `fib_productos_marcas` (
  `ID_marca` int(6) UNSIGNED NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos_marcas`
--

INSERT INTO `fib_productos_marcas` (`ID_marca`, `nombre`, `visible`) VALUES
(1, 'Samsung', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_sub_cate`
--

CREATE TABLE `fib_productos_sub_cate` (
  `ID_sub_cate` int(6) UNSIGNED NOT NULL,
  `subcategoria` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL,
  `ord` int(2) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos_sub_cate`
--

INSERT INTO `fib_productos_sub_cate` (`ID_sub_cate`, `subcategoria`, `des`, `ord`, `ID_cate`, `cover`, `visible`) VALUES
(1, 'RESINAS POLI&Eacute;STER', '', 1, 1, 'resinas.png', 0),
(2, 'RESINAS EP&Oacute;XICAS', 'CRISTALINA, AUNTONIVELANTE Y ALTA RESISTENCIA', 2, 1, 'recipiente.png', 0),
(5, 'SILICON DE CUACHO', '', 5, 5, 'moldesilicon.png', 0),
(6, 'OTROS', '', 6, 5, 'OTROSSILICONES.png', 0),
(7, 'GRANO FINO', '', 7, 9, 'granofino.jpg', 0),
(8, 'GRANO GRUESO', '', 8, 9, 'nodisponible.jpg', 0),
(9, 'PASTAS RESANADORAS', 'USO AUTOMOTR&Iacute;Z Y USO MARINO', 0, 14, 'RESANADORASPASTA.png', 0),
(10, 'DILUYENTES', 'Dise&ntilde;ado para disolver y/o diluir la resina. A diferencia de lo que generalmente uno conoce por &quot;diluyente&quot; el mismo polimeriza junto a la resina o el gelcoat, es decir, no se evapora como un disolvente.', 0, 14, 'DILUYENTES.png', 0),
(11, 'PASTAS PARA PULIR', '', 0, 14, 'barraspulir.png', 0),
(12, 'EPOXIPARCHES', '', 0, 14, 'EPOXIPARCHEE.png', 0),
(13, 'SOLVENTES', '', 0, 14, 'SOLVENTES.png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_registros`
--

CREATE TABLE `fib_registros` (
  `ID` int(9) UNSIGNED NOT NULL,
  `ip` varchar(25) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `asunto` varchar(150) NOT NULL,
  `msj` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_list` varchar(50) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `adjuntos` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_login` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_registros`
--

INSERT INTO `fib_registros` (`ID`, `ip`, `nombre`, `email`, `tel`, `titulo`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `status`, `ID_user`, `ID_login`, `visible`) VALUES
(1, '177.232.84.90', 'Envios -', 'memojl08@gmail.com', '', '', 'WEB FIBRECEN(Prueba web)', '&lt;p&gt;Mensaje de prueba&lt;/p&gt;', '2019-08-03 04:39:17', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(2, '187.145.91.50', 'Envios -', 'lopez.selene95@gmail.com', '', '', 'WEB FIBRECEN(PRUEBA)', '&lt;p&gt;Hola buenos d&iacute;as, prueba.&amp;nbsp;&lt;/p&gt;', '2019-08-03 16:46:22', 'papelera', 'registros', '', '', 1, 0, 0, 0, 1),
(3, '187.145.36.227', 'Envios -', 'Danydgj15@gmail.com', '', '', 'WEB FIBRECEN(Fibra de Carbon)', '&lt;p&gt;\r\n\r\nHola buenos d&iacute;as, el Metro cuadrado lo tenemos en $830. \r\nLe comento el numero del departamento de precios y cotizaciones es 4422128353.&lt;br&gt;&lt;/p&gt;&lt;p&gt;Nuestro horario de atenci&oacute;n es de Lunes a Viernes de 9:00am a 6:00pm y S&aacute;bados de 9:00am a 2:00pm&lt;/p&gt;', '2019-08-22 15:48:49', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(4, '187.145.30.251', 'Envios -', 'solof11@yahoo.com.mx', '', '', 'WEB FIBRECEN(FIBRECEN CELAYA )', '----------------------- responder -------------------------\r\n	\r\n&lt;div&gt;Hola buenos d&iacute;as, lamentablemente ya no contamos con sucursal en celaya, pero si gusta indicarme alg&uacute;n numero de contacto, con gusto lo comunico con nuestro departamento de entregas en Celaya.&amp;nbsp;&lt;/div&gt;', '2019-10-14 14:06:22', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(5, '189.180.56.241', 'Envios -', 'heliodoromtzg@hotmail.com', '', '', 'WEB FIBRECEN(FIBRECEN)', '---------------------- Hola buenos d&iacute;as, nos ha llegado una solicitud de cotizaci&oacute;n de un tambo de 200litros de resina, en un momento m&aacute;s se le har&aacute; llegar la cotizaci&oacute;n. -------------------------', '2019-11-12 15:48:09', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(6, '189.180.56.241', 'Envios -', 'heliodoromtzg@hotmail.com', '', '', 'WEB FIBRECEN(FIBRECEN)', '---------------------- Hola buenos d&iacute;as, nos ha llegado una solicitud de cotizaci&oacute;n de un tambo de 200litros de resina, en un momento m&aacute;s se le har&aacute; llegar la cotizaci&oacute;n. -------------------------', '2019-11-12 15:49:07', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(7, '189.180.56.241', 'Envios -', 'lantonioss73@gmail.com', '', '', 'WEB FIBRECEN(FIBRECEN CELAYA )', '-----------------------&amp;nbsp; &amp;nbsp; &amp;nbsp;Hola buenos d&iacute;as, para comentarle que lamentablemente ya no contamos con sucursal en celaya, si requiere un pedido en especifico nos lo puede comunicar, ya que hacemos una ruta todos los Martes para celaya.&lt;div&gt;Si gusta realizar un pedido puede comunicarse al 442 219 9937.&amp;nbsp;&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;Quedamos a sus &oacute;rdenes.&amp;nbsp;&lt;/div&gt;&lt;div&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; -------------------------\r\n	\r\n&lt;/div&gt;', '2019-11-12 15:53:24', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(8, '189.180.104.42', 'Envios -', 'jaime.espinoza@cidesi.edu.mx', '', '', 'WEB FIBRECEN(FIBRECEN)', 'Hola buenas tardes, SR. Espinoza, nos ha llegado su solicitud acerca de una cotizaci&oacute;n, estaremos trabajando en ella y en un momento m&aacute;s se la haremos llegar.&amp;nbsp;', '2019-11-14 21:41:08', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(9, '189.180.104.42', 'Envios -', 'fibrecendiseno@gmail.com', '', '', 'WEB FIBRECEN(FIBRECEN)', '&lt;p&gt;Hola estoy haciendo una prueba&lt;/p&gt;&lt;p&gt;acabo de hacer un esnter&lt;/p&gt;', '2019-11-14 21:42:51', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1),
(10, '189.180.78.69', 'Envios -', 'compraspromare@gmail.com', '', '', 'WEB FIBRECEN(FIBRECEN)', 'Buenos d&iacute;as, Srita Brenda su solicitud ya fue procesada, en unos momentos m&aacute;s se le estar&aacute; haciendo llegar su cotizaci&oacute;n.&amp;nbsp;', '2020-01-17 15:58:49', 'enviados', 'registros', '', '', 1, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_signup`
--

CREATE TABLE `fib_signup` (
  `ID` int(9) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` varchar(2) NOT NULL,
  `lastlogin` datetime NOT NULL,
  `tema` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apaterno` varchar(100) NOT NULL,
  `amaterno` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `tel` varchar(100) NOT NULL,
  `ext` int(4) NOT NULL,
  `fnac` date NOT NULL,
  `fb` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `ndepa` int(1) NOT NULL,
  `depa` varchar(100) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `mpio` varchar(100) NOT NULL,
  `edo` varchar(100) NOT NULL,
  `genero` varchar(20) NOT NULL,
  `exp` varchar(1000) NOT NULL,
  `likes` int(6) NOT NULL,
  `filtro` varchar(50) NOT NULL,
  `zona` varchar(50) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `actualizacion` varchar(20) NOT NULL,
  `page` varchar(250) NOT NULL,
  `nivel_oper` int(2) NOT NULL,
  `rol` int(2) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_signup`
--

INSERT INTO `fib_signup` (`ID`, `username`, `password`, `email`, `level`, `lastlogin`, `tema`, `nombre`, `apaterno`, `amaterno`, `foto`, `cover`, `tel`, `ext`, `fnac`, `fb`, `tw`, `puesto`, `ndepa`, `depa`, `empresa`, `adress`, `direccion`, `mpio`, `edo`, `genero`, `exp`, `likes`, `filtro`, `zona`, `alta`, `actualizacion`, `page`, `nivel_oper`, `rol`, `activo`) VALUES
(1, 'admin', 'admin458x', 'multiportal@outlook.com', '-1', '2020-01-20 15:31:53', 'default', 'Guillermo', 'Jimenez', 'Lopez', 'sinfoto.png', '', '4421944950', 1, '0000-00-00', '', '', 'Programador  Web', 0, '', 'Multiportal', '', '', '', '', 'M', '', 0, '', '', '', '', '', 0, 0, 1),
(5, 'FIBRECEN', 'fibrecen.2019', 'fibrecen@gmail.com', '-1', '2019-09-17 17:21:04', '', 'Admin Fibrecen', '', '', 'sinfoto.png', '', '', 0, '0000-00-00', '', '', 'Director', 0, '', '', '', '', '', '', 'M', '', 0, '', '', '', '', '', 0, 0, 1),
(6, 'Yusel', 'fibrecen2019', 'fibrecendiseno@gmail.com', '1', '2019-08-27 21:15:19', '', 'Yulibeth ', 'Lopez', 'Hern&aacute;ndez', 'sinfoto.png', '', '', 0, '0000-00-00', '', '', 'Dise&ntilde;adora Gr&aacute;fica', 0, '', '', '', '', '', '', 'M', '', 0, '', '', '', '', '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_slider`
--

CREATE TABLE `fib_slider` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ima` varchar(100) NOT NULL,
  `tit1` varchar(200) NOT NULL,
  `tit2` varchar(200) NOT NULL,
  `btn_nom` varchar(50) NOT NULL,
  `url` varchar(300) NOT NULL,
  `tema_slider` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_slider`
--

INSERT INTO `fib_slider` (`ID`, `ima`, `tit1`, `tit2`, `btn_nom`, `url`, `tema_slider`, `visible`) VALUES
(1, 'home3.jpg', 'fibrecen', 'Fibra de vidrio y carbono', 'Boton', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(2, '2.jpg', 'fibrecen', 'Fibras y Resinas', 'boton2', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(3, '1.jpg', 'fibra', 'Sector Industrial', 'video', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_tareas`
--

CREATE TABLE `fib_tareas` (
  `ID` int(11) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_tareas`
--

INSERT INTO `fib_tareas` (`ID`, `nom`, `descripcion`, `visible`) VALUES
(1, 'Nueva', 'Leer un libro', 0),
(2, 'Comprara memoria ', 'Comparar memoria usb de 64gb', 0),
(3, 'Nueva tarea', 'Descripcion', 0),
(4, '', '', 0),
(5, 'Tarea', 'Descripcion', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_temas`
--

CREATE TABLE `fib_temas` (
  `ID` int(3) UNSIGNED NOT NULL,
  `tema` varchar(100) NOT NULL,
  `subtema` varchar(100) NOT NULL,
  `selec` tinyint(1) NOT NULL,
  `nivel` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_temas`
--

INSERT INTO `fib_temas` (`ID`, `tema`, `subtema`, `selec`, `nivel`) VALUES
(1, 'default', '', 0, '0'),
(2, 'temp', '', 0, '0'),
(3, 'fibrecen', '', 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_testimonios`
--

CREATE TABLE `fib_testimonios` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `pro` varchar(100) NOT NULL,
  `comentario` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_testimonios`
--

INSERT INTO `fib_testimonios` (`ID`, `cover`, `pro`, `comentario`, `visible`) VALUES
(1, 'testimonial_person2.jpg', 'Ingeniera Civil', 'Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda', 1),
(2, 'testimonial_person1.jpg', 'Emprendedor', 'Su curso se me hizo f&aacute;cil y muy creativo, impartidos por excelentes maestros.', 1),
(3, 'testimonial_person3.jpg', 'Ingeniera Industrial', 'Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda.', 1),
(4, 'TESTIMONIO01.png', 'Emprendedor', 'Excelente curso introducci&oacute;n a los materiales compuestos, muchas gracias.', 1),
(5, 'testimonio02.png', 'Emprendedor', 'Excelente curso de Mesas Ep&oacute;xicas en Parota y Cristal Templado.  &iexcl;No dejen pasar la oportunidad de tomar este curso!', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fib_blog`
--
ALTER TABLE `fib_blog`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_blog_coment`
--
ALTER TABLE `fib_blog_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_comp`
--
ALTER TABLE `fib_comp`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_config`
--
ALTER TABLE `fib_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_contacto`
--
ALTER TABLE `fib_contacto`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_contacto_forms`
--
ALTER TABLE `fib_contacto_forms`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_cursos`
--
ALTER TABLE `fib_cursos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_cursos_coment`
--
ALTER TABLE `fib_cursos_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_depa`
--
ALTER TABLE `fib_depa`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_directorio`
--
ALTER TABLE `fib_directorio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_galeria`
--
ALTER TABLE `fib_galeria`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_glosario`
--
ALTER TABLE `fib_glosario`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_histo_backupdb`
--
ALTER TABLE `fib_histo_backupdb`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_home_config`
--
ALTER TABLE `fib_home_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_home_elementos`
--
ALTER TABLE `fib_home_elementos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_iconos`
--
ALTER TABLE `fib_iconos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_ipbann`
--
ALTER TABLE `fib_ipbann`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_landingpage_seccion`
--
ALTER TABLE `fib_landingpage_seccion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_map_config`
--
ALTER TABLE `fib_map_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_map_ubicacion`
--
ALTER TABLE `fib_map_ubicacion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_menu_admin`
--
ALTER TABLE `fib_menu_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_menu_web`
--
ALTER TABLE `fib_menu_web`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_mode_page`
--
ALTER TABLE `fib_mode_page`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_modulos`
--
ALTER TABLE `fib_modulos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_nosotros`
--
ALTER TABLE `fib_nosotros`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_notificacion`
--
ALTER TABLE `fib_notificacion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_opciones`
--
ALTER TABLE `fib_opciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_pages`
--
ALTER TABLE `fib_pages`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_portafolio`
--
ALTER TABLE `fib_portafolio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_productos`
--
ALTER TABLE `fib_productos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_productos_cate`
--
ALTER TABLE `fib_productos_cate`
  ADD PRIMARY KEY (`ID_cate`);

--
-- Indices de la tabla `fib_productos_coti`
--
ALTER TABLE `fib_productos_coti`
  ADD PRIMARY KEY (`ID_bills`);

--
-- Indices de la tabla `fib_productos_marcas`
--
ALTER TABLE `fib_productos_marcas`
  ADD PRIMARY KEY (`ID_marca`);

--
-- Indices de la tabla `fib_productos_sub_cate`
--
ALTER TABLE `fib_productos_sub_cate`
  ADD PRIMARY KEY (`ID_sub_cate`);

--
-- Indices de la tabla `fib_registros`
--
ALTER TABLE `fib_registros`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_signup`
--
ALTER TABLE `fib_signup`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_slider`
--
ALTER TABLE `fib_slider`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_tareas`
--
ALTER TABLE `fib_tareas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_temas`
--
ALTER TABLE `fib_temas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_testimonios`
--
ALTER TABLE `fib_testimonios`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `fib_blog`
--
ALTER TABLE `fib_blog`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_blog_coment`
--
ALTER TABLE `fib_blog_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_comp`
--
ALTER TABLE `fib_comp`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_config`
--
ALTER TABLE `fib_config`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_contacto`
--
ALTER TABLE `fib_contacto`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `fib_contacto_forms`
--
ALTER TABLE `fib_contacto_forms`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fib_cursos`
--
ALTER TABLE `fib_cursos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `fib_cursos_coment`
--
ALTER TABLE `fib_cursos_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_depa`
--
ALTER TABLE `fib_depa`
  MODIFY `ID` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `fib_directorio`
--
ALTER TABLE `fib_directorio`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fib_galeria`
--
ALTER TABLE `fib_galeria`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `fib_glosario`
--
ALTER TABLE `fib_glosario`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `fib_histo_backupdb`
--
ALTER TABLE `fib_histo_backupdb`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `fib_home_config`
--
ALTER TABLE `fib_home_config`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_home_elementos`
--
ALTER TABLE `fib_home_elementos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_iconos`
--
ALTER TABLE `fib_iconos`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `fib_ipbann`
--
ALTER TABLE `fib_ipbann`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_landingpage_seccion`
--
ALTER TABLE `fib_landingpage_seccion`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_map_config`
--
ALTER TABLE `fib_map_config`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fib_map_ubicacion`
--
ALTER TABLE `fib_map_ubicacion`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_menu_admin`
--
ALTER TABLE `fib_menu_admin`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `fib_menu_web`
--
ALTER TABLE `fib_menu_web`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `fib_mode_page`
--
ALTER TABLE `fib_mode_page`
  MODIFY `ID` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_modulos`
--
ALTER TABLE `fib_modulos`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `fib_nosotros`
--
ALTER TABLE `fib_nosotros`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_notificacion`
--
ALTER TABLE `fib_notificacion`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fib_opciones`
--
ALTER TABLE `fib_opciones`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `fib_pages`
--
ALTER TABLE `fib_pages`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_portafolio`
--
ALTER TABLE `fib_portafolio`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `fib_productos`
--
ALTER TABLE `fib_productos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT de la tabla `fib_productos_cate`
--
ALTER TABLE `fib_productos_cate`
  MODIFY `ID_cate` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `fib_productos_coti`
--
ALTER TABLE `fib_productos_coti`
  MODIFY `ID_bills` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fib_productos_marcas`
--
ALTER TABLE `fib_productos_marcas`
  MODIFY `ID_marca` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_productos_sub_cate`
--
ALTER TABLE `fib_productos_sub_cate`
  MODIFY `ID_sub_cate` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `fib_registros`
--
ALTER TABLE `fib_registros`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `fib_signup`
--
ALTER TABLE `fib_signup`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `fib_slider`
--
ALTER TABLE `fib_slider`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_tareas`
--
ALTER TABLE `fib_tareas`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_temas`
--
ALTER TABLE `fib_temas`
  MODIFY `ID` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_testimonios`
--
ALTER TABLE `fib_testimonios`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
