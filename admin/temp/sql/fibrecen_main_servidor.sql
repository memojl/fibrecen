-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-07-2019 a las 21:55:24
-- Versión del servidor: 5.5.62-cll
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fibrecen_main`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_blog`
--

CREATE TABLE `fib_blog` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_blog`
--

INSERT INTO `fib_blog` (`ID`, `cover`, `titulo`, `descripcion`, `contenido`, `tag`, `autor`, `fmod`, `fecha`, `visible`) VALUES
(1, 'blog_FO_petrolera.jpg', 'Mi primer blog', 'Si vives con EPOC, tener una fuente de oxígeno confiable es importante para mantener...', '<p>Si vives con EPOC, tener una fuente de ox&iacute;geno confiable es importante para mantener tu calidad de vida. Sin embargo, existen tantos tipos diferentes de concentradores de ox&iacute;geno en el mercado hoy en d&iacute;a, que puede ser dif&iacute;cil elegir el que mejor se adapte a sus necesidades. A medida que esta tecnolog&iacute;a contin&uacute;a avanzando, aparecen caracter&iacute;sticas m&aacute;s nuevas y opciones m&aacute;s c&oacute;modas, &iexcl;y desea aprovecharlas al m&aacute;ximo!</p>\r\n<p>La buena noticia es que hay m&aacute;s opciones para la terapia de ox&iacute;geno disponibles para ti; a continuaci&oacute;n, hemos recopilado informaci&oacute;n excelente sobre los dos principales concentradores de oxigeno dom&eacute;sticos de Philips Respironics:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"299\"><strong>EVERFLO</strong>\r\n<p>&nbsp;</p>\r\n<p>El concentrador de ox&iacute;geno EverFlo de 5 litros es una m&aacute;quina silenciosa, liviana y compacta que es menos llamativa que muchas otras.</p>\r\n<p>Los usuarios pueden comprar el modelo est&aacute;ndar, o el que tiene un indicador de porcentaje de ox&iacute;geno (OPI) y usa ultrasonido para medir el flujo de ox&iacute;geno.</p>\r\n<p>Los controles se encuentran en el lado delantero izquierdo de la m&aacute;quina y una perilla de rodillo controla el medidor de flujo de ox&iacute;geno empotrado en el centro. Una botella de humidificador se puede conectar a la parte posterior izquierda de la m&aacute;quina con velcro. &iexcl;El tubo se conecta f&aacute;cilmente a la c&aacute;nula de metal encima del interruptor de encendido, y tambi&eacute;n se pueden almacenar tubos adicionales en el interior.</p>\r\n<p>&iexcl;EverFlo 5L pesa 14 kgs y entrega ox&iacute;geno a .5-5 LPM con una concentraci&oacute;n de ox&iacute;geno de hasta 95% en todas las velocidades de flujo. La m&aacute;quina mide 58 cm de profundidad.</p>\r\n<p>El concentrador EverFlo de 5L viene con una garant&iacute;a est&aacute;ndar de 1 a&ntilde;o.</p>\r\n</td>\r\n<td width=\"299\"><strong>MILLENNIUM</strong>\r\n<p>&nbsp;</p>\r\n<p>El concentrador de ox&iacute;geno Millenium proporciona hasta 10 LPM de ox&iacute;geno, d&aacute;ndole las especificaciones de una unidad de &ldquo;alto flujo&rdquo;.</p>\r\n<p>El concentrador de ox&iacute;geno est&aacute; disponible en dos modelos: el modelo est&aacute;ndar y uno dise&ntilde;ado con un indicador de porcentaje de ox&iacute;geno (OPI), una funci&oacute;n que utiliza tecnolog&iacute;a de ultrasonido para medir el flujo de ox&iacute;geno.</p>\r\n<p>El dise&ntilde;o rectangular blanco es fuerte y resistente, y cuatro ruedas grandes (junto con un asa insertada en la parte superior) lo hacen bastante f&aacute;cil de mover.</p>\r\n<p>Este concentrador tiene una v&aacute;lvula SMC de &ldquo;ciclo seguro&rdquo;, dise&ntilde;ada espec&iacute;ficamente para manejar los mayores flujos de presi&oacute;n necesarios para una m&aacute;quina de 10 LPM. Millenium tambi&eacute;n est&aacute; dise&ntilde;ado con un compresor de doble cabezal equipado para impulsar m&aacute;s aire a trav&eacute;s de los lechos de tamices de la m&aacute;quina para eliminar el nitr&oacute;geno.</p>\r\n<p>Philips Respironics &ldquo;Millennium&rdquo; viene con una garant&iacute;a est&aacute;ndar de un a&ntilde;o.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Caracter&iacute;sticas y Beneficios</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Silencioso y f&aacute;cil de usar\r\n<p>&nbsp;</p>\r\n<p>Controles claros y visibles</p>\r\n<p>Dise&ntilde;o ergon&oacute;mico: rueda f&aacute;cilmente</p>\r\n<p>Peso ligero de 14 kg</p>\r\n<p>Medidor de flujo empotrado para proteger contra la rotura</p>\r\n<p>Velcro asegura la botella del humidificador en la m&aacute;quina</p>\r\n<p>Proporciona ox&iacute;geno a .5-5 LPM con 95% de ox&iacute;geno</p>\r\n<p>Alarmas de seguridad por fallas</p>\r\n<p>Garant&iacute;a de producto de tres a&ntilde;os</p>\r\n</td>\r\n<td width=\"299\">F&aacute;cil de usar: los controles son claros y visibles\r\n<p>&nbsp;</p>\r\n<p>Pesa 24 kg</p>\r\n<p>Indicador de Porcentaje de Ox&iacute;geno (OPI) se puede agregar en</p>\r\n<p>Proporciona ox&iacute;geno a 1-10 LPM al 96% de ox&iacute;geno</p>\r\n<p>Alarmas de seguridad por fallas y bajo porcentaje de ox&iacute;geno</p>\r\n<p>Menos partes m&oacute;viles que otros concentradores</p>\r\n<p>Garant&iacute;a est&aacute;ndar de un a&ntilde;o</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Pros</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Funcionamiento silencioso y sonido silencioso cuando se inicia (45 db)\r\n<p>&nbsp;</p>\r\n<p>F&aacute;cil de usar</p>\r\n<p>Confiable y ligero</p>\r\n<p>Port&aacute;til y f&aacute;cil de mover</p>\r\n<p>Consumo de energ&iacute;a de 350 w</p>\r\n<p>&nbsp;</p>\r\n</td>\r\n<td width=\"299\">Bien hecho y f&aacute;cil de configurar\r\n<p>&nbsp;</p>\r\n<p>Robusto, confiable y de bajo mantenimiento</p>\r\n<p>Produce hasta 10 LPM de ox&iacute;geno</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Contras</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Bip fuerte cuando se inicia\r\n<p>&nbsp;</p>\r\n<p>Baja altitud de trabajo</p>\r\n<p>Produce hasta 5 LPM de ox&iacute;geno</p>\r\n</td>\r\n<td width=\"299\">Demasiado ruidoso para algunos usuarios (50 db)\r\n<p>&nbsp;</p>\r\n<p>Pesa 24 kg</p>\r\n<p>Tiene m&aacute;s potencia de la que muchos usuarios necesitan</p>\r\n<p>Consumo de energ&iacute;a de 600 w</p>\r\n<p><strong>&nbsp;</strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>La elecci&oacute;n de los dispositivos de administraci&oacute;n de ox&iacute;geno depende del requerimiento del paciente, la eficacia del dispositivo, la fiabilidad, la facilidad de aplicaci&oacute;n terap&eacute;utica y la aceptaci&oacute;n del paciente. <a href=\"http://samsung-healthcare.mx/contacto\"><span style=\"text-decoration: underline;\">Para m&aacute;s informaci&oacute;n sobre la elecci&oacute;n de su concentrador de ox&iacute;geno no dude en contactarnos.</span></a></p>', 'EPOC, Oxígeno', 'admin', '2018-09-24 22:23:34', '2017-01-18 14:05:23', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_blog_coment`
--

CREATE TABLE `fib_blog_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_blog_coment`
--

INSERT INTO `fib_blog_coment` (`ID`, `ip`, `nombre`, `email`, `comentario`, `id_b`, `fecha`, `visible`) VALUES
(1, '127.0.0.1', 'Arturo L&oacute;pez', 'alopez@gmail.com', 'Comentario de prueba.', 1, '09/02/2018 23:43', 1),
(2, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'memojl08@gmail.com', 'Mensaje de Prueba.', 1, '11/02/2018 21:31', 1),
(3, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'memotablet08@gmail.com', 'mensaje de prueba', 1, '2018-09-11 10:03:31', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_comp`
--

CREATE TABLE `fib_comp` (
  `ID` int(1) UNSIGNED NOT NULL,
  `page` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_comp`
--

INSERT INTO `fib_comp` (`ID`, `page`) VALUES
(1, 'usuarios/login.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_config`
--

CREATE TABLE `fib_config` (
  `ID` int(1) UNSIGNED NOT NULL,
  `logo` varchar(100) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `dominio` varchar(100) NOT NULL,
  `path_root` varchar(150) NOT NULL,
  `page_url` varchar(100) NOT NULL,
  `keyword` varchar(200) NOT NULL,
  `description` varchar(300) NOT NULL,
  `metas` text NOT NULL,
  `g_analytics` varchar(500) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `tel2` varchar(20) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `wapp` varchar(20) NOT NULL,
  `webMail` varchar(100) NOT NULL,
  `contactMail` varchar(100) NOT NULL,
  `contactMail2` varchar(100) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `chartset` varchar(30) NOT NULL,
  `dboard` varchar(50) NOT NULL,
  `dboard2` varchar(50) NOT NULL,
  `direc` varchar(250) NOT NULL,
  `direc2` varchar(250) NOT NULL,
  `CoR` varchar(100) NOT NULL,
  `CoE` varchar(100) NOT NULL,
  `BCC` varchar(100) NOT NULL,
  `CoP` varchar(100) NOT NULL,
  `fb` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `gp` varchar(100) NOT NULL,
  `lk` varchar(100) NOT NULL,
  `yt` varchar(100) NOT NULL,
  `ins` varchar(100) NOT NULL,
  `licencia` varchar(300) NOT NULL,
  `version` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_config`
--

INSERT INTO `fib_config` (`ID`, `logo`, `page_name`, `title`, `dominio`, `path_root`, `page_url`, `keyword`, `description`, `metas`, `g_analytics`, `tel`, `tel2`, `phone`, `wapp`, `webMail`, `contactMail`, `contactMail2`, `mode`, `chartset`, `dboard`, `dboard2`, `direc`, `direc2`, `CoR`, `CoE`, `BCC`, `CoP`, `fb`, `tw`, `gp`, `lk`, `yt`, `ins`, `licencia`, `version`) VALUES
(1, 'logo_circulo.png', 'FIBRECEN', 'FIBRECEN - Fibra de vidrio y carbono - Resina Poli&eacute;ster - Ep&oacute;xica y mucho m&aacute;s...', 'http://www.fibrecen.com.mx/', '/2019', 'http://www.fibrecen.com.mx/2019/', 'Resinas, Fibras, Ep&oacute;xico, Curso, Encapsulados, Fibra de vidrio, L&aacute;tex, Pigmentos, Fotoluminiscentes, Fibra de Carbon, Resina Eco, Uso General,  ', 'Somos fibras y resinas del centro, tenemos m&aacute;s de 30 a&ntilde;os de experiencia en la creaci&oacute;n estructuras de fibra de vidrio as&iacute; como de la venta de materiales como resinas, fibras de vidrio, gel coats, cat', '<!--Responsive Meta-->\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\r\n<!-- META-TAGS generadas por http://metatags.miarroba.es -->\r\n<META NAME=\"DC.Language\" SCHEME=\"RFC1766\" CONTENT=\"Spanish\">\r\n<META NAME=\"AUTHOR\" CONTENT=\"Guillermo Jimenez\">\r\n<META NAME=\"REPLY-TO\" CONTENT=\"multiportal@outlook.com\">\r\n<LINK REV=\"made\" href=\"mailto:multiportal@outlook.com\">\r\n', '<!-- Global site tag (gtag.js) - Google Ads: 786424895 --> <script async src=\"https://www.googletagmanager.com/gtag/js?id=AW-786424895\"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag(\'js\', new Date()); gtag(\'config\', \'AW-786424895\'); </script> ', '(442) 2128353', '(442) 2141643', '524422199937', '4422199937', 'contacto@fibrecen.com.mx', 'qro@fibrecen.com.mx', 'celaya@fibrecen.com.mx', 'page', 'iso-8859-1', 'dashboard', 'AdminLTE', 'Guanajuato No. 5-B, Col. San Francisquito, Querétaro, Qro.', 'Blv. Adolfo López Mateos No. 1500, Col. Rosalinda, Celaya, Gto.', 'qro@fibrecen.com.mx', 'contacto@fibrecen.com.mx', '', 'memojl08@gmail.com', 'https://www.facebook.com/fibrecen.mx/', '', '', 'https://www.linkedin.com/in/fibrecen-fibras-y-resinas-bba407176/', 'https://www.youtube.com/channel/UCi6YWo0xjGHXWQcbgH_qMCA', 'https://www.instagram.com/fibrecen/', 'cms-px31q2hponix31q2x.admx31q2in458x31q2x.201x31q29.05.x31q212.01x31q2.2.4.x31q27x31q2', '01.2.5.1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_contacto`
--

CREATE TABLE `fib_contacto` (
  `ID` int(9) UNSIGNED NOT NULL,
  `ip` varchar(25) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `asunto` varchar(150) NOT NULL,
  `msj` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_list` varchar(50) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `adjuntos` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_contacto`
--

INSERT INTO `fib_contacto` (`ID`, `ip`, `nombre`, `email`, `tel`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `visible`) VALUES
(1, '177.232.84.138', 'Guillermo Jim&eacute;nez L&oacute;pez ', 'd@d.com', '', 'CONTACTO - Prueba', 'Mensaje de prueba', '2019-06-16 17:59:14', 'inbox', 'contacto', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_contacto_forms`
--

CREATE TABLE `fib_contacto_forms` (
  `ID` int(6) UNSIGNED NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `modulo` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `bcc` varchar(200) NOT NULL,
  `CoE` varchar(100) NOT NULL,
  `CoP` varchar(100) NOT NULL,
  `usuario` varchar(300) NOT NULL,
  `url_m` varchar(500) NOT NULL,
  `fecha` varchar(22) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_contacto_forms`
--

INSERT INTO `fib_contacto_forms` (`ID`, `seccion`, `modulo`, `email`, `bcc`, `CoE`, `CoP`, `usuario`, `url_m`, `fecha`, `activo`) VALUES
(1, 'Contacto', 'contacto', 'contacto@fibrecen.com.mx', '', 'contacto@fibrecen.com.mx', 'memojl08@gmail.com', 'admin', 'index.php?mod=contacto', '2018-09-28 18:31:45', 1),
(2, 'Cursos', 'cursos', 'contacto@fibrecen.com.mx', '', 'contacto@fibrecen.com.mx', 'memojl08@gmail.com', 'admin', 'index.php?mod=cursos', '2019-01-28 21:33:03', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_cursos`
--

CREATE TABLE `fib_cursos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `fechas` varchar(100) NOT NULL,
  `lugar` varchar(200) NOT NULL,
  `horario` varchar(100) NOT NULL,
  `video` varchar(300) NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_cursos`
--

INSERT INTO `fib_cursos` (`ID`, `cover`, `titulo`, `descripcion`, `contenido`, `fechas`, `lugar`, `horario`, `video`, `tag`, `autor`, `fmod`, `fecha`, `visible`) VALUES
(4, 'moldes3d-01.jpg', 'MOLDES FLEXIBLES 3D', ' &iquest;Te gustar&iacute;a CREAR tus propios moldes y Reproducir en Serie? Entonces este curso es para ti. ', '<p><strong><span>&nbsp;&iquest;Te gustar&iacute;a CREAR tus propios moldes y Reproducir en Serie? Entonces este curso es para ti.&nbsp;<br /><br /></span></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull;&nbsp;Porcentajes de cat&aacute;lisis del silic&oacute;n de caucho</p>\r\n<p>&bull;&nbsp;Aplicaci&oacute;n de SILICON DE CAUCHO con la t&eacute;cnica de molde tipo Bloque</p>\r\n<p>&bull; Porcentajes de cat&aacute;lisis de la Resina Poli&eacute;ster usos generales</p>\r\n<p>&bull; T&eacute;cnica de Vaciado utilizando la resina</p>\r\n<p><span style=\"font-size: 0.9em;\"><br /></span></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente necesitar&aacute;s traer una pieza que no rebase 10cm de largo y 5cm de alto y que no tenga nada de humedad.</p>\r\n<p><br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $840 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>', 'S&aacute;bados 17 y 24 de Agosto ', 'Calle  Guanajuato #5B Col. San Francisquito. Quer&eacute;taro, Qro. ', '9:30am a 1:30pm ', '', 'Curso poliester', 'admin', '2019-07-15 11:42:54', '', 1),
(5, 'alumnos.jpg', 'INTRODUCCI&Oacute;N A LOS MATERIALES COMPUESTOS EN GUANAJUATO ', 'Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.', '<p><strong>&nbsp;</strong></p>\r\n<p><strong>Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.<br /><br /></strong></p>\r\n<p><span><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull; Teor&iacute;a de cargas en resinas y pr&aacute;ctica.<br />&bull; Teor&iacute;a de Resinas.<br />&bull; Practica de encapsulados con resina cristal.<br />&bull; Teor&iacute;a de Silic&oacute;n de caucho.<br />&bull; Pr&aacute;ctica donde se har&aacute;n moldes flexibles sencillos de una sola cara (2D).<br />&bull; Pr&aacute;ctica de vaciados con resina y cargas.<br /><br /><br /></p>\r\n<p><span><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente requerimos que traiga una foto de 10 x 15cm en papel fotografico, tambi&eacute;n una pieza para reproducir y otra para encapsular que sea de no m&aacute;s de 3cm.<br /><br /></p>\r\n<p><span><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $1 150 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>&nbsp;</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $200.00 pues tenemos cupo limitado.</p>', '15 y 16 de Agosto ', 'Preparatoria  CETAB , ALHONDIGA 25 A CENTRO GUANAJUATO; GTO.', '11:00am a 3:00pm', '', 'Curso poliester', '', '2019-07-15 11:31:20', '', 1),
(6, 'moldes2d.jpg', 'MOLDES FLEXIBLES 2D', 'Aprender&aacute;s a crear un Molde 2D con silicon de Caucho para reproducir en serie. ', '<p><strong>Aprender&aacute;s a crear un Molde 2D con silic&oacute;n de Caucho para reproducir en serie.&nbsp;</strong></p>\r\n<p><strong><br /></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO</strong></span></p>\r\n<p>&bull; Porcentajes de cat&aacute;lisis del silic&oacute;n de caucho</p>\r\n<p>&bull; Aplicaci&oacute;n de SILICON DE CAUCHO con la t&eacute;cnica de molde tipo Bloque</p>\r\n<p>&bull; Porcentajes de cat&aacute;lisis de la Resina Poli&eacute;ster usos generales</p>\r\n<p>&bull; T&eacute;cnica de Vaciado utilizando la resina<span style=\"font-size: 0.9em;\">&nbsp;</span></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente necesitar&aacute;s traer una pieza que no rebase 10cm de largo y 5cm de alto y que no tenga nada de humedad.</p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>Tiene un costo de $840.00 por persona e incluye todos los materiales para las pr&aacute;cticas as&iacute; como tu equipo de seguridad.&nbsp;Es necesario apartar el lugar con un anticipo de m&iacute;nimo $250.00 pues tenemos cupo limitado.</p>', 'S&aacute;bados  20 y 27 de Julio', 'Calle  Guanajuato #5B Col. San Francisquito. Quer&eacute;taro, Qro. ', '10:00am a 1:30pm', '', 'Curso poliester', '', '2019-07-01 12:36:45', '', 1),
(13, 'mesas.jpg', 'MESAS DE PAROTA CON RESINA EP&Oacute;XICA QUER&Eacute;TARO', 'Desarrollar proyectos a gran escala en mesas de parota con r&iacute;o de resina ep&oacute;xica.', '<p><strong>Obtendr&aacute;s los conocimientos necesarios para emprender tu propio negocio o hacerlo crecer con nuevas ideas.&nbsp;</strong></p>\r\n<p><strong><br /></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>Aprendizaje pr&aacute;ctico intensivo, aprender&aacute;s las t&eacute;cnicas de aplicaci&oacute;n de materiales ep&oacute;xicos para diferentes acabados y superficies.</p>\r\n<p>&#9642;Tablas de porcentajes</p>\r\n<p>&#9642; Tablas de proporciones</p>\r\n<p>&#9642; Datos t&eacute;cnicos</p>\r\n<p><strong>Acabados</strong></p>\r\n<p><span>&#9642; Trasl&uacute;cido</span></p>\r\n<p><span>&bull; Met&aacute;lico</span></p>\r\n<p><span>&bull; S&oacute;lido&nbsp;</span></p>\r\n<p>&bull; Fotoluminiscente<br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&bull; Material para realizar una cubierta de Parota de 40x35cm.</p>\r\n<p>&bull; Parota</p>\r\n<p>&bull; Molde</p>\r\n<p>&bull; Todos los materiales de preparaci&oacute;n.</p>\r\n<p>&bull; Resina para Madera Altos S&oacute;lidos</p>\r\n<p>&bull; Pigmentos</p>\r\n<p>&bull; Equipo de protecci&oacute;n personal.</p>\r\n<p>&bull; Pr&aacute;ctica de aplicaci&oacute;n.<br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>El costo de inversi&oacute;n es de&nbsp; $2,500 por persona, en caso de requerir factura ser&iacute;a m&aacute;s IVA.</p>\r\n<p>Para apartar se requiere de un anticipo de $300 y se liquida el d&iacute;a de curso</p>', '13 de Julio ', 'Work Zone  Blvd. Bernardo Quintana No. 4100,  Int. 2, Local 33, Col. San Pablo, Plaza  El Punto. Quer&eacute;taro, Qro., C.P. 76130.', '9:00am a 5:00pm ', 'https://www.facebook.com/fibrecen.mx/videos/426400854616729/', 'Curso epoxicos', '', '2019-07-08 12:25:47', '', 1),
(7, 'cursodeverano.jpg', 'CURSO DE VERANO', 'Aprende a CREAR una pieza desde cero, desde el modelado hasta detallado con pintura.', '<p><strong>Este verano haz que tu peque&ntilde;o explore su creatividad creando piezas &uacute;nicas . </strong></p>\r\n<p><strong>Puedes participar si tienes 9 a&ntilde;os en adelante</strong></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO</strong>&nbsp;</span></p>\r\n<p>&bull; Modelado en plastilina.</p>\r\n<p>&bull; Creaci&oacute;n de figuras de acci&oacute;n</p>\r\n<p>&bull; Carcasa para celular personalizada.</p>\r\n<p>&bull; Creaci&oacute;n de molde.</p>\r\n<p>&bull; Reproducci&oacute;n de piezas en Yeso.</p>\r\n<p>&bull; Aplicaci&oacute;n de pintura en piezas 3D.<br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente necesitar&aacute;s &nbsp;traer la carcasa del celular.&nbsp;<br /><br /></p>\r\n<p><strong><span style=\"font-size: 18px;\">COSTOS</span></strong></p>\r\n<p>Tiene un costo de $695.00 por persona e incluye todos los materiales para las pr&aacute;cticas.&nbsp;</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $200.00 pues tenemos cupo limitado</p>', 'Del 22 al 26 de Julio', 'Calle  Guanajuato #5B Col. San Francisquito. Quer&eacute;taro, Qro. ', '9:30am a 1:30pm ', '', 'Curso poliester', '', '2019-07-01 12:37:35', '', 1),
(8, 'cursos1.jpg', 'INTRODUCCI&Oacute;N A LOS MATERIALES COMPUESTOS', 'Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.', '<p><strong><span>Curso te&oacute;rtico - practico, donde nuestros instructores te llevaran de la mano para conocer las caracter&iacute;sticas y usos correctos de cada material.<br /><br /></span></strong></p>\r\n<p><span style=\"font-size: 18px;\"><strong>TEMARIO&nbsp;</strong></span></p>\r\n<p>&bull; Teor&iacute;a de cargas en resinas y pr&aacute;ctica.<br />&bull; Fibra de vidrio, se har&aacute; un laminado utilizando desmoldantes.<br />&bull; Teor&iacute;a de Resinas.<br />&bull; Practica de encapsulados con resina cristal.<br />&bull; Teor&iacute;a de Silic&oacute;n de caucho.<br />&bull; Pr&aacute;ctica donde se har&aacute;n moldes flexibles sencillos de una sola cara (2D).<br />&bull; Pr&aacute;ctica de vaciados con resina y cargas.<br />&bull; Teor&iacute;a y muestra de usos y aplicaciones de espuma de poliuretano y m&aacute;s.<br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>MATERIAL&nbsp;</strong></span></p>\r\n<p>&Uacute;nicamente requerimos que traiga una foto de 10 x 15cm en papel fotografico, tambi&eacute;n una pieza para reproducir y otra para encapsular que sea de no m&aacute;s de 3cm.<br /><br /></p>\r\n<p><span style=\"font-size: 18px;\"><strong>COSTOS</strong></span></p>\r\n<p>El costo&nbsp; es de $670 por persona, incluye todos los materiales y equipo de seguridad para las pr&aacute;cticas.</p>\r\n<p>Es necesario apartar el lugar con un anticipo de m&iacute;nimo $150.00 pues tenemos cupo limitado.</p>', 'Del 5 al 9 de Agosto', 'Calle  Guanajuato #5B Col. San Francisquito. Quer&eacute;taro, Qro. ', '10:00am a 12:00pm', '', 'Curso poliester', '', '2019-07-15 11:27:47', '', 1),
(9, '', 'BORRADOR', '.', '', '', '', '', '', 'Curso poliester', '', '', '', 0),
(10, '', 'BORRADOS 2', 'BORRADOS 2', '', '', '', '', '', 'Curso poliester', '', '', '', 0),
(11, '', 'BORRADOS 2', 'BORRADOS 2', '', '', '', '', '', 'Curso poliester', '', '', '', 0),
(12, '', 'bORRADO 3', 'BORRADOR 3', '', '', '', '', '', 'Curso poliester', '', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_cursos_coment`
--

CREATE TABLE `fib_cursos_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_cursos_coment`
--

INSERT INTO `fib_cursos_coment` (`ID`, `ip`, `nombre`, `email`, `comentario`, `id_b`, `fecha`, `visible`) VALUES
(1, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'mherco@hotmail.com', 'Mensaje de prueba para noticias', 1, '2018-12-21 22:14:55', 1),
(2, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'mherco@hotmail.com', 'Mensaje de prueba para cursos.', 1, '2019-01-23 23:09:25', 1),
(3, '127.0.0.1', 'Miguel', 'mherco@hotmail.com', 'Mensaje de prueba', 1, '2019-01-26 20:00:32', 1),
(4, '127.0.0.1', 'Arturo', 'mherco@hotmail.com', 'Mensaje de prueba.', 2, '2019-01-26 20:06:03', 1),
(5, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'multiportal@outlook.com', 'Mensaje de prueba de comentario.', 2, '2019-01-28 22:09:21', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_depa`
--

CREATE TABLE `fib_depa` (
  `ID` int(2) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `list_depa` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `nivel` int(1) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_depa`
--

INSERT INTO `fib_depa` (`ID`, `nombre`, `list_depa`, `puesto`, `nivel`, `icono`, `visible`) VALUES
(1, 'Administrador', 'Administradores', 'Administrador', 1, '', 0),
(2, 'Edecan', 'Edecanes', 'Edecan', 2, '', 1),
(3, 'Modelo', 'Modelos', 'Modelo', 2, '', 0),
(4, 'Fotografo', 'Fotografos', 'Fotografo', 2, '', 1),
(5, 'Agencia', 'Agencias', 'Agencia', 2, '', 1),
(6, 'Escuela', 'Escuelas', 'Escuela', 2, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_directorio`
--

CREATE TABLE `fib_directorio` (
  `ID` int(6) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `url_link` varchar(300) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `des` varchar(250) NOT NULL,
  `filtro` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `fecha` varchar(22) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_galeria`
--

CREATE TABLE `fib_galeria` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` varchar(500) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_galeria`
--

INSERT INTO `fib_galeria` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `precio`, `cate`, `resena`, `url_page`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `visible`, `alta`, `fmod`, `user`) VALUES
(1, '', '', '10.jpg', '', 'Cursos en tu escuela', '0.00', 'Poliester', '<p>Descripcion</p>', '', '', '', '', '', '', 1, '2019-03-27 12:33:38', '', 'admin'),
(17, '', '', '2.jpg', '', 'Pigmentos S&oacute;lidos ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:13:34', '', 'admin'),
(16, '', '', '13.jpg', '', 'Curso M&aacute;scara de Fibra de Vidrio', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:12:20', '', 'admin'),
(15, '', '', 'hom3.jpg', '', 'T&eacute;cnicos Especializados', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:09:24', '', 'admin'),
(14, '', '', 'hom_.jpg', '', 'Manufactura', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:08:13', '', 'admin'),
(13, '', '', '17.jpg', '', 'Fibra de Vidrio', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:07:09', '', 'admin'),
(19, '', '', '1.jpg', '', 'Pigmentos Transl&uacute;cidos ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:59:14', '', 'admin'),
(20, '', '', '14.jpg', '', 'Mesas de Parota con R&iacute;o de Resina ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 13:59:51', '', 'admin'),
(21, '', '', '15.jpg', '', 'Mesas de Parota con R&iacute;o de Resina ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-05-31 14:00:25', '', 'admin'),
(22, '', '', '6.jpg', '', 'Encapsulado de Poli&eacute;ster. ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-06-25 12:14:37', '', 'admin'),
(23, '', '', '16.jpg', '', 'Encapsulados', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-06-25 15:07:03', '', 'admin'),
(24, '', '', 'joyeria.jpg', '', 'Joyer&iacute;a con Resina', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-06-25 15:07:48', '', 'admin'),
(25, '', '', 'ri.jpg', '', 'Crea con Fibrecen. ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-06-27 11:55:12', '', 'admin'),
(26, '', '', 'muchas-01.jpg', '', 'Crea con Fibrecen. ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-06-27 12:00:10', '', 'admin'),
(27, '', '', 'muchas-02.jpg', '', 'Crea con Fibrecen. ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-06-27 12:03:28', '', 'admin'),
(30, '', '', 'alumnos.jpg', '', 'Crea con Fibrecen. ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-07-15 11:51:40', '', 'admin'),
(31, '', '', 'ima.jpg', '', 'Fibrecen en la CONQUE 2019', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-07-15 11:53:52', '', 'admin'),
(32, '', '', 'encapsuladon.jpg', '', 'Encapsulado de Poli&eacute;ster. ', '0.00', 'Poliester', '', '', '', '', '', '', '', 1, '2019-07-15 11:54:44', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_glosario`
--

CREATE TABLE `fib_glosario` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_glosario`
--

INSERT INTO `fib_glosario` (`ID`, `cover`, `nom`, `descripcion`, `visible`) VALUES
(1, '', 'Curado', '<p>Proceso qu&iacute;mico posterior al gelado, donde la resina se solidifica por completo gracias a la reacci&oacute;n exot&eacute;rmica provocada por adici&oacute;n de un catalizador, dando lugar a un producto que no cambia sus propiedades.</p>', 1),
(2, '', 'Gelado', '<p><span>Proceso que forma parte del curado en el cual la resina cambia su aspecto l&iacute;quido a uno gelatinoso. Ocasionado por la reacci&oacute;n qu&iacute;mica de adici&oacute;n de catalizador.</span></p>', 1),
(3, '', 'Exotermia', '<p><span>Reacci&oacute;n qu&iacute;mica en la cual se produce energ&iacute;a desprendiendo luz o calor.</span></p>', 1),
(4, '', 'Resistencia Qu&iacute;mica', '<p><span>Describe la capacidad de resistencia de los materiales a los diferentes agentes qu&iacute;micos.</span></p>', 1),
(5, '', 'Resistencia Mec&aacute;nica', '<p><span>La resistencia mec&aacute;nica es la capacidad de los cuerpos para resistir las fuerzas aplicadas sin romperse.</span></p>', 1),
(6, '', 'Tac', '<p><span>Termino coloquial que se da cuando el curado de la resina no es correcto y el producto final tiene el aspecto pegajoso al tacto.</span></p>', 1),
(7, '', 'Resina  Virgen', '<p><span>Se refiere a toda ausencia de acelerantes y diluyentes. En &eacute;ste estado no se puede polimerizar.</span></p>', 1),
(8, '', 'Pre-acelerado', '<p><span>Sustancia que est&aacute; previamente acelerada, y solo necesita agregarle catalizador para la polimerizaci&oacute;n.</span></p>', 1),
(9, '', 'Polimerizaci&oacute;n', '<p><span>Proceso qu&iacute;mico en el cual un pol&iacute;mero pasa de estado l&iacute;quido a s&oacute;lido.</span></p>', 1),
(10, '', 'Tixotropia', '<p><span>Capacidad de una resina para que no escurra en posici&oacute;n vertical.</span></p>', 1),
(11, '', 'Viscosidad', '<p><span>Apariencia de una sustancia l&iacute;quida refiri&eacute;ndose a su espesor. A mayor viscosidad menor velocidad de impregnaci&oacute;n y menor facilidad de desburbujeo. A temperaturas bajas aumenta la viscosidad.</span></p>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_histo_backupdb`
--

CREATE TABLE `fib_histo_backupdb` (
  `ID` int(9) UNSIGNED NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `archivo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_histo_backupdb`
--

INSERT INTO `fib_histo_backupdb` (`ID`, `fecha`, `archivo`) VALUES
(1, '2019-04-04 09:40:24', '/db-backup-fibrecen_main-20190404-094024.sql');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_home_config`
--

CREATE TABLE `fib_home_config` (
  `ID` int(1) UNSIGNED NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `selc` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_home_config`
--

INSERT INTO `fib_home_config` (`ID`, `titulo`, `contenido`, `selc`, `activo`) VALUES
(1, 'Fibrecen - Home1', '<div class=\"container\">\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-5 bt-bottom-info\" data-aos=\"fade-right\">\r\n<h5 style=\"font-size: 42px; font-weight: bold;\">&iexcl;Bienvenido!</h5>\r\n</div>\r\n<div class=\"col-lg-7 bt-bottom-info\" data-aos=\"fade-left\">\r\n<p style=\"font-size: 29px;\">Nosotros MOLDEAMOS SOLUCIONES junto contigo transformamos una idea en PROYECTOS A GRAN ESCALA.</p>\r\n</div>\r\n</div>\r\n<!--\r\n            <div class=\"row inner-sec-wthree\">\r\n                <div class=\"col-lg-4 bottom-sub-grid text-center aos-init aos-animate\" data-aos=\"zoom-in\">\r\n                    <div class=\"bt-icon\">\r\n                        <span class=\"fas fa-trophy\"></span>\r\n                    </div>\r\n\r\n                    <h4 class=\"sub-tittle\">Humility</h4>\r\n                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n                    <p>\r\n                        <a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    </p>\r\n                </div>\r\n                <div class=\"col-lg-4 bottom-sub-grid text-center aos-init aos-animate\" data-aos=\"zoom-in\">\r\n                    <div class=\"bt-icon\">\r\n                        <span class=\"far fa-thumbs-up\"></span>\r\n                    </div>\r\n\r\n                    <h4 class=\"sub-tittle\">Passion</h4>\r\n                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n                    <p>\r\n                        <a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    </p>\r\n                </div>\r\n                <div class=\"col-lg-4 bottom-sub-grid text-center aos-init aos-animate\" data-aos=\"zoom-in\">\r\n                    <div class=\"bt-icon\">\r\n                        <span class=\"fas fa-tv\"></span>\r\n                    </div>\r\n\r\n                    <h4 class=\"sub-tittle\">Honesty</h4>\r\n                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n                    <p>\r\n                        <a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    </p>\r\n                </div>\r\n            </div>\r\n--></div>\r\n<!-- /stats ->\r\n    <section class=\"stats_test container-fluid\">\r\n\r\n        <div class=\"row inner_stat\">\r\n            <div class=\"col-lg-6 stats-left-info\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6 stats_left counter_grid\">\r\n                        <i class=\"far fa-building\"></i>\r\n                        <p class=\"counter\">32</p>\r\n                        <h4>A&ntilde;os De Experiencia</h4>\r\n                    </div>\r\n                    <div class=\"col-md-6 stats_left counter_grid1\">\r\n                        <i class=\"far fa-edit\"></i>\r\n                        <p class=\"counter\">3840</p>\r\n                        <h4>Proyectos Realizados</h4>\r\n                    </div>\r\n                    <div class=\"col-md-6 stats_left counter_grid2\">\r\n                        <i class=\"far fa-smile\"></i>\r\n                        <p class=\"counter\">10879</p>\r\n                        <h4>Clientes Atendidos</h4>\r\n                    </div>\r\n                    <div class=\"col-md-6 stats_left counter_grid3\">\r\n                        <i class=\"fas fa-users\"></i>\r\n                        <p class=\"counter\">0</p>\r\n                        <h4>Visitas</h4>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-lg-6 stats-right-img\">\r\n\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!-- //stats -->\r\n<p>&nbsp;</p>\r\n<!--/services-->\r\n<div class=\"container\">\r\n<h3 class=\"tittle\">Crea Con Nosotros</h3>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-left\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes rigidos y piezas de fibra de vidrio</h5>\r\n<p>Crea piezas de gran tama&ntilde;o y poco detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Vaciado, encapsulados y roto moldeo</h5>\r\n<p>Crea figuras a partir de tus moldes.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-4 grid_info_main mid\" data-aos=\"flip-down\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/ab02.jpg\" alt=\" \" /></div>\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-right\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes flexibles</h5>\r\n<p>Crea piezas peque&ntilde;as con gran detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Resina para fotograf&iacute;a</h5>\r\n<p>Recubre tu fotograf&iacute;a con un acabado unico.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!--//services->\r\n    <section class=\"grids_sec_2\">\r\n        <div class=\"row style-grids_main\">\r\n            <div class=\"col-md-6 grid_sec_info\">\r\n                <div class=\"style-grid-2-text_info\" data-aos=\"fade-right\">\r\n                    <h3>\r\n                        Turning big ideas into great products.</h3>\r\n                    <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur.sed quia non numquam eius modi tempora incidunt ut labore .Lorem ipsum dolor sit amet,vehicula vel sapien et.</p>\r\n                    <div class=\"bnr-button\">\r\n                        <button type=\"button\" class=\"btn btn-primary play\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n							<i class=\"fas fa-play\"></i>\r\n						</button>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-6 style-image-2\">\r\n            </div>\r\n            <div class=\"col-md-6 style-image-2 second\">\r\n            </div>\r\n            <div class=\"col-md-6 grid_sec_info\">\r\n                <div class=\"style-grid-2-text_info\" data-aos=\"fade-right\">\r\n                    <h3>\r\n                        Turning big ideas into great products.</h3>\r\n                    <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur.sed quia non numquam eius modi tempora incidunt ut labore .Lorem ipsum dolor sit amet,vehicula vel sapien et</p>\r\n                    <div class=\"bnr-button\">\r\n                        <button type=\"button\" class=\"btn btn-primary play\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n							<i class=\"fas fa-play\"></i>\r\n						</button>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </section>\r\n    <!---->\r\n<p>&nbsp;</p>\r\n<!--/Projects->\r\n    <section class=\"services\">\r\n        <div class=\"container\">\r\n            <h3 class=\"tittle\">Our Latest Projects\r\n            </h3>\r\n            <div class=\"row inner-sec-wthree\">\r\n\r\n                <div class=\"col-md-4 proj_gallery_grid\" data-aos=\"zoom-in\">\r\n                    <div class=\"section_1_gallery_grid\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g1.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g1.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g2.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g2.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g3.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g3.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4 proj_gallery_grid\" data-aos=\"zoom-in\">\r\n                    <div class=\"section_1_gallery_grid\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g4.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g4.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g5.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g5.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g6.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g6.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4 proj_gallery_grid\" data-aos=\"zoom-in\">\r\n                    <div class=\"section_1_gallery_grid\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g7.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g7.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g8.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g8.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"section_1_gallery_grid\" data-aos=\"zoom-in\">\r\n                        <a title=\"Donec sapien massa, placerat ac sodales ac, feugiat quis est.\" href=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g9.jpg\">\r\n                            <div class=\"section_1_gallery_grid1\">\r\n                                <img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/g9.jpg\" alt=\" \" class=\"img-fluid\" />\r\n                                <div class=\"proj_gallery_grid1_pos\">\r\n                                    <h3>Smelter</h3>\r\n                                    <p>Add some text</p>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!--//Projects-->', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_home_elementos`
--

CREATE TABLE `fib_home_elementos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `tit1` text NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tit2` text NOT NULL,
  `exp` varchar(12) NOT NULL,
  `pro` varchar(12) NOT NULL,
  `cli` varchar(12) NOT NULL,
  `txt1` text NOT NULL,
  `txt2` text NOT NULL,
  `txt3` text NOT NULL,
  `txt4` text NOT NULL,
  `ima0` varchar(100) NOT NULL,
  `ima1` varchar(100) NOT NULL,
  `ima2` varchar(100) NOT NULL,
  `ima3` varchar(100) NOT NULL,
  `ima4` varchar(100) NOT NULL,
  `ima5` varchar(100) NOT NULL,
  `ima6` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_home_elementos`
--

INSERT INTO `fib_home_elementos` (`ID`, `tit1`, `foto`, `tit2`, `exp`, `pro`, `cli`, `txt1`, `txt2`, `txt3`, `txt4`, `ima0`, `ima1`, `ima2`, `ima3`, `ima4`, `ima5`, `ima6`) VALUES
(1, '<h5 style=\"font-size: 42px; font-weight: bold;\">&iexcl;Bienvenido!</h5>', '', '<p style=\"font-size: 29px;\">Nosotros MOLDEAMOS SOLUCIONES junto contigo transformamos una idea en PROYECTOS A GRAN ESCALA.</p>', '32', '3840', '10879', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes r&iacute;gidos y piezas de fibra de vidrio</h5>\r\n<p>Crea piezas de gran tama&ntilde;o y poco detalle.</p>', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Vaciado, encapsulados y roto moldeo</h5>\r\n<p>Crea figuras a partir de tus moldes.</p>', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes Flexibles</h5>\r\n<p>Crea piezas peque&ntilde;as con gran detalle.</p>', '<h5 style=\"text-transform: capitalize; color: #1f205a;\">Resina Para Fotograf&iacute;a</h5>\r\n<p>Recubre tu fotograf&iacute;a con un acabado &uacute;nico.</p>', '', 'home46.jpg', 'MOSTRADOR.png', 'mascaras.jpg', '13.jpg', 'mooldesdflexiblez.jpg', 'foto67.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_iconos`
--

CREATE TABLE `fib_iconos` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `fa_icon` varchar(100) NOT NULL,
  `icon` text NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_iconos`
--

INSERT INTO `fib_iconos` (`ID`, `nom`, `fa_icon`, `icon`, `tipo`) VALUES
(1, 'Descarga', 'fa-download', '<i class=\"fa fa-download\"></i>', 'awesome'),
(2, 'Menu', 'fa-list', '<i class=\"fa fa-list\"></i>', 'awesome'),
(3, 'Configuracion', 'fa-gear', '<i class=\"fa fa-gear\"></i>', 'awesome'),
(4, 'Configuraciones', 'fa-gears', '<i class=\"fa fa-gears\"></i>', 'awesome'),
(5, 'Modulos', 'fa-cubes', '<i class=\"fa fa-cubes\"></i>', 'awesome'),
(6, 'Home', 'fa-home', '<i class=\"fa fa-home\"></i>', 'awesome'),
(7, 'Portafolio', 'fa-briefcase', '<i class=\"fa fa-briefcase\"></i>', 'awesome'),
(8, 'Blog', 'fa-comments', '<i class=\"fa fa-comments\"></i>', 'awesome'),
(9, 'BlockIP', 'fa-crosshairs', '<i class=\"fa fa-crosshairs\"></i>', 'awesome'),
(10, 'Estadisticas', 'fa-bar-chart', '<i class=\"fa fa-bar-chart\"></i>', 'awesome'),
(11, 'Moneda', 'fa-usd', '<i class=\"fa fa-usd\"></i>', 'awesome'),
(12, 'Dashboard', 'fa-dashboard', '<i class=\"fa fa-dashboard\"></i>', 'awesome'),
(13, 'Usuario', 'fa-user', '<i class=\"fa fa-user\"></i>', 'awesome'),
(14, 'Usuarios', 'fa-users', '<i class=\"fa fa-users\"></i>', 'awesome'),
(15, 'Global', 'fa-globe', '<i class=\"fa fa-globe\"></i>', 'awesome'),
(16, 'Ver', 'fa-eye', '<i class=\"fa fa-eye\"></i>', 'awesome'),
(17, 'Enviar', 'fa-send-o', '<i class=\"fa fa-send-o\"></i>', 'awesome'),
(18, 'Mail', 'fa-envelope', '<i class=\"fa  fa-envelope\"></i>', 'awesome'),
(19, 'Marca de Mapa', 'fa-map-marker', '<i class=\"fa  fa-map-marker\"></i>', 'awesome'),
(20, 'Formularios', 'fa-pencil-square-o', '<i class=\"fa  fa-pencil-square-o\"></i>', 'awesome'),
(21, 'Carrito', 'fa-shopping-cart', '<i class=\"fa fa-shopping-cart\"></i>', 'awesome'),
(22, 'Folder Open Blanco', 'fa-folder-open-o', '<i class=\"fa fa-folder-open-o\"></i>', 'awesome'),
(23, 'Folder Open', 'fa-folder-open', '<i class=\"fa fa-folder-open\"></i>', 'awesome'),
(24, 'Tesmoniales', 'fa-commenting', '<i class=\"fa fa-commenting\"></i>', 'awesome'),
(25, 'Clientes', 'fa-child', '<i class=\"fa fa-child\"></i>', 'awesome'),
(26, 'Mapa', 'fa-map', '<i class=\"fa fa-map\" aria-hidden=\"true\"></i>', 'awesome'),
(27, 'Sitemap', 'fa-sitemap', '<i class=\"fa fa-sitemap\"></i>', 'awesome'),
(28, 'Check Square', 'fa-check-square', '<i class=\"fa fa-check-square\"></i>', 'awesome'),
(29, 'Play', 'fa-caret-square-o-right ', '<i class=\"fa fa-caret-square-o-right \"></i>', 'awesome'),
(30, 'Lupa', 'fa-search', '<i class=\"fa fa-search\"></i>', 'awesome'),
(31, 'Book', 'fa-book', '<i class=\"fa fa-book\"></i>', 'awesome'),
(32, 'Saludo', 'fa-handshake-o', '<i class=\"fa fa-handshake-o\"></i>', 'awesome'),
(33, 'Imagen', 'fa-file-image-o', '<i class=\"fa fa-file-image-o\"></i>', 'awesome'),
(34, 'Universidad', 'fa-university', '<i class=\"fa fa-university\"></i>', 'awesome'),
(35, 'Birrete', 'fa-graduation-cap', '<i class=\"fa fa-graduation-cap\"></i>', 'awesome'),
(36, 'Herramienta', 'fa-wrench', '<i class=\"fa fa-wrench\"></i>', 'awesome'),
(37, 'Señas', 'fa-american-sign-language-interpreting', '<i class=\"fa fa-american-sign-language-interpreting\"></i>', 'awesome'),
(38, 'Imagen', 'fa-picture-o', '<i class=\"fa fa-picture-o\"></i>', 'awesome'),
(39, 'Lista top', 'fa-list-alt', '<i class=\"fa fa-list-alt\"></i>', 'awesome');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_ipbann`
--

CREATE TABLE `fib_ipbann` (
  `ID` int(11) NOT NULL,
  `ip` varchar(256) NOT NULL DEFAULT '',
  `bloqueo` tinyint(1) NOT NULL,
  `alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_ipbann`
--

INSERT INTO `fib_ipbann` (`ID`, `ip`, `bloqueo`, `alta`) VALUES
(1, '127.0.0.5', 0, '2017-10-17 04:55:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_landingpage_seccion`
--

CREATE TABLE `fib_landingpage_seccion` (
  `ID` int(6) UNSIGNED NOT NULL,
  `seccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tit` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `conte` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `url_page` varchar(300) NOT NULL,
  `visible` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_landingpage_seccion`
--

INSERT INTO `fib_landingpage_seccion` (`ID`, `seccion`, `tit`, `conte`, `url_page`, `visible`) VALUES
(1, '', 'Nuevo Landing', '<p style=\"text-align: center;\"><span style=\"font-size: 36px; color: #ff0000;\">Nuevo Landing</span></p>\r\n<p>Contenido del landingpage</p>', 'http://www.fibrecen.com.mx/2019/landingpage/fibrecen/1-Nuevo-Landing', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_map_config`
--

CREATE TABLE `fib_map_config` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `lng` varchar(20) NOT NULL,
  `zoom` varchar(2) NOT NULL,
  `cover` varchar(50) NOT NULL,
  `on_costo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_map_config`
--

INSERT INTO `fib_map_config` (`ID`, `nom`, `lat`, `lng`, `zoom`, `cover`, `on_costo`) VALUES
(1, 'Querétaro', '20.5931297', '-100.3920483', '12', 'g_intelmex.png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_map_ubicacion`
--

CREATE TABLE `fib_map_ubicacion` (
  `ID` int(9) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `adres` varchar(150) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `info` varchar(250) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `tel` varchar(30) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nivel` varchar(2) NOT NULL,
  `rol` varchar(2) NOT NULL,
  `lat` varchar(15) NOT NULL,
  `lng` varchar(15) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_map_ubicacion`
--

INSERT INTO `fib_map_ubicacion` (`ID`, `nom`, `adres`, `descripcion`, `info`, `precio`, `tel`, `cover`, `nivel`, `rol`, `lat`, `lng`, `alta`, `fmod`, `visible`, `activo`) VALUES
(1, 'Intelmex', 'Calle 1 303, Jurica, 76130 Santiago de Querétaro, Qro.', '', 'Reparación de telefonos', '0.00', '4421234567', 'nodisponible.jpg', '1', '3', '20.6500317', '-100.4290312', '2018-04-03 13:44:50', '2018-04-03 13:59:06', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_menu_admin`
--

CREATE TABLE `fib_menu_admin` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom_menu` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `nivel` int(1) NOT NULL,
  `ID_menu_adm` int(2) NOT NULL,
  `ID_mod` int(2) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_menu_admin`
--

INSERT INTO `fib_menu_admin` (`ID`, `nom_menu`, `icono`, `link`, `nivel`, `ID_menu_adm`, `ID_mod`, `visible`) VALUES
(1, 'Config. Sistema', 'fa-gear', 'index.php?mod=sys&ext=admin/index', -1, 0, 9, 1),
(2, 'Modulos', 'fa-cubes', 'index.php?mod=sys&ext=modulos', -1, 0, 0, 1),
(3, 'Logs', 'fa-globe', 'index.php?mod=sys&ext=admin/index&opc=logs', -1, 0, 9, 1),
(4, 'Bloquear IP', 'fa-crosshairs', 'index.php?mod=sys&ext=admin/index&opc=bloquear', -1, 0, 9, 1),
(5, 'Temas', 'fa-sticky-note-o', 'index.php?mod=sys&ext=admin/index&opc=temas', -1, 0, 9, 1),
(6, 'Admin. Usuarios', 'fa-users', 'index.php?mod=usuarios&ext=admin/index', -1, 0, 4, 0),
(7, 'Menu Admin', 'fa-list', 'index.php?mod=sys&ext=menu_admin', -1, 0, 9, 1),
(8, 'Iconos', 'fa-smile-o', 'index.php?mod=sys&ext=admin/index&opc=iconos', -1, 0, 9, 1),
(9, 'Informe de Visitas', 'fa-download', 'index.php?mod=estadisticas&ext=admin/index', 1, 0, 10, 1),
(10, 'Backup DB', 'fa-download', 'index.php?mod=sys&ext=backup', -1, 0, 9, 1),
(11, 'Config. Mailbox', 'fa-gear', 'index.php?mod=mailbox&ext=admin/index', 1, 0, 12, 1),
(12, 'Mensajes', 'fa-envelope', 'index.php?mod=mailbox', 1, 0, 12, 1),
(13, 'Editar', 'fa-home', 'index.php?mod=Home&ext=admin/index', 1, 0, 3, 1),
(14, 'Menu Web', 'fa-list', 'index.php?mod=Home&ext=admin/index&opc=menu_web', 1, 0, 3, 1),
(15, 'Admin productos', 'fa-shopping-cart', 'index.php?mod=productos&ext=admin/index&opc=producto', 1, 0, 15, 1),
(16, 'Categoria de productos', 'fa-folder-open', 'index.php?mod=productos&ext=admin/index&opc=categoria', 1, 0, 15, 1),
(17, 'Subcategoria de productos', 'fa-folder-open-o', 'index.php?mod=productos&ext=admin/index&opc=subcategoria', 1, 0, 15, 1),
(18, 'Config. Gmaps', 'fa-gear', 'index.php?mod=gmaps&ext=admin/index', 1, 0, 16, 1),
(19, 'Ubicaciones', 'fa-map-marker', 'index.php?mod=gmaps&ext=admin/index&opc=ubicaciones', 1, 0, 16, 1),
(20, 'Config. Contacto', 'fa-gear', 'index.php?mod=contacto&ext=admin/index', 1, 0, 8, 1),
(21, 'Correos de Formulario', 'fa-pencil-square-o', 'index.php?mod=contacto&ext=admin/index&opc=forms', 1, 0, 8, 1),
(22, 'Generador Sitemap', 'fa-sitemap', 'index.php?mod=sys&ext=admin/index&opc=sitemap', 1, 0, 9, 1),
(23, 'Opciones', 'fa-gears', 'index.php?mod=sys&ext=opciones', 1, 0, 9, 1),
(24, 'Slider', 'fa-caret-square-o-right ', 'index.php?mod=Home&ext=admin/index&opc=slider', 1, 0, 3, 1),
(25, 'Licencia', 'fa-eye', 'index.php?mod=sys&ext=licencia', -1, 0, 9, 1),
(26, 'Portafolio', 'fa-briefcase', 'index.php?mod=portafolio&ext=admin/index', 1, 0, 20, 1),
(27, 'Editar', 'fa-pencil-square-o', 'index.php?mod=glosario&ext=admin/index', 1, 0, 23, 0),
(28, 'Definiciones', 'fa-list', 'index.php?mod=glosario&ext=admin/index&opc=def', 1, 0, 23, 1),
(29, 'Testimonios', 'fa-child', 'index.php?mod=Home&ext=admin/index&opc=testimonios', 1, 0, 3, 1),
(30, 'Elementos', 'fa-folder-open', 'index.php?mod=Home&ext=admin/index&opc=elementos&action=form&ctrl=edit&id=1', 1, 0, 3, 1),
(31, 'Editar', 'fa-pencil-square-o', 'index.php?mod=nosotros&ext=admin/index&action=editar', 1, 0, 5, 1),
(32, 'Imagenes Top', 'fa-file-image-o', 'index.php?mod=Home&ext=admin/index&opc=ima_top', 1, 0, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_menu_web`
--

CREATE TABLE `fib_menu_web` (
  `ID` int(6) UNSIGNED NOT NULL,
  `menu` varchar(50) NOT NULL,
  `url` varchar(254) NOT NULL,
  `modulo` varchar(100) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `ord` varchar(2) NOT NULL,
  `subm` varchar(3) NOT NULL,
  `tit_sec` varchar(100) NOT NULL,
  `ima_top` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_menu_web`
--

INSERT INTO `fib_menu_web` (`ID`, `menu`, `url`, `modulo`, `ext`, `ord`, `subm`, `tit_sec`, `ima_top`, `visible`) VALUES
(1, 'Nosotros', 'nosotros/', 'nosotros', '', '1', '', '', 'nosotros1.jpg', 1),
(2, 'Productos', 'productos/', 'productos', '', '2', '', '', 'producots.jpg', 1),
(3, 'Cursos', '#', 'cursos', '', '3', '', '', '99.jpg', 1),
(4, 'Como Se Hace', 'comosehace/', 'comosehace', '', '4', '', 'Videos Tutoriales', 'comosehace.jpg', 1),
(5, 'Manufactura', '#', 'manufactura', '', '5', '', '', 'manu78.jpg', 1),
(6, 'Blog', 'blog/', 'blog', '', '6', '', '', 'hom4.jpg', 0),
(7, 'Contacto', 'contacto/', 'contacto', '', '7', '', 'Contactanos', 'contactos89-01.jpg', 1),
(8, 'Galer&iacute;a', 'galeria/', 'galeria', '', '8', '', '', 'medidor.jpg', 1),
(9, 'Cursos Poli&eacute;ster', 'cursos/poliester/', 'cursos', 'poliester', '9', '3', '', '', 1),
(10, 'Cursos Ep&oacute;xicos', 'cursos/epoxicos/', 'cursos', 'epoxicos', '10', '3', '', '', 1),
(11, 'Poliester', 'manufactura/poliester/', 'manufactura', 'poliester', '11', '5', 'Manufactura Poli&eacute;ster', '', 1),
(12, 'Ep&oacute;xicos', 'manufactura/epoxicos/', 'manufactura', 'epoxicos', '12', '5', 'Manufactura Ep&oacute;xicos', '', 1),
(13, 'Buscar', 'index.php?mod=search', 'search', '', '13', '', 'Busqueda', 'home1.jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_mode_page`
--

CREATE TABLE `fib_mode_page` (
  `ID` int(2) UNSIGNED NOT NULL,
  `page_mode` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_mode_page`
--

INSERT INTO `fib_mode_page` (`ID`, `page_mode`) VALUES
(1, 'page'),
(2, 'landingpage'),
(3, 'extranet'),
(4, 'ecommerce'),
(5, 'CRM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_modulos`
--

CREATE TABLE `fib_modulos` (
  `ID` int(6) NOT NULL,
  `nombre` varchar(25) NOT NULL DEFAULT '',
  `modulo` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL,
  `dashboard` tinyint(1) NOT NULL,
  `nivel` tinyint(4) NOT NULL DEFAULT '0',
  `home` tinyint(4) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `sname` varchar(10) NOT NULL DEFAULT '',
  `icono` varchar(50) NOT NULL,
  `link` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_modulos`
--

INSERT INTO `fib_modulos` (`ID`, `nombre`, `modulo`, `description`, `dashboard`, `nivel`, `home`, `visible`, `activo`, `sname`, `icono`, `link`) VALUES
(1, 'admin', 'admin', '', 0, 0, 0, 0, 0, 'false', '', ''),
(2, 'Dashboard', 'dashboard', '', 1, -1, 0, 0, 1, 'false', '', 'index.php?mod=dashboard'),
(3, 'Home', 'Home', 'Administración y gestión del Home.', 0, 0, 1, 1, 1, 'false', 'fa-home', 'index.php?mod=Home'),
(4, 'Usuarios', 'usuarios', 'Administación y gestión de usuarios.', 0, -1, 0, 1, 1, 'false', 'fa-users', 'index.php?mod=usuarios'),
(5, 'Nosotros', 'nosotros', 'Administración del contenido del modulo de nosotros.', 0, 0, 0, 1, 1, 'false', 'fa-users', 'index.php?mod=nosotros'),
(6, 'Portafolio', 'portafolio', 'Administración y gestión del portafolio.', 0, 1, 0, 0, 1, 'false', 'fa-briefcase', 'index.php?mod=portafolio'),
(7, 'Blog', 'blog', 'Administración del contenido del modulo de blog.', 0, 0, 0, 0, 0, 'false', 'fa-comments', 'index.php?mod=blog'),
(8, 'Contacto', 'contacto', 'Consultas del modulo de contacto.', 0, 0, 0, 1, 1, 'false', 'fa-map-marker', 'index.php?mod=contacto'),
(9, 'Sistema', 'sys', 'Configuración y administración del sistema.', 1, -1, 0, 1, 1, 'false', 'fa-gear', 'index.php?mod=sys'),
(10, 'Estadistica', 'estadisticas', 'Estadisticas de trafico. ', 0, -1, 0, 1, 1, 'false', 'fa-bar-chart', 'index.php?mod=estadisticas'),
(11, 'Formularios', 'forms', 'Administracion de Formularios para la web.', 1, 1, 0, 0, 0, 'false', 'fa-pencil-square-o', 'index.php?mod=forms'),
(12, 'Mailbox', 'mailbox', 'Mailbox de formularios', 1, 1, 0, 1, 1, 'false', ' fa-envelope', 'index.php?mod=mailbox'),
(13, 'Ecommerce', 'ecommerce', 'Administración y gestión del modulo ecommerce.', 0, 1, 0, 0, 0, 'false', 'fa-shopping-cart', 'index.php?mod=ecommerce'),
(14, 'Marketing', 'marketing', '', 0, 1, 0, 0, 0, 'false', 'fa-globe', 'index.php?mod=marketing'),
(15, 'Productos', 'productos', 'Administración de productos', 0, 1, 0, 1, 1, 'false', 'fa-shopping-cart', 'index.php?mod=productos'),
(16, 'Gmaps', 'gmaps', 'Mapas de Google', 0, 0, 0, 0, 0, 'false', 'fa-map', 'index.php?mod=gmaps'),
(17, 'Chat', 'chat', 'Administración del modulo chat.', 0, 1, 0, 0, 1, 'false', 'fa-commenting', 'index.php?mod=chat'),
(18, 'Directorio', 'directorio', 'Administrador del modulo de Directorio.', 0, 1, 0, 0, 0, 'false', 'fa-globe', 'index.php?mod=directorio'),
(19, 'Cursos', 'cursos', 'Administrador del modulo cursos', 0, 0, 0, 1, 1, 'false', 'fa-university', 'index.php?mod=cursos'),
(20, 'Manufactura', 'manufactura', 'Administrador del modulo Manufactura', 0, 0, 0, 1, 1, 'false', 'fa-handshake-o', 'index.php?mod=manufactura'),
(21, 'Como se hace', 'comosehace', 'Administrador del modulo Como se hace', 0, 0, 0, 0, 1, 'false', 'fa-american-sign-language-interpreting', 'index.php?mod=comosehace'),
(22, 'Buscar', 'search', 'Buscador', 0, 0, 0, 0, 1, 'false', 'fa-eye', 'index.php?mod=search'),
(23, 'Glosario', 'glosario', 'Administrador del modulo glosario', 0, 1, 0, 1, 1, 'false', 'fa-book', 'index.php?mod=glosario'),
(24, 'Galeria', 'galeria', 'Administrador del modulo galeria', 0, 1, 0, 1, 1, 'true', 'fa-picture-o', 'index.php?mod=galeria'),
(25, 'LandingPage', 'landingpage', 'Administrador del modulo landingpage', 0, 1, 0, 1, 1, 'false', 'fa-folder-open', 'index.php?mod=landingpage');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_nosotros`
--

CREATE TABLE `fib_nosotros` (
  `ID` int(9) UNSIGNED NOT NULL,
  `historia` text NOT NULL,
  `mision` text NOT NULL,
  `vision` text NOT NULL,
  `valores` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_nosotros`
--

INSERT INTO `fib_nosotros` (`ID`, `historia`, `mision`, `vision`, `valores`) VALUES
(1, '<p style=\"text-align: justify;\"><strong><span style=\"font-size: 16px;\">Somos una empres</span><span style=\"font-size: 16px;\">a con m&aacute;s de 30 a&ntilde;os en el mercado en constante crecimiento, nuestra ubicaci&oacute;n nos permite ofrecer un servicio eficiente principalmente en la zona baj&iacute;o y actualmente nuestra marca es reconocida a nivel nacional.</span></strong></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">En FIBRECEN trabajamos de manera sostenible con el fin de generar valor a nuestros clientes, proveedores y colaboradores. Por lo que nuestro equipo ha enfocado su esfuerzo en desarrollar mejoras continuas para proveer un servicio y productos de alta calidad, as&iacute; mismo actuamos con responsabilidad en los procesos para mantener una relaci&oacute;n sana y amigable con el medio ambiente.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Entendemos las necesidades de nuestros clientes, as&iacute; que vamos m&aacute;s all&aacute; del producto, gracias a nuestra experiencia tenemos la capacidad de ofrecer y crear soluciones apropiadas enfocadas al proyecto espec&iacute;fico de cada cliente en las diversas industrias: Automotriz, Artesanal, Construcci&oacute;n, Aeron&aacute;utica, Acu&aacute;tica, Industrial, Pinturas, y m&aacute;s.</p>', '<p style=\"font-size: 16px; text-align: justify;\">Ser el proveedor m&aacute;s reconocido y especializado en la industria de los materiales compuestos (resinas y fibras de vidrio), ofreciendo productos de alta calidad y asesor&iacute;a a trav&eacute;s de un grupo humano comprometido a dar el mejor servicio, para contribuir al desarrollo exitoso de nuestros clientes, colaboradores y del pa&iacute;s.</p>', '<p style=\"font-size: 16px; text-align: justify;\">Ser una empresa l&iacute;der y reconocida en el mercado nacional como proveedores de materiales compuestos, generando valor y ventaja competitiva sostenible a sus clientes y colaboradores. FIBRECEN se consolida como una empresa l&iacute;der en el mercado nacional de resinas sint&eacute;ticas y fibras de vidrio, ofreciendo productos de alta calidad, a precios competitivos y con un servicio excepcional.</p>', '<p style=\"font-size: 16px; text-align: justify;\">Honestidad, Calidad, Confianza y Servicio son los pilares para fortalecer la relaci&oacute;n de nuestros clientes y sus proyectos.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_notificacion`
--

CREATE TABLE `fib_notificacion` (
  `ID` int(11) UNSIGNED NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_user2` int(11) NOT NULL,
  `nombre_envio` varchar(255) NOT NULL,
  `mensaje` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `fecha` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_notificacion`
--

INSERT INTO `fib_notificacion` (`ID`, `ID_user`, `ID_user2`, `nombre_envio`, `mensaje`, `visto`, `activo`, `fecha`) VALUES
(1, 1, 0, 'admin', 'Mensaje de prueba', 1, 1, ''),
(2, 1, 0, 'admin', 'Se ha actualizado el modulo Home', 1, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_opciones`
--

CREATE TABLE `fib_opciones` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `valor` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_opciones`
--

INSERT INTO `fib_opciones` (`ID`, `nom`, `descripcion`, `valor`) VALUES
(1, 'google_analytics', '', '0'),
(2, 'form_registro', '', '0'),
(3, 'geo_loc_visitas', '', '0'),
(4, 'slide_active', '', '1'),
(5, 'API_facebook', '', '0'),
(6, 'API_google_maps', '', '0'),
(7, 'api_noti_chrome', '', '0'),
(8, 'link_var', '', '0'),
(9, 'link_productos', '', '0'),
(10, 'tiny_text_des', '', '0'),
(11, 'email_test', '', '1'),
(12, 'skin_AdminLTE', '', 'blue'),
(13, 'mini_bar_AdminLTE', '', '0'),
(14, 'wordpress', '', '0'),
(15, 'bar_login', '', '0'),
(16, 'bar_productos', '', '1'),
(17, 'toogle_nombre', '', '0'),
(18, 'mostrar_precio', '', '0'),
(19, 'mostrar_nombre', '', '0'),
(20, 'mostrar_des_corta', '', '0'),
(21, 'mostrar_des', '', '1'),
(22, 'mostrar_galeria', '', '1'),
(23, 'b_vista_rapida', '', '0'),
(24, 'b_ver_pro', '', '0'),
(25, 'b_cotizar', '', '0'),
(26, 'b_cart', '', '0'),
(27, 'b_paypal', '', '0'),
(28, 'blog_coment', '', '1'),
(29, 'noticias_coment', '', '0'),
(30, 'cursos_coment', '', '0'),
(31, 'productos_coment', '', '0'),
(32, 'all_productos', '', '0'),
(33, 'e_rates', '', '0'),
(34, 'footer_dir', '', '0'),
(35, 'validacion_json', '', '0'),
(36, 'url_var_json', '', '0'),
(37, 'VUE2', '', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_pages`
--

CREATE TABLE `fib_pages` (
  `ID` int(6) UNSIGNED NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `tema` varchar(100) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `url` varchar(250) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_pages`
--

INSERT INTO `fib_pages` (`ID`, `titulo`, `contenido`, `tema`, `modulo`, `ext`, `url`, `fmod`, `alta`, `visible`, `activo`) VALUES
(1, 'Nosotros 1', '<p><br />&nbsp;</p>\r\n<!--/Nosotros-->\r\n<div class=\"container\">\r\n<h5>&nbsp;</h5>\r\n<h3 class=\"tittle\">Nosotros</h3>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-5 bt-bottom-info aos-init aos-animate\" data-aos=\"fade-right\">\r\n<h5>Historia</h5>\r\n<h5><span style=\"font-size: 10px;\"><br /></span></h5>\r\n<h5><span style=\"font-size: 10px;\"><br /></span></h5>\r\n</div>\r\n<div class=\"col-lg-7 bt-bottom-info aos-init aos-animate\" data-aos=\"fade-left\"><!--img src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/banner1.jpg\" class=\"img-fluid\" alt=\" \"-->\r\n<p><span style=\"font-size: 16px;\"><strong>Somos una empres</strong></span><span style=\"font-size: 16px;\"><strong>a con m&aacute;s de 30 a&ntilde;os en el mercado en constante crecimiento, nuestra ubicaci&oacute;n nos permite ofrecer un servicio eficiente principalmente en la zona baj&iacute;o y actualmente nuestra marca es reconocida a nivel nacional.</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>En FIBRECEN trabajamos de manera sostenible con el fin de generar valor a nuestros clientes, proveedores y colaboradores. Por lo que nuestro equipo ha enfocado su esfuerzo en desarrollar mejoras continuas para proveer un servicio y productos de alta calidad, as&iacute; mismo actuamos con responsabilidad en los procesos para mantener una relaci&oacute;n sana y amigable con el medio ambiente.</p>\r\n<p>&nbsp;</p>\r\n<p>Entendemos las necesidades de nuestros clientes, as&iacute; que vamos m&aacute;s all&aacute; del producto, gracias a nuestra experiencia tenemos la capacidad de ofrecer y crear soluciones apropiadas enfocadas al proyecto espec&iacute;fico de cada cliente en las diversas industrias: Automotriz, Artesanal, Construcci&oacute;n, Aeron&aacute;utica, Ac&uacute;atica, Industrial, Pinturas, y m&aacute;s.</p>\r\n</div>\r\n</div>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-4 bottom-sub-grid text-center aos-init\" data-aos=\"zoom-in\">\r\n<div class=\"bt-icon\">&nbsp;</div>\r\n<h4 class=\"sub-tittle\" style=\"font-size: 24px;\">Misi&oacute;n</h4>\r\n<p style=\"font-size: 16px;\">Ser el proveedor m&aacute;s reconocido y especializado en la industria de los materiales compuestos (resinas y fibras de vidrio), ofreciendo productos de alta calidad y asesor&iacute;a a trav&eacute;s de un grupo humano comprometido a dar el mejor servicio, para contribuir al desarrollo exitoso de nuestros clientes, colaboradores y del pa&iacute;s.</p>\r\n<!--p>\r\n                        	<a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    	</p--></div>\r\n<!-- /.col-lg-4 -->\r\n<div class=\"col-lg-4 bottom-sub-grid text-center aos-init\" data-aos=\"zoom-in\">\r\n<div class=\"bt-icon\">&nbsp;</div>\r\n<h4 class=\"sub-tittle\" style=\"font-size: 24px;\">Visi&oacute;n</h4>\r\n<p style=\"font-size: 16px;\">Ser una empresa l&iacute;der y reconocida en el mercado nacional como proveedores de materiales compuestos, generando valor y ventaja competitiva sostenible a sus clientes y colaboradores. FIBRECEN se consolida como una empresa l&iacute;der en el mercado nacional de resinas sint&eacute;ticas y fibras de vidrio, ofreciendo productos de alta calidad, a precios competitivos y con un servicio excepcional.</p>\r\n<!--p>\r\n                        	<a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    	</p--></div>\r\n<!-- /.col-lg-4 -->\r\n<div class=\"col-lg-4 bottom-sub-grid text-center aos-init\" data-aos=\"zoom-in\">\r\n<div class=\"bt-icon\">&nbsp;</div>\r\n<h4 class=\"sub-tittle\" style=\"font-size: 24px;\">Valores</h4>\r\n<p style=\"font-size: 16px;\">Honestidad, Calidad, Confianza y Servicio son los pilares para fortalecer la relaci&oacute;n de nuestros clientes y sus proyectos.</p>\r\n<!--p>\r\n                        	<a class=\"btn btn-secondary view\" href=\"single.html\" role=\"button\">View details</a>\r\n                    	</p--></div>\r\n<!-- /.col-lg-4 --></div>\r\n</div>\r\n<!--//Nostotros-->', 'fibrecen', 'nosotros', '', '', '', '', 0, 0),
(2, 'Fibrecen - Home1', '<div class=\"row inner_stat\">\r\n<div class=\"col-lg-6 stats-left-info\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 stats_left counter_grid\"><em class=\"far fa-building\"></em>\r\n<p class=\"counter\">32</p>\r\n<h4>A&ntilde;os De Experiencia</h4>\r\n</div>\r\n<div class=\"col-md-6 stats_left counter_grid1\"><em class=\"far fa-edit\" style=\"color: #ed1c24;\"></em>\r\n<p class=\"counter\" style=\"color: #ed1c24;\">3840</p>\r\n<h4 style=\"color: #ed1c24;\">Proyectos Realizados</h4>\r\n</div>\r\n<div class=\"col-md-6 stats_left counter_grid2\"><em class=\"far fa-smile\" style=\"color: #ed1c24;\"></em>\r\n<p class=\"counter\" style=\"color: #ed1c24;\">10879</p>\r\n<h4 style=\"color: #ed1c24;\">Clientes Atendidos</h4>\r\n</div>\r\n<div class=\"col-md-6 stats_left counter_grid3\"><em class=\"fas fa-users\"></em>\r\n<p class=\"counter\">43</p>\r\n<h4>Visitas</h4>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-6 stats-right-img\">&nbsp;</div>\r\n</div>\r\n<!-- //stats -->\r\n<p>&nbsp;</p>\r\n<!--//ban-->\r\n<div class=\"container\">\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-5 bt-bottom-info\" data-aos=\"fade-right\">\r\n<h5 style=\"font-size: 42px; font-weight: bold;\">&iexcl;Bienvenido!</h5>\r\n</div>\r\n<div class=\"col-lg-7 bt-bottom-info\" data-aos=\"fade-left\">\r\n<p style=\"font-size: 29px;\">Nosotros MOLDEAMOS SOLUCIONES junto contigo transformamos una idea en PROYECTOS A GRAN ESCALA.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<!--/services-->\r\n<div class=\"container\">\r\n<h3 class=\"tittle\">Crea Con Nosotros</h3>\r\n<div class=\"row inner-sec-wthree\">\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-left\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes rigidos y piezas de fibra de vidrio</h5>\r\n<p>Crea piezas de gran tama&ntilde;o y poco detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Vaciado, encapsulados y roto moldeo</h5>\r\n<p>Crea figuras a partir de tus moldes.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-4 grid_info_main mid\" data-aos=\"flip-down\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/ab.jpg\" alt=\" \" /></div>\r\n<div class=\"col-lg-4 grid_info_main\" data-aos=\"flip-right\">\r\n<div class=\"grid_info\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Moldes flexibles</h5>\r\n<p>Crea piezas peque&ntilde;as con gran detalle.</p>\r\n</div>\r\n</div>\r\n<div class=\"grid_info second\">\r\n<div class=\"icon_info\">\r\n<h5 style=\"text-transform: capitalize; color: #1f205a;\">Resina para fotograf&iacute;a</h5>\r\n<p>Recubre tu fotograf&iacute;a con un acabado unico.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!--//services->\r\n   <!--reviews_sec-->\r\n<h3 class=\"tittle cen\">Testimonios</h3>\r\n<div class=\"inner-sec-wthree\">\r\n<div class=\"flexslider\">\r\n<ul class=\"slides\">\r\n<li>\r\n<div class=\"testimonials_grid\">\r\n<div class=\"testimonials_grid-inn\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/testimonial_person2.jpg\" alt=\" \" width=\"120\" />\r\n<div class=\"test_social_pos\">&nbsp;</div>\r\n</div>\r\n<h3><span>- Ingeniera Civil -</span></h3>\r\n<!--i>United States</i-->\r\n<p style=\"font-size: 25px;\">\"Super recomendado, la atencion es buenisima y te ayudan con cualquier duda\"</p>\r\n</div>\r\n</li>\r\n<li>\r\n<div class=\"testimonials_grid\">\r\n<div class=\"testimonials_grid-inn\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/testimonial_person1.jpg\" alt=\" \" width=\"120\" />\r\n<div class=\"test_social_pos\">&nbsp;</div>\r\n</div>\r\n<h3><span>- Emprendedor -</span></h3>\r\n<!--i>United States</i-->\r\n<p style=\"font-size: 25px;\">\"Su curso se me hizo f&aacute;cil y muy creativo, impartidos por excelentes maestros.\"</p>\r\n</div>\r\n</li>\r\n<li>\r\n<div class=\"testimonials_grid\">\r\n<div class=\"testimonials_grid-inn\"><img class=\"img-fluid\" src=\"http://www.fibrecen.com.mx/2019/temas/fibrecen/images/testimonial_person3.jpg\" alt=\" \" width=\"120\" />\r\n<div class=\"test_social_pos\">&nbsp;</div>\r\n</div>\r\n<h3><span>- Ingeniera Industrial -</span></h3>\r\n<!--i>United States</i-->\r\n<p style=\"font-size: 25px;\">\"Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda.\"</p>\r\n</div>\r\n</li>\r\n</ul>\r\n</div>\r\n</div>', 'fibrecen', 'Home', '', '', '', '', 0, 0),
(3, 'Glosario', '<p>&nbsp;</p>\r\n<!--/Glosario-->\r\n<div class=\"container\">\r\n<h3 class=\"tittle\">Glosario</h3>\r\n<div class=\"row inner-sec-wthree\"><!-- glosario URL:(modulos/glosario/glosario.json)--> <!-- glosario.json --><!--[1] -1-->\r\n<div id=\"Curado\" class=\"col-md-12\" style=\"padding-bottom: 50px;\">\r\n<h3 class=\"red-line\">Curado</h3>\r\n<p>Proceso qu&iacute;mico posterior al gelado, donde la resina se solidifica por completo gracias a la reacci&oacute;n exot&eacute;rmica provocada por adici&oacute;n de un catalizador, dando lugar a un producto que no cambia sus propiedades.</p>\r\n</div>\r\n<!-- /glosario.json --></div>\r\n</div>\r\n<!--//Glosario-->', 'fibrecen', 'glosario', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_portafolio`
--

CREATE TABLE `fib_portafolio` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` varchar(500) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_portafolio`
--

INSERT INTO `fib_portafolio` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `precio`, `cate`, `resena`, `url_page`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `visible`, `alta`, `fmod`, `user`) VALUES
(1, '', 'PISO INDUSTRIAL', '08.png', '', 'PISOS EP&Oacute;XICOS', '0.00', 'Manufactura epoxicos', '<p>REVESTIMIENTO CON EP&Oacute;XICO</p>\r\n<p>PARA TR&Aacute;FICO PESADO</p>', '', '', '', '', '', '', 1, '07/01/2018 21:10', '', 'admin'),
(2, '', 'PISO ACABADO MET&Aacute;LICO', '02.png', '', 'RESINA PARA PISOS AUTONIVELANTE', '0.00', 'Manufactura epoxicos', '<p>Descripci&oacute;n</p>\r\n<p>Acabado met&aacute;lico marmoleado</p>', '', '', '', '', '', '', 1, '03/03/2019 22:05', '', 'admin'),
(3, '', 'DOMO PARA EXTERIOR', '3.jpg', '', 'Fabricaci&oacute;n de domos para construcciones arquitect&oacute;nicas, y dem&aacute;s decoraciones para el hogar.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '03/03/2019 22:06', '', 'admin'),
(5, '', 'CONTENEDORES', '5.jpg', '', 'Fabricaci&oacute;n de contenedores para sustancias liquidas corrosivas y no corrosivas.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.&nbsp;</p>', '', '', '', '', '', '', 1, '04/03/2019 22:11', '', 'admin'),
(6, '', 'Pull trution', '6.jpg', '', 'Ensamblado de estructuras por medio de prefabricados por pull trutions, anti &aacute;cidas, para interiores, sumergibles y exteriores.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '05/03/2019 22:11', '', 'admin'),
(7, '', ' REVESTIMIENTO ', '7.jpg', '', 'Revestimientos de resinas anticorrosivas reforzados con fibra de vidrio.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '06/03/2019 22:11', '', 'admin'),
(9, '', 'Nombre', '9.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '08/03/2019 22:11', '', 'admin'),
(10, '', 'Nombre', '10.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n. -</p>', '', '', '', '', '', '', 0, '09/03/2019 22:11', '', 'admin'),
(11, '', ' REVESTIMIENTO', '11.jpg', '', 'Resinas isoft&aacute;licas con resistencia qu&iacute;mica media y protecci&oacute;n a los rayos  UV.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n..</p>', '', '', '', '', '', '', 1, '10/03/2019 22:11', '', 'admin'),
(12, '', 'REVESTIMIENTO', '12.jpg', '', 'Resinas isoft&aacute;licas con resistencia qu&iacute;mica media y protecci&oacute;n a los rayos UV.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '11/03/2019 22:11', '', 'admin'),
(13, '', 'Nombre', '13.jpg', '', 'Descripcion.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '12/03/2019 22:11', '', 'admin'),
(14, '', 'Nombre', '14.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '13/03/2019 22:11', '', 'admin'),
(15, '', 'Nombre', '15.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '14/03/2019 22:11', '', 'admin'),
(16, '', 'Nombre', '16.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '15/03/2019 22:11', '', 'admin'),
(17, '', 'Nombre', '17.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '16/03/2019 22:11', '', 'admin'),
(18, '', 'Nombre', '18.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '17/03/2019 22:11', '', 'admin'),
(19, '', 'FILAMENT WINDING', '19.jpg', '', 'Elaboraci&oacute;n de contenedores de mayor capacidad por medio de un embobinado con roving y resinas de acuerdo a las necesidades.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '18/03/2019 22:11', '', 'admin'),
(20, '', ' EXTRACTORES', '20.jpg', '', 'Extractores para gases &aacute;cidos, fabricados en prfv con resinas anti&aacute;cidas.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n</p>', '', '', '', '', '', '', 1, '19/03/2019 22:11', '', 'admin'),
(21, '', 'Nombre', '21.jpg', '', 'Descripcion', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 0, '20/03/2019 22:11', '', 'admin'),
(22, '', ' RECUBRIMIENTO DE PRFV', '22.jpg', '', 'Revestimiento de tinas para lavado de metales y o galvanizado de aceros.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '21/03/2019 22:11', '', 'admin'),
(23, '', 'RECUBRIMIENTO  DE  PRFV ', '23.jpg', '', 'Revestimiento con resinas anticorrosivas y anti&aacute;cidas.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '22/03/2019 22:11', '', 'admin'),
(24, '', 'LAVADORES DE AIRE', '24.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '23/03/2019 22:11', '', 'admin'),
(25, '', 'REVESTIMIENTO  ', '25.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '24/03/2019 22:11', '', 'admin'),
(26, '', 'REVESTIMIENTO  ', '26.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '25/03/2019 22:11', '', 'admin'),
(27, '', 'REVESTIMIENTO  ', '27.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '26/03/2019 22:11', '', 'admin'),
(28, '', 'REVESTIMIENTO  ', '28.jpg', '', 'Revestimiento anticorrosivo de resistencia qu&iacute;mica  media, previniendo incrustaciones.', '0.00', 'Manufactura poliester', '<p>Descripci&oacute;n.</p>', '', '', '', '', '', '', 1, '27/03/2019 22:11', '', 'admin'),
(29, '', 'ACABADO ESCAMAS', '03.png', '', 'Aplicaci&oacute;n de Piso Antiderrapante', '0.00', 'Manufactura epoxicos', '<p>Deja un acabado</p>', '', '', '', '', '', '', 1, '2019-07-12 16:28:23', '', 'admin'),
(30, '', 'PISO ACABADO M&Aacute;RMOL', '04.png', '', '', '0.00', 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 16:59:42', '', 'admin'),
(31, '', '', '05.png', '', '', '0.00', 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 17:31:16', '', 'admin'),
(32, '', '', '06.png', '', '', '0.00', 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 17:31:40', '', 'admin'),
(33, '', '', '07.png', '', '', '0.00', 'Manufactura epoxicos', '', '', '', '', '', '', '', 1, '2019-07-12 17:52:51', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos`
--

CREATE TABLE `fib_productos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `tag` varchar(500) NOT NULL,
  `marca` varchar(150) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `moneda` varchar(10) NOT NULL,
  `unidad` varchar(10) NOT NULL,
  `stock` int(6) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `ID_sub_cate` int(6) NOT NULL,
  `ID_marca` int(6) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `url_name` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` text NOT NULL,
  `land` tinyint(1) NOT NULL,
  `file` varchar(100) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos`
--

INSERT INTO `fib_productos` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `tag`, `marca`, `tipo`, `precio`, `moneda`, `unidad`, `stock`, `ID_cate`, `ID_sub_cate`, `ID_marca`, `visible`, `url_name`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `cate`, `resena`, `land`, `file`, `alta`, `fmod`, `user`) VALUES
(1, '', 'RESINA VIRGEN', 'resinavirgen.png', '', '', 'resina, resinas, virgen', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Es una resina poli&eacute;ster ortoft&aacute;lica virgen. Cuenta con las caracter&iacute;sticas de excelente humectaci&oacute;n con la fibra de vidrio, buen&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;aun en secciones delgadas, buen acabado superficial, buena aceptaci&oacute;n de cargas, buen ajuste de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a>, puede ser modificado a las necesidades del usuario.</p>\r\n<p style=\"text-align: justify;\">Usos: Fabricaci&oacute;n de m&aacute;rmol sint&eacute;tico, reproducci&oacute;n de piezas y figuras s&oacute;lidas o huecas, piezas decorativas, laminados o vaciados, y piezas de fibra de vidrio automotriz.</p>', 0, '', '2019-03-03 22:27:03', '', 'admin'),
(2, '', 'RESINA VIRGEN PARA VACIADOS', 'resinavirgen.png', '', 'DESDE $99.00 / 1KG', 'resina virgen, recinas', '', '', '99.00', 'MNX', 'KG', 100, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"box-sizing: border-box; margin-bottom: 1.5em; color: rgba(0, 0, 0, 0.5); font-family: Roboto, Arial, sans-serif; letter-spacing: normal; text-align: justify;\">Es una resina poli&eacute;ster virgen con reactividad media que permite mayor control sobre el&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #da1212;\" href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>, excelente aceptaci&oacute;n en la mezcla con cargas, baja contracci&oacute;n lo que ayuda a evitar el craquelado a altos espesores, buen ajuste de&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #da1212;\" href=\"http://www.fibrecen.com.mx/productos/glosario.html#Viscosidad\">viscosidad</a>. Buena aceptaci&oacute;n de fibra de vidrio.</p>\r\n<p style=\"box-sizing: border-box; margin-bottom: 1.5em; color: rgba(0, 0, 0, 0.5); font-family: Roboto, Arial, sans-serif; letter-spacing: normal; text-align: justify;\">Usos: Reproducci&oacute;n de piezas y figuras s&oacute;lidas o huecas, piezas decorativas, piezas interiores como plafones, marcos y cubiertas. Para sector el&eacute;ctrico. Aplicaciones diversas en fabricaci&oacute;n de m&aacute;rmol sint&eacute;tico y laminaci&oacute;n de pl&aacute;stico reforzado con fibra de vidrio.</p>', 0, '', '2019-03-03 22:33:26', '', 'admin'),
(3, '', 'RESINAS DE USO GENERAL', 'RESINA-ROJA.jpg', '', '', 'resina, resinas, general', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Tambi&eacute;n conocida como de usos generales, por su versatilidad en diferentes aplicaciones. Es una resina poli&eacute;ster ortoft&aacute;lica&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-aclerada</a>&nbsp;no&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pica</a>, de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;r&aacute;pido aun en secciones delgadas, excelente humectaci&oacute;n con la fibra de vidrio, excelente compatibilidad con cargas minerales, buen desarrollo de dureza.</p>\r\n<p>Usos: Reproducci&oacute;n de piezas y figuras s&oacute;lidas o huecas. Fabricaci&oacute;n de laminados, vaciados y piezas de fibra de vidrio, as&iacute; como piezas automotrices interiores.</p>', 0, '', '2019-03-04 20:51:35', '', 'admin'),
(4, '', 'CRISTAL PRE-ACELERADA', 'resina-trans.jpg', '', '', 'cristal, preacelerada, pre-acelerada', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Es una resina poli&eacute;ster ortoft&aacute;lica&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-aclerada</a>&nbsp;con promotor, de gran transparencia y brillo siendo &eacute;sta su caracter&iacute;stica principal, de reactividad media, aceptaci&oacute;n de cargas minerales, baja contracci&oacute;n,&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#Viscosidad\">viscosidad</a>&nbsp;adecuada para eliminar el aire del producto final.</p>\r\n<p style=\"text-align: justify;\">Usos: Reproducci&oacute;n de piezas y figuras s&oacute;lidas con un encapsulado traslucido. &Uacute;til para art&iacute;culos promocionales, joyer&iacute;a y piezas artesanales o decorativas.</p>', 0, '', '2019-03-04 20:53:18', '', 'admin'),
(5, '', 'RESINA ISOFT&Aacute;LICA DE MEDIA RESISTENCIA QU&Iacute;MICA', 'isoftalica.png', '', '', 'resinas, resina isoftalica', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina isoft&aacute;lica virgen&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pica</a>&nbsp;de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;r&aacute;pido, alta dureza,&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica</a>&nbsp;media, caracter&iacute;sticas anticorrosivas, buena aceptaci&oacute;n de estireno, excelente afinidad a la fibra de vidrio, buen acabado superficial. Permite ser modificada a las necesidades del usuario.</p>\r\n<p style=\"text-align: justify;\">Usos: Reproducci&oacute;n de piezas automotrices exteriores. En piezas interiores para la fabricaci&oacute;n de consolas y otros accesorios. Piezas del sector acu&aacute;tico como tanques, tinacos, albercas y canaletas.</p>', 0, '', '2019-03-04 20:55:59', '', 'admin'),
(6, '', 'RESINA DION DE ALTA RESISTENCIA QU&Iacute;MICA (ATLAC)', 'resina-amarilla012.jpg', '', '', 'resina, resina dion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina virgen de alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica</a>, excelente resistencia a la corrosi&oacute;n, afinidad con fibra de vidrio, alta dureza, control sobre&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>, excelente&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#Viscosidad\">viscosidad</a>. A diferencia de las otras resinas, se presenta en hojuelas color &aacute;mbar y para transformarla a&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-aclerada</a>&nbsp;lleva un proceso con mon&oacute;mero de estireno hasta que las hojuelas quedan totalmente diluidas. Puede ser almacenada indefinidamente sin perder sus propiedades, una vez que se transforma en&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-aclerada</a>&nbsp;pierde esa cualidad. Permite ser modificada a las necesidades del usuario.</p>\r\n<p style=\"text-align: justify;\">Usos: Para la construcci&oacute;n de equipamiento qu&iacute;mico de poli&eacute;ster reforzado con fibra de vidrio incluyendo tanques de almacenamientos, reactores, tuber&iacute;as, canalizaciones, chimeneas y recubrimientos sobre hormig&oacute;n y acero. Es excelente para t&eacute;cnicas de fabricaci&oacute;n con enrollamiento filamentario, moldeo por contacto, prensado en fr&iacute;o e inyecci&oacute;n.</p>', 0, '', '2019-03-05 13:53:02', '', 'admin'),
(7, '', 'RESINA FLEXIBLE PURA', 'resina-trans.jpg', '', '', 'resinas, resina flexible', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina poli&eacute;ster ortoft&aacute;lica flexible de baja reactividad, proporciona un mayor control sobre el&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>, gran afinidad con mezcla de cargas. Se puede combinar con resinas r&iacute;gidas con el fin de evitar el craquelado y aumentar la resistencia al impacto en las piezas. Se puede modificar de acuerdo a sus necesidades.</p>\r\n<p style=\"text-align: justify;\">Usos: Fabricaci&oacute;n de molduras con capacidad de soportar ser deformadas en su uso</p>', 0, '', '2019-03-05 13:54:22', '', 'admin'),
(8, '', 'RESINA PARA TERMOFORMADO', 'resina-verde.jpg', '', '', 'resinas, resina termoformado', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Resina poli&eacute;ster&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-aclerada</a>,&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pica</a>&nbsp;y de baja reactividad, alta adherencia al acr&iacute;lico, buena aceptaci&oacute;n en la mezcla con cargas minerales, reacci&oacute;n&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#exotermia\">exot&eacute;rmica</a>&nbsp;moderada, buena impregnaci&oacute;n de la fibra de vidrio,&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-aclerado</a>&nbsp;y&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pico</a>, Debido a su&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#exotermia\">exotermia</a>&nbsp;controlada y a sus componentes de formulaci&oacute;n, no da&ntilde;a al acr&iacute;lico.</p>\r\n<p style=\"text-align: justify;\">Uso: En la industria de construcci&oacute;n de muebles de ba&ntilde;o, y fabricaci&oacute;n de art&iacute;culos de acr&iacute;lico termoformado.</p>', 0, '', '2019-03-05 13:55:34', '', 'admin'),
(9, '', 'RESINA PARA TIROL', 'resina-blanca.jpg', '', '', 'resinas, resina tirol', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify;\">Mejora la consistencia e incrementa la adherencia de las pastas para Tirol, facilita el planchado y aplanados y evita posibles desprendimientos</p>\r\n<p style=\"text-align: justify;\">Usos: Fabricaci&oacute;n de revoques para pared. Pegamento de papel, cart&oacute;n, madera, etc. sellador de paredes porosas y aprestos textiles.</p>', 0, '', '2019-03-05 13:56:40', '', 'admin'),
(10, '', 'COLCHONETA', 'colchoneta012.jpg', '', '', 'colchoneta, fibra de vidrio', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es un Mat de hilos cortados, distribuidos multidireccionalmente en un solo plano. Para soporte de Gel coat, Resinas Poli&eacute;ster, Ep&oacute;xicas y Vinil&eacute;ster. Cuenta con una r&aacute;pida impregnaci&oacute;n, uniformidad de espesor y alta resistencia a la tensi&oacute;n. Es multicompatible e id&oacute;neo como refuerzo para laminados y aplicaciones de moldeo abierto. F&aacute;cil de manipular y cortar.</p>\r\n<p style=\"text-align: justify;\">Existen diferentes pesos est&aacute;ndar siendo el m&aacute;s com&uacute;n de 450g/m2</p>', 0, '', '2019-03-05 15:31:27', '', 'admin'),
(11, '', 'PETATILLO', 'petatillo.jpg', '', '', 'petatillo, fibra de vidrio', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es un material de refuerzo que se fabrica tejiendo un patr&oacute;n de Roving en forma de cuadr&iacute;cula, formando una tela de construcci&oacute;n pesada dise&ntilde;ada para reforzar laminados est&aacute;ndar en aplicaciones de moldeo manual. Es compatible con resinas poli&eacute;ster, ep&oacute;xicas y vinil&eacute;ster. Se utiliza principalmente para moldeo en las que se requiere alto contenido de vidrio y principalmente para dar alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_mecanica\">resistencia mec&aacute;nica</a>. Es de r&aacute;pida humectaci&oacute;n, compatible con sistemas de resina con o sin carga. Da como resultado una superficie atractiva en apariencia en las piezas finales.</p>\r\n<p style=\"text-align: justify;\">Pesos est&aacute;ndar: 600 g/m2 y 800 g/m2, siendo el m&aacute;s com&uacute;n el de 800g/m2</p>', 0, '', '2019-03-05 15:32:49', '', 'admin'),
(12, '', 'FILAMENTO CORTADO', 'filamentocortado01.png', '', '', 'Filamento Recortado - Fibra de Vidrio', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">El filamento cortado est&aacute; formado por hilos continuos a una longitud espec&iacute;fica, est&aacute;n dise&ntilde;ados para usarse en compuestos moldeables de resinas poli&eacute;ster, ep&oacute;xicas y para mezclado en seco en aplicaciones con resinas fen&oacute;licas.</p>\r\n<p style=\"text-align: justify;\">Contiene excelente pigmentaci&oacute;n, excelentes propiedades de laminado.</p>', 0, '', '2019-03-05 15:47:24', '', 'admin'),
(13, '', 'TELA MARINA', 'TELAMARINA.png', '', '', 'tela marina, fibra de vidrio', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es una tela fabricada 100% con hilo de fibra de vidrio, usada especialmente para aplicaciones donde se requiere buena resistencia qu&iacute;mica al contacto del laminado con alg&uacute;n agente corrosivo.</p>\r\n<p style=\"text-align: justify;\">Es de bajo gramaje lo que beneficia en la reducci&oacute;n de peso del producto final, es ahorrador en costos de producci&oacute;n ya que hay menor consumo de resina-fibra, y de f&aacute;cil acomodo en curvaturas peque&ntilde;as o en geometr&iacute;as complejas.</p>\r\n<p style=\"text-align: justify;\">Se puede utilizar en laminados interiores como barrera anticorrosiva o que est&eacute;n expuestos al intemperie. Se aplica manualmente o con equipos de aspersi&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Proporciona una resistencia qu&iacute;mica de 575&deg;C con acabado blanco.</p>', 0, '', '2019-03-05 15:48:17', '', 'admin'),
(14, '', 'FIBRA DE CARBONO', 'fibradecarbon.png', '', '', 'fibra de carbono, fibra de vidrio', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\"><span>Materia de refuerzo tejido en forma de cuadr&iacute;cula color negro. Se utiliza principalmente para altos requerimientos para incrementar la resistencia estructural sin a&ntilde;adirle un peso significativo al producto final. Aporta alta&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_mecanica\">resistencia mec&aacute;nica</a><span>, resistividad el&eacute;ctrica, tambi&eacute;n resiste al ataque de &aacute;cidos y solventes org&aacute;nicos. Otra caracter&iacute;stica principal es que se utiliza para alta conductividad t&eacute;rmica, ya que es capaz de soportar hasta 1500&deg;C sin perder sus caracter&iacute;sticas. Se utiliza principalmente en la industria aeron&aacute;utica como exteriores de aviones, partes de sat&eacute;lites; en la industria de construcci&oacute;n, qu&iacute;mica y petrolera; en la industria deportiva como pesca, para palos de golf, raquetas, lanchas, estructuras de bicicletas. Entre otros.</span></p>', 0, '', '2019-03-05 15:49:42', '', 'admin'),
(15, '', 'ROVING DE ASPERSI&Oacute;N Y DE POLTRUSI&Oacute;N', 'ROVING.png', '', '', 'roving, fibra de vidrio', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Filamentos de longitud indefinida puestos en forma pr&aacute;cticamente paralela, formando una mecha sin torsi&oacute;n mec&aacute;nica. Compatible con sistemas de resina con o sin carga.</p>\r\n<p style=\"text-align: justify;\">Contiene una excelente impregnaci&oacute;n, facilidad de corte, facilidad de desembobinado. En laminados con resinas poli&eacute;ster da excelente translucidez en los paneles. Ahorro en el mantenimiento de cortadores y en el consumo de resina.</p>', 0, '', '2019-03-05 15:50:42', '', 'admin'),
(16, '', 'TRANSPARENTE / BLANCO / NEGRO', 'gelcoat003.png', '', '', 'transparente, blanco, negro', '', '', '0.00', 'MNX', 'PZ', 0, 3, 0, 0, 1, '', '', '', '', '', '', '3', '<p style=\"text-align: justify;\">Compuesto de resina poli&eacute;ster ortoft&aacute;lico y&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pico</a>&nbsp;con dos finalidades, ayuda a darle un acabado liso a la pieza final y proteger la misma de agresiones exteriores e impactos. Al igual que la resina, tiene la posibilidad de agregarle pigmentos y requiere de un catalizador para el&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>. Puede aplicarse con brocha o con pistola.</p>\r\n<p style=\"text-align: justify;\">Para pigmentar gel coat transparente se recomienda el siguiente porcentaje: 10% para colores azul, rojo, negro y verde primavera. 20% para colores amarillo, verde pasto, blanco, naranja, caf&eacute; y gris.</p>\r\n<p style=\"text-align: justify;\">Uso: Para la fabricaci&oacute;n de piezas desde artesanales hasta automotrices y proceso de laminaci&oacute;n</p>', 0, '', '2019-03-05 16:09:25', '', 'admin'),
(17, '', 'TOOLING', 'TOOLING01.png', '', '', 'gel coat', '', '', '0.00', 'MNX', 'PZ', 0, 3, 0, 0, 1, '', '', '', '', '', '', '3', '<p>Compuesto de resina poli&eacute;ster ortoft&aacute;lica&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-acelerado</a>, gran retenci&oacute;n de brillo, su principal caracter&iacute;stica es su excelente nivel&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pico</a>, de alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica.</a></p>\r\n<p style=\"text-align: justify;\">Usos: Para la fabricaci&oacute;n de diversos moldes, desde artesanal hasta automotriz. Para proceso de laminaci&oacute;n.</p>', 0, '', '2019-03-05 16:12:04', '', 'admin'),
(18, '', 'ISOFT&Aacute;LICO', 'gelcoat003.png', '', '', 'isoftalico', '', '', '0.00', 'MNX', 'PZ', 0, 3, 0, 0, 1, '', '', '', '', '', '', '3', '<p style=\"text-align: justify;\">Compuesto de resina Isoft&aacute;lica&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#pre_acel\">pre-acelerado</a>, alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica</a>, adecuado nivel de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#Viscosidad\">viscosidad</a>&nbsp;y&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotrop&iacute;a</a>,&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#Viscosidad\">curado</a>&nbsp;r&aacute;pido. Resistencia a intemperie y corrosi&oacute;n. Apto para la aplicaci&oacute;n manual o aspersi&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Uso: Para piezas automotrices y acu&aacute;ticas, moldeo en general y fabricaci&oacute;n de tanques de almacenamiento. Para piezas donde se necesite protecci&oacute;n contra la corrosi&oacute;n.</p>', 0, '', '2019-03-05 16:13:39', '', 'admin'),
(19, '', 'TRANSPARENTE Y EBLANCO', 'PEGAMENTOTRANSPARENTEBLANCO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 4, 0, 0, 1, '', '', '', '', '', '', '4', '<p style=\"text-align: justify;\">Es un adherente para superficies, principalmente de m&aacute;rmol.</p>\r\n<p style=\"text-align: justify;\">Resina poli&eacute;ster de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>&nbsp;r&aacute;pido, da excelente brillo y adhesi&oacute;n sobre diferentes superficies, se puede limpiar con solvente y volver a pulir para obtener su brillo nuevamente. Color blanco y transparente. Para el&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;debe aplicarse catalizador del 1 al 2%.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>&nbsp;aprox. 5&plusmn;2min. Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;10&plusmn;4min.</p>', 0, '', '2019-03-05 16:34:37', '', 'admin'),
(20, '', 'MASILLA MARFIL', 'resina-del-centro.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 4, 0, 0, 1, '', '', '', '', '', '', '4', '<p style=\"text-align: justify;\">Pasta s&oacute;lida que funciona como pegamento para m&aacute;rmol, granito y piedra. Requiere de per&oacute;xido de benzoilo como catalizador para su&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>, aplicar del 2 al 3% lo que dar&aacute; un tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;de 7 a 10 min.</p>\r\n<p style=\"text-align: justify;\">En presentaci&oacute;n marfil, transparente y neutra.</p>', 0, '', '2019-03-05 16:38:25', '', 'admin'),
(21, '', 'BUTANOX M-50', 'BUTANOX.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\">Es utilizado para el&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;a temperatura ambiente de resinas poli&eacute;ster y gel-coats, es l&iacute;quido e incoloro, r&aacute;pido tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>. Alta reactividad, concentraci&oacute;n y reacci&oacute;n&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#exotermia\">exot&eacute;rmica</a>. Gran productividad por molde. Ayuda a catalizar en menor tiempo en condiciones de humedad.</p>\r\n<p style=\"text-align: justify;\">Es un catalizador de buena pureza y estabilidad.</p>', 0, '', '2019-03-05 16:40:21', '', 'admin'),
(22, '', 'TRIGONOX', 'TRIGONOX.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\">Excelente&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;en resinas poli&eacute;ster y gel-coats, excelente tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>, de alta reactividad y alta concentraci&oacute;n, sin embargo no eleva la reacci&oacute;n&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#exotermia\">exot&eacute;rmica</a>, evitando la contracci&oacute;n del producto final durante el proceso de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#polimerizacion\">polimerizaci&oacute;n</a>.</p>\r\n<p style=\"text-align: justify;\">Es un catalizador de excelente pureza ayudando a una mejor estabilidad.</p>', 0, '', '2019-03-05 16:42:10', '', 'admin'),
(23, '', 'TP', 'CATALIZADOR-TP.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\"><span>Es un catalizador mezclado con alcoxisilianos y se tu utiliza &uacute;nicamente para el inicio del proceso de&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#polimerizacion\">polimerizaci&oacute;n</a><span>&nbsp;de silicones. Proporciona excelente tiempo de&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a><span>&nbsp;as&iacute; como uniformidad en el mismo.</span></p>', 0, '', '2019-03-05 16:44:22', '', 'admin'),
(24, '', 'PER&Oacute;XIDO DE BENZOILO', 'PEROXIDODEBENZOILO.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 6, 0, 0, 1, '', '', '', '', '', '', '6', '<p style=\"text-align: justify;\"><span>Catalizador en pasta suave que se utiliza principalmente para pastas resanadoras automotriz y de uso marino, as&iacute; como para masilla para m&aacute;rmol. En presentaci&oacute;n de color blanco, rojo y azul.</span></p>', 0, '', '2019-03-05 16:47:13', '', 'admin'),
(25, '', 'CAUCHO DE SILIC&Oacute;N P-53', 'P53SILICON.png', '', '', 'silicones, silicon', '', '', '0.00', 'MNX', 'PZ', 0, 5, 5, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">Liquido blanco y viscoso que permite la reproducci&oacute;n de piezas con detalle fino a regular. No requiere de diluyente para trabajarlo, sin embargo se le puede a&ntilde;adir si as&iacute; se desea, aunque en exceso puede afectar la resistencia del mismo. Admite el vaciado de diversos materiales como: resinas, yeso, cera, espuma de poliuretano, cuenta con alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica</a>, al igual que resiste a altas temperaturas de hasta 220&deg;C (no exceder los 220&deg;C). Se cataliza del 2 al 3% con Catalizador TP. Se puede aplicar con brocha, esp&aacute;tula o manualmente. Debe dejarse madurar 24hrs. antes del primer vaciado. Para alargar la vida del molde, ayudarlo a bajar la temperatura y a darle estabilidad hay aplicar lubricante para silic&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>&nbsp;aprox. 10&plusmn;5min. Tiempo de vulcanizado 18&plusmn;5min.</p>', 0, '', '2019-03-05 16:48:17', '', 'admin'),
(26, '', 'CAUCHO DE SILIC&Oacute;N P-85', 'P85SILICON001.png', '', '', 'silicones, silicon', '', '', '0.00', 'MNX', 'PZ', 0, 5, 5, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">L&iacute;quido blanco y viscoso cuenta con excelente flexibilidad y permite fabricar moldes que requieran una reproducci&oacute;n con detalles muy finos. No necesita a&ntilde;adirle diluyente. Resistente a desgarres y alargamientos. Admite el vaciado de diversos materiales como: resinas, yeso, cera, espuma de poliuretano, cuenta con alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica</a>, al igual que resiste a altas temperaturas de hasta 220&deg;C (no exceder los 220&deg;C). Se cataliza del 3 al 4% con Catalizador TP. Se puede aplicar con brocha, esp&aacute;tula o manualmente. Debe dejarse madurar 24hrs. antes del primer vaciado. Para alargar la vida del molde, ayudarlo a bajar la temperatura y a darle estabilidad aplicar lubricante para silic&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>&nbsp;aprox. 80&plusmn;20min. Tiempo de vulcanizado 110&plusmn;30min.</p>', 0, '', '2019-03-05 17:16:51', '', 'admin'),
(27, '', 'LATEX', 'latex.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 5, 6, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">Es utilizado para moldear artesan&iacute;as en yeso, y como pegamento para unir materiales naturales o sint&eacute;ticos. De fuerte olor a amoniaco. Especial para vaciado en fr&iacute;o. Se puede aplicar con brocha, rodillo o por sumersi&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;depende de la cantidad aplicada. De 3 a 10min.</p>', 0, '', '2019-03-05 17:19:03', '', 'admin'),
(28, '', 'MOLDUFLEX', 'MOLDUFLEX.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 5, 6, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">Silic&oacute;n viscoso de color gris&aacute;ceo, que permite reproducir piezas de poco detalle. Requiere de diluyente para ajustar su&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a>. Resiste a altas temperaturas de hasta 180&deg;C (no exceder los 180&deg;C). Buen rendimiento y para el vulcanizado se cataliza al 3% con catalizador TP.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>&nbsp;aprox. 4min. Tiempo de vulcanizado 8&plusmn;3min.</p>', 0, '', '2019-03-05 17:20:18', '', 'admin'),
(29, '', 'DIMETIL ANILINA', 'dimetil03.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 7, 0, 0, 1, '', '', '', '', '', '', '7', '<p style=\"text-align: justify;\">Es un acelerador para resina poli&eacute;ster, da una leve coloraci&oacute;n amarilla clara a resina, alta reacci&oacute;n&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#exotermia\">exot&eacute;rmica</a>.</p>\r\n<p style=\"text-align: justify;\">Se recomienda una o dos gotas cada 100gr. de resina. El uso inadecuado puede craquelar el producto final.</p>', 0, '', '2019-03-10 20:34:13', '', 'admin'),
(30, '', 'COBALTO', 'COBALTO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 7, 0, 0, 1, '', '', '', '', '', '', '7', '<p style=\"text-align: justify;\">Es un acelerador de Carboxilato de cobalto diluido en solventes, de fuerte olor a solvente, color violeta. Aplicado a las resinas poli&eacute;ster tiende a darles un leve tono violeta.</p>\r\n<p style=\"text-align: justify;\">Se recomienda aplicar de 5 a 10 gotas por cada 100gr. de resina o al 5%, dependiendo sus necesidades.</p>', 0, '', '2019-03-10 20:53:57', '', 'admin'),
(31, '', 'PROMOTOR NCV-3', 'PROMOTOR.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 7, 0, 0, 1, '', '', '', '', '', '', '7', '<p style=\"text-align: justify;\"><span>Es un acelerador a base de cobalto que descompone r&aacute;pidamente los per&oacute;xidos a temperatura ambiente, da un resultado de&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a><span>&nbsp;y&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a><span>&nbsp;r&aacute;pido y uniforme. No da coloraci&oacute;n lo que ayuda a mantener la transparencia de la resina. Ideal para resina cristal.</span></p>', 0, '', '2019-03-10 20:55:48', '', 'admin'),
(32, '', 'CAUCHO DE SILIC&Oacute;N PE-21', 'P21SILICON.png', '', '', 'silicones, caucho de silicon', '', '', '0.00', 'MNX', 'PZ', 0, 5, 5, 0, 1, '', '', '', '', '', '', '5', '<p style=\"text-align: justify;\">L&iacute;quido blanco y viscoso que permite la fabricaci&oacute;n de moldes con mayor flexibilidad y que requiera una reproducci&oacute;n con detalles finos. No necesita a&ntilde;adirle diluyente. Resistente a desgarres y alargamientos. Admite el vaciado de diversos materiales como: resinas, yeso, cera, espuma de poliuretano, cuenta con alta&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#res_quimica\">resistencia qu&iacute;mica</a>, al igual que resiste a altas temperaturas de hasta 220&deg;C (no exceder los 220&deg;C). Se cataliza del 4% con Catalizador TP. Se puede aplicar con brocha, esp&aacute;tula o manualmente. Debe dejarse madurar 24hrs. antes del primer vaciado. Para alargar la vida del molde, ayudarlo a bajar la temperatura y a darle estabilidad, hay aplicar lubricante para silic&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a>&nbsp;aprox. 90&plusmn;20min. Tiempo de vulcanizado 110&plusmn;30min.</p>', 0, '', '2019-03-10 21:02:14', '', 'admin'),
(33, '', 'DESMOLDANTE CP-500', 'CP500.png', '', '', 'desmoldante', '', '', '0.00', 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\">Presentaci&oacute;n en pasta de f&aacute;cil aplicaci&oacute;n manual, secado r&aacute;pido, buena fluidez, da excelente acabado y brillo a moldes, econ&oacute;mico.</p>\r\n<p style=\"text-align: justify;\">Dise&ntilde;ada para lograr un excelente desprendimiento en art&iacute;culos de resina y fibra de vidrio, y en moldes de diversos materiales como vidrio, madera, yeso, vidrio etc.</p>', 0, '', '2019-03-10 21:03:09', '', 'admin'),
(34, '', 'CERA D OPORTO', 'deoporto.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\"><span>Pasta formulada con ceras vegetales y minerales, de f&aacute;cil aplicaci&oacute;n manual, de mayor rendimiento, m&aacute;s de 10 moldeos por aplicaci&oacute;n, de r&aacute;pido secado, f&aacute;cil de pulir, f&aacute;cil desmolde en caucho de silic&oacute;n, art&iacute;culos de resina y fibra de vidrio.</span></p>', 0, '', '2019-03-10 21:04:16', '', 'admin'),
(35, '', 'PELICULA SEPARADORA', 'desmoldante.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\"><span>L&iacute;quido azuloso que forma una pel&iacute;cula para evitar contacto entre el molde y la resina. Da acabado brilloso a la pieza final, es de f&aacute;cil aplicaci&oacute;n y limpieza, r&aacute;pida evaporaci&oacute;n, soluble en agua, usar despu&eacute;s de haber aplicado la cera desmoldante CP-500. Puede aplicarse por aspersi&oacute;n o manualmente.</span></p>', 0, '', '2019-03-10 21:05:02', '', 'admin'),
(36, '', 'LUBRICANTE PARA SILIC&Oacute;N', 'LUBRICIL.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\">Lubricante en aerosol de diversas aplicaciones. Funciona como antiadherente para moldes, ayuda a lubricar piezas en constante fricci&oacute;n. Funciona como abrillantado de muebles y superficies con laca. Limpia y lubrica componentes el&eacute;ctricos. Da mayor vida a los moldes de silic&oacute;n de caucho, pl&aacute;stico, metal, etc. resistiendo altas temperaturas. Con una sola capa es sufriente.</p>\r\n<p style=\"text-align: justify;\">Debe dejarse actuar por 1 minuto.</p>', 0, '', '2019-03-10 21:05:46', '', 'admin'),
(37, '', 'TR CERA', 'TRCERA01.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\">Cera desmoldante ideal para aplicaciones aceleradas por calor. Debe dejarse secar antes de quitarlo. &Eacute;sta cera contiene carnauba (cera que se obtiene de las hojas de la palma Copernicia prunifera de grandes propiedades), lo que ayuda a que se acumule menos desmoldante.</p>\r\n<p style=\"text-align: justify;\">Especialmente para altas temperaturas.</p>', 0, '', '2019-03-10 21:06:30', '', 'admin'),
(38, '', 'FREKOTE WOLO', 'frekotewolo.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 8, 0, 0, 1, '', '', '', '', '', '', '8', '<p style=\"text-align: justify;\"><span>L&iacute;quido desmoldante polim&eacute;rico de r&aacute;pido&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a><span>, para m&uacute;ltiples desmoldes en cualquier tipo de resina poli&eacute;ster. De f&aacute;cil y r&aacute;pida aplicaci&oacute;n, se debe frotar y dejar evaporar, quedando una capa delgada y transparente que no requiere pulirse y es de bajo acomulamiento. Ayuda a reducir gastos de mantenimiento.</span></p>', 0, '', '2019-03-10 21:07:09', '', 'admin'),
(39, '', 'TALCO INDUSTRIAL', 'TALCOINSDUSTRIAL.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\">El talco es un mineral blanco que incrementa las propiedades mec&aacute;nicas de los laminados y tiende a incrementar la&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a>&nbsp;de las resinas poli&eacute;ster. Aporta tonalidad gris aperlado.</p>\r\n<p style=\"text-align: justify;\">Uso: Se puede utilizar en el yeso. Por su resistencia a elevadas temperaturas se utiliza en la fabricaci&oacute;n de materiales termoresistentes</p>', 0, '', '2019-03-10 21:08:14', '', 'admin'),
(40, '', 'CALCITA (CARBONATO DE CALCIO)', 'CALCITA.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Polvo blanco altamente puro que se homog&eacute;nea perfectamente con la resina, en el&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a><span>&nbsp;da un color hueso a la resina y le aumenta peso al producto final. Utilizada en la industria qu&iacute;mica y para la fabricaci&oacute;n de pintura, pigmentos, papel, pl&aacute;stico, en el &aacute;rea alimenticia e industrial.</span></p>', 0, '', '2019-03-10 21:08:51', '', 'admin'),
(41, '', 'BIOXIDO DE TITANIO', 'BIOXIDODETITANIO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\">Polvo blanco de f&aacute;cil integraci&oacute;n con otras cargas para obtener un mejor acabado. Generalmente lo utilizan para darle tono blanco la resina.</p>\r\n<p style=\"text-align: justify;\">Se utiliza para la fabricaci&oacute;n de piezas artesanales, medicamentos y cosm&eacute;ticos.</p>', 0, '', '2019-03-10 21:10:37', '', 'admin'),
(42, '', 'AL&Uacute;MINA TRIHIDRATADA', 'ALUMINAHIDRATADA.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Polvo blanco extra fino, en el&nbsp;</span><a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a><span>&nbsp;da una tonalidad ligeramente rosada y opaca. Mezclado con la resina se utiliza principalmente como retardante a la flama. Se recomienda aplicar del 30 al 60%.</span></p>', 0, '', '2019-03-10 21:11:21', '', 'admin'),
(43, '', 'BLANCO DE ESPA&Ntilde;A', 'BLANCODEESP.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Es un sulfato de calcio Dihidratado en presentaci&oacute;n de polvo blanco fino y homog&eacute;neo que se utiliza para diversas industrias y aplicaciones, como resanado de techos y paredes tipo pasta, trabajos de carpinter&iacute;a, carga en pinturas, preparaci&oacute;n de tirol, entre otros.</span></p>', 0, '', '2019-03-10 21:12:25', '', 'admin'),
(44, '', 'DICALITE', 'DICALITE-1.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\"><span>Es un polvo blanco espeso que se utiliza principalmente para darle cuerpo a la resina, ayuda a que la pieza final no quede traslucida. Proporciona propiedades mec&aacute;nicas a la resina. Tiene la cualidad de no precipitarse en corto tiempo.</span></p>', 0, '', '2019-03-10 21:13:13', '', 'admin'),
(45, '', 'MICROESFERA', 'MICROESFERA.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p style=\"text-align: justify;\">Carga extra ligera formada por peque&ntilde;as esferas de vidrio, que ayuda a dar propiedades de aislamiento y ayudan a dar volumen a la resina. Es de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a>&nbsp;alta lo que no la hace apta para vaciados que requieran de fluidez.</p>\r\n<p style=\"text-align: justify;\">Se recomienda agregar un 10% en relaci&oacute;n a la resina. Var&iacute;a el porcentaje dependiendo del acabado o la necesidad que se tenga para el producto final.</p>', 0, '', '2019-03-10 21:14:01', '', 'admin'),
(46, '', 'PIGMENTOS L&Iacute;QUIDOS', 'PIGMENTOSliquidos.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p style=\"text-align: justify;\">Desarrollados para pigmentar la resina cristal, no le quita su propiedad traslucida.</p>\r\n<p style=\"text-align: justify;\">Existencia de colores: Amarillo, verde, violeta, azul y rojo.</p>', 0, '', '2019-03-11 08:48:28', '', 'admin'),
(47, '', 'PIGMENTOS EN PASTA', 'pigmentossolidosss.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p style=\"text-align: justify;\">Desarrollado para pigmentar gel coat, todo tipo de resinas y pegamentos. En resina cristal el producto final queda de color s&oacute;lido, no traslucido.</p>\r\n<p style=\"text-align: justify;\">Existencia de colores: blanco, negro, verde pasto, verde primavera, rojo, amarillo cromo, naranja, azul y caf&eacute;.</p>', 0, '', '2019-03-11 08:49:45', '', 'admin'),
(48, '', ' PIGMENTO FOTOLUMINICENTE', 'FOTOLUMINISCENTES.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 10, 0, 0, 1, '', '', '', '', '', '', '10', '<p style=\"text-align: justify;\">Pigmento en polvo con la caracter&iacute;stica principal de absorber, almacenar y emitir luz con largo tiempo de activaci&oacute;n, hasta 12 hrs. en la obscuridad. Libre de sustancias peligrosas o radioactivas, por lo que lo hace seguro en aplicaciones como juguetes, ropa, calzado, relojes, decoraci&oacute;n, etc.</p>\r\n<p style=\"text-align: justify;\">Un gramo cubre 25cm cuadrados aprox.</p>', 0, '', '2019-03-11 08:51:38', '', 'admin'),
(49, '', 'MBI (METAL BULDING INSULATION)', 'ailantembi.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 4, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Rollo de fibra de vidrio aislante fabricado con fibra de vidrio aglutinada con resinas termo- fijas puede ser utilizado como aislamiento t&eacute;rmico o ac&uacute;stico, con barrera de vapor en una de sus caras.</p>\r\n<p style=\"text-align: justify;\">Ideal para recubrimiento de muros o techos de naves industriales y comerciales, hangares, centros comerciales, bodegas, colegios, etc.</p>\r\n<p style=\"text-align: justify;\">Las medidas de este rollo son de 15.24m o 30.48m de largo x 1.22m de ancho y los hay en espesores de 2\", 3\", 3.5\" y 4\".</p>', 0, '', '2019-03-11 08:56:37', '', 'admin'),
(50, '', 'TRS 10', 'aislanteTRS.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 4, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Se presenta en rollos y placas flexibles en color blanco y se utiliza como aislamiento para hornos de estufas est&aacute;ndar, calentadores de agua, calentadores solares, chimeneas, hornos comerciales, etc. Se puede utilizar hasta en aplicaciones de 538&deg;C</p>\r\n<p style=\"text-align: justify;\">Las dimensiones de los rollos son de 15.24m de largo x .61m de ancho y de 1\" o 2\" de espesor.</p>', 0, '', '2019-03-11 09:00:28', '', 'admin'),
(51, '', 'RF3000 (RF3075, RF3100, RF3150 Y RF3200)', 'rf3000.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 4, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Se utiliza principalmente en el exterior de ductos de aire acondicionado y calefacci&oacute;n soportando hasta 232&deg;C.</p>\r\n<p style=\"text-align: justify;\">Los hay sin barrera de vapor o con barrera de vapor (foil de aluminio o aluminio reforzado)</p>\r\n<p style=\"text-align: justify;\">Se encuentra en espesores de 1\", 1.5\" &oacute; 2.5\" x 15.24m de largo y .61m de ancho para el foil de aluminio. Y de 1.22m ancho para el aluminio reforzado.</p>', 0, '', '2019-03-11 09:23:40', '', 'admin'),
(52, '', 'AISLHOGAR', 'ailanteAISLHOGAR.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 4, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Aislamiento termo-ac&uacute;stico fabricado con fibra de vidrio de baja densidad, aglutinada con resina fen&oacute;lica de fraguado t&eacute;rmico.</p>\r\n<p style=\"text-align: justify;\">Se recomienda como aislante t&eacute;rmico y ac&uacute;stico en el ramo de la construcci&oacute;n, en usos como interior de muros y canceles divisorios, sobre falsos plafones y como absorbentes de sonido, ayudando a crear un ambiente m&aacute;s silencioso y c&oacute;modo.</p>\r\n<p style=\"text-align: justify;\">Los hay en rollos desde 2\" hasta 5\" de espesor x 15.24m de largo x .61m de ancho.</p>', 0, '', '2019-03-11 09:25:10', '', 'admin'),
(53, '', 'FIBERGLAS', 'FIBERGLASS.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 4, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Es un aislante para tuber&iacute;a, preformado con corte longitudinal abisagrado y puede ser con barrera de vapor. Se produce con fibras minerales de vidrio aglutinado con resinas especiales para emplearse en tuber&iacute;as que operen en un rango de temperatura de -18&deg;C hasta 454&deg;C.</p>\r\n<p style=\"text-align: justify;\">Son ideales para tuber&iacute;as de proceso y servicio que conducen vapor, agua caliente, agua helada, refrigerantes, gases y toda clase de fluidos en que se requiere ahorrar energ&iacute;a.</p>\r\n<p style=\"text-align: justify;\">Se presenta en abisagrados de 36\" de largo (91.4m), con espesores m&iacute;nimos de 1\" y di&aacute;metro m&iacute;nimo de &frac12;\".</p>', 0, '', '2019-03-11 09:26:17', '', 'admin'),
(54, '', 'PLACAS SERIE 700', 'placas.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 4, 0, 1, '', '', '', '', '', '', '2', '<p style=\"text-align: justify;\">Aislamiento termo-ac&uacute;stico fabricado con fibra de vidrio aglutinada con resinas fen&oacute;licas de fraguado t&eacute;rmico para soportar temperaturas de hasta 232&deg;C. Se presentan en forma de placas flexibles, semir&iacute;gidas y r&iacute;gidas.</p>\r\n<p style=\"text-align: justify;\">Serie 700 Fiberglas&reg; se recomienda para el aislamiento t&eacute;rmico de equipos industriales, tales como calderas, hornos, tanques, reactores y equipos de proceso. Tiene un excelente desempe&ntilde;o de absorci&oacute;n de ac&uacute;stica en cines, teatros, auditorios, estudios de grabaci&oacute;n, etc.</p>', 0, '', '2019-03-11 09:27:24', '', 'admin'),
(55, '', 'ESPUMA DE POLIURETANO PARA VACIADOS', 'ESPUMADEPOLIURETANO.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Son dos componentes l&iacute;quidos que al mezclarlos en partes iguales dan como resultado un poliuretano s&oacute;lido poroso. Debe mezclarse en partes iguales (A Y B) para que inicie la reacci&oacute;n y as&iacute; comience a expandir. Resiste a envejecimiento, condiciones clim&aacute;ticas y solventes. Tiempo de&nbsp;<a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a>&nbsp;aproximadamente de 2 a 3 minutos a temperatura ambiente. Un kilo abarca un aproximado de 1 m<span>2</span>&nbsp;a 1 pulgada de espesor.</p>\r\n<p style=\"text-align: justify;\">Se utiliza para el aislamiento t&eacute;rmico en refrigeradores, termos peque&ntilde;os o carritos de paletas. Se pueden aplicar manualmente o por aspersi&oacute;n en m&aacute;quinas especiales.</p>', 0, '', '2019-03-11 09:28:54', '', 'admin'),
(56, '', 'PRESENTACI&Oacute;N EN SPRAY', 'espuma-de-poliuretano-400ml-selladora-y-aislante-D_NQ_NP_725903-MLM29934684941_042019-F.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Espuma de poliuretano de un solo componente que endurece con la humedad. La porci&oacute;n aplicada se expande de 2 a 3 veces. Se adhiere a toda superficie, excepto poliestireno, silic&oacute;n y tefl&oacute;n.</p>\r\n<p style=\"text-align: justify;\">Se utiliza para aislamiento t&eacute;rmico de ventanas, puertas, marcos, etc. Rellena huecos, paredes, conductos, etc. Sella herm&eacute;ticamente fisuras y grietas.</p>', 0, '', '2019-03-11 09:40:38', '', 'admin'),
(57, '', 'ESPUMA DE POLIURETANO PARA BOYAS', 'espumaparaboy.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Son dos componentes l&iacute;quidos que al mezclarlos dan como resultado un poliuretano s&oacute;lido poroso, especial para la fabricaci&oacute;n manual de boyas para W.C. y flotadores de varios usos. Su tiempo de reacci&oacute;n es lento para un mejor manejo</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: en partes iguales. Tiempo de mezclado: 5-8 seg. Tiempo de elevaci&oacute;n: 25-40 seg.</p>', 0, '', '2019-03-11 09:46:38', '', 'admin'),
(58, '', 'ESPUMA DE POLIURETANO PARA ASIENTOS', 'ESPUMAPARAASIENTO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Sistema de dos componentes que necesariamente se mezclan para la fabricaci&oacute;n de asientos y respaldos, dando buena apariencia y comodidad.</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: 100(A)X 44(B)partes por peso. Tiempo de mezclado: 5-7 seg. Tiempo de elevaci&oacute;n: 55-1&acute;10 min.</p>', 0, '', '2019-03-11 09:51:03', '', 'admin'),
(59, '', 'ESPUMA DE POLIURETANO IMITACI&Oacute;N MADERA', 'espumaparamadera.jpg.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Sistema de dos componentes que necesariamente se mezclan para la fabricaci&oacute;n de piezas imitaci&oacute;n madera como marcos, frentes de caj&oacute;n, puertas para cocinas, etc.</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: partes iguales. Tiempo de mezclado: 30seg. Tiempo de elevaci&oacute;n: 100+-10 seg</p>', 0, '', '2019-03-11 09:52:09', '', 'admin'),
(60, '', 'ESPUMA DE POLIURETANO PARA PIEL', 'espumaparapiel01.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 11, 0, 0, 1, '', '', '', '', '', '', '11', '<p style=\"text-align: justify;\">Espuma de Poliuretano de dos componentes, desarrollado para la fabricaci&oacute;n de asientos de bicicleta, coderas, y dem&aacute;s piezas en la industria automotriz y mueblera.</p>\r\n<p style=\"text-align: justify;\">Relaci&oacute;n de mezcla: 100(A) X 41(B) partes por peso. Tiempo de mezclado: 15 seg. Tiempo de elevaci&oacute;n: 1&rsquo;10-1&acute;15min.</p>', 0, '', '2019-03-11 09:54:18', '', 'admin'),
(62, '', 'MALLA DE REFUERZO', 'MAYADEREFUERZO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 3, 0, 1, '', '', '', '', '', '', '2', '', 0, '', '2019-06-13 09:10:58', '', 'admin');
INSERT INTO `fib_productos` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `tag`, `marca`, `tipo`, `precio`, `moneda`, `unidad`, `stock`, `ID_cate`, `ID_sub_cate`, `ID_marca`, `visible`, `url_name`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `cate`, `resena`, `land`, `file`, `alta`, `fmod`, `user`) VALUES
(63, '', 'PASTA USO AUTOMOTR&Iacute;Z', 'PASTAAUTO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 9, 0, 1, '', '', '', '', '', '', '14', '<h3 style=\"text-align: center;\">PASTA USO AUTOMOTRIZ</h3>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Pasta suave a base de resina poli&eacute;ster, de f&aacute;cil aplicaci&oacute;n, adecuada <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a>, gran durabilidad y f&aacute;cil lijado, lo que ayuda a acortar tiempos de trabajo. Se le debe aplicar catalizador al 2% para un tiempo de <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a> de media hora aproximadamente. Aparte del mercado automotriz se utiliza tambi&eacute;n para restaurar o modificar figuras o muebles.</p>', 0, '', '2019-06-25 10:25:19', '', 'admin'),
(64, '123456', 'PASTA USO MARINO', 'USOMARINO.jpg', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 9, 0, 1, '', '', '', '', '', '', '14', '<h3 style=\"text-align: center;\">PASTA USO MARINO</h3>\r\n<p>&nbsp;</p>\r\n<p>Pasta suave a base de resina poli&eacute;ster con filamentos de fibra de vidrio, permite una aplicaci&oacute;n sin escurrimientos, de f&aacute;cil aplicaci&oacute;n, adecuada <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a>, gran durabilidad y f&aacute;cil lijado, lo que ayuda a acortar tiempos de trabajo. Se le debe aplicar catalizador al 2% para un tiempo de <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#curado\">curado</a> de media hora aproximadamente. Sella, rellena y restaura lanchas, yates y botes. La diferencia que hay con la pasta automotriz, es que &eacute;sta es resistente a ambientes marinos y h&uacute;medos.</p>', 0, '', '2019-06-26 09:00:20', '', 'admin'),
(71, '', 'PROTECTOR ANTIRAYONES', 'ANTIRAYONES.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p><span style=\"font-size: 16px;\"><strong>PROTECTOR ANTIRAYONES</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>Sistema de recubrimiento de uretano base agua de dos componentes. Proporciona una apariencia excepcional, as&iacute; como excelentes propiedades f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>\r\n<p>&nbsp;</p>\r\n<p>Nivel de COV: 31 g / L</p>\r\n<p>Velocidad de secado r&aacute;pido</p>\r\n<p>R&aacute;pida resistencia al agua</p>\r\n<p>Excelentes propiedades adhesivas, que permiten su aplicaci&oacute;n en otros recubrimientos firmes y duros, as&iacute; como una buena adherencia al material sustrato.</p>\r\n<p>Aspecto excepcional</p>', 0, '', '2019-07-11 11:41:59', '', 'admin'),
(72, '', 'PROTECTOR RAYOS UV', 'PROTECCIONUV.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p><strong>PROTECTOR RAYOS UV</strong></p>\r\n<p>&nbsp;</p>\r\n<p>Sistema de 2 componentes, 83% s&oacute;lidos, cumple con COV, poliurea poliasp&aacute;rtica alif&aacute;tica que fue desarrollada para recubrimientos de suelo estables a los rayos UV. Presenta excelentes propiedades f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>', 0, '', '2019-07-11 11:48:43', '', 'admin'),
(66, '', 'RESINA PARA VACIADOS VF 2.5CM', 'PARAVACIADOS25VF.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p>RESINA PARA VACIADOS VF 2.5CM DE ESPESOR</p>\r\n<p>Sistema de 2 componentes, 100% s&oacute;lidos de curado a temperatura ambiente, f&aacute;cil manejo y excelente adhesi&oacute;n. Ha sido formulado con resina ep&oacute;xica dise&ntilde;ada para el encapsulado y decoraci&oacute;n en madera dando acabados con transparencia y brillo inigualables.</p>\r\n<p>&nbsp;</p>\r\n<p>F&aacute;cil de mezclar, relaci&oacute;n de volumen A:B = 2:1</p>\r\n<p>Tiempo de manejo y de curado largo</p>\r\n<p>Encapsulados de m&aacute;ximo 2.5cm</p>\r\n<p>L&iacute;quido libre de grumos y part&iacute;culas en suspensi&oacute;n</p>', 0, '', '2019-07-10 13:56:51', '', 'admin'),
(67, '', 'RESINA PARA VACIADOS VF 5CM', 'PARAVACIADOS5CM.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p>RESINA PARA VACIADOS 5CM DE ESPESOR</p>\r\n<p>Sistema de 2 componentes, 100% s&oacute;lidos de curado a temperatura ambiente, f&aacute;cil manejo y excelente adhesi&oacute;n. Ha sido formulado con resina ep&oacute;xica dise&ntilde;ada para el encapsulado y decoraci&oacute;n en madera dando acabados con transparencia y brillo inigualables.</p>\r\n<p>F&aacute;cil de mezclar, relaci&oacute;n de volumen A:B = 4:1</p>\r\n<p>Tiempo de manejo y de curado largo</p>\r\n<p>Encapsulados de 2.5 a 5 cm</p>\r\n<p>L&iacute;quido libre de grumos y part&iacute;culas en suspensi&oacute;n</p>\r\n<p>Siguiendo el proceso de capas se puede alcanzar espesores hasta de 10 cm sin afectar la transparencia del encapsulado.</p>', 0, '', '2019-07-10 13:59:06', '', 'admin'),
(68, '', 'RESINA PARA PISOS VF', 'PARAPISOSVF.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p>RESINA PARA PISOS</p>\r\n<p>Sistema de 2 componentes, 100% s&oacute;lidos, formulado para adquirir</p>\r\n<p>excelentes propiedades mec&aacute;nicas y qu&iacute;micas en 24 horas a temperatura ambiente.</p>\r\n<p>F&aacute;cil de mezclar, relaci&oacute;n de volumen 2:1</p>\r\n<p>No emite olores y puede aplicarse en interiores</p>\r\n<p>Autonivelante</p>\r\n<p>L&iacute;quido libre de grumos y part&iacute;culas en suspensi&oacute;n</p>', 0, '', '2019-07-10 14:00:37', '', 'admin'),
(70, '', 'RESINA EPOXICA PARA USO VERTICAL', 'DEUSOVERTICAL.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '', 0, '', '2019-07-11 11:41:13', '', 'admin'),
(69, '', 'RESINA SUPERFICIES HORIZONTALES', 'SOLIDOSEPOXICA.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p>Sistema de recubrimiento ep&oacute;xico de dos componentes. Tiene muy buena apariencia, as&iacute; como propiedades qu&iacute;micas y f&iacute;sicas. Este sistema ha sido aprobado por la Agencia Canadiense de Inspecci&oacute;n de Alimentos (CFIA).</p>\r\n<p>&nbsp;</p>\r\n<p>No contiene solventes.</p>\r\n<p>Superficie densa resistente a las bacterias y la humedad y f&aacute;cil de limpiar.</p>\r\n<p>Se pueden aplicar varias capas sobre s&iacute; misma con excelente adherencia.</p>\r\n<p>Excelentes propiedades de adhesi&oacute;n, permite aplicaciones de otras marcas y con recubrimiento duro, as&iacute; como un buen enlace al sustrato.</p>\r\n<p>No emite olores y puede aplicarse en interiores.</p>', 0, '', '2019-07-10 14:11:38', '', 'admin'),
(73, '', 'ESCAMAS PARA EP&Oacute;XICO', 'ESCAMAS.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p>Escamas decorativas compuestas por resinas a base de agua, minerales inorg&aacute;nicos, aditivos y varios colorantes. Este producto original tiene colores firmes y brillantes en su totalidad, varia en sus formas, puede ser recortado a rangos estandarizados y combinado en opciones infinitas para obtener la apariencia &oacute;ptima y la calidad de textura requerida para pisos a base de resina, o sistema de recubrimientos en paredes.</p>\r\n<p>&nbsp;</p>\r\n<p>Personalizados para cualquier ambiente</p>\r\n<p>M&aacute;s de 50 estilos de color</p>\r\n<p>No inflamable</p>\r\n<p>Qu&iacute;mica a base agua</p>\r\n<p>Dureza y flexibilidad &oacute;ptima</p>\r\n<p>Pigmentos de color estables a los rayos UV</p>\r\n<p>Aplicaci&oacute;n sencilla</p>\r\n<p>Igualaci&oacute;n de color personalizada</p>\r\n<p>Medidas de &frac14;&rdquo;, 1/8&rdquo; y 1/16&rdquo;</p>\r\n<p>Compatible con la mayor&iacute;a de las resinas</p>\r\n<p>Esconde imperfecciones en el piso</p>', 0, '', '2019-07-11 12:00:50', '', 'admin'),
(74, '', 'ABRASIVOS Y HERRAMIENTAS', 'herramientas.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 12, 0, 0, 1, '', '', '', '', '', '', '12', '<p>ABRACIVOS Y HERRAMIENTAS</p>\r\n<p>&nbsp;</p>\r\n<p>Encuentra todo lo necesario para desarrollar tus proyectos.</p>\r\n<ul>\r\n<li>Rodillos</li>\r\n<li>Brochas</li>\r\n<li>V&aacute;sculas</li>\r\n<li>Tapes</li>\r\n<li>Sopletes</li>\r\n<li>Muchas Herramientas m&aacute;s...</li>\r\n</ul>', 0, '', '2019-07-12 15:52:45', '', 'admin'),
(75, '', 'EQUIPO DE SEGURIDAD', 'equipodeseguridad.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 13, 0, 0, 1, '', '', '', '', '', '', '13', '<p>EQUIPO DE SEGUIRIDAD Y PROTECCI&Oacute;N PERSONAL</p>\r\n<p>&nbsp;</p>\r\n<p>El equipo de seguridad y protecci&oacute;n personal para tus trabajos y proyectos los encontrar&aacute;s con nosotros</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Lentes de seguridad</li>\r\n</ul>\r\n<ul>\r\n<li>Mascarilla con o sin v&aacute;lvula</li>\r\n<li>Mascarilla cara completa par vapores org&aacute;nicos</li>\r\n<li>Mascarilla media cara</li>\r\n<li>Cartuchos de repuesto</li>\r\n<li>Overol blanco</li>\r\n<li>Entre otras cosas m&aacute;s...</li>\r\n</ul>', 0, '', '2019-07-12 16:03:00', '', 'admin'),
(77, '', 'MON&Oacute;MERO DE ESTIRENO', 'MONOMERO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 10, 0, 1, '', '', '', '', '', '', '14', '<p>Dise&ntilde;ado para disolver y/o diluir la resina. A diferencia de lo que generalmente uno conoce por \"diluyente\" el mismo <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#polimerizacion\">polimeriza</a> junto a la resina o el gelcoat, es decir, no se evapora como un disolvente.</p>', 0, '', '2019-07-19 14:37:29', '', 'admin'),
(78, '', 'PASTA PARA PULIR BLANCA', 'PASTABLANCA.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 11, 0, 1, '', '', '', '', '', '', '14', '<p>A diferencia de la pasta rosa, &eacute;sta nos ayuda a dar brillo a la pieza despu&eacute;s del pulido. Permite resultados r&aacute;pidos sin da&ntilde;ar las superficies a las que se aplica y sin dejar residuos, es de f&aacute;cil aplicaci&oacute;n en superficies como m&aacute;rmol, pisos, metales en general, pulido automotriz y acabados poli&eacute;ster.</p>', 0, '', '2019-07-19 15:02:54', '', 'admin'),
(79, '', 'PASTA PARA PULIR ROSA', 'PASTAROSA.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 11, 0, 1, '', '', '', '', '', '', '14', '<p>Pasta dise&ntilde;ada para pulir materiales r&uacute;sticos y de uso rudo, permite resultados r&aacute;pidos sin da&ntilde;ar las superficies a las que se aplica y sin dejar residuos. De f&aacute;cil aplicaci&oacute;n en superficies como m&aacute;rmol, pisos, metales y loseta.</p>', 0, '', '2019-07-19 15:04:31', '', 'admin'),
(80, '', 'DILUYENTE PARA SILIC&Oacute;N', 'DILUYENTEPSILICON.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 10, 0, 1, '', '', '', '', '', '', '14', '<p>Su funci&oacute;n es disminuir la <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a> del caucho as&iacute; como eliminar suciedad y desengrase de los moldes. Se recomienda agregarlo del 30 al 50%.</p>', 0, '', '2019-07-19 15:40:09', '', 'admin'),
(81, '', 'CAB-O-SIL', 'CABOSIL.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 9, 7, 0, 1, '', '', '', '', '', '', '9', '<p>Polvo fino de baja densidad e inoloro, aporta la cualidad <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#tixotropia\">tixotr&oacute;pica</a> a las resinas proporcionando una <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#viscosidad\">viscosidad</a> aparente. Est&aacute; dise&ntilde;ada para mejorar las propiedades de los compuestos pl&aacute;sticos reforzados con fibra de vidrio. No aporta ninguna tonalidad a la resina, sin embargo opaca el color.</p>', 0, '', '2019-07-19 15:56:16', '', 'admin'),
(82, '', 'EPOXIPARCHE 1C', 'EPOXIPARCHE1C.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 12, 0, 1, '', '', '', '', '', '', '14', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un adhesivo de dos componentes (resina y endurecedor) que al mezclarlos crean un sellador de alta resistencia, <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> r&aacute;pido y vers&aacute;til. Es resistente a aceites, solventes, calor, humedad y detergentes. Se utiliza principalmente para uni&oacute;n de concreto, fisuras, reparaci&oacute;n de grietas en superficies que van en contacto con agua como cisternas y tuber&iacute;as. O reparaci&oacute;n de moldes, modelos de fundici&oacute;n, reparaci&oacute;n de porcelana, laminados y fugas. A prueba de fallas el&eacute;ctricas.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Color: BLANCO</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Relaci&oacute;n: Resina (100) x Endurecedor (44). Tiempo de <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> de 10 a 15 min. Endurecimiento de 3 a 4 hrs. Curado: 24hrs.</p>', 0, '', '2019-07-19 16:04:46', '', 'admin'),
(83, '', 'EPOXIPARCHE 6C', 'EPOXIPARCHE6C.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 12, 0, 1, '', '', '', '', '', '', '14', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un adhesivo de dos componentes (resina y endurecedor) que al mezclarlos crean un sellador de alta resistencia, <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> r&aacute;pido y vers&aacute;til. Es resistente a aceites, solventes, calor, humedad y detergentes. Se utiliza principalmente para uni&oacute;n de concreto, fisuras, reparaci&oacute;n de grietas en superficies que van en contacto con agua como cisternas y tuber&iacute;as. O reparaci&oacute;n de moldes, modelos de fundici&oacute;n, reparaci&oacute;n de porcelana, laminados y fugas. A prueba de fallas el&eacute;ctricas.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Color: GRIS</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Relaci&oacute;n: Resina (100) x Endurecedor (44). Tiempo de <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> de 10 a 15 min. Endurecimiento de 3 a 4 hrs. Curado: 24hrs.</p>', 0, '', '2019-07-19 16:16:58', '', 'admin'),
(84, '', 'EPOXIPARCHE 151', 'EPOXIPARCHE151.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 12, 0, 1, '', '', '', '', '', '', '14', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un adhesivo de dos componentes (resina y endurecedor) que al mezclarlos crean un sellador de alta resistencia, <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> r&aacute;pido y vers&aacute;til. Es resistente a aceites, solventes, calor, humedad y detergentes. Se utiliza principalmente para uni&oacute;n de concreto, fisuras, reparaci&oacute;n de grietas en superficies que van en contacto con agua como cisternas y tuber&iacute;as. O reparaci&oacute;n de moldes, modelos de fundici&oacute;n, reparaci&oacute;n de porcelana, laminados y fugas. A prueba de fallas el&eacute;ctricas.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Color: TRANSPARENTE</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Relaci&oacute;n: Resina (100) x Endurecedor (44). Tiempo de <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> de 10 a 15 min. Endurecimiento de 3 a 4 hrs. Curado: 24hrs.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">De uso: Flexible</p>', 0, '', '2019-07-19 16:34:25', '', 'admin'),
(85, '', 'THINNER', 'THINNER.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 13, 0, 1, '', '', '', '', '', '', '14', '<p>Es un una mezcla de solventes que ha sido desarrollado para disolver, diluir o adelgazar sustancias insolubles en agua, como pinturas, aceites y grasas. Es incoloro y de olor fuerte caracter&iacute;stico.</p>', 0, '', '2019-07-19 16:51:54', '', 'admin'),
(86, '', 'ACETONA INDUSTRIAL', 'ACETONA.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 14, 13, 0, 1, '', '', '', '', '', '', '14', '<p>Solvente l&iacute;quido incoloro y de olor caracter&iacute;stico, se evapora f&aacute;cilmente, es inflamable y soluble en agua. Para limpieza de superficies y manos despu&eacute;s de estar en contacto con la resina.</p>', 0, '', '2019-07-19 16:52:09', '', 'admin'),
(87, '', 'BARINIZ Y QUITA BURBUJA PARA FOTOGRAF&Iacute;A', 'BARNICESPARAFOTO.png', '', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 1, '', '', '', '', '', '', '1', '<p style=\"text-align: justify; font-size: 0.9em;\">Es un sistema de dos componentes 06A/06B , 12A/12B Y 24A/24B (tiempo de secado de 06, 12 y 24 hrs), curan a temperatura ambiente creando una capa brillante y transparente, para recubrimiento de fotograf&iacute;as y p&oacute;sters. Para eliminar las burbujas atrapadas, despu&eacute;s de vaciar el barniz sobre la foto rociar una capa con quitaburbuja en aerosol. Su principal aplicaci&oacute;n es en encapsulados de fotograf&iacute;as, p&oacute;sters, trabajos de decoraci&oacute;n y protecci&oacute;n de superficies como madera y metal.</p>\r\n<p style=\"text-align: justify; font-size: 0.9em;\">Tiempo de <a href=\"http://www.fibrecen.com.mx/productos/glosario.html#gelado\">gelado</a> 30&plusmn;10min. Tiempo de secado al tacto 3 a 4 hrs. Dejar reposar para el curado completo el tiempo de la presentaci&oacute;n que se ocupe (06, 12 o 24 hrs).</p>', 0, '', '2019-07-19 17:09:07', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_cate`
--

CREATE TABLE `fib_productos_cate` (
  `ID_cate` int(6) UNSIGNED NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL,
  `ord` int(2) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos_cate`
--

INSERT INTO `fib_productos_cate` (`ID_cate`, `categoria`, `des`, `ord`, `cover`, `visible`) VALUES
(1, 'RESINAS', 'Liquido viscoso que se transforma en solido', 1, 'resinas.png', 1),
(2, 'FIBRA DE VIDRIO', 'Filamentos que a&ntilde;aden resistencia mecanica', 2, 'FIBRASS.jpg', 1),
(3, 'GEL COAT', 'Da acabado liso y brilloso a la pieza final', 3, 'gelcoat.png', 1),
(4, 'PEGAMENTOS', 'Adhesivo para el pegado de m&aacute;rmol natural y sint&eacute;tico', 4, 'pegamento-masilla.jpg', 1),
(5, 'SILICONES', 'Material para hacer moldes flexibles', 5, 'moldesilicon.png', 1),
(6, 'CATALIZADORES', 'Hace gelar y curar la resina', 6, 'CATALIZADORESS.jpg', 1),
(7, 'ACELERADORES', 'Acelera el curado de la resina', 7, 'ACELERADORES.jpg', 1),
(8, 'DESMOLDANTES', 'Ayuda a desprender la forma del molde', 8, 'DESMOL.jpg', 1),
(9, 'CARGAS', 'Se a&Atilde;&plusmn;ade a la resina para dar volumen o forma al producto final', 9, 'CARGASSS.jpg', 1),
(10, 'PIGMENTOS', 'A&ntilde;ade color deseado', 10, 'PIGMENTOSS.jpg', 1),
(11, 'ESPUMAS DE POLIURETANO', 'Material empleado para rellenar, sellar y aislar', 11, 'ESPUMASPOLIURETANO.jpg', 1),
(12, 'ABRASIVOS Y HERRAMIENTAS', '', 12, 'herramientassss.jpg', 1),
(13, 'EQUIPO DE SEGURIDAD', '', 13, 'seguridad.jpg', 1),
(14, 'OTROS PRODUCTOS', '', 14, 'thinneryacetona.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_coti`
--

CREATE TABLE `fib_productos_coti` (
  `ID_bills` int(9) UNSIGNED NOT NULL,
  `ID_pro` int(9) NOT NULL,
  `cant` int(6) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `ID_cate` int(9) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `login` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_marcas`
--

CREATE TABLE `fib_productos_marcas` (
  `ID_marca` int(6) UNSIGNED NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos_marcas`
--

INSERT INTO `fib_productos_marcas` (`ID_marca`, `nombre`, `visible`) VALUES
(1, 'Samsung', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_productos_sub_cate`
--

CREATE TABLE `fib_productos_sub_cate` (
  `ID_sub_cate` int(6) UNSIGNED NOT NULL,
  `subcategoria` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL,
  `ord` int(2) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_productos_sub_cate`
--

INSERT INTO `fib_productos_sub_cate` (`ID_sub_cate`, `subcategoria`, `des`, `ord`, `ID_cate`, `cover`, `visible`) VALUES
(1, 'RESINAS POLI&Eacute;STER', '', 1, 1, 'resinas.png', 1),
(2, 'RESINAS EP&Oacute;XICAS', 'CRISTALINA, AUNTONIVELANTE Y ALTA RESISTENCIA', 2, 1, 'recipiente.png', 1),
(3, 'DE REFUERZO', '', 3, 2, 'DEREFUERZO.png', 1),
(4, 'AISLANTES', '', 4, 2, 'ailantes.png', 1),
(5, 'SILICON DE CUACHO', '', 5, 5, 'moldesilicon.png', 1),
(6, 'OTROS', '', 6, 5, 'OTROSSILICONES.png', 1),
(7, 'GRANO FINO', '', 7, 9, 'granofino.jpg', 1),
(8, 'GRANO GRUESO', '', 8, 9, 'nodisponible.jpg', 1),
(9, 'PASTAS RESANADORAS', 'USO AUTOMOTR&Iacute;Z Y USO MARINO', 0, 14, 'RESANADORASPASTA.png', 1),
(10, 'DILUYENTES', 'Dise&ntilde;ado para disolver y/o diluir la resina. A diferencia de lo que generalmente uno conoce por &quot;diluyente&quot; el mismo polimeriza junto a la resina o el gelcoat, es decir, no se evapora como un disolvente.', 0, 14, 'DILUYENTES.png', 1),
(11, 'PASTAS PARA PULIR', '', 0, 14, 'barraspulir.png', 1),
(12, 'EPOXIPARCHES', '', 0, 14, 'EPOXIPARCHEE.png', 1),
(13, 'SOLVENTES', '', 0, 14, 'SOLVENTES.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_registros`
--

CREATE TABLE `fib_registros` (
  `ID` int(9) UNSIGNED NOT NULL,
  `ip` varchar(25) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `asunto` varchar(150) NOT NULL,
  `msj` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_list` varchar(50) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `adjuntos` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_registros`
--

INSERT INTO `fib_registros` (`ID`, `ip`, `nombre`, `email`, `tel`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `visible`) VALUES
(1, '177.232.84.138', 'Miguel ', 'd@d.com', '441234567', 'CURSOS - Mi primer curso', 'Mensaje de prueba secci&oacute;n cursos', '2019-06-16 18:03:43', 'inbox', 'cursos', '', '', 1, 1),
(2, '187.145.59.26', 'ramses ', 'fibrecen@gmail.com', '4422199937', 'CURSOS - MESAS DE PAROTA CON RESINA EP&Oacute;XICA QUER&Eacute;TARO', 'quiero inscribirme', '2019-07-03 15:11:49', 'inbox', 'cursos', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_signup`
--

CREATE TABLE `fib_signup` (
  `ID` int(9) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` varchar(2) NOT NULL,
  `lastlogin` datetime NOT NULL,
  `tema` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apaterno` varchar(100) NOT NULL,
  `amaterno` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `tel` varchar(100) NOT NULL,
  `ext` int(4) NOT NULL,
  `fnac` date NOT NULL,
  `fb` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `ndepa` int(1) NOT NULL,
  `depa` varchar(100) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `mpio` varchar(100) NOT NULL,
  `edo` varchar(100) NOT NULL,
  `genero` varchar(20) NOT NULL,
  `exp` varchar(1000) NOT NULL,
  `likes` int(6) NOT NULL,
  `filtro` varchar(50) NOT NULL,
  `zona` varchar(50) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `actualizacion` varchar(20) NOT NULL,
  `page` varchar(250) NOT NULL,
  `nivel_oper` int(2) NOT NULL,
  `rol` int(2) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_signup`
--

INSERT INTO `fib_signup` (`ID`, `username`, `password`, `email`, `level`, `lastlogin`, `tema`, `nombre`, `apaterno`, `amaterno`, `foto`, `cover`, `tel`, `ext`, `fnac`, `fb`, `tw`, `puesto`, `ndepa`, `depa`, `empresa`, `adress`, `direccion`, `mpio`, `edo`, `genero`, `exp`, `likes`, `filtro`, `zona`, `alta`, `actualizacion`, `page`, `nivel_oper`, `rol`, `activo`) VALUES
(1, 'admin', 'admin79x', 'multiportal@outlook.com', '-1', '2019-07-15 15:28:39', 'default', 'Guillermo', 'Jimenez', 'Lopez', 'sinfoto.png', '', '4421944950', 1, '0000-00-00', '', '', 'Director', 0, '', 'Multiportal', '', '', '', '', 'M', '', 0, '', '', '', '', '', 0, 0, 1),
(2, 'demo', 'demo2017', 'd@d@gmail.com', '-1', '0000-00-00 00:00:00', 'default', 'Demo', 'Apaterno', 'Amaterno', 'sinfoto.png', 'sincover.jpg', '4421234567', 0, '0000-00-00', '', '', 'Director', 0, '', 'PHPONIX', '', '', '', '', 'M', '', 0, '0', '', '', '', '', 0, 0, 0),
(5, 'FIBRECEN', 'fibrecen.2019', 'fibrecen@gmail.com', '-1', '0000-00-00 00:00:00', '', 'Admin Fibrecen', '', '', '', '', '', 0, '0000-00-00', '', '', 'Director', 0, '', '', '', '', '', '', 'M', '', 0, '', '', '', '', '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_slider`
--

CREATE TABLE `fib_slider` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ima` varchar(100) NOT NULL,
  `tit1` varchar(200) NOT NULL,
  `tit2` varchar(200) NOT NULL,
  `btn_nom` varchar(50) NOT NULL,
  `url` varchar(300) NOT NULL,
  `tema_slider` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_slider`
--

INSERT INTO `fib_slider` (`ID`, `ima`, `tit1`, `tit2`, `btn_nom`, `url`, `tema_slider`, `visible`) VALUES
(1, 'home3.jpg', 'fibrecen', 'Fibra de vidrio y carbono', 'Boton', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(2, 'home4.jpg', 'fibrecen', 'Resinas ', 'boton2', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(3, 'home7.jpg', 'fibra', 'Cursos', 'video', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_tareas`
--

CREATE TABLE `fib_tareas` (
  `ID` int(11) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_temas`
--

CREATE TABLE `fib_temas` (
  `ID` int(3) UNSIGNED NOT NULL,
  `tema` varchar(100) NOT NULL,
  `subtema` varchar(100) NOT NULL,
  `selec` tinyint(1) NOT NULL,
  `nivel` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_temas`
--

INSERT INTO `fib_temas` (`ID`, `tema`, `subtema`, `selec`, `nivel`) VALUES
(1, 'default', '', 0, '0'),
(2, 'temp', '', 0, '0'),
(3, 'fibrecen', '', 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_testimonios`
--

CREATE TABLE `fib_testimonios` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `pro` varchar(100) NOT NULL,
  `comentario` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fib_testimonios`
--

INSERT INTO `fib_testimonios` (`ID`, `cover`, `pro`, `comentario`, `visible`) VALUES
(1, 'testimonial_person2.jpg', 'Ingeniera Civil', 'Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda', 1),
(2, 'testimonial_person1.jpg', 'Emprendedor', 'Su curso se me hizo f&aacute;cil y muy creativo, impartidos por excelentes maestros.', 1),
(3, 'testimonial_person3.jpg', 'Ingeniera Industrial', 'Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda.', 1),
(4, 'TESTIMONIO01.png', 'Emprendedor', 'Excelente curso introducci&oacute;n a los materiales compuestos, muchas gracias.', 1),
(5, 'testimonio02.png', 'Emprendedor', 'Excelente curso de Mesas Ep&oacute;xicas en Parota y Cristal Templado.  &iexcl;No dejen pasar la oportunidad de tomar este curso!', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fib_visitas`
--

CREATE TABLE `fib_visitas` (
  `ID` int(9) UNSIGNED NOT NULL,
  `IPv4` bigint(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `browser` varchar(100) NOT NULL,
  `navegador` varchar(50) NOT NULL,
  `version` varchar(100) NOT NULL,
  `os` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `user` varchar(50) NOT NULL,
  `page` varchar(500) NOT NULL,
  `refer` varchar(500) NOT NULL,
  `vhref` varchar(500) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `idp` varchar(50) NOT NULL,
  `unload` datetime NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fib_blog`
--
ALTER TABLE `fib_blog`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_blog_coment`
--
ALTER TABLE `fib_blog_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_comp`
--
ALTER TABLE `fib_comp`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_config`
--
ALTER TABLE `fib_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_contacto`
--
ALTER TABLE `fib_contacto`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_contacto_forms`
--
ALTER TABLE `fib_contacto_forms`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_cursos`
--
ALTER TABLE `fib_cursos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_cursos_coment`
--
ALTER TABLE `fib_cursos_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_depa`
--
ALTER TABLE `fib_depa`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_directorio`
--
ALTER TABLE `fib_directorio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_galeria`
--
ALTER TABLE `fib_galeria`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_glosario`
--
ALTER TABLE `fib_glosario`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_histo_backupdb`
--
ALTER TABLE `fib_histo_backupdb`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_home_config`
--
ALTER TABLE `fib_home_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_home_elementos`
--
ALTER TABLE `fib_home_elementos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_iconos`
--
ALTER TABLE `fib_iconos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_ipbann`
--
ALTER TABLE `fib_ipbann`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_landingpage_seccion`
--
ALTER TABLE `fib_landingpage_seccion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_map_config`
--
ALTER TABLE `fib_map_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_map_ubicacion`
--
ALTER TABLE `fib_map_ubicacion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_menu_admin`
--
ALTER TABLE `fib_menu_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_menu_web`
--
ALTER TABLE `fib_menu_web`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_mode_page`
--
ALTER TABLE `fib_mode_page`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_modulos`
--
ALTER TABLE `fib_modulos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_nosotros`
--
ALTER TABLE `fib_nosotros`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_notificacion`
--
ALTER TABLE `fib_notificacion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_opciones`
--
ALTER TABLE `fib_opciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_pages`
--
ALTER TABLE `fib_pages`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_portafolio`
--
ALTER TABLE `fib_portafolio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_productos`
--
ALTER TABLE `fib_productos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_productos_cate`
--
ALTER TABLE `fib_productos_cate`
  ADD PRIMARY KEY (`ID_cate`);

--
-- Indices de la tabla `fib_productos_coti`
--
ALTER TABLE `fib_productos_coti`
  ADD PRIMARY KEY (`ID_bills`);

--
-- Indices de la tabla `fib_productos_marcas`
--
ALTER TABLE `fib_productos_marcas`
  ADD PRIMARY KEY (`ID_marca`);

--
-- Indices de la tabla `fib_productos_sub_cate`
--
ALTER TABLE `fib_productos_sub_cate`
  ADD PRIMARY KEY (`ID_sub_cate`);

--
-- Indices de la tabla `fib_registros`
--
ALTER TABLE `fib_registros`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_signup`
--
ALTER TABLE `fib_signup`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_slider`
--
ALTER TABLE `fib_slider`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_tareas`
--
ALTER TABLE `fib_tareas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_temas`
--
ALTER TABLE `fib_temas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_testimonios`
--
ALTER TABLE `fib_testimonios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fib_visitas`
--
ALTER TABLE `fib_visitas`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `fib_blog`
--
ALTER TABLE `fib_blog`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_blog_coment`
--
ALTER TABLE `fib_blog_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_comp`
--
ALTER TABLE `fib_comp`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_config`
--
ALTER TABLE `fib_config`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_contacto`
--
ALTER TABLE `fib_contacto`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_contacto_forms`
--
ALTER TABLE `fib_contacto_forms`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fib_cursos`
--
ALTER TABLE `fib_cursos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `fib_cursos_coment`
--
ALTER TABLE `fib_cursos_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_depa`
--
ALTER TABLE `fib_depa`
  MODIFY `ID` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `fib_directorio`
--
ALTER TABLE `fib_directorio`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fib_galeria`
--
ALTER TABLE `fib_galeria`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `fib_glosario`
--
ALTER TABLE `fib_glosario`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `fib_histo_backupdb`
--
ALTER TABLE `fib_histo_backupdb`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_home_config`
--
ALTER TABLE `fib_home_config`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_home_elementos`
--
ALTER TABLE `fib_home_elementos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_iconos`
--
ALTER TABLE `fib_iconos`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `fib_ipbann`
--
ALTER TABLE `fib_ipbann`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_landingpage_seccion`
--
ALTER TABLE `fib_landingpage_seccion`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_map_config`
--
ALTER TABLE `fib_map_config`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fib_map_ubicacion`
--
ALTER TABLE `fib_map_ubicacion`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_menu_admin`
--
ALTER TABLE `fib_menu_admin`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `fib_menu_web`
--
ALTER TABLE `fib_menu_web`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `fib_mode_page`
--
ALTER TABLE `fib_mode_page`
  MODIFY `ID` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_modulos`
--
ALTER TABLE `fib_modulos`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `fib_nosotros`
--
ALTER TABLE `fib_nosotros`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_notificacion`
--
ALTER TABLE `fib_notificacion`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fib_opciones`
--
ALTER TABLE `fib_opciones`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `fib_pages`
--
ALTER TABLE `fib_pages`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_portafolio`
--
ALTER TABLE `fib_portafolio`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `fib_productos`
--
ALTER TABLE `fib_productos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT de la tabla `fib_productos_cate`
--
ALTER TABLE `fib_productos_cate`
  MODIFY `ID_cate` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `fib_productos_coti`
--
ALTER TABLE `fib_productos_coti`
  MODIFY `ID_bills` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fib_productos_marcas`
--
ALTER TABLE `fib_productos_marcas`
  MODIFY `ID_marca` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fib_productos_sub_cate`
--
ALTER TABLE `fib_productos_sub_cate`
  MODIFY `ID_sub_cate` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `fib_registros`
--
ALTER TABLE `fib_registros`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fib_signup`
--
ALTER TABLE `fib_signup`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_slider`
--
ALTER TABLE `fib_slider`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_tareas`
--
ALTER TABLE `fib_tareas`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fib_temas`
--
ALTER TABLE `fib_temas`
  MODIFY `ID` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fib_testimonios`
--
ALTER TABLE `fib_testimonios`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fib_visitas`
--
ALTER TABLE `fib_visitas`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
