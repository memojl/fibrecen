<?php 
function cadena_replace(&$replace1,&$replace2){
	$replace1=array(' ','.',',','(',')','/','"',"’",'á','é','í','ó','ú','&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','Á','É','Í','Ó','Ú','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','ñ','Ñ','&ntilde;','&Ntilde;');
	$replace2=array('-','-','-','-','-','-','-',"",'a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','n','N','n','N');
}

function html_iso(&$nombre,&$cate,&$des){
global $chartset;
 if($chartset=='iso-8859-1'){
	$nombre=htmlentities($nombre, ENT_COMPAT,'ISO-8859-1', true);
	$des = htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
	//$resena = $resena, ENT_COMPAT,'ISO-8859-1', true);	
	$cate=htmlentities($cate, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function html_iso_productos(&$nombre,&$modelo,&$des){
global $chartset;
 if($chartset=='iso-8859-1'){
	$nombre=htmlentities($nombre, ENT_COMPAT,'ISO-8859-1', true);
	$modelo=htmlentities($modelo, ENT_COMPAT,'ISO-8859-1', true);	
	$des = htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function html_iso_cate(&$cate,&$des){
global $chartset;
 if($chartset=='iso-8859-1'){
	$cate=htmlentities($cate, ENT_COMPAT,'ISO-8859-1', true);
	$des=htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function html_iso_subcate(&$subcate,&$cate,&$des){
global $chartset;
 if($chartset=='iso-8859-1'){
	$subcate=htmlentities($subcate, ENT_COMPAT,'ISO-8859-1', true);
	$cate=htmlentities($cate, ENT_COMPAT,'ISO-8859-1', true);
	$des=htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function control_cover($cover,&$file,$m){
global $page_url,$mod,$ext;
$cover=($cover!='')?$cover:'nodisponible.jpg';
$file='<input type="hidden" class="form-control" id="cover" name="cover" value="'.$cover.'">
<div><img src="'.$page_url.'modulos/'.$mod.'/fotos/'.$cover.'" style="width:100%;"></div>
<div><a href="javascript:up(1);">'.$m.' Imagen</a><div id="upload"></div></div>
';
}

function jQuery_select_cate_subcate(){
echo '
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script language="javascript">
$(document).ready(function(){
    $("#cate").on(\'change\', function () {
        $("#cate option:selected").each(function () {
            elegido=$(this).val();
            $.post("modulos/productos/admin/_query_categorias.php", { elegido: elegido }, function(data){
                $("#subcate").html(data);
            });			
        });
   });
});
</script>
';
}

function select_cate($cate){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="cate" name="cate">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){$ID_cate=$row['ID_cate'];$clave=$row['ord'];$categoria=$row['categoria'];
		$seleccion=($ID_cate==$cate)?'selected' : '';
		echo '<option value="'.$ID_cate.'" '.$seleccion.'>'.$clave.' '.$categoria.'</option>';
	}
echo '</select>';	
}

function select_sub_cate($cate,$subcate){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_cate='{$cate}' ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="subcate" name="subcate">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){$ID_sub_cate=$row['ID_sub_cate'];$clave=$row['ord'];$subcategoria=$row['subcategoria'];
		$seleccion=($ID_sub_cate==$subcate) ? 'selected' : '';
		echo '<option value="'.$ID_sub_cate.'" '.$seleccion.'>'.$clave.' '.$subcategoria.'</option>';
	}
echo '</select>';	
}


/*
function select_cate($cate){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="cate" name="cate">';
	while($row=mysqli_fetch_array($sql)){
		$seleccion=($row['ID_cate']==$cate) ? 'selected' : '';
		echo '<option value="'.$row['ID_cate'].'" '.$seleccion.'>'.$row['categoria'].'</option>';
	}
echo '</select>';	
}

function select_sub_cate($cate,$subcate){
global $mysqli,$DBprefix,$opc;
$cond=($opc=='edit')?"WHERE ID_cate='{$cate}' ":'';	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate ".$condi."ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="subcate" name="subcate">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){
		$sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='".$row['ID_cate']."' ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
		while($row1=mysqli_fetch_array($sql1)){$categoria=$row1['categoria'];}
		$seleccion=($row['ID_sub_cate']==$subcate) ? 'selected' : '';
		echo '<option value="'.$row['ID_sub_cate'].'" '.$seleccion.'>'.$row['subcategoria'].'-'.$categoria.'</option>';
	}
echo '</select>';	
}
*/
function select_marcas($ID_marca){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_marcas WHERE visible=1 ORDER BY ID_marca;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="marcas" name="marcas">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){$ID_marca1=$row['ID_marca'];$nombre=$row['nombre'];
		$seleccion=($ID_marca1==$ID_marca)?'selected':'';
		echo '<option value="'.$ID_marca1.'" '.$seleccion.'>'.$nombre.'</option>';
	}
echo '</select>';	
}

function nombre_cate_subcate($id,&$data_cate_subcate){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;

$menu_json='productos.json';
$path_JSON='modulos/productos/'.$menu_json;

 if(file_exists($path_JSON) && filesize($path_JSON)!=0){
  $objData=file_get_contents($path_JSON);
  $Data=json_decode($objData,true);
  if($Data!='' && $Data!=NULL){
  $i=0;
  //echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$ID_cate_p=$reg['ID_cate'];
		$ID_sub_cate_p=$reg['ID_sub_cate'];
		$visible=$reg['visible'];
		if($visible==1 && $ID==$id){
			
			//echo '<!-- categorias.json -->';
			$path_JSON="modulos/productos/categorias.json";		
			if(file_exists($path_JSON) && filesize($path_JSON)!=0){
				$objData2=file_get_contents($path_JSON);
				$Data2=json_decode($objData2,true);
				foreach ($Data2 as $row){
					$ID_cate=$row['ID_cate'];
					if($ID_cate_p==$ID_cate){
						$categoria=$row['categoria'];
					}
				}
			}else{
	    		$sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$ID_cate_p}';") or print mysqli_error($mysqli);
				while($row1=mysqli_fetch_array($sql1)){
					$categoria=$row1['categoria'];
				}
			}
			//echo '<!-- /categorias.json -->';

			//echo '<!-- subcategorias.json -->';		
			$path_JSON="modulos/productos/categorias.json";		
			if(file_exists($path_JSON) && filesize($path_JSON)!=0){
				$objData3=file_get_contents("modulos/productos/subcategorias.json");
				$Data3=json_decode($objData3,true);
				foreach ($Data3 as $row){
					$ID_sub_cate=$row['ID_sub_cate'];
					if($ID_sub_cate_p==$ID_sub_cate){
						$subcategoria=$row['subcategoria'];
					}
				}
			}else{
				$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate='{$ID_sub_cate_p}';") or print mysqli_error($mysqli);
					while($row2=mysqli_fetch_array($sql2)){
						$subcategoria=$row2['subcategoria'];
				}
			}
			//echo '<!-- /subcategorias.json -->';
			$data_cate_subcate=array($ID,$ID_cate_p,$categoria,$ID_sub_cate_p,$subcategoria);						
		}
	}
  }
  //echo '<!-- /productos.json -->';
 }else{
  $sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID='{$id}';") or print mysqli_error($mysqli);
  while($row=mysqli_fetch_array($sql)){
		$ID=$row['ID'];
		$ID_cate_p=$row['ID_cate'];
		$ID_sub_cate_p=$row['ID_sub_cate'];
	    $sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$ID_cate_p}';") or print mysqli_error($mysqli);
		while($row1=mysqli_fetch_array($sql1)){
			$categoria=$row1['categoria'];
		}
		$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate='{$ID_sub_cate_p}';") or print mysqli_error($mysqli);
		while($row2=mysqli_fetch_array($sql2)){
			$subcategoria=$row2['subcategoria'];
		}
  }
  $data_cate_subcate=array($ID,$ID_cate_p,$categoria,$ID_sub_cate_p,$subcategoria);						
 } 
}

function menu_cate1(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$id,$idp;	
echo '
<link href="'.$page_url.'modulos/productos/css/menup.css" rel="stylesheet">
<script src="'.$page_url.'modulos/productos/js/ddaccordion.js" type="text/javascript"></script>
<script type="text/javascript">
//Initialize first demo:
ddaccordion.init({
    headerclass: "mypets", //Shared CSS class name of headers group-compartido CSS nombre de la clase de grupo encabezados
    contentclass: "thepet", //Shared CSS class name of contents group-compartido CSS nombre de la clase de grupo de contenidos
    revealtype: "clickgo", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"-Reveal contenido cuando el usuario hace clic o onmouseover la cabecera ? Valor válido: " click", " clickgo " , o " mouseover "
    mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover-si revealtype = " mouseover " , establecer el retardo en milisegundos antes de cabecera expande onMouseover
    collapseprev: true, //Collapse previous content (so only one open at any time)? true/false-//Contraer contenido anterior ( lo único abierto en cualquier momento) ? verdadero / falso 
    defaultexpanded: [], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.-//índice de contenido ( s ) abierta por defecto [ index1 , index2 , etc] . [] Denota ningún contenido .
    onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
    animatedefault: true, //Should contents open by default be animated into view?-En caso de contenidos abiertos por defecto será animado a la vista ?
    persiststate: true, //persist state of opened contents within browser session?-//persistir estado de contenidos abiertos en la sesión del navegador ?
    toggleclass: ["", "openpet"], //Two CSS classes to be applied to the header when it\'s collapsed and expanded, respectively ["class1", "class2"]-// Dos clases CSS que se aplicarán a la cabecera cuando se derrumbó y se expandió , respectivamente [" class1 " , " clase 2 " ]
    togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it\'s collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)-//HTML adicional añade a la cabecera cuando se derrumbó y se expandió , respectivamente [" posición" , " html1 " , " HTML2 " ] ( ver docs )
    animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"-//velocidad de la animación : entero en milisegundos ( es decir : 200 ), o palabras clave "rápida", "normal " , o "lento"
    oninit:function(headers, expandedindices){ //custom code to run when headers have initalized-//código personalizado para ejecutarse cuando cabeceras han initalized
        //do nothing
    },
    onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
        //do nothing
    }
})
</script>
';
//Menu-Categoria
echo '<style>.glo{text-align:center;}.glo a{color:#6b6e80;}.glo a:hover{color:#ed1c24;}</style>';
if($mod=='productos' && $ext=='categoria'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 AND ID_cate='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_cate=$row['ID_cate'];}
	if($ID_cate==$idp){
		echo '<style>.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
//Menu-Subcategoria
if($mod=='productos' && $ext=='subcategoria'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_sub_cate='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_sub_cate=$row['ID_sub_cate'];$ID_cate=$row['ID_cate'];}
	if($ID_sub_cate==$idp){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
if($mod=='productos' && $ext=='item'){
	nombre_cate_subcate($idp,$data_cate_subcate);
	$ID_cate=$data_cate_subcate[1];
	$ID_sub_cate=$data_cate_subcate[3];
	//echo '['.$idp.'] '.$ID_cate.'-'.$ID_sub_cate;
	if($idp!='' && $ID_cate!='' && $ID_sub_cate!=''){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
}
function menu_cate2(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$id,$idp;	
//MENU
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 ORDER BY ord") or print mysqli_error($mysqli);
	while($row=mysqli_fetch_array($sql)){$icon=$row['icon'];
	cadena_replace($replace1,$replace2);
	$categoria=str_replace($replace1,$replace2,$row['categoria']);
		$link1=($_GET['tema_previo']!='')?$page_url.'index.php?mod=productos&ext=categoria&id='.$row['ID_cate'].'&tema_previo='.$_GET['tema_previo']:$page_url.'productos/categoria/'.$row['ID_cate'].'-'.$categoria;
		$icono=($icon=='' && $icon==NULL)?'<div id="circulo"></div>&nbsp;&nbsp;':'<i class="fa '.$icon.'"></i>&nbsp;&nbsp;';
		echo '<div class="mypets">'.$icono.'<a class="cate_'.$row['ID_cate'].'" href="'.$link1.'">'.$row['categoria'].'</a></div>'."\n";
		echo '<div class="thepet">'."\n";
        $sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_cate={$row[ID_cate]} ORDER BY ID_sub_cate") or print mysqli_error($mysqli);
        while($row2=mysqli_fetch_array($sql2)){$sub_cate=ucfirst($row2["subcategoria"]);
		cadena_replace($replace1,$replace2);
		$subcategoria=str_replace($replace1,$replace2,$row2['subcategoria']);
			$link2=($_GET['tema_previo']!='')?$page_url.'index.php?mod=productos&ext=subcategoria&id='.$row2['ID_sub_cate'].'&tema_previo='.$_GET['tema_previo']:$page_url.'productos/subcategoria/'.$row2['ID_sub_cate'].'-'.$subcategoria;
            echo '-&nbsp;&nbsp;<a class="subcat_'.$row2['ID_sub_cate'].'" href="'.$link2.'">'.wordwrap($sub_cate, 75, '<br>').'</a><div id="espacio"></div>';				       
        }
        echo '</div>'."\n";
	}
	echo '<hr>';
	echo '<div class="glo">-&nbsp;&nbsp;<a href="'.$page_url.'glosario/">GLOSARIO</a>&nbsp;&nbsp;-</div>';	
}

function menu_categoria(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$id,$idp;	
echo '
<link href="'.$page_url.'modulos/productos/css/menup.css" rel="stylesheet">
<script src="'.$page_url.'modulos/productos/js/ddaccordion.js" type="text/javascript"></script>
<script type="text/javascript">
//Initialize first demo:
ddaccordion.init({
    headerclass: "mypets", //Shared CSS class name of headers group-compartido CSS nombre de la clase de grupo encabezados
    contentclass: "thepet", //Shared CSS class name of contents group-compartido CSS nombre de la clase de grupo de contenidos
    revealtype: "clickgo", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"-Reveal contenido cuando el usuario hace clic o onmouseover la cabecera ? Valor válido: " click", " clickgo " , o " mouseover "
    mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover-si revealtype = " mouseover " , establecer el retardo en milisegundos antes de cabecera expande onMouseover
    collapseprev: true, //Collapse previous content (so only one open at any time)? true/false-//Contraer contenido anterior ( lo único abierto en cualquier momento) ? verdadero / falso 
    defaultexpanded: [], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.-//índice de contenido ( s ) abierta por defecto [ index1 , index2 , etc] . [] Denota ningún contenido .
    onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
    animatedefault: true, //Should contents open by default be animated into view?-En caso de contenidos abiertos por defecto será animado a la vista ?
    persiststate: true, //persist state of opened contents within browser session?-//persistir estado de contenidos abiertos en la sesión del navegador ?
    toggleclass: ["", "openpet"], //Two CSS classes to be applied to the header when it\'s collapsed and expanded, respectively ["class1", "class2"]-// Dos clases CSS que se aplicarán a la cabecera cuando se derrumbó y se expandió , respectivamente [" class1 " , " clase 2 " ]
    togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it\'s collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)-//HTML adicional añade a la cabecera cuando se derrumbó y se expandió , respectivamente [" posición" , " html1 " , " HTML2 " ] ( ver docs )
    animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"-//velocidad de la animación : entero en milisegundos ( es decir : 200 ), o palabras clave "rápida", "normal " , o "lento"
    oninit:function(headers, expandedindices){ //custom code to run when headers have initalized-//código personalizado para ejecutarse cuando cabeceras han initalized
        //do nothing
    },
    onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
        //do nothing
    }
})
</script>
';
//Menu-Categoria
echo '<style>.glo{text-align:center;}.glo a{color:#6b6e80;}.glo a:hover{color:#ed1c24;}</style>';
if($mod=='productos' && $ext=='categoria'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 AND ID_cate='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_cate=$row['ID_cate'];}
	if($ID_cate==$idp){
		echo '<style>.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
//Menu-Subcategoria
if($mod=='productos' && $ext=='subcategoria'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_sub_cate='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_sub_cate=$row['ID_sub_cate'];$ID_cate=$row['ID_cate'];}
	if($ID_sub_cate==$idp){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
if($mod=='productos' && $ext=='item'){
	nombre_cate_subcate($idp,$data_cate_subcate);
	$ID_cate=$data_cate_subcate[1];
	$ID_sub_cate=$data_cate_subcate[3];
	//echo '['.$idp.'] '.$ID_cate.'-'.$ID_sub_cate;
	if($idp!='' && $ID_cate!='' && $ID_sub_cate!=''){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
//MENU
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 ORDER BY ord") or print mysqli_error($mysqli);
	while($row=mysqli_fetch_array($sql)){$icon=$row['icon'];
	cadena_replace($replace1,$replace2);
	$categoria=str_replace($replace1,$replace2,$row['categoria']);
		$link1=($_GET['tema_previo']!='')?$page_url.'index.php?mod=productos&ext=categoria&id='.$row['ID_cate'].'&tema_previo='.$_GET['tema_previo']:$page_url.'productos/categoria/'.$row['ID_cate'].'-'.$categoria;
		$icono=($icon=='' && $icon==NULL)?'<div id="circulo"></div>&nbsp;&nbsp;':'<i class="fa '.$icon.'"></i>&nbsp;&nbsp;';
		echo '<div class="mypets">'.$icono.'<a class="cate_'.$row['ID_cate'].'" href="'.$link1.'">'.$row['categoria'].'</a></div>'."\n";
		echo '<div class="thepet">'."\n";
        $sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_cate={$row[ID_cate]} ORDER BY ID_sub_cate") or print mysqli_error($mysqli);
        while($row2=mysqli_fetch_array($sql2)){$sub_cate=ucfirst($row2["subcategoria"]);
		cadena_replace($replace1,$replace2);
		$subcategoria=str_replace($replace1,$replace2,$row2['subcategoria']);
			$link2=($_GET['tema_previo']!='')?$page_url.'index.php?mod=productos&ext=subcategoria&id='.$row2['ID_sub_cate'].'&tema_previo='.$_GET['tema_previo']:$page_url.'productos/subcategoria/'.$row2['ID_sub_cate'].'-'.$subcategoria;
            echo '-&nbsp;&nbsp;<a class="subcat_'.$row2['ID_sub_cate'].'" href="'.$link2.'">'.wordwrap($sub_cate, 75, '<br>').'</a><div id="espacio"></div>';				       
        }
        echo '</div>'."\n";
	}
	echo '<hr>';
	echo '<div class="glo">-&nbsp;&nbsp;<a href="'.$page_url.'glosario/">GLOSARIO</a>&nbsp;&nbsp;-</div>';	
}

function all_item_productos(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
//$topic='item';
$menu_json='productos.json';
$path_JSON='modulos/'.$mod.'/'.$menu_json;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/productos/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- productos.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$cover=$reg['cover'];
			cadena_replace($replace1,$replace2);
			$producto=str_replace($replace1,$replace2,$reg['nombre']);
			$nom_producto=$reg['nombre'];
			$descripcion=$reg['descripcion'];
			$ID_cate=$reg['ID_cate'];
			$ID_sub_cate=$reg['ID_sub_cate'];
			$visible=$reg['visible'];
			if($visible==1){
				echo '<!--['.$i.'] -'.$ID.'-->
				<div class="col-md-4 proj_gallery_grid" data-aos="zoom-in"><!--div class="col-md-4 proj_gallery_grid act_popup" data-aos="zoom-in"-->
					<div class="section_1_gallery_grid"><!--div class="section_1_gallery_grid"-->
						<a title="'.$nom_producto.'" href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">
							<div class="section_1_gallery_grid1">
								<img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt=" " class="img-responsive" />
								<div class="proj_gallery_grid1_pos">
									<h3>'.$nom_producto.'</h3>
									<!--p>Add some text</p-->
								</div>
							</div>
						</a>
					</div>
				</div>
				';
			}
		}
		echo '<!-- /productos.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay productos disponibles.</div>
		</div>
	';
 }
}

function flechas($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
 $sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID>'{$id}' AND visible=1 ORDER BY ID ASC;") or print mysqli_error($mysqli); 
 if($row=mysqli_fetch_array($sql)){
	$sig=$row['ID'];
	cadena_replace($replace1,$replace2);
	$nombre_sig=str_replace($replace1,$replace2,$row['nombre']);	
 }else{$sig=0;}

 $sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID<'{$id}' AND visible=1 ORDER BY ID DESC;") or print mysqli_error($mysqli); 
 if($row=mysqli_fetch_array($sql)){
	$ant=$row['ID'];
	cadena_replace($replace1,$replace2);
	$nombre_ant=str_replace($replace1,$replace2,$row['nombre']);	
 }else{$ant=0;}

 if($ant!=0){
 echo '<a href="'.$page_url.$mod.'/item/'.$ant.'-'.$nombre_ant.'/" rel="prev">
		<i class="fa fa-chevron-left" title="Anterior"></i>
	  </a>';
 }else{echo '<i class="fa fa-chevron-left" title="Anterior" style="line-height:40px"></i>';}
 echo '&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="'.$page_url.'productos/"><i class="fa fa-th" title="Productos"></i></a>
      &nbsp;&nbsp;&nbsp;&nbsp;';   
 if($sig!=0){
 echo '<a href="'.$page_url.$mod.'/item/'.$sig.'-'.$nombre_sig.'/" rel="next">
      	<i class="fa fa-chevron-right" title="Siguiente"></i>
      </a>';
 }else{echo '<i class="fa fa-chevron-right" title="Siguiente" style="line-height:40px"></i>';}
}

function flechas1($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
 $sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID>'{$id}' AND visible=1 ORDER BY ID ASC;") or print mysqli_error($mysqli); 
 if($row=mysqli_fetch_array($sql)){
	$sig=$row['ID'];
 }else{$sig=0;}

 $sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID<'{$id}' AND visible=1 ORDER BY ID DESC;") or print mysqli_error($mysqli); 
 if($row=mysqli_fetch_array($sql)){
	$ant=$row['ID'];
 }else{$ant=0;}

 if($ant!=0){
 echo '<a href="'.$page_url.'index.php?mod='.$mod.'&ext='.$ext.'&action=edit&id='.$ant.'" rel="prev">
		<i class="fa fa-chevron-left" title="Anterior"></i>
	  </a>';
 }else{echo '<i class="fa fa-chevron-left" title="Anterior" style="line-height:40px"></i>';}
 echo '&nbsp;&nbsp;&nbsp;&nbsp;
      <!--a href="'.$page_url.'productos/"><i class="fa fa-th" title="Productos"></i></a-->
      &nbsp;&nbsp;&nbsp;&nbsp;';   
 if($sig!=0){
 echo '<a href="'.$page_url.'index.php?mod='.$mod.'&ext='.$ext.'&action=edit&id='.$sig.'" rel="next">
      	<i class="fa fa-chevron-right" title="Siguiente"></i>
      </a>';
 }else{echo '<i class="fa fa-chevron-right" title="Siguiente" style="line-height:40px"></i>';}
}

function item_productos($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$fjson='productos';
$path_JSON='modulos/productos/'.$fjson.'.json';
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/'.$fjson.'/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";

 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- productos.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$cover=$reg['cover'];
			cadena_replace($replace1,$replace2);
			$producto=str_replace($replace1,$replace2,$reg['nombre']);
			$nom_producto=$reg['nombre'];
			$descripcion=$reg['descripcion'];
			$ID_cate=$reg['ID_cate'];
			$ID_sub_cate=$reg['ID_sub_cate'];
			$visible=$reg['visible'];
		
			if($visible==1 && $ID_cate==$id && $ID_sub_cate==0){
				$objData2=file_get_contents("modulos/productos/categorias.json");
				$Data2=json_decode($objData2,true);
				foreach ($Data2 as $row){
					$ID_cate2=$row['ID_cate'];
					if($ID_cate==$ID_cate2){
						$categoria=$row['categoria'];
					}
				}
			
				$tema_p=$_GET['tema_previo'];
				$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod='.$mod.'&ext=item&id='.$ID.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">';
				echo '<!--['.$i.'] -'.$ID.'-->
				<div class="col-md-4 proj_gallery_grid" data-aos="zoom-in"><!--div class="col-md-4 proj_gallery_grid act_popup" data-aos="zoom-in"-->
					<div class="section_1_gallery_grid"><!--div class="section_1_gallery_grid"-->
						<a title="'.$nom_producto.'" href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">
							<div class="section_1_gallery_grid1">
								<img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt=" " class="img-responsive" />
								<div class="proj_gallery_grid1_pos">
									<h3>'.$nom_producto.'</h3>
									<!--p>Add some text</p-->
								</div>
							</div>
						</a>
					</div>
				</div>
				';
			}
		}
		echo '<!-- /productos.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay productos disponibles.</div>
		</div>
	';
 }
}

function item_subproductos($id,$idp){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$fjson='productos';
$path_JSON='modulos/productos/'.$fjson.'.json';
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/'.$fjson.'/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- productos.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$cover=$reg['cover'];
			cadena_replace($replace1,$replace2);
			$producto=str_replace($replace1,$replace2,$reg['nombre']);
			$nom_producto=$reg['nombre'];
			$descripcion=$reg['descripcion'];
			$ID_cate=$reg['ID_cate'];
			$ID_sub_cate=$reg['ID_sub_cate'];
			$visible=$reg['visible'];

			if($visible==1 && $ID_sub_cate==$id){
				$objData2=file_get_contents("modulos/productos/categorias.json");
				$Data2=json_decode($objData2,true);
				foreach ($Data2 as $row){
					$ID_cate2=$row['ID_cate'];
					if($ID_cate==$ID_cate2){
						$categoria=$row['categoria'];
					}
				}

				$tema_p=$_GET['tema_previo'];
				$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod='.$mod.'&ext=item&id='.$ID.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">';
				echo '<!--['.$i.'] -'.$ID.'-->
				<div class="col-md-4 proj_gallery_grid" data-aos="zoom-in"><!--div class="col-md-4 proj_gallery_grid act_popup" data-aos="zoom-in"-->
					<div class="section_1_gallery_grid"><!--div class="section_1_gallery_grid"-->
						'.$link_zp.'
							<div class="section_1_gallery_grid1">
								<img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt=" " class="img-responsive" />
								<div class="proj_gallery_grid1_pos">
									<h3>'.$nom_producto.'</h3>
									<p>Add some text</p>
								</div>
							</div>
						</a>
					</div>
				</div>
				';
			}
			
		}
		echo '<!-- /productos.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay productos disponibles.</div>
		</div>
	';
 }
}


function item_cate(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;

$menu_json='categorias.json';
$path_JSON='modulos/'.$mod.'/'.$menu_json;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/productos_cate/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- categorias.json -->';
		foreach ($Data as $reg){$i++;
			$ID_cate=$reg['ID_cate'];
			cadena_replace($replace1,$replace2);
			$categoria=str_replace($replace1,$replace2,$reg['categoria']);
			$nom_categoria=$reg['categoria'];
			$des=$reg['des'];
			$cover=$reg['cover'];
			$visible=$reg['visible'];

$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod='.$mod.'&ext=categoria&id='.$ID_cate.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/categoria/'.$ID_cate.'-'.$categoria.'">';
			if($visible==1){
				echo '<!--['.$i.'] -'.$ID.'-->
				<div class="col-md-4 proj_gallery_grid" data-aos="zoom-in"><!--div class="col-md-4 proj_gallery_grid act_popup" data-aos="zoom-in"-->
					<div class="section_1_gallery_grid"><!--div class="section_1_gallery_grid"-->
						'.$link_zp.'
							<div class="section_1_gallery_grid1">
								<img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt=" " class="img-responsive" />
								<div class="proj_gallery_grid1_pos">
									<h3>'.$nom_categoria.'</h3>
									<p>'.$des.'</p>
								</div>
							</div>
						</a>
					</div>
				</div>
				';
			}
		}
		echo '<!-- /cetegorias.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay categorias disponibles.</div>
		</div>
	';
 }
}


function item_sub_cate($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;

$menu_json='subcategorias.json';
$path_JSON='modulos/'.$mod.'/'.$menu_json;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/productos_sub_cate/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- subcategorias.json -->';
		foreach ($Data as $reg){$i++;
			$ID_sub_cate=$reg['ID_sub_cate'];
			$ID_cate=$reg['ID_cate'];
			cadena_replace($replace1,$replace2);		
			$subcategoria=str_replace($replace1,$replace2,$reg['subcategoria']);
			$nom_subcategoria=$reg['subcategoria'];
			$cover=$reg['cover'];
			$visible=$reg['visible'];		

$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod='.$mod.'&ext=subcategoria&id='.$ID_sub_cate.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcategoria.'">';
			if($visible==1 && $ID_cate==$id){
				echo '<!--['.$i.'] -'.$ID.'-->
				<div class="col-md-4 proj_gallery_grid" data-aos="zoom-in"><!--div class="col-md-4 proj_gallery_grid act_popup" data-aos="zoom-in"-->
					<div class="section_1_gallery_grid"><!--div class="section_1_gallery_grid"-->
						'.$link_zp.'
							<div class="section_1_gallery_grid1">
								<img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt=" " class="img-responsive" />
								<div class="proj_gallery_grid1_pos">
									<h3>'.$nom_subcategoria.'</h3>
									<p>Add some text</p>
								</div>
							</div>
						</a>
					</div>
				</div>
				';
			}
		}
		echo '<!-- /subcategorias.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay subcategorias disponibles.</div>
		</div>
	';
 }
}

function one_producto($id){
global $mysqli,$DBprefix,$page_url,$path_tema,$mod,$ext,$opc;
$menu_json='productos.json';
$path_JSON='modulos/productos/'.$menu_json;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/productos/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
 $objData=file_get_contents($path_JSON);
 $Data=json_decode($objData,true);
	if($Data!='' && $Data!=NULL){$i=0;
		echo '<!-- productos.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$cover=$reg['cover'];
			cadena_replace($replace1,$replace2);
			$producto=str_replace($replace1,$replace2,$reg['nombre']);
			$nom_producto=$reg['nombre'];
			$descripcion=$reg['descripcion'];
			$resena=$reg['resena'];
			$precio=$reg['precio'];
			$moneda=$reg['moneda'];
			$ID_cate=$reg['ID_cate'];
			$ID_sub_cate=$reg['ID_sub_cate'];
			$imagen1=$reg['imagen1'];
			$imagen2=$reg['imagen2'];
			$imagen3=$reg['imagen3'];
			$imagen4=$reg['imagen4'];
			$imagen5=$reg['imagen5'];
			$pdf=$reg['file'];
			$tipo=$reg['tipo'];
			$visible=$reg['visible'];

			$cov=($cover!='')?'<div id="c1" class="gal" onMouseOver="gal(0)"><img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="images"></div>':'';
			$ima1=($imagen1!='')?'<div id="g1" class="gal" onMouseOver="gal(1)"><img src="'.$page_url.'modulos/productos/fotos/'.$imagen1.'" class="images"></div>':'';
			$ima2=($imagen2!='')?'<div id="g2" class="gal" onMouseOver="gal(2)"><img src="'.$page_url.'modulos/productos/fotos/'.$imagen2.'" class="images"></div>':'';
			$ima3=($imagen3!='')?'<div id="g3" class="gal" onMouseOver="gal(3)"><img src="'.$page_url.'modulos/productos/fotos/'.$imagen3.'" class="images"></div>':'';
			$ima4=($imagen4!='')?'<div id="g4" class="gal" onMouseOver="gal(4)"><img src="'.$page_url.'modulos/productos/fotos/'.$imagen4.'" class="images"></div>':'';
			$ima5=($imagen5!='')?'<div id="g5" class="gal" onMouseOver="gal(5)"><img src="'.$page_url.'modulos/productos/fotos/'.$imagen5.'" class="images"></div>':'';

			sql_opciones('mostrar_nombre',$valor);
			$valor_nom_producto=($valor==1)?'<h5>'.$nom_producto.'</h5>':'';

			sql_opciones('mostrar_precio',$valor);
			$valor_precio=($valor==1)?'<p>PRECIO: $'.$precio.' '.$moneda.'</p>':'';

			sql_opciones('mostrar_des_corta',$valor);
			$valor_des_corta=($valor==1)?'<p>DESCRIPCI&Oacute;N:</p><p>'.$descripcion.'</p>':'';

			sql_opciones('mostrar_des',$valor);
			$valor_des=($valor==1)?'<h4>Descripci&oacute;n</h4>'.$resena:'';

			sql_opciones('mostrar_galeria',$valor);
			$valor_galeria=($valor==1)?'<p>GALERIA</p>'.$cov.$ima1.$ima2.$ima3.$ima4.$ima5:'';

			if($visible==1 && $ID==$id){
				$pdf_img=($pdf!='')?'<img src="'.$page_url.'modulos/productos/fotos/pdf_icon.png" width="25"> <a target="_blank" href="'.$page_url.'modulos/productos/pdf/'.$pdf.'" class="al">DESCARGA FICHAS TECNICAS</a>':'&nbsp;';
				echo '<!--['.$i.'] -'.$ID.'-->
					<!--div class="row"-->
						<!--div class="col-md-12"><h3>'.$nom_producto.'</h3><hr></div-->
						<div class="col-md-8">
							<div id="imagenes" style="background:#ccc; padding:10px; margin:0 30px;">
								<img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" width="100%"/>
							</div>
						</div>
						<div class="col-md-4">
							<h5>'.$valor_nom_producto.'</h5>
							<hr>
							'.$valor_precio.'
							'.$valor_des_corta.'
							'.$valor_galeria.'
						</div>
					<!--/div-->
					<!--div class="row"-->
						<div class="col-md-12" style="padding-top:50px;">
						'.$valor_des.'
						</div>
					<!--/div-->
				';
			}
		}
		echo '<!--/ productos.json -->';
	}
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay Producto disponible.</div>
		</div>
	';
 }
}

function productos_destacados(){
global $mysqli,$DBprefix,$page_url,$path_tema,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$menu_json='productos.json';
$path_JSON='modulos/productos/'.$menu_json;

if(file_exists($path_JSON) && filesize($path_JSON)!=0){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);

if($Data!='' && $Data!=NULL){
$i=0;
echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		cadena_replace($replace1,$replace2);
		$producto=str_replace($replace1,$replace2,$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$destacado=$reg['land'];
		$visible=$reg['visible'];
		
		if($visible==1 && $destacado==1){
		$objData2=file_get_contents("modulos/productos/categorias.json");
		$Data2=json_decode($objData2,true);
			foreach ($Data2 as $row){
				$ID_cate2=$row['ID_cate'];
				if($ID_cate==$ID_cate2){
					$categoria=$row['categoria'];
				}
			}
			
		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod=productos&ext=item&id='.$ID.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">';
		echo '<!--['.$i.'] -'.$reg['ID'].'-->
<!--
         <div class="column mcb-column one-third column_column ">
		 	'.$link_zp.'
            <div class="column_attr clearfix align_center" style="">
               <div class="cover-img"><img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" width="270" height="270"></div>
               <div class="info-content">
               		<p class="themecolor">'.$nom_producto.'</p>
					<!--('.$ID_cate.')->
               		<h4 style="">'.$categoria.'</h4>
			   </div>
               <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                  <!--div class="image_wrapper"><img class="scale-with-grid" src="https://themes.muffingroup.com/be/agro/wp-content/uploads/2015/06/home_agro_heading_icon2.png" alt="home_agro_heading_icon2" title="home_agro_heading_icon2" width="27" height="24"></div->
               </div>
               <!--hr class="no_line" style="margin: 0 auto 20px;"->
               <!--a class="button  button_size_2 button_theme button_js" href="#"><span class="button_label">Read more</span></a->
            </div>
			</a>
         </div>
-->
                                        <div class="column mcb-column one-fourth column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style=" padding:0 3% 0 0;">
                                                <hr class="no_line" style="margin: 0 auto 20px;" />
                                                <div style="padding: 20px 6%; background: url('.$page_url.$path_tema.'img/home_agro_sep2.png) repeat-y right top">
												'.$link_zp.'
                                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt="home_agro_product1s" width="380" height="282" /></div>
                                                </div>
                                                    <p style="font-weight:700; text-transform:uppercase;" class="themecolor">'.$nom_producto.'</p>
                                                    <p style="color:#444; font-weight:700;">'.$categoria.'</p>
                                                    <!--p style="margin: 0px;"><i class="icon-doc-text themecolor"></i> <a href="#">Phasellus fermen</a></p-->
                                                </div>
												</a>
                                            </div>
                                        </div>
			';
		}
	}
	if($i==0){
				echo '<div class="col-lg-12 col-xs-12">
					<div>Por el momento no hay productos disponibles.</div>
				</div>
  				';
	}

 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
 }
 echo '<!-- /productos.json -->';
}else{
echo '<!-- mysql -->';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE visible=1 AND land=1 ORDER BY ID ASC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	while($reg=mysqli_fetch_array($sql)){
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		cadena_replace($replace1,$replace2);
		$producto=str_replace($replace1,$replace2,$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		//$visible=$reg['visible'];

		$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate=".$ID_cate.";") or print mysqli_error($mysqli); 
		while($row=mysqli_fetch_array($sql2)){$categoria=$row['categoria'];}

		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod=productos&ext=item&id='.$ID.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">';
		echo '<!--'.$reg['ID'].'-->
                                        <div class="column mcb-column one-fourth column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style=" padding:0 3% 0 0;">
                                                <hr class="no_line" style="margin: 0 auto 20px;" />
                                                <div style="padding: 20px 6%; background: url('.$page_url.$path_tema.'img/home_agro_sep2.png) repeat-y right top">
												'.$link_zp.'
                                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt="home_agro_product1s" width="380" height="282" /></div>
                                                </div>
                                                    <p style="font-weight:700; text-transform:uppercase;" class="themecolor">'.$nom_producto.'</p>
                                                    <p style="color:#444; font-weight:700;">'.$categoria.'</p>
                                                    <!--p style="margin: 0px;"><i class="icon-doc-text themecolor"></i> <a href="#">Phasellus fermen</a></p-->
                                                </div>
												</a>
                                            </div>
                                        </div>
			';
	}
  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
 echo '<!--/ mysql -->';
 }
}
?>