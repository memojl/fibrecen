<?php 
include 'admin/functions.php';
$id=$_GET['id'];
menu_cate1(); 
sql_opciones('bar_productos',$valor);
$sidebar_productos=$valor;
if($sidebar_productos==1){
	$display='block';
	$col_md='col-md-9';
}else{
	$display='none';
	$col_md='col-md-12';
}
?>
<style>
.sc_team .sc_team_item .sc_team_item_info{ padding:1px 20px !important;}
.side{display:<?php echo $display;?>;}
</style>
<!--Productos-->
	<section class="services">
		<div class="container">
			<h3 class="tittle">Productos</h3>
			<div class="row inner-sec-wthree">
                <div class="col-md-3 side">
                	<div class="bar-lg">
                		<h3>Categorias</h3>
						<?php menu_cate2();?>
                    </div>
                	<div class="bar-resp">
                    	<a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list"></i> Categorias</a>
                    </div>
                </div>
				<div class="<?php echo $col_md;?>">
                	<div class="row">
						<?php item_subproductos($id,$idp);?>
                    </div>
                </div>
			</div>
		</div>
	</section>
<!--/Productos-->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Categorias</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">X</span>
					</button>
                </div>
                <div class="modal-body">
                	<?php menu_cate2();?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
        </div>
    </div>
    <!--//model-->                        

