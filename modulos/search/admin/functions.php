<?php
function cadena_replace(&$replace1,&$replace2){
	$replace1=array(' ','.',',','(',')','/','"','á','é','í','ó','ú','&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','Á','É','Í','Ó','Ú','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','ñ','Ñ','&ntilde;','&Ntilde;');
	$replace2=array('-','-','-','-','-','-','-','a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','n','N','n','N');
}

function buscar(&$q,&$num_rows,&$busqueda){
global $mysqli,$DBprefix,$page_url;
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE nombre LIKE '%{$q}%' OR tag LIKE '%{$q}%' AND visible=1 ORDER BY ID ASC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
if($num_rows!=0 && $q!=''){
	while($row=mysqli_fetch_array($sql)){
		$ID=$row['ID'];
		$nombre=$row['nombre'];
		cadena_replace($replace1,$replace2);
		$nom_producto=str_replace($replace1,$replace2,$nombre);
		$descripcion=$row['descripcion'];
		$resena=$row['resena'];
		$cover=$row['cover'];
		$ID_cate=$row['ID_cate'];
		$ID_sub_cate=$row['ID_sub_cate'];
		$resena1=substr($resena, 0, 180);
/*
$sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$ID_cate}';") or print mysqli_error($mysqli);
while($row1=mysqli_fetch_array($sql1)){$replace=array(' ','.',',','(',')');$cate=str_replace($replace,"-",$row1['categoria']);}

$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate='{$ID_sub_cate}';") or print mysqli_error($mysqli);
while($row2=mysqli_fetch_array($sql)){$subcate=$row2['subcategoria'];}
*/
		$busqueda.='
				<div class="col-md-4 proj_gallery_grid aos-init aos-animate" data-aos="zoom-in">
                    <div class="section_1_gallery_grid">
                        <a title="" href="'.$page_url.'productos/item/'.$ID.'-'.$nom_producto.'">
                            <div class="section_1_gallery_grid1">
                                <img src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt=" " class="img-fluid">
                                <div class="proj_gallery_grid1_pos">
                                    <h3>'.$nombre.'</h3>
                                    <p></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-md-8 proj_gallery_grid aos-init aos-animate" data-aos="zoom-in">
					<H3>'.$nombre.'</H3>
                	<p>Descripci&oacute;n:<br>
						'.$resena1.'...
					</p>
					<p><a href="'.$page_url.'productos/item/'.$ID.'-'.$nom_producto.'">Ver M&aacute;s</a></p>
				</div>


	';
	}
}else{
	$busqueda='No hay resultados.';
}

}
?>