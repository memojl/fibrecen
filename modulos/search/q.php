<?php

?>

<div class="page_top_wrap page_top_title page_top_breadcrumbs">
   <div class="content_wrap">
      <div class="breadcrumbs">
         <a class="breadcrumbs_item home" href="http://construction.themerex.net">Home</a><span class="breadcrumbs_delimiter"></span><a class="breadcrumbs_item all" href="http://construction.themerex.net/blog-streampage/">All posts</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Aside Title</span>							
      </div>
      <h1 class="page_title">Aside Title</h1>
   </div>
</div>

<div class="page_content_wrap">
	<div class="content_wrap">
		<div class="content">


<article class="post_item post_item_excerpt post_featured_right post_format_standard odd post-1887 post type-post status-publish format-standard has-post-thumbnail hentry category-aside-title category-blog-masonry-2-columns tag-construction tag-supply-management">
   <div class="post_featured">
      <div class="post_thumb">
         <img class="wp-post-image" width="750" height="422" alt="Construction &amp; Building" src="http://construction.themerex.net/wp-content/uploads/2015/05/construction_9-750x422.jpg">
         <div class="hover_wrap">
            <div class="link_wrap">
               <a class="hover_link icon-link" href="http://construction.themerex.net/construction-building/"></a>
               <a class="hover_view icon-resize-full inited" href="http://construction.themerex.net/wp-content/uploads/2015/05/construction_9.jpg" title="Construction &amp; Building" rel="magnific"></a>
            </div>
         </div>
      </div>
   </div>
   <div class="post_content clearfix">
      <h3 class="post_title"><a href="http://construction.themerex.net/construction-building/">Construction &amp; Building</a></h3>
      <div class="post_info">
         <span class="post_info_item post_info_posted"><a href="http://construction.themerex.net/construction-building/" class="post_info_date">May 11, 2015</a></span>
         <span class="post_info_item post_info_posted_by"><a href="http://construction.themerex.net/author/admin/" class="post_info_author">Admin</a></span>
         <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_views icon-eye-3" title="Views - 31" href="http://construction.themerex.net/construction-building/">31</a>
         </span>
      </div>
      <div class="post_descr">
         <p>Phasellus sit amet elit sem. Maecenas eleifend ex id magna pretium tincidunt. Nam vel venenatis...</p>
         <a href="http://construction.themerex.net/construction-building/" class="sc_button sc_button_square sc_button_bg_custom"><span>READ MORE</span><span>READ MORE</span></a>
      </div>
   </div>
   <!-- /.post_content -->
</article>

		</div>
	</div>
</div>
