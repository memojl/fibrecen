<?php 
function html_iso_curso(&$nombre,&$email,&$subject,&$msj){
global $chartset;
 if($chartset=='iso-8859-1'){
 	$nombre = htmlentities($nombre, ENT_COMPAT,'ISO-8859-1', true);
	$email=htmlentities($email, ENT_COMPAT,'ISO-8859-1', true);
	$subject = htmlentities($subject, ENT_COMPAT,'ISO-8859-1', true);
	$msj = htmlentities($msj, ENT_COMPAT,'ISO-8859-1', true);	
 }
}

function html_iso_cursos(&$titulo,&$des,&$tag,&$autor,&$lugar,&$fechas){
global $chartset;
 if($chartset=='iso-8859-1'){
	$titulo=htmlentities($titulo, ENT_COMPAT,'ISO-8859-1', true);
	$des = htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
	$tag = htmlentities($tag, ENT_COMPAT,'ISO-8859-1', true);	
	$autor=htmlentities($autor, ENT_COMPAT,'ISO-8859-1', true);
	$lugar=htmlentities($lugar, ENT_COMPAT,'ISO-8859-1', true);
	$fechas=htmlentities($fechas, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function item_cursos(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
$topic='item';

$fjson='cursos.json';
$path_JSON='modulos/'.$mod.'/'.$fjson;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/'.$fjson.'/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- '.$fjson.' -->';
		foreach ($Data as $reg){$i++;
			cadena_replace_m($replace1,$replace2);
		$ID=$reg['ID'];
		$titulo=$reg['titulo'];
		$desc=$reg['descripcion'];
		$contenido=$reg['contenido'];
		$cover=$reg['cover'];
		$autor=$reg['autor'];
		$tag=$reg['tag'];
		$fechas=$reg['fechas'];
		$horario=$reg['horario'];
		$lugar=$reg['lugar'];
		$video=$reg['video'];
		$fecha=$reg['fecha'];
		$visible=$reg['visible'];
			if($visible==1){
				if($video!=''){
					$modal='
    <!-- Modal -->
    <div class="modal fade" id="exampleModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
                </div>
                <div class="modal-body video">
                    <iframe src="'.$video.'" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
';
					$btn_video='
                    <div class="bnr-button">
                        <button type="button" class="btn btn-primary play" data-toggle="modal" data-target="#exampleModal'.$i.'">
							<i class="fas fa-play"></i>
						</button>
                    </div>
';
				}else{$modal='';$btn_video='';}
				if($i % 2 == 0){			
					echo '
		<!--['.$i.'] -'.$ID.'-->
            <div class="col-md-6 style-image-2 second" style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url('.$page_url.'modulos/'.$mod.'/fotos/'.$cover.') no-repeat; background-size: cover;"></div>
            <div class="col-md-6 grid_sec_info">
                <div class="style-grid-2-text_info" data-aos="fade-right">
                    <h3 style="border-bottom:2px solid #ed1c24;color:#1f205a;">'.$titulo.'</h3>
                    <p>'.$desc.'</p>
					<div class="color-curso">| <i class="fa fa-calendar"></i> Fecha: '.$fechas.' | <i class="fa fa-clock-o"></i> Horario: '.$horario.' | </div>
					<div class="color-curso">| <i class="fa fa-map-marker"></i> Lugar: '.$lugar.' | <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item" class="btn_azul"><i class="fa fa-external-link"></i> Ver M&aacute;s</a> |</div>
					'.$btn_video.'
                </div>
            </div>
			'.$modal;
				}else{
		echo '
		<!--['.$i.'] -'.$ID.'-->
            <div class="col-md-6 grid_sec_info">
                <div class="style-grid-2-text_info" data-aos="fade-right">
                    <h3 style="border-bottom:2px solid #ed1c24;color:#1f205a;">'.$titulo.'</h3>
                    <p>'.$desc.'</p>
					<div class="color-curso">| <i class="fa fa-calendar"></i> Fecha: '.$fechas.' | <i class="fa fa-clock-o"></i> Horario: '.$horario.' | </div>
					<div class="color-curso">| <i class="fa fa-map-marker"></i> Lugar: '.$lugar.' | <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item" class="btn_azul"><i class="fa fa-external-link"></i> Ver M&aacute;s</a> |</div>
					'.$btn_video.'
                </div>
            </div>
            <div class="col-md-6 style-image-2" style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url('.$page_url.'modulos/'.$mod.'/fotos/'.$cover.') no-repeat; background-size: cover;"></div>
			'.$modal;			
				}
			}
		}
		echo '<!-- /'.$fjson.' -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay productos disponibles.</div>
		</div>
	';
 }
}

function item_cursos1($cur){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
$topic='item';

$fjson='cursos.json';
$path_JSON='modulos/'.$mod.'/'.$fjson;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/'.$fjson.'/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- '.$fjson.' -->';
		foreach ($Data as $reg){$i++;
		cadena_replace_m($replace1,$replace2);
		$ID=$reg['ID'];
		$titulo=$reg['titulo'];
		$desc=$reg['descripcion'];
		$contenido=$reg['contenido'];
		$cover=$reg['cover'];
		$autor=$reg['autor'];
		$tag=$reg['tag'];
		$fechas=$reg['fechas'];
		$horario=$reg['horario'];
		$lugar=$reg['lugar'];
		$video=$reg['video'];
		$fecha=$reg['fecha'];
		$visible=$reg['visible'];
		$curso=strpos($tag, $cur);
		if($curso==false){$cu=0;}else{$cu=1;}
			if($visible==1 && $cu==1){
				if($video!=''){
					$modal='
    <!-- Modal -->
    <div class="modal fade" id="exampleModal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
                </div>
                <div class="modal-body video">
                    <iframe src="'.$video.'" width="640" height="360" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
';
					$btn_video='
                    <div class="bnr-button">
                        <button type="button" class="btn btn-primary play" data-toggle="modal" data-target="#exampleModal'.$i.'">
							<i class="fas fa-play"></i>
						</button>
                    </div>
';
				}else{$modal='';$btn_video='';}
				if($i % 2 == 0){			
					echo '
		<!--['.$i.'] -'.$ID.'-->
            <div class="col-md-6 style-image-2 second" style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url('.$page_url.'modulos/'.$mod.'/fotos/'.$cover.') no-repeat; background-size: cover;"></div>
            <div class="col-md-6 grid_sec_info">
                <div class="style-grid-2-text_info" data-aos="fade-right">
                    <h3 style="border-bottom:2px solid #ed1c24;color:#1f205a;">'.$titulo.'</h3>
                    <p>'.$desc.'</p>
					<div class="color-curso">| <i class="fa fa-calendar"></i> Fecha: '.$fechas.' | <i class="fa fa-clock-o"></i> Horario: '.$horario.' | </div>
					<div class="color-curso">| <i class="fa fa-map-marker"></i> Lugar: '.$lugar.' | <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item" class="btn_azul"><i class="fa fa-external-link"></i> Ver M&aacute;s</a> |</div>
					'.$btn_video.'
                </div>
            </div>
			'.$modal;
				}else{
		echo '
		<!--['.$i.'] -'.$ID.'-->
            <div class="col-md-6 grid_sec_info">
                <div class="style-grid-2-text_info" data-aos="fade-right">
                    <h3 style="border-bottom:2px solid #ed1c24;color:#1f205a;">'.$titulo.'</h3>
                    <p>'.$desc.'</p>
					<div class="color-curso">| <i class="fa fa-calendar"></i> Fecha: '.$fechas.' | <i class="fa fa-clock-o"></i> Horario: '.$horario.' | </div>
					<div class="color-curso">| <i class="fa fa-map-marker"></i> Lugar: '.$lugar.' | <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item" class="btn_azul"><i class="fa fa-external-link"></i> Ver M&aacute;s</a> |</div>
					'.$btn_video.'
                </div>
            </div>
            <div class="col-md-6 style-image-2" style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url('.$page_url.'modulos/'.$mod.'/fotos/'.$cover.') no-repeat; background-size: cover;"></div>
			'.$modal;			
				}
			}
		}
		echo '<!-- /'.$fjson.' -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento No hay productos disponibles.</div>
		</div>
	';
 }
}


function one_item_cursos($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
$topic='item';
$menu_json='cursos.json';
$path_JSON='modulos/'.$mod.'/'.$menu_json;
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/menu_web/';}
 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- cursos.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$titulo=$reg['titulo'];
			$desc=$reg['descripcion'];
			$contenido=$reg['contenido'];
			$cover=$reg['cover'];
			$autor=$reg['autor'];
			$tag=$reg['tag'];
			$fechas=$reg['fechas'];
			$horario=$reg['horario'];
			$lugar=$reg['lugar'];
			$video=$reg['video'];
			$fecha=$reg['fecha'];
			$visible=$reg['visible'];
			if($visible==1 && $ID==$id){
				$lug=($lugar!='')?$lugar:'';
				$hr=($horario!='')?$horario:'';
				$fec=($fechas!='')?$fechas:'';
				echo '<!--['.$i.'] -'.$ID.'-->
                        <div class="blog-img">
								<img src="'.$page_url.'modulos/'.$mod.'/fotos/'.$cover.'" alt="" class="img-fluid" />
                        </div>
                        <div class="blog_info">
                            <h5 style="border-bottom:2px solid #ed1c24;color:#1f205a;">'.$titulo.'</h5>
                            <!--p>Por Admin</p-->
                            <p>'.$contenido.'</p>
                            <ul class="blog_list_info">
                                <li><i class="fa fa-map-marker"></i> '.$lug.' <i>|</i></li>
                                <li><i class="fa fa-clock-o"></i> '.$hr.' <i>|</i></li>
								<li><i class="fa fa-calendar"></i> '.$fec.' <i>|</i></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
				';
			}
		}
		echo '<!-- /cursos.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento el curso no esta disponible.</div>
		</div>
	';
 }
}

function sidebar_cursos(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
$topic='item';
$menu_json='cursos.json';
$path_JSON='modulos/'.$mod.'/'.$menu_json;

if(file_exists($path_JSON)){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
if($Data!=''){
$i=0;
echo '<!-- cursos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$titulo=$reg['titulo'];
		$desc=$reg['descripcion'];
		$contenido=$reg['contenido'];
		$cover=$reg['cover'];
		$autor=$reg['autor'];
		$tag=$reg['tag'];
		$fechas=$reg['fechas'];
		$horario=$reg['horario'];
		$lugar=$reg['lugar'];
		$video=$reg['video'];
		$fecha=$reg['fecha'];
		$visible=$reg['visible'];
		if($visible==1){
			echo '
                        <div class="blog-grids">
                            <div class="blog-grid-left">
                                <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item">
									<img src="'.$page_url.'modulos/'.$mod.'/fotos/'.$cover.'" class="img-fluid" alt="">
								</a>
                            </div>
                            <div class="blog-grid-right">

                                <h5>
                                    <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item">'.$titulo.'</a>
                                </h5>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
			';
		}
	}
	echo '<!-- /cursos.json -->';
 }
}else{
echo '<!-- mysql -->';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."cursos WHERE visible=1 ORDER BY ID DESC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	while($reg=mysqli_fetch_array($sql)){
		$ID=$reg['ID'];
		$titulo=$reg['titulo'];
		$desc=$reg['descripcion'];
		$contenido=$reg['contenido'];
		$cover=$reg['cover'];
		$autor=$reg['autor'];
		$tag=$reg['tag'];
		$fechas=$reg['fechas'];
		$horario=$reg['horario'];
		$lugar=$reg['lugar'];
		$video=$reg['video'];
		$fecha=$reg['fecha'];
		$visible=$reg['visible'];

			echo '
                        <div class="blog-grids">
                            <div class="blog-grid-left">
                                <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item">
									<img src="'.$page_url.'modulos/'.$mod.'/fotos/'.$cover.'" class="img-fluid" alt="">
								</a>
                            </div>
                            <div class="blog-grid-right">

                                <h5>
                                    <a href="'.$page_url.$mod.'/'.$topic.'/'.$ID.'-item">'.$titulo.'</a>
                                </h5>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
			';

	}
	echo '<!--/ mysql -->';
  }
 }
}
?>