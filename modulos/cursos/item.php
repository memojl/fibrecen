<?php include 'admin/functions.php';?>
<?php
$id=$_GET['id'];
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."cursos WHERE ID='{$id}' AND visible=1;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	if($row=mysqli_fetch_array($sql)){
		//$ID=$row['ID'];
		$titulo=$row['titulo'];
		$desc=$row['descripcion'];
		$contenido=$row['contenido'];
		$cover=$row['cover'];
		$autor=$row['autor'];
		$tag=$row['tag'];
		$fechas=$row['fechas'];
		$horario=$row['horario'];
		$lugar=$row['lugar'];
		$video=$row['video'];
		$fecha=$row['fecha'];
		$visible=$row['visible'];		
	}
  }//else{}

?>
    <!--/Blog-->
    <section class="services">
        <div class="container">
            <h3 class="tittle">Curso</h3>

            <div class="row inner-sec-wthree">
                <div class="col-lg-8 blog-sp" data-aos="fade-right">
                    <article class="blog-x row">
                    	<?php one_item_cursos($id);?>
                    </article>
					<div class="comment-top">
                    	<?php include 'form_curso.php';?>
                    </div>
                    <hr>
<?php
sql_opciones('cursos_coment',$valor);
$coment=$valor;
if($coment!=0){
?>
                    <div class="comment-top">
                        <h4>Comentarios</h4>

					<div id="form_coment_div"></div>

<?php
echo '<script>
/*
* Parametros mandatorios
*/
    var seconds = 1; // el tiempo en que se refresca
	var divid = "contenido"; // el div que quieres actualizar!
	var url = "'.$page_url.'modulos/'.$mod.'/comentarios.php?id='.$id.'"; // el archivo que ira en el div

	function refreshdiv(){
		// The XMLHttpRequest object
		var xmlHttp;
		try{
			xmlHttp=new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
		}
		catch (e){
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
			}
			catch (e){
				try{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e){
					alert("Tu explorador no soporta AJAX.");
					return false;
				}
			}
		}
		// Timestamp for preventing IE caching the GET request
		var timestamp = parseInt(new Date().getTime().toString().substring(0, 10));
		var nocacheurl = url+"?t="+timestamp;
		// The code...
		xmlHttp.onreadystatechange=function(){
			if(xmlHttp.readyState== 4 && xmlHttp.readyState != null){
				document.getElementById(divid).innerHTML=xmlHttp.responseText;
				setTimeout(\'refreshdiv()\',seconds*1000);
			}
		}
		xmlHttp.open("GET",nocacheurl,true);
		xmlHttp.send(null);
	}
	// Empieza la función de refrescar
	window.onload = function(){
		refreshdiv(); // corremos inmediatamente la funcion
	}
</script>';
?>
<div class="lm-col-12">
<div id="contenido">
<h3>Refrescar una div tag con Ajax</h3>
// Aqui el Div en el que se coloca el contenido de comentarios.php
<div name="timediv" id="timediv"></div>
</div>
</div>                                               
                    </div>
                    <div class="comment-top">
                        <h4>Deja un comentario</h4>
                        <div class="comment-bottom">
<!--?php include 'form_coment.php';?-->
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script>
	var $ = jQuery.noConflict();
    $(document).on('ready',function(){
      $('#btn-ingresar').click(function(){
        var url = "<?php echo $page_url.'modulos/'.$mod.'/';?>form_coment.php";                                      
        $.ajax({                        
           type: "POST",                 
           url: url,                    
           data: $("#formcoment").serialize(),
           success: function(data)            
           {
             $('#form_coment_div').html(data);
			 $('#form_coment').trigger("reset");           
           }
         });
      });
    });
</script>
                            <form id="formcoment" method="post">
                                <input type="text" name="nom" id="nom" placeholder="Nombre (*)" required>
                                <input type="email" name="email" id="email" placeholder="Email (*)" required>
                                <textarea name="comment" id="comment" placeholder="Escribe un comentario..." required></textarea>
                                <input type="submit" id="btn-ingresar" value="Enviar">
                                
                           		<!--input type="button" id="btn-ingresar" value="Enviar" /-->
                           		<input type="hidden" name="fecha" id="fecha" value="<?php echo $date;?>">
                           		<input type="hidden" name="id_b" id="id_b" value="<?php echo $id;?>">
                           		<input type="hidden" name="ip" id="ip" value="<?php echo $ip;?>">
                            </form>
                        </div>
                    </div>
<?php }else{echo '<div class="col-lg-12">Se han desactivado los comentarios para esta secci&oacute;n</div>';}?>                    
                </div>
                <aside class="col-lg-4 single-left" data-aos="fade-left">
                    <div class="single-gd-wthree-agileits">
                        <!--img src="images/banner1.jpg" class="img-fluid" alt=""-->
                        <h4>Noticias</h4>
                        <form action="#" method="post">
                            <input type="email" name="Email" placeholder="Email" required>
                            <div class="button">
                                <input type="submit" value="Subscribete">
                            </div>
                        </form>
                    </div>
                    <div class="single-gd-wthree-agileits tech-btm" data-aos="fade-down">
                        <h4>Otros Cursos </h4>
						<?php sidebar_cursos();?>
                    </div>

                </aside>
            </div>
        </div>
    </section>
    <!--//Blog-->
