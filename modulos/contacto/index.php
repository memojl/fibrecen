<?php
include 'admin/functions.php';
?>
    <!--/Contact-->
    <section class="services">
        <div class="container">
            <h3 class="tittle"><?php tit_seccion();?></h3>

            <div class="row inner-sec-wthree">
<!--Querétaro-->
            	<h4 style="border-bottom:2px solid #ed1c24; color:#1f205a; margin-bottom:30px">Sucursal Quer&eacute;taro</h4>
                <div class="contact-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.003867457709!2d-100.38434997843738!3d20.58789966650197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d2955c9989%3A0x68bcbcd16881326b!2sFibrecen!5e0!3m2!1ses-419!2smx!4v1547177963584" class="map" style="border:0" allowfullscreen=""></iframe>
                </div>
                <div class="address row">
                    <div class="col-md-4 address-grid-inf-w3layouts" data-aos="zoom-out">
                        <div class="address-info">
                            <div class="address-left">
                                <span class="fas fa-phone-volume" aria-hidden="true"></span>
                            </div>
                            <div class="address-right">
                                <h6>Tel<?php if($phone!=''){echo ' / Cel';}?><?php if($wapp!=''){echo ' / Whatsapp';}?></h6>
                                <p style="color:#6b6e80;">
                                <?php if($tel1!=''){echo '<a href="tel:'.$tel1.'">'.$tel1.'</a>';}?>
                                <?php if($phone!=''){echo ' / <a href="tel:'.$phone.'">'.$phone.'</a>';}?>
                                <?php if($wapp!=''){echo ' / <a href="tel:'.$wapp.'">'.$wapp.'</a>';}?>
                                <br>&nbsp;</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 address-grid-inf-w3layouts" data-aos="zoom-out">
                        <div class="address-info">
                           	<a href="mailto:<?php echo $contactMail;?>">
                            <div class="address-left">
                                <span class="far fa-envelope" aria-hidden="true"></span>
                            </div></a>
                            <div class="address-right">
                                <h6>Email </h6>
                                <p style="color:#6b6e80;">
                                	<?php if($contactMail!=''){echo '<a href="mailto:'.$contactMail.'">'.$contactMail.'</a>';}?><br>&nbsp;                                  
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 address-grid-inf-w3layouts" data-aos="zoom-out">
                        <div class="address-info">
                            <div class="address-left">
                                <span class="fas fa-map-marker-alt" aria-hidden="true"></span>
                            </div>
                            <div class="address-right">
                                <h6>Ubicaci&oacute;n</h6>
                                <p style="color:#6b6e80;"><?php if($direc!=''){echo $direc;}else{echo '&nbsp;';}?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
<!--/Querétaro-->
<?php 
sql_opciones('act_dir2',$valor);
if($valor==1){
?>
<!--Guanajuato-->
            	<h4 style="border-bottom:2px solid #ed1c24; color:#1f205a; margin-bottom:30px">Sucursal Celaya</h4>
                <div class="contact-map">
                	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3736.6992556763435!2d-100.83355179748754!3d20.51855135777345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842cbaf76218caef%3A0x5fa5f22e535af487!2sFibrecen!5e0!3m2!1ses-419!2smx!4v1547177854475" class="map" style="border:0" allowfullscreen=""></iframe>
                </div>
                <div class="address row">
                    <div class="col-md-4 address-grid-inf-w3layouts" data-aos="zoom-out">
                        <div class="address-info">
                        	<a href="tel:<?php echo $tel2;?>">
                            <div class="address-left">
                                <span class="fas fa-phone-volume" aria-hidden="true"></span>
                            </div></a>
                            <div class="address-right">
                                <h6>Telefono</h6>
                                <p style="color:#6b6e80;"><?php if($tel2!=''){echo $tel2;}?><br>&nbsp;</p>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 address-grid-inf-w3layouts" data-aos="zoom-out">
                        <div class="address-info">
                        	<a href="mailto:<?php echo $contactMail2;?>">
                            <div class="address-left">
                                <span class="far fa-envelope" aria-hidden="true"></span>
                            </div></a>
                            <div class="address-right">
                                <h6>Email </h6>
                                <p style="color:#6b6e80;">
                                	<?php if($contactMail2!=''){echo '<a href="mailto:'.$contactMail2.'">'.$contactMail2.'</a>';}?><br>&nbsp;                                  
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 address-grid-inf-w3layouts" data-aos="zoom-out">
                        <div class="address-info">
                            <div class="address-left">
                                <span class="fas fa-map-marker-alt" aria-hidden="true"></span>
                            </div>
                            <div class="address-right">
                                <h6>Ubicaci&oacute;n</h6>
                                <p style="color:#6b6e80;"><?php if($direc2!=''){echo $direc2;}else{echo '&nbsp;';}?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
<!--/Guanajuato-->
<?php }?>
                <div class="contact_grid_right">
                	<h3 style="border-bottom:2px solid #ed1c24; color:#1f205a; margin-bottom:30px; display:inline-block;">Env&iacute;enos un Mensaje</h3>
                    <h6>Por favor llene el formulario y nosotros lo contactaremos.</h6>
                    <?php include 'contacto.php';?>
                </div>
            </div>
        </div>
    </section>

    <!--//Contact-->
