<?php 
function cadena_replace(&$replace1,&$replace2){
	$replace1=array(' ','.',',','(',')','/','"',"’",'á','é','í','ó','ú','&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','Á','É','Í','Ó','Ú','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','ñ','Ñ','&ntilde;','&Ntilde;');
	$replace2=array('-','-','-','-','-','-','-',"",'a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','n','N','n','N');
}

function html_iso(&$nom,&$des){
global $chartset;
 if($chartset=='iso-8859-1'){
	$nom=htmlentities($nom, ENT_COMPAT,'ISO-8859-1', true);
	$des = htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function glosario(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
//sql_opciones('link_productos',$valor);
$fjson='glosario';
$path_JSON='modulos/'.$mod.'/'.$fjson.'.json';
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/'.$fjson.'/';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";

 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- '.$fjson.'.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$cover=$reg['cover'];
			$nom=$reg['nom'];
			$descripcion=$reg['descripcion'];
			$visible=$reg['visible'];

			cadena_replace($replace1,$replace2);
			$nombre=str_replace($replace1,$replace2,$nom);
		
			if($visible==1){
				echo '<!--['.$i.'] -'.$ID.'-->
				<div id="'.$nombre.'" class="col-md-12" style="padding-bottom:50px;">
                	<h3 class="red-line">'.$nom.'</h3>
                    <h6>'.$descripcion.'</h6>
                </div>
				';
			}
		}
		echo '<!-- /glosario.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>No hay definiciones disponibles.</div>
		</div>
	';
 }
}
?>