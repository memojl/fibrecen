<?php
include 'admin/functions.php';
sql_opciones('slide_active',$valor);
$slide=$valor;
?>
<?php if($slide==1){
	slider();
 }?>
<?php
pages($mod,$ext,$contenido,$activo);
if($activo==1){
	echo $contenido;
}else{
elem($elemento);
?>

    <!--//ban-->
    <section class="banner-bottom">
        <div class="container">
            <div class="row inner-sec-wthree">
                <div class="col-lg-5 bt-bottom-info" data-aos="fade-right">
                    <?php echo $elemento[2];?>
                </div>
                <div class="col-lg-7 bt-bottom-info" data-aos="fade-left">
                	<?php echo $elemento[3];?>
                </div>
            </div>
        </div>
    </section>
    <!---->
    <!-- /stats -->
    <section class="stats_test container-fluid">
        <div class="row inner_stat">
            <div class="col-lg-6 stats-left-info">
                <div class="row">
                    <div class="col-md-6 stats_left counter_grid">
                        <i class="far fa-building"></i>
                        <p class="counter"><?php echo $elemento[4];?></p>
                        <h4>A&ntilde;os De Experiencia</h4>
                    </div>
                    <div class="col-md-6 stats_left counter_grid1">
                        <i class="far fa-edit" style="color:#ed1c24;"></i>
                        <p class="counter" style="color:#ed1c24;"><?php echo $elemento[5];?></p>
                        <h4 style="color:#ed1c24;">Proyectos Realizados</h4>
                    </div>
                    <div class="col-md-6 stats_left counter_grid2">
                        <i class="far fa-smile" style="color:#ed1c24;"></i>
                        <p class="counter" style="color:#ed1c24;"><?php echo $elemento[6];?></p>
                        <h4 style="color:#ed1c24;">Clientes Atendidos</h4>
                    </div>
                    <div class="col-md-6 stats_left counter_grid3">
                        <i class="fas fa-users"></i>
                        <p class="counter"><?php visitas_total();?></p>
                        <h4>Visitas</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 stats-right-img" style="background: linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.18)), url(<?php echo $page_url.'modulos/Home/media/elementos/'.$elemento[12];?>) no-repeat; background-size: cover;"></div>
        </div>
    </section>
    <!-- //stats -->
    <!--/services-->
    <section class="services">
        <div class="container">
            <h3 class="tittle">Crea Con Nosotros</h3>
            <div class="row inner-sec-wthree">
                <div class="col-lg-4 grid_info_main" data-aos="flip-left">
                    <div class="grid_info" onMouseOver="image_home(1)" onMouseOut="image_home(0)">
                        <div class="icon_info">
                            <span class="icon">
								<i class="fa fa-superpowers"></i>
							</span>
                            <!--
                            <h5 style="text-transform:capitalize;color:#1f205a;">Moldes rigidos y piezas de fibra de vidrio</h5>
                            <p>Crea piezas de gran tama&ntilde;o y poco detalle.</p>
                            -->
                            <?php echo $elemento[7];?>
                        </div>
                    </div>
                    <div class="grid_info second" onMouseOver="image_home(2)" onMouseOut="image_home(0)">
                        <div class="icon_info">
                            <span class="icon">
								<i class="fa fa-codepen"></i>
							</span>
                            <?php echo $elemento[8];?>
                        </div>
                    </div>
                </div>
                <div id="cimage" class="col-lg-4 grid_info_main mid" data-aos="flip-down">
                    <?php echo '<img src="'.$page_url.'modulos/Home/media/elementos/'.$elemento[13].'" class="img-fluid" alt=" ">';?>
                </div>
                <div class="col-lg-4 grid_info_main" data-aos="flip-right">
                    <div class="grid_info" onMouseOver="image_home(3)" onMouseOut="image_home(0)">
                        <div class="icon_info">
                            <span class="icon">
								<i class="fa fa-codiepie"></i>
							</span>
                            <?php echo $elemento[9];?>
                        </div>
                    </div>
                    <div class="grid_info second" onMouseOver="image_home(4)" onMouseOut="image_home(0)">
                        <div class="icon_info">
                            <span class="icon">
								<i class="fa fa-tint"></i>
							</span>
                            <?php echo $elemento[10];?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!--//services->
<?php
}
?>
    <!--reviews_sec-->
    <section class="reviews_sec" id="testimonials" style="background:#06003E;">
        <h3 class="tittle cen">Testimonios</h3>
        <div class="inner-sec-wthree">
            <section class="slider">
                <div class="flexslider">
                    <ul class="slides">
                    	<?php testimonios();?>
                    </ul>
                </div>
            </section>
        </div>
    </section>
    <!---->
