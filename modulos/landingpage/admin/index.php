<?php 
if(isset($_SESSION["username"])){
//	if($_SESSION["level"]==-1 || $_SESSION["level"]==1){
	include 'functions.php';
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $nombre_mod;?>
        <small><?php echo $description_mod;?></small>
      </h1>
	  <?php menu_rutas();?>
    </section>

    <!-- Main content -->
    <section class="content">
	<div class="row">

<?php
switch(true){
	case($opc=='form'):
	editor_tiny_mce();

	switch(true){
		case($action=='add'):
			$titulo1='Agregar';
			$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."landingpage_seccion ORDER BY ID DESC;") or print mysqli_error($mysqli); 
			if($reg=mysqli_fetch_array($sql)){$id=$reg['ID']+1;}
		break;
		case($action=='edit'  && !empty($_GET['id'])):
			$titulo1='Editar';
			$id=$_GET['id'];
			$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."landingpage_seccion WHERE ID='{$id}';") or print mysqli_error($mysqli); 
			while($reg=mysqli_fetch_array($sql)){
				$id=$reg['ID'];
				$titulo=$reg['tit'];
				$contenido=$reg['conte'];
				$url_page=$reg['url_page'];
				$visible=$reg['visible'];
			}
		break;
	}

if($_POST['Guardar']){
$titulo=$_POST['titulo'];
$contenido=$_POST['contenido'];
//$url_page=$_POST['url_page'];
$visible=$_POST['visible'];
cadena_replace($replace1,$replace2);
$tit=str_replace($replace1,$replace2,$titulo);
$url_page=$page_url.'landingpage/fibrecen/'.$id.'-'.$tit;

	if($titulo == ''){
		$error = "  *El campo esta vacio.\\n\\r"; $c++; 
	}
	if($c > 0){
		$aviso='
			<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>'.$error.'
			</div>
			';
	}else{
		if($action=='edit'){
			$save=mysqli_query($mysqli,"UPDATE ".$DBprefix."landingpage_seccion SET seccion='{$seccion}', tit='{$titulo}', conte='{$contenido}', url_page='{$url_page}', visible='{$visible}' WHERE ID='{$id}';") or print mysqli_error($mysqli);
		}else{
			$save=mysqli_query($mysqli,"INSERT INTO ".$DBprefix."landingpage_seccion (seccion,tit,conte,url_page,visible) VALUES ('{$seccion}','{$titulo}','{$contenido}','{$url_page}','{$visible}');") or print mysqli_error($mysqli);		
		}
	//validar_aviso($save,'mensaje_bien','mensaje_mal',&$aviso)
	validar_aviso($save,'El contenido se ha guardo correctamente','No se puedo guardar intentelo nuevamente',$aviso);
	$URL=$page_url.'index.php?mod='.$mod.'&ext='.$ext;
	recargar(3,$URL,$target);
	}
}

?>
	<div class="col-md-12">
    	<?php echo $aviso;?>
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $titulo1;?> Landingpage Contenido</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form name="form1" role="form" method="post" enctype="multipart/form-data" action="<?php echo $URL;?>" accept-charset="<?php echo $chartset;?>">
              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="titulo">Titulo</label>
                  <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $titulo;?>">
                </div>
                <div class="form-group col-md-12">
                  <label for="contenido">Contenido</label>
                  <textarea class="form-control" id="contenido" name="contenido" style="height:300px;"><?php echo $contenido;?></textarea>
                </div>

				<div class="form-group col-md-6">
                  <label>Visible</label>
                  <select class="form-control" id="visible" name="visible">
                    <option value="0" <?php echo $sel=($visible==0) ? 'selected' : '';?>>No</option>
                    <option value="1" <?php echo $sel=($visible==1) ? 'selected' : '';?>>Si</option>
                  </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                 <input type="hidden" class="form-control" id="url_page" name="url_page" value="<?php echo $url_page;?>">

                <input type="submit" name="Guardar" class="btn btn-primary" value="Guardar"> 
                <button type="button" class="btn btn-default" onClick="javascript:window.history.go(-1);">Cancelar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
	<!-- /.col-->

<?php
	break;	
	default:
?>
<script type="text/javascript">
    function confirm1(id){
    var r=confirm("Realmente desea eliminar este Contenido "+id+"?.");
    if(r==true){
	<?php
	print " window.location.href='{$URL}&id='+id+'&delete=1'; ";
	if($_GET['delete']==1 && !empty($_GET['delete'])){
		mysqli_query($mysqli,"DELETE FROM ".$DBprefix."landingpage_seccion WHERE id='".$_GET['id']."';") or print mysqli_error($mysqli);
	}
	?>}
    }
</script>  
<?php 
if($_GET['delete']==1 && !empty($_GET['delete'])){
	$URL=$page_url.'index.php?mod='.$mod.'&ext='.$ext;
	recargar(1,$URL,$target);
}
?>
<div class="col-xs-12">
	<div class="box">
    	<div class="box-header">
        	<h3 class="box-title">Tabla <?php echo $mod;?></h3>
			<span style="float: right;"><a href="<?php echo $page_url.'index.php?mod='.$mod.'&ext='.$ext.'&opc=form&action=add';?>" title="Agregar Menu"><i class="fa fa-plus-square"></i></a></span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<table id="example1" class="table table-bordered table-striped">
            	<thead>
                <tr>
                  <th>ID</th>
                  <th>Titulo</th>
                  <th>Contenido</th>
                  <th>URL</th>
                  <th>Activo</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>

<?php 
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."landingpage_seccion;") or print mysqli_error($mysqli); 
while($reg=mysqli_fetch_array($sql)){
$id=$reg['ID'];
$titulo=$reg['tit'];
$contenido=$reg['conte'];
$url_page=$reg['url_page'];
$visible=$reg['visible'];
echo '
                <tr>
                  <td>'.$id.'</td>
                  <td>'.$titulo.'</td>
				  <td></td>
				  <td><a target="_blank" href="'.$url_page.'">'.$url_page.'</a></td>
				  <td>'.$visible.'</td>
				  <td><a href="'.$page_url.'index.php?mod='.$mod.'&ext='.$ext.'&opc=form&action=edit&id='.$id.'" title="Editar"><i class="fa fa-edit"></i></a> | <a href="javascript:confirm1('.$id.');" title="Borrar"><i class="fa fa-trash"></i></a></td>
                </tr>
';
}
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Titulo</th>
                  <th>Contenido</th>
                  <th>URL</th>
                  <th>Activo</th>
                  <th>Acciones</th>
                </tr>
                </tfoot>
              </table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
        
	</div><!-- /.col -->
</div><!-- /.col-xs-12 -->
<?php
	break;
}
?>
	</div>
    <!-- /.row-->
    </section>
    <!-- /.content -->
<?php 		
//	}else{echo '<div id="cont-user">No tiene permiso para ver esta secci&oacute;n.</div>';}
}else{header("Location: ".$page_url."index.php");}
?>