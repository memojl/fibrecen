<?php
include 'admin/functions.php';
landingpage($id,&$contenido);
if($activo==1){echo $contenido;}else{
?>
    <!--/Nosotros-->
    <section class="services">
        <div class="container">
            <h3 class="tittle">Nosotros</h3>
				<div class="row inner-sec-wthree">
                	<div class="col-lg-5 bt-bottom-info aos-init aos-animate" data-aos="fade-right">
                    	<h5>Historia</h5>
                	</div>
                	<div class="col-lg-7 bt-bottom-info aos-init aos-animate" data-aos="fade-left">
                    	<!--img src="<?php echo $page_url.$path_tema;?>images/banner1.jpg" class="img-fluid" alt=" "-->
                    	<p class="mt-3" style="font-size:18px;">
                        	Contamos con m&aacute;s de 30 a&ntilde;os de experiencia, en FIBRECEN nos hemos convertido en una empresa competitiva en la distribuci&oacute;n de materias primas tales como:
                		<br><br>
                        <ul class="dr">      
							<li>Fibra de vidrio, Resinas Poli&eacute;ster y Vinil&eacute;ster, Gelcoats, Complementos para la fabricaci&oacute;n de Compuestos de Pl&aacute;stico Reforzado, Resinas anticorrosivas, Concreto Polim&eacute;rico y M&aacute;rmol Sint&eacute;tico. </li>
                    		<li>Aislamientos Termo ac&uacute;sticos para techos, paredes, pisos y equipos industriales. </li>
                    		<li>Silicones de caucho para la fabricaci&oacute;n de moldes de piezas art&iacute;sticas, elementos arquitect&oacute;nicos y prototipos de muebles.</li> 
                    	</ul>
                        </p>
                	</div>
            	</div>            
				<div class="row inner-sec-wthree">
                	<div class="col-lg-4 bottom-sub-grid text-center aos-init" data-aos="zoom-in">
                    	<div class="bt-icon">
                        	<span class="fa fa-rocket"></span>
                    	</div>

                    	<h4 class="sub-tittle" style="font-size:24px;">Misi&oacute;n</h4>
                    	<p style="font-size:16px;">Nuestra misi&oacute;n es atenderle de la manera m&aacute;s atenta y r&aacute;pida con materias primas y productos de alta calidad en los mercados de Pl&aacute;stico Reforzado y Aislamientos Termo ac&uacute;sticos, as&iacute; como la Aplicaci&oacute;n de los mismos en el &aacute;rea de Manufactura.</p>
                    	<!--p>
                        	<a class="btn btn-secondary view" href="single.html" role="button">View details</a>
                    	</p-->
                	</div>
                	<!-- /.col-lg-4 -->
                	<div class="col-lg-4 bottom-sub-grid text-center aos-init" data-aos="zoom-in">
                    	<div class="bt-icon">
                        	<span class="fa fa-eye"></span>
                    	</div>
                    	<h4 class="sub-tittle" style="font-size:24px;">Visi&oacute;n</h4>
                    	<p style="font-size:16px;">L&iacute;deres en manufactura y mantenimiento de piezas de Fibra de Vidrio y dando soluciones a los proyectos de nuestros clientes.</p>
                    	<!--p>
                        	<a class="btn btn-secondary view" href="single.html" role="button">View details</a>
                    	</p-->
                	</div>
                	<!-- /.col-lg-4 -->
                	<div class="col-lg-4 bottom-sub-grid text-center aos-init" data-aos="zoom-in">
                    	<div class="bt-icon">
                        	<span class="fa fa-diamond"></span>
                    	</div>

                    	<h4 class="sub-tittle" style="font-size:24px;">Valores</h4>
                    	<p style="font-size:16px;">Honestidad, Calidad, Confianza y Servicio son los pilares para fortalecer la relaci&oacute;n de nuestros clientes y sus proyectos.</p>
                    	<!--p>
                        	<a class="btn btn-secondary view" href="single.html" role="button">View details</a>
                    	</p-->
                	</div>
                	<!-- /.col-lg-4 -->
            	</div>
        </div>
    </section>
    <!--//Nostotros-->
<?php }?>
