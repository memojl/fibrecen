<?php
function cadena_replace_m(&$replace1,&$replace2){
	$replace1=array(' ','.',',','(',')','/','"','á','é','í','ó','ú','&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','Á','É','Í','Ó','Ú','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','ñ','Ñ','&ntilde;','&Ntilde;','&','amp;');
	$replace2=array('-','-','-','-','-','-','-','a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','n','N','n','N','','');
}

function title(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$title;	
echo ucfirst($mod).' | '.$title;
}

function login(&$login_se,$username,$ID_login){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$dboard;
 if(isset($_SESSION['username'])){
	$login_se='<a href="'.$page_url.'index.php?mod='.$dboard.'"><i class="fa fa-sign-in"></i> '.$username.'</a> <a href="'.logout($ID_login).'" title="Salir"><i class="fa fa-power-off"></i></a>';
 }else{
	$login_se='<a href="'.$page_url.'admin/"><i class="fa fa-sign-in"></i> Login</a>';
 }
}

function top_bg(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."menu_web WHERE modulo='{$mod}' AND ext='{$ext}' AND ima_top!='';") or print mysqli_error($mysqli); 
    if($reg=mysqli_fetch_array($sql)){$ima_top=$reg['ima_top'];}
    echo'<style>
    .banner_mod {
         background: url('.$page_url.'modulos/Home/media/top/'.$ima_top.')no-repeat 0px 0px;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        -ms-background-size: cover;
        background-size: cover;
        background-position: center;
     	min-height: 380px;
		border-bottom: 5px solid #c8102e;
     }
     </style>';	
}

function tit_seccion(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$cond=($ext!='' && $ext!='index')?" AND ext='{$ext}'":"";
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."menu_web WHERE modulo='{$mod}'".$cond.";") or print mysqli_error($mysqli); 
 if($reg=mysqli_fetch_array($sql)){
    $menu=$reg['menu'];$tit_sec=$reg['tit_sec'];
	if($tit_sec!=''){echo $tit_sec;}else{echo $menu;}
 }	
}

/*function des_seccion(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$cond=($ext!='' && $ext!='index')?" AND ext='{$ext}'":"";
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."menu_web WHERE modulo='{$mod}'".$cond.";") or print mysqli_error($mysqli); 
 if($reg=mysqli_fetch_array($sql)){
	 $des_tit=$reg['des_sec'];
	 echo $des_tit;
 }	
}*/

function nom_cate(&$cate){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$idp}';") or print mysqli_error($mysqli); 
 if($reg=mysqli_fetch_array($sql)){
 	$cate=$reg['categoria'];
 }
}

function link_url(&$link_url,$url_m,$mod_menu,$ext_menu){
global $mysqli,$DBprefix,$url,$URL,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
 if($tema_previo!='' && $tema_previo!=NULL){
	$url_m2=(substr($url_m,0,1)=='#')?$url_m:'';
	if($mod_menu!=''){$mod_menu='&mod='.$mod_menu;}
	if($ext_menu!=''){$ext_menu='&ext='.$ext_menu;}
	$link_url=$page_url.'index.php?tema_previo='.$tema_previo.$mod_menu.$ext_menu.$url_m2;	
 }else{
	$link_url=(substr($url_m,0,1)=='#')?$URL.$url_m:$page_url.$url_m;
 }
}

function menu_web(){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
$menu_json='menu.json';
$path_JSON=$path_tema.'menu/'.$menu_json;
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t=menu_web';}
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
usort($Data, function($a, $b){return strnatcmp($a['ord'], $b['ord']);});//Orden del menu
$i=0;
if($_SESSION['level']!=-1){echo '<!-- menu.json -->'."\n\r";}else{echo '<!-- menu.json URL:('.$path_JSON.')-->'."\n\r";}

	foreach ($Data as $rowm){$i++;
		$ID_menu=$rowm['ID'];
		$nom_menu=$rowm['menu'];
		$url_m=$rowm['url'];
		$mod_menu=$rowm['modulo'];
		$ext_menu=$rowm['ext'];
		$orden=$rowm['ord'];
		$sub_menu=$rowm['subm'];
		$visible=$rowm['visible'];
		
		if($visible==1 && $sub_menu==NULL){
			$link_css1=($mod==$rowm['modulo'])?' active':'';
			$k=0;$link_css2=$link_css3='';$link_css4='';$a_datos='';
			foreach ($Data as $row){
				$ID_menu1=$row['ID'];
				$nom_menu1=$row['menu'];
				//$url_m1=$row['url'];
				$sub_menu1=$row['subm'];
				$visible1=$row['visible'];
				if($visible1!=0 && $sub_menu1!='' && $sub_menu1==$ID_menu){$k++;
					$link_css2=' dropdown';
					$link_css3=' dropdown-toggle';
					$a_datos=' id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"';
					$link_css4=' <i class="fas fa-angle-down"></i>';
				}				
			}			
			link_url($link_url,$url_m,$mod_menu,$ext_menu);						
			echo '<!--'.$i.'('.$orden.')-'.$mod_menu.'--><li class="nav-item'.$link_css1.$link_css2.'"><a class="nav-link'.$link_css3.'" href="'.$link_url.'"'.$a_datos.'>'.$nom_menu.$link_css4.'</a>';
			if($mod_menu=='Home'){echo '<span class="sr-only">(current)</span>';}
			$n=0;$j=0;
			foreach ($Data as $rowm){if($rowm['visible']==1 && $rowm['subm']==$ID_menu){$n++;}}
			echo '<!--'.$n.'-->';
			foreach ($Data as $rowm){
				$ID_menu2=$row['ID'];
				$nom_menu2=$rowm['menu'];
				$url_m2=$rowm['url'];
				$mod_menu2=$rowm['modulo'];
				$ext_menu2=$rowm['ext'];
				$sub_menu2=$rowm['subm'];
				$visible2=$rowm['visible'];
				if($visible2==1 && $sub_menu2==$ID_menu){$j++;
					if($j==1){echo "\n\r".'<div class="dropdown-menu" aria-labelledby="navbarDropdown">'."\n\r";}
					link_url($link_url2,$url_m2,$mod_menu2,$ext_menu2);
					if($_SESSION['level']!=-1){echo '<!-- j:'.$j.' -->'."\n\r";}else{echo '<!-- j:'.$j.' ID:'.$sub_menu2.' ID_menu:'.$ID_menu2.' -->'."\n\r";}
					echo '<a class="dropdown-item" href="'.$link_url2.'">'.$nom_menu2.'</a>';
					if($n==$j){echo '</div>'."\n\r";}
				}
			}
			echo '</li>'."\n\r";
		}
	}
 echo '<!-- /menu.json -->
 ';
 }
}

function menu_responsivo(){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
$menu_json='menu.json';
$path_JSON=$path_tema.'menu/'.$menu_json;
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/menu_web/';}
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
usort($Data, function($a, $b){return strnatcmp($a['ord'], $b['ord']);});
$i=0;
if($_SESSION['level']!=-1){echo '<!-- menu.json -->'."\n\r";}else{echo '<!-- menu.json URL:('.$path_JSON.')-->'."\n\r";}

	foreach ($Data as $rowm){$i++;
		$ID_menu=$rowm['ID'];
		$nom_menu=$rowm['menu'];
		$url_m=$rowm['url'];
		$mod_menu=$rowm['modulo'];
		$ext_menu=$rowm['ext'];
		$orden=$rowm['ord'];
		$sub_menu=$rowm['subm'];
		$visible=$rowm['visible'];
		
		if($visible==1 && $sub_menu==NULL){
			$link_css1=($mod==$rowm['modulo'])?' active':'';
			$k=0;$link_css2='';$resp_arrow='';
			foreach ($Data as $row){
				$ID_menu1=$row['ID'];
				$nom_menu1=$row['menu'];
				//$url_m1=$row['url'];
				$sub_menu1=$row['subm'];
				$visible1=$row['visible'];
				if($visible1!=0 && $sub_menu1!='' && $sub_menu1==$ID_menu){$k++;
					$link_css2=' menu-item-has-children has-sub';
					if($_SESSION['level']!=-1){echo '<!-- k:'.$k.' -->'."\n\r";}else{echo '<!--1 k:'.$k.' ID:'.$sub_menu1.' ID_menu:'.$ID_menu1.' CSS:'.$link_css2.' -->'."\n\r";}
					$resp_arrow=' <span class="arrow"></span>';					
				}				
			}			
			link_url($link_url,$url_m,$mod_menu,$ext_menu);						
			echo '<!--'.$i.'-'.$mod_menu.'--><li id="accordion-menu-item-'.$ID_menu.'" class="menu-item menu-item-type-post_type menu-item-object-page'.$link_css1.$link_css2.' narrow "><a href="'.$page_url.$link_url.'" rel="nofollow" class="">'.$nom_menu.'</a>'.$resp_arrow;
			$n=0;$j=0;
			foreach ($Data as $rowm){if($rowm['visible']==1 && $rowm['subm']==$ID_menu){$n++;}}
			echo '<!--'.$n.'-->';
			foreach ($Data as $rowm){
				$ID_menu2=$row['ID'];
				$nom_menu2=$rowm['menu'];
				$url_m2=$rowm['url'];
				$mod_menu2=$rowm['modulo'];
				$ext_menu2=$rowm['ext'];
				$sub_menu2=$rowm['subm'];
				$visible2=$rowm['visible'];
				if($visible2==1 && $sub_menu2==$ID_menu){$j++;
					if($j==1){echo "\n\r".'<ul class="sub-menu">'."\n\r";}
					link_url($link_url2,$url_m2,$mod_menu2,$ext_menu2);
					echo '<li id="accordion-menu-item-'.$ID_menu.'" class="menu-item menu-item-type-post_type menu-item-object-page " data-cols="1"><!--j:'.$j.'--><a href="'.$page_url.$rowm['url'].'">'.$rowm['menu'].'</a></li>'."\n\r";
					if($n==$j){echo '</ul>'."\n\r";}
				}
			}
			echo '</li>'."\n\r";
		}
	}
 echo '<!-- /menu.json -->
 ';
 }
}

function slider(){
global $mysqli,$DBprefix,$tema,$page_url,$path_tema,$mod,$ext,$opc;
$nomt='slider';
$fjson=$nomt.'_'.$tema.'.json';
$path_JSON='modulos/Home/'.$fjson;
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/'.$nomt.'/';}
 if($path_JSON){
 $objData=file_get_contents($path_JSON);
 $Data=json_decode($objData,true);
 $i=-1;
	$op_f=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
	foreach ($Data as $rowm){$i++;
		$id=$rowm['ID'];
		$cover=$rowm['ima'];
		$tit1=$rowm['tit1'];
		$tit2=$rowm['tit2'];
		$btn=$rowm['btn_nom'];
		$url_s=$rowm['url'];
		$tema_slider=$rowm['tema_slider'];
		$visible=$rowm['visible'];

		$act=($i!=0)?'':' active';
		if($visible==1 && $tema_slider==$tema){
			$indicador.='
			<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class="'.$act.'"></li>';

			$tit1_1=($tit1!='')?'<h3>'.$tit1.'</h3>':'';
			$tit2_1=($tit2!='')?'<h3>'.$tit2.'</h3>':'';
			$btn_1=($btn!='')?'<div class="bnr-button"><button type="button" class="btn btn-primary play" data-toggle="modal" data-target="#exampleModal'.$id.'"><i class="fas fa-play"></i></button><!--a class="btn-cer" href="'.$url_s.'">'.$btn.'</a--></div>':'';
			
			$diapositivas.='
                <div class="carousel-item item'.$id.$act.'" style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.3)), url(./modulos/Home/media/slide/'.$cover.') no-repeat;
    			background: -moz-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.3)), url(./modulos/Home/media/slide/'.$cover.') no-repeat;
    			background: -ms-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.3)), url(./modulos/Home/media/slide/'.$cover.') no-repeat;
    			background: linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.3)), url(./modulos/Home/media/slide/'.$cover.') no-repeat; background-size: cover;">
                    <div class="carousel-caption">
					'.$tit2_1.$btn_1.'
                    </div>
                </div>
			';
			$modal.=($url_s!='')?'
    <!-- Modal -->
    <div class="modal fade" id="exampleModal'.$id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
                </div>
                <div class="modal-body video">
                    <iframe src="'.$url_s.'" width="640" height="360" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <!--button type="button" class="btn btn-primary">Save changes</button-->
                </div>
            </div>
        </div>
    </div>
    <!--//model-->':'';
			
		}//if
	}//foreach
		echo '
    <!-- banner -->
	'.$op_f.'
    <div class="banner">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
				'.$indicador.'
            </ol>
            <div class="carousel-inner" role="listbox">
        		'.$diapositivas.'
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
        </div>
    </div>
    <!--/banner-->
	'.$modal.''; 
 }
}

function testimonios(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
//sql_opciones('link_productos',$valor);
$fjson='testimonios';
$path_JSON='modulos/'.$mod.'/'.$fjson.'.json';
 if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t='.$fjson.'';}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";

 if($path_JSON){
	$objData=file_get_contents($path_JSON);
	$Data=json_decode($objData,true);
	$i=0;
	if($Data!='' && $Data!=NULL){
		echo '<!-- '.$fjson.'.json -->';
		foreach ($Data as $reg){$i++;
			$ID=$reg['ID'];
			$cover=$reg['cover'];
			$pro=$reg['pro'];
			$comentario=$reg['comentario'];
			$visible=$reg['visible'];
			//cadena_replace($replace1,$replace2);
		
			if($visible==1){
				echo '<!--['.$i.'] -'.$ID.'-->
                        <li>
                            <div class="testimonials_grid">
                                <div class="testimonials_grid-inn">
                                    <img src="'.$page_url.'modulos/Home/media/'.$fjson.'/'.$cover.'" alt=" " width="120" class="img-fluid" />
                                    <div class="test_social_pos">
                                    </div>
                                </div>
                                <h3>
                                    <span>- '.$pro.' -</span>
                                </h3>
                                <!--i>United States</i-->
                                <p style="font-size:25px;">&quot;'.$comentario.'&quot;</p>
                            </div>
                        </li>
				';
			}
		}
		echo '<!-- /'.$fjson.'.json -->';
	}	
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>No hay definiciones disponibles.</div>
		</div>
	';
 }
}

function menu_rutas_productos($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;	
		switch(true){
			case ($ext!='' && $ext=='item'):
			cadena_replace_m($replace1,$replace2);
				$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID={$id};") or print mysqli_error($mysqli); 
				if($row=mysqli_fetch_array($sql)){$nom_producto=$row['nombre'];
				$producto=str_replace($replace1,$replace2,$nom_producto);$ID_sub_cate=$row['ID_sub_cate'];$ID_cate=$row['ID_cate'];}

				$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate={$ID_cate};") or print mysqli_error($mysqli); 
				if($row=mysqli_fetch_array($sql)){$nom_cate=$row['categoria'];
				$cate=str_replace($replace1,$replace2,$nom_cate);}

				$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate={$ID_sub_cate};") or print mysqli_error($mysqli); 
				if($row=mysqli_fetch_array($sql)){$nom_subcate=$row['subcategoria'];
				$subcate=str_replace($replace1,$replace2,$nom_subcate);}

				echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/">'.$mod.'</a></li>';				
				if($mod!='productos'){echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/'.$ext.'/">'.$ext.'</a></li>';}				
				echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/categoria/'.$ID_cate.'-'.$cate.'">'.$nom_cate.'</a></li>';
				echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/subcategoria/'.$ID_sub_cate.'-'.$subcate.'">'.$nom_subcate.'</a></li>';
				echo '<li class="breadcrumb-item">'.$nom_producto.'</li>';
			break;		
			case ($ext!='' && $ext=='subcategoria'):
				$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate={$id};") or print mysqli_error($mysqli); 
				if($row=mysqli_fetch_array($sql)){$nom_subcate=$row['subcategoria'];$ID_cate=$row['ID_cate'];}
				$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate={$ID_cate};") or print mysqli_error($mysqli); 
				if($row=mysqli_fetch_array($sql)){$nom_cate=$row['categoria'];
				cadena_replace_m($replace1,$replace2);
				$cate=str_replace($replace1,$replace2,$nom_cate);
				}
				echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/">'.$mod.'</a></li>';
				if($mod!='productos'){echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/'.$ext.'/">'.$ext.'</a></li>';}
				echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/categoria/'.$ID_cate.'-'.$cate.'">'.$nom_cate.'</a></li>';
				echo '<li class="breadcrumb-item">'.$nom_subcate.'</li>';
			break;
			case ($ext!='' && $ext=='categoria'):
				$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$id}';") or print mysqli_error($mysqli); 
				if($row=mysqli_fetch_array($sql)){$nom_cate=$row['categoria'];}
				echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/">'.$mod.'</a></li>';
				if($mod!='productos' && $mod!='landingpage'){echo '<li class="breadcrumb-item"><a href="'.$page_url.$mod.'/'.$ext.'/">'.$ext.'</a></li>';}
				echo '<li class="breadcrumb-item">'.$nom_cate.'</li>';
			break;			
			default:
				echo '<li class="breadcrumb-item"><a href="#">'.$mod.'</a></li>';
				if($mod!='productos' && $mod!='landingpage'){echo '<li class="breadcrumb-item">'.$ext.'</li>';}
			break;
		}
}

function galeria(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID={$idp};") or print mysqli_error($mysqli);
if($row=mysqli_fetch_array($sql)){
	$ima0=$row['cover'];
	$ima1=$row['imagen1'];
	$ima2=$row['imagen2'];
	$ima3=$row['imagen3'];
	$ima4=$row['imagen4'];
	$ima5=$row['imagen5'];
}
$ima=array($ima0,$ima1,$ima2,$ima3,$ima4,$ima5);
for($i=0;$i<6;$i++){
	if($ima[$i]!=''){
	$case.='case '.$i.':
		document.getElementById(\'imagenes\').innerHTML = \'<img src="'.$page_url.'modulos/productos/fotos/'.$ima[$i].'" width="100%">\';
	break;';	
	}
}
echo '<script>
function gal(val){
	switch (val){
	'.$case.'
	}
}
</script>';
}

function imagenh(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."home_elementos;") or print mysqli_error($mysqli);
if($row=mysqli_fetch_array($sql)){
	$ima0=$row['ima0'];
	$ima1=$row['ima1'];
	$ima2=$row['ima2'];
	$ima3=$row['ima3'];
	$ima4=$row['ima4'];
	$ima5=$row['ima5'];
	$ima6=$row['ima6'];
}
$ima=array($ima2,$ima3,$ima4,$ima5,$ima6);
for($i=0;$i<5;$i++){
	if($i!=0){
		$case.='
	case '.$i.':
		document.getElementById(\'cimage\').innerHTML = \'<img src="'.$page_url.'modulos/Home/media/elementos/'.$ima[$i].'" class="img-fluid" alt=" ">\';
	break;';	
	}else{
		$case.='default: 
		document.getElementById(\'cimage\').innerHTML = \'<img src="'.$page_url.'modulos/Home/media/elementos/'.$ima[0].'" class="img-fluid" alt=" ">\';
	break;';	
	}	
}
echo '<script>
function image_home(val){
	switch (val){
	'.$case.'
	}
}
</script>';
}


/*
function imagenh(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."home_ima;") or print mysqli_error($mysqli);
$num_rows=mysqli_num_rows($sql);$i=-1;
while($row=mysqli_fetch_array($sql)){$i++;
	$ima=$row['imagen'];
	if($i!=0){
		$case.='
	case '.$i.':
		document.getElementById(\'cimage\').innerHTML = \'<img src="'.$page_url.'modulos/Home/img/'.$ima.'" class="img-fluid" alt=" ">\';
	break;';	
	}else{
		$case.='default: 
		document.getElementById(\'cimage\').innerHTML = \'<img src="'.$page_url.'modulos/Home/img/'.$ima.'" class="img-fluid" alt=" ">\';
	break;';	
	}
}

echo '<script>
function image_home(val){
	switch (val){
	'.$case.'
	}
}
</script>';
}
*/
?>