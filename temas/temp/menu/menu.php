<?php
function login(&$login_se,$username,$ID_login){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$dboard;
 if(isset($_SESSION['username'])){
	$login_se='<a href="'.$page_url.'index.php?mod='.$dboard.'"><i class="fa fa-sign-in"></i> '.$username.'</a> <a href="'.logout($ID_login).'" title="Salir"><i class="fa fa-power-off"></i></a>';
 }else{
	$login_se='<a href="'.$page_url.'admin/"><i class="fa fa-sign-in"></i> Login</a>';
 }
}

function link_url(&$link_url,$url_m,$mod_menu,$ext_menu){
global $mysqli,$DBprefix,$url,$URL,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
 if($tema_previo!='' && $tema_previo!=NULL){
	$url_m2=(substr($url_m,0,1)=='#')?$url_m:'';
	if($mod_menu!=''){$mod_menu='&mod='.$mod_menu;}
	if($ext_menu!=''){$ext_menu='&ext='.$ext_menu;}
	$link_url=$page_url.'index.php?tema_previo='.$tema_previo.$mod_menu.$ext_menu.$url_m2;	
 }else{
	if(substr($url_m,0,1)=='#'){$url_m='index.php'.$url_m;}
	$link_url=$page_url.$url_m;
 }
}

function menu_web(){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
$menu_json='menu.json';
$path_JSON=$path_tema.'menu/'.$menu_json;
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/menu_web/';}
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
$i=0;
if($_SESSION['level']!=-1){echo '<!-- menu.json -->'."\n\r";}else{echo '<!-- menu.json URL:('.$path_JSON.')-->'."\n\r";}

	foreach ($Data as $rowm){$i++;
		$ID_menu=$rowm['ID'];
		$nom_menu=$rowm['menu'];
		$url_m=$rowm['url'];
		$mod_menu=$rowm['modulo'];
		$ext_menu=$rowm['ext'];
		$orden=$rowm['ord'];
		$sub_menu=$rowm['subm'];
		$visible=$rowm['visible'];
		
		if($visible==1 && $sub_menu==NULL){
			$link_css1=($mod==$rowm['modulo'])?' active':'';
			$k=0;$link_css2='';
			foreach ($Data as $row){
				$ID_menu1=$row['ID'];
				$nom_menu1=$row['menu'];
				//$url_m1=$row['url'];
				$sub_menu1=$row['subm'];
				$visible1=$row['visible'];
				if($visible1!=0 && $sub_menu1!='' && $sub_menu1==$ID_menu){$k++;
					$link_css2=' menu-item-has-children has-sub';
					if($_SESSION['level']!=-1){echo '<!-- k:'.$k.' -->'."\n\r";}else{echo '<!--1 k:'.$k.' ID:'.$sub_menu1.' ID_menu:'.$ID_menu1.' CSS:'.$link_css2.' -->'."\n\r";}
				}				
			}			
			link_url($link_url,$url_m,$mod_menu,$ext_menu);
			echo ' / <!--'.$i.'-'.$mod_menu.'--><a href="'.$link_url.'">'.$nom_menu.'</a>';
			$n=0;$j=0;
			foreach ($Data as $rowm){if($rowm['visible']==1 && $rowm['subm']==$ID_menu){$n++;}}
			echo '<!--'.$n.'-->';
			foreach ($Data as $rowm){
				$nom_menu2=$rowm['menu'];
				$url_m2=$rowm['url'];
				$mod_menu2=$rowm['modulo'];
				$ext_menu2=$rowm['ext'];
				$sub_menu2=$rowm['subm'];
				$visible2=$rowm['visible'];
				if($visible2==1 && $sub_menu2==$ID_menu){$j++;
					if($j==1){echo "\n\r".'<ul class="sub-menu">'."\n\r";}
					link_url($link_url2,$url_m2,$mod_menu2,$ext_menu2);
					echo '<li><!--j:'.$j.'--><a href="'.$link_url2.'">'.$nom_menu2.'</a></li>'."\n\r";
					if($n==$j){echo '</ul>'."\n\r";}
				}
			}
			echo '</li>';
		}
	}
 echo '<!-- /menu.json -->
 ';
 }
}

function menu_responsivo(){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
$menu_json='menu.json';
$path_JSON=$path_tema.'menu/'.$menu_json;
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/menu_web/';}
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
$i=0;
if($_SESSION['level']!=-1){echo '<!-- menu.json -->'."\n\r";}else{echo '<!-- menu.json URL:('.$path_JSON.')-->'."\n\r";}

	foreach ($Data as $rowm){$i++;
		$ID_menu=$rowm['ID'];
		$nom_menu=$rowm['menu'];
		$url_m=$rowm['url'];
		$mod_menu=$rowm['modulo'];
		$ext_menu=$rowm['ext'];
		$orden=$rowm['ord'];
		$sub_menu=$rowm['subm'];
		$visible=$rowm['visible'];
		
		if($visible==1 && $sub_menu==NULL){
			$link_css1=($mod==$rowm['modulo'])?' active':'';
			$k=0;$link_css2='';$resp_arrow='';
			foreach ($Data as $row){
				$ID_menu1=$row['ID'];
				$nom_menu1=$row['menu'];
				//$url_m1=$row['url'];
				$sub_menu1=$row['subm'];
				$visible1=$row['visible'];
				if($visible1!=0 && $sub_menu1!='' && $sub_menu1==$ID_menu){$k++;
					$link_css2=' menu-item-has-children has-sub';
					if($_SESSION['level']!=-1){echo '<!-- k:'.$k.' -->'."\n\r";}else{echo '<!--1 k:'.$k.' ID:'.$sub_menu1.' ID_menu:'.$ID_menu1.' CSS:'.$link_css2.' -->'."\n\r";}
					$resp_arrow=' <span class="arrow"></span>';					
				}				
			}			
			link_url($link_url,$url_m,$mod_menu,$ext_menu);						
			echo '<!--'.$i.'-'.$mod_menu.'--><li id="accordion-menu-item-'.$ID_menu.'" class="menu-item menu-item-type-post_type menu-item-object-page'.$link_css1.$link_css2.' narrow "><a href="'.$link_url.'" rel="nofollow" class="">'.$nom_menu.'</a>'.$resp_arrow;
			$n=0;$j=0;
			foreach ($Data as $rowm){if($rowm['visible']==1 && $rowm['subm']==$ID_menu){$n++;}}
			echo '<!--'.$n.'-->';
			foreach ($Data as $rowm){
				$nom_menu2=$rowm['menu'];
				$url_m2=$rowm['url'];
				$mod_menu2=$rowm['modulo'];
				$ext_menu2=$rowm['ext'];
				$sub_menu2=$rowm['subm'];
				$visible2=$rowm['visible'];
				if($visible2==1 && $sub_menu2==$ID_menu){$j++;
					if($j==1){echo "\n\r".'<ul class="sub-menu">'."\n\r";}
					link_url($link_url2,$url_m2,$mod_menu2,$ext_menu2);
					echo '<li id="accordion-menu-item-'.$ID_menu.'" class="menu-item menu-item-type-post_type menu-item-object-page " data-cols="1"><!--j:'.$j.'--><a href="'.$page_url.$rowm['url'].'">'.$rowm['menu'].'</a></li>'."\n\r";
					if($n==$j){echo '</ul>'."\n\r";}
				}
			}
			echo '</li>';
		}
	}
 echo '<!-- /menu.json -->
 ';
 }
}
?>